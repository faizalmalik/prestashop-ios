//
//  AddWishlistDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class AddWishlistDataViewController: UIViewController {

    @IBOutlet weak var addWishlistBtn: UIButton!
    @IBOutlet weak var nameWishlistLabel: UILabel!
    @IBOutlet weak var nametextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        nameWishlistLabel.text = "nameWishlist".localized
        self.navigationItem.title = "addWishlist".localized
        addWishlistBtn.setTitle("addWishlist".localized, for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnClicked(_ sender: Any) {
        if !ErrorChecking.isEmptyString(text: nametextField.text) {
            var wishlistDict = [String: Any]()
            wishlistDict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            wishlistDict["name"] = nametextField.text
            self.makeRequest(dict: wishlistDict, call: WhichApiCall.none)
        } else {
             ErrorChecking.warningView(message: "wishlistName".localized)
            nametextField.shake()
        }
    }

      func makeRequest(dict: [String: Any], call: WhichApiCall ) {
        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get

        params = GlobalConstants.ApiNames.addWishlist
        if let custId =  UrlParameters.fetchCustomerId() {
            params += custId
        }
        newDict[GlobalConstants.ApiNames.addWishlist.FetchApiName()] = dict
        verbs = .post
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.addWishlist, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if error != nil {

                print("loginError".localized, error!)

            } else {

                if ErrorChecking.getData(data: responseObject) != JSON.null && ErrorChecking.getData(data: responseObject) != nil {
                   self.navigationController?.popViewController(animated: true)
                }
            }
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
