//
//  FilterDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import RangeSeekSlider

protocol FilterDataExchange {

    func filterDataExchange(dict: [String: [String]], call: WhichApiCall)
}

class FilterDataViewController: UIViewController {

     @IBOutlet weak var priceMainView: UIView!
    @IBOutlet weak var selectedMaximumValueLabel: UILabel!
    @IBOutlet weak var minimumValueLabel: UILabel!
    @IBOutlet weak var priceView: RangeSeekSlider!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterViewHeight: NSLayoutConstraint!
    var filterData = [FilterData]()
    var filterDict = [String: [String]]()

    var filterValue = 0

    var FilterDataExchangeDelegate: FilterDataExchange?

    @IBOutlet weak var filterTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
      priceView.isHidden = true
        priceMainView.isHidden = true
        priceView.delegate = self
        self.navigationController?.navigationBar.navigationChanges()
        // Do any additional setup after loading the view.
        filterTableView.tableFooterView = UIView()
        self.showData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func showData() {

        var intY: CGFloat = 8
        for i in 0..<filterData.count {

            let varLabel: UIButton = UIButton()
            varLabel.frame = CGRect(x: 8, y: intY + 13, width: self.filterView.bounds.width, height: 17)
            varLabel.tag = 100 + i
            varLabel.addTarget(self, action: #selector(self.buttonClicked(sender:)), for: .touchUpInside)
            varLabel.setTitle(filterData[i].attribute_group_name, for: .normal)
            varLabel.setTitleColor(UIColor.black, for: .normal)
            varLabel.contentHorizontalAlignment = .left
            self.filterView.addSubview(varLabel)
            intY += 44
            if let array =  filterDict[filterData[i].attribute_group_rewrite] {
                if array.count > 0 {
                    let title = String.init(format: "%@ (%i)", filterData[i].attribute_group_name, array.count)
                     varLabel.setTitle(title, for: .normal)
                }
            }
            
        }

        if let btn = self.view.viewWithTag(filterValue + 100 ) as? UIButton {
            btn.setTitleColor(GlobalConstants.Colors.accentColor, for: .normal)
        }

        filterViewHeight.constant = intY
        filterTableView.delegate = self
        filterTableView.dataSource = self
        filterTableView.reloadData()
    }

    @IBAction func barButtonClicked(_ sender: UIBarButtonItem) {
        FilterDataExchangeDelegate?.filterDataExchange(dict: filterDict, call: WhichApiCall.none)
        self.dismiss(animated: true, completion: nil)
    }

    @objc func buttonClicked(sender: UIButton) {
        
        filterValue = sender.tag - 100
        
        if filterData[filterValue].slider == "1" {
            priceMainView.isHidden = false
            priceView.isHidden = false
            priceView.minValue = CGFloat(Int(filterData[filterValue].filterValuesData[1].priceValues) ?? 0)
            priceView.maxValue = CGFloat(Int(filterData[filterValue].filterValuesData[0].priceValues) ?? 0)
            
            if filterDict[filterData[filterValue].attribute_group_rewrite] != nil {
                priceView.selectedMinValue = CGFloat(Int(filterData[filterValue].filterValuesData[1].priceValues) ?? 0)
                priceView.selectedMaxValue = CGFloat(Int(filterData[filterValue].filterValuesData[0].priceValues) ?? 0)
                filterDict[filterData[filterValue].attribute_group_rewrite] =  [ String.init(format: "%@", String(describing: priceView.selectedMinValue)), String.init(format: "%@", String(describing: priceView.selectedMaxValue)) ]
            } else {
                if let arr =  filterDict[filterData[filterValue].attribute_group_rewrite] {
                    priceView.selectedMinValue = CGFloat(Int(arr[0]) ?? 0)
                    priceView.selectedMaxValue = CGFloat(Int(arr[1]) ?? 0)
                }
            }
            filterTableView.isHidden = true
            minimumValueLabel.text = String.init(format: "minValue".localized + " %@", filterData[filterValue].filterValuesData[1].priceValues)
            selectedMaximumValueLabel.text = String.init(format: "maxValue".localized + " %@", filterData[filterValue].filterValuesData[0].priceValues)
        } else {
            priceMainView.isHidden = true
            priceView.isHidden = true
            filterTableView.isHidden = false
            filterTableView.reloadData()
        }
        
        if let array =  filterDict[filterData[filterValue].attribute_group_rewrite] {
            if array.count > 0 {
                let title = String.init(format: "%@ (%i)", filterData[filterValue].attribute_group_name, array.count)
                sender.setTitle(title, for: .normal)
            } else {
                let title = String.init(format: "%@ ", filterData[filterValue].attribute_group_name )
                sender.setTitle(title, for: .normal)
            }
        } else {
            let title = String.init(format: "%@ ", filterData[filterValue].attribute_group_name )
            sender.setTitle(title, for: .normal)
        }
        for i in 0..<filterData.count {
            if let btn = self.view.viewWithTag(100 + i) as? UIButton {
                btn.setTitleColor(UIColor.black, for: .normal)
            }
        }
        sender.setTitleColor(GlobalConstants.Colors.accentColor, for: .normal)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func applyclicked(_ sender: UIButton) {
        FilterDataExchangeDelegate?.filterDataExchange(dict: filterDict, call: WhichApiCall.applyFilter)
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func clearClicked(_ sender: UIButton) {
        filterDict.removeAll()
        filterTableView.reloadData()
        FilterDataExchangeDelegate?.filterDataExchange(dict: filterDict, call: WhichApiCall.applyFilter)
        self.dismiss(animated: true, completion: nil)
    }

}

extension FilterDataViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterData[filterValue].filterValuesData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
            cell.textLabel?.textAlignment = .right
        } else {
            cell.textLabel?.textAlignment = .left
        }
        cell.textLabel?.text = filterData[filterValue].filterValuesData[indexPath.row].name
        if let array =  filterDict[filterData[filterValue].attribute_group_rewrite] {
            if array.contains(filterData[filterValue].filterValuesData[indexPath.row].name_rewrite) {
               cell.accessoryType = .checkmark
            }

        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        var array =  filterDict[filterData[filterValue].attribute_group_rewrite]
        if array != nil {
            if (array?.contains(filterData[filterValue].filterValuesData[indexPath.row].name_rewrite))! {
                var newArray  = array
                newArray?.remove(at: (array?.index(of: filterData[filterValue].filterValuesData[indexPath.row].name_rewrite))!)
                array = newArray
                cell?.accessoryType = .none
            } else {
                array?.append(filterData[filterValue].filterValuesData[indexPath.row].name_rewrite)
                cell?.accessoryType = .checkmark
            }
        } else {
            array = [String]()
            array?.append(filterData[filterValue].filterValuesData[indexPath.row].name_rewrite)
            cell?.accessoryType = .checkmark
        }
        filterDict[filterData[filterValue].attribute_group_rewrite] = array        
        if let varLabel: UIButton = self.view.viewWithTag(filterValue + 100) as? UIButton {
            if let array =  filterDict[filterData[filterValue].attribute_group_rewrite] {
                if array.count > 0 {
                    let title = String.init(format: "%@ (%i)", filterData[filterValue].attribute_group_name, array.count)
                    varLabel.setTitle(title, for: .normal)
                } else {
                    let title = String.init(format: "%@ ", filterData[filterValue].attribute_group_name )
                    varLabel.setTitle(title, for: .normal)
                }
            } else {
                let title = String.init(format: "%@ ", filterData[filterValue].attribute_group_name )
                varLabel.setTitle(title, for: .normal)
            }
        }
        print(filterDict)
    }

}

extension FilterDataViewController: RangeSeekSliderDelegate {

    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        if slider === priceView {
            print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
            minimumValueLabel.text = String.init(format: "minValue".localized + " %.0f", minValue)
            selectedMaximumValueLabel.text = String.init(format: "maxValue".localized + "%.0f", maxValue)
            filterDict[filterData[filterValue].attribute_group_rewrite] =  [ String.init(format: "%.0f", minValue ), String.init(format: "%.0f", maxValue) ]
        }
//        } else if slider === rangeSliderCurrency {
//            print("Currency slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
//        } else if slider === rangeSliderCustom {
//            print("Custom slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
//        }
    }

    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }

    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
}
