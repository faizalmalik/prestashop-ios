//
//  ChatDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import JSQMessagesViewController
import MobileCoreServices
import SwiftyJSON
import Alamofire
import IQKeyboardManagerSwift
import Firebase
import FirebaseDatabase
import  SVProgressHUD

class ChatDataViewController: JSQMessagesViewController {

   var messages = [JSQMessage]()
    var incomingBubble: JSQMessagesBubbleImage!
    var outgoingBubble: JSQMessagesBubbleImage!
     var ref: DatabaseReference!
    var custId: String!
     var id = ""
    var name = ""
    var email = ""
    var notificationKey = ""
    var buyer_unread = 0
    var admin_unread = 0
    
    override func viewDidLoad() {
         super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.navigationChanges()
         setupBackButton()
        self.inputToolbar.contentView.leftBarButtonItem = nil
        incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
        outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor.lightGray)
        self.navigationItem.title = self.senderDisplayName
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(ChatDataViewController.self)
        
//        self.makeRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SVProgressHUD.show(withStatus: "loading".localized)
        if let custId = self.custId {
            id = custId
        } else {
            id = senderId
        }
        if !id.isEmpty {
            
            ref = Database.database().reference().child("messages").child(id)
            
            ref.observe(.value, with: { snapshot in
                print(snapshot.value!)
                SVProgressHUD.dismiss()
                guard snapshot.exists() else {
                    return
                }
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot] {
                    var newMessage: JSQMessage!
                    self.messages.removeAll()
                    for snap in snapshots {
                        if let postDict = snap.value as? Dictionary<String, AnyObject> {
                            print(postDict)
                            let date  = Date(timeIntervalSince1970: Double(truncating: (postDict["timestamp"] as? NSNumber) ?? 0)/1000)                            
                            newMessage = JSQMessage(senderId: postDict["id"] as? String, senderDisplayName: postDict["name"] as? String, date: date, text: postDict["text"] as? String)
                            self.messages.append(newMessage)
                        }
                    }
                    self.finishReceivingMessage(animated: true)
                }
            })
        }
    }
    
    func setupBackButton() {
        let backButton = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: self, action: #selector(backButtonTapped))
        backButton.image = #imageLiteral(resourceName: "Back-Chevron-White")
        navigationItem.leftBarButtonItem = backButton
    }
    
    @objc func backButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
    
    func makeRequest(dict: [String: Any]) {
        let params = "https://us-central1-prestashop-marketplace.cloudfunctions.net/updateNotificationKey"
        var request = URLRequest(url: URL(string: params)!)
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.httpBody = jsonData
            request.httpMethod = "POST"
            let jsonSortString: String = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            print(jsonSortString)            
            Alamofire.request(request).responseJSON { (response) in
                //    print(response.value)
                switch response.result {
                case .success:
                    //let jsonData  = try! JSON.init(data: response.data!)
                    let jsonData  = JSON(response.data as Any)
                    print(jsonData)
                case .failure:
                    break
                }
            }
        } catch {
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageDataForItemAt indexPath: IndexPath) -> JSQMessageData {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, messageBubbleImageDataForItemAt indexPath: IndexPath) -> JSQMessageBubbleImageDataSource {
        
        return messages[indexPath.item].senderId == self.senderId ? outgoingBubble : incomingBubble
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
//        let message = messages[indexPath.item]
        return JSQMessagesAvatarImageFactory.avatarImage(withPlaceholder: #imageLiteral(resourceName: "User-Profile-Icon"), diameter: 25)!
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func didPressSend(_ button: UIButton, withMessageText text: String, senderId: String, senderDisplayName: String, date: Date) {
        /**
         *  Sending a message. Your implementation of this method should do *at least* the following:
         *
         *  1. Play sound (optional)
         *  2. Add new id<JSQMessageData> object to your data source
         *  3. Call `finishSendingMessage`
         */
        var newDict = [String: Any]()
        newDict["id"] = self.id
        newDict["name"] = self.name
        newDict["email"] =  self.email
        newDict["notificationKey"] = notificationKey
        if FetchDetails.isLoggedInAdmin() {
            newDict["buyer_unread"] = 1
            newDict["admin_unread"] = admin_unread
        } else {
            newDict["admin_unread"] = 1
            newDict["buyer_unread"] =  buyer_unread
        }
        
        self.makeRequest(dict: newDict)
        
        var dict = [String: Any]()
        dict["id"] = senderId
        dict["name"] = senderDisplayName
        dict["text"] =  text
        dict["timestamp"] = ServerValue.timestamp()
       
        ref = Database.database().reference()
        self.ref.child("messages").child(self.id).childByAutoId().setValue(dict)
        self.ref.child("data1").child(self.id).updateChildValues(["timestamp": ServerValue.timestamp()])
        self.finishSendingMessage()
       
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        let message = messages[indexPath.item]
        
        // Displaying names above messages
        // MARK: Removing Sender Display Name
        /**
         *  Example on showing or removing senderDisplayName based on user settings.
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         */
        if message.senderId == self.senderId {
            return nil
        }
        
        return NSAttributedString(string: message.senderDisplayName)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        //return 17.0
        let message = messages[indexPath.item]
        
        if message.senderId == senderId {
            return 0.0
        } else {
            
            return 17.0
            
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        if (indexPath.item % 3 == 0) {
            return 17.0
        }
        return 0.0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForCellTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        /**
         *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
         *  The other label text delegate methods should follow a similar pattern.
         *
         *  Show a timestamp for every 3rd message
         */
        if (indexPath.item % 3 == 0) {
            let message = self.messages[indexPath.item]
            
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        }
//
        return nil
    }

}
