//
//  NotificationViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import UIKit
import SwiftyJSON

class NotificationViewModal: NSObject {
    
    var notification = [NotificationData]()
    var moveDelegate: MoveController!
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = NotificationModal(data: jsonData) else {
            return
        }
        if !data.notification.isEmpty {
            self.notification = data.notification
            completion(true)
        } else {
            completion(false)
        }
    }
}

extension NotificationViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTableViewCell.identifier) as? NotificationTableViewCell {
            cell.item = notification[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if notification[indexPath.row].type == "category" {
            moveDelegate.moveController(id: notification[indexPath.row].id_page!, name: notification[indexPath.row].title!, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productCategory)
        } else
            if notification[indexPath.row].type == "product" {
                moveDelegate.moveController(id: notification[indexPath.row].id_page!, name: notification[indexPath.row].title!, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productController)
            } else {
                moveDelegate.moveController(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.home)
        }        
    }
    
}
