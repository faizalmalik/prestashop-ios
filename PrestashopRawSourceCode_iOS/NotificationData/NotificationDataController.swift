//
//  NotificationData.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class NotificationDataController: UIViewController {
    var emptyView: EmptyView?
    fileprivate let notificationViewModalObject = NotificationViewModal()
    @IBOutlet weak var notificationTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        notificationTableView.delegate = notificationViewModalObject
        notificationTableView.dataSource = notificationViewModalObject
        notificationTableView.register(NotificationTableViewCell.nib, forCellReuseIdentifier: NotificationTableViewCell.identifier)
        notificationTableView.tableFooterView = UIView()
        notificationTableView.separatorStyle = .none
        notificationTableView.estimatedRowHeight = 100
        notificationTableView.rowHeight = UITableView.automaticDimension
        notificationTableView.showsVerticalScrollIndicator = false
        self.navigationItem.title = "notifications".localized
        notificationViewModalObject.moveDelegate = self
        self.makeRequest()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeRequest() {
        var params = GlobalConstants.ApiNames.getNotifications
        params += UrlParameters.width
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                
                print("loginError".localized, error!)
                
            } else {
                
                UserDefaults.standard.set(0, forKey: "badge")
                UserDefaults.standard.synchronize()
                UIApplication.shared.applicationIconBadgeNumber = 0                
                
                self.notificationViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    print(data)
                    if data {
                        UserDefaults(suiteName: "group.com.webkul.MobikulPrestashop")?.set(responseObject!.rawString()!, forKey: AllController.notification.rawValue)
                        UserDefaults(suiteName: "group.com.webkul.MobikulPrestashop")?.synchronize()
                        self.emptyView?.isHidden = true
                        self.notificationTableView.isHidden = false
                        self.notificationTableView.reloadData()
                    } else {
                        self.emptyView?.isHidden = false
                        self.notificationTableView.isHidden = true
                    }
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = "notificationEmpty".localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-notification")
            emptyView?.button.isHidden = true
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension NotificationDataController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        switch controller {
        case .productController:
            if let view = UIStoryboard.loadProductViewController() {
                view.productId = id
                view.productName = name
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .productCategory:
            if let view = UIStoryboard.loadProductCategoryViewController() {
                view.id = id
                view.name = name
                self.navigationController?.pushViewController(view, animated: true)
            }
        default:
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popViewController(animated: true)
        }
    }
}
