//
//  NotificationModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

struct NotificationModal {

   var notification = [NotificationData]()
     init?(data: JSON) {

        if data["notifications"]["notification"] != JSON.null {
            if let data  = data["notifications"]["notification"].array {
                self.notification = data.map { NotificationData( data: $0) }
            } else {
                self.notification.append(NotificationData(data: data["notifications"]["notification"]))
            }
        }
        
    }
}

struct NotificationData {

    var id_page: String?
    var title: String?
    var image_link: String?
    var description: String?
    var type: String!
    init(data: JSON) {
        self.id_page = data["id_page"].stringValue
        self.image_link = data["image_link"].stringValue
        self.title = data["title"].stringValue
        self.description = data["description"].stringValue
        self.type = data["type"].stringValue
    }
}
