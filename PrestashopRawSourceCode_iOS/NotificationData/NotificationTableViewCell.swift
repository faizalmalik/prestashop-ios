//
//  NotificationTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!

    @IBOutlet weak var notificationImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        notificationView.shadowBorder()
        notificationView.backgroundColor = UIColor.white
        subTitleLabel.textColor = GlobalConstants.Colors.defaultTextColor
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: NotificationData? {
        didSet {
            subTitleLabel.text = item?.description
            titleLabel.text = item?.title
            let url = (item?.image_link)!
            notificationImage.setImage(imageUrl: url)
            
        }
    }
}
