//
//  OrderDetailViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class OrderDetailViewModal: NSObject {
    var items = [OrderDetailViewItem]()
    
    var paymentName: String!
    var shippingName: String!
    var shippingPrice: String!
    var controller: AllController!
    var invoiceAvailable: Bool!
    var moveDelegate: MoveController?
    var statusArray = [StatusHistory]()
    var jsonData: JSON!
    var products = [ProductData]()
    var downloadDelegate: DownloadProduct?
    
    func getValue( jsonData: JSON, call: WhichApiCall, completion: ((_ data: Bool) -> Void) ) {
        self.jsonData = jsonData
        guard let data =  OrderDetailModal(data: jsonData) else {
            return
        }
        items.removeAll()
        if !data.products.isEmpty {
            shippingPrice = data.shippingPrice
            shippingName = data.shippingName
            paymentName = data.paymentName
            invoiceAvailable = data.invoiceAvailable
            statusArray = data.statusArray
            if controller == AllController.sellerOrder {
                items.append(OrderViewModalStatusItem(status: data.orderStatusName))
                items.append(OrderViewModalProductItem(products: data.products))
                if !data.voucherArray.isEmpty {
                    items.append(OrderViewModalAppliedVoucherItem(vouchers: data.voucherArray))
                }
                items.append(OrderViewModalAddressItem(shipping: data.addresses))
                items.append(OrderViewModalBillingAddressItem(billing: data.billingAddresses))
            } else {
                products = data.products
                items.append(OrderViewModalProductItem(products: data.products))
                items.append(OrderViewModalOrdersItem(orderData: data.orderPrice!))
                if !data.voucherArray.isEmpty {
                    items.append(OrderViewModalAppliedVoucherItem(vouchers: data.voucherArray))
                }
                items.append(OrderViewModalAddressItem(shipping: data.addresses))
                items.append(OrderViewModalBillingAddressItem(billing: data.billingAddresses))
                items.append(OrderViewModalPaymentItem(payment: paymentName))
                if shippingName != "" {
                    items.append(OrderViewModalShippingItem(shipping: shippingName, price: shippingPrice))
                }
                if !data.messages.isEmpty {
                    items.append(OrderViewModalMessagesItem(messages: data.messages))
                }
                items.append(OrderViewModalAddMessageItem(messages: "addamessage".localized))
            }
            completion(true)
        }
    }
}

extension OrderDetailViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        switch item.type {
        case .products:
            if let item = item as? OrderViewModalProductItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: ProductOrderTableViewCell.identifier) as? ProductOrderTableViewCell {
                    cell.item = item.products[indexPath.row]
                    cell.downloadDelegate = self
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .pricedata :
            if let item = item as? OrderViewModalOrdersItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: OrderPriceView.identifier) as? OrderPriceView {
                    cell.item = item.orderData
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .voucher:
            if let item = item as? OrderViewModalAppliedVoucherItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: VouchersAppliedTableViewCell.identifier) as? VouchersAppliedTableViewCell {
                    cell.item = item.vouchers[indexPath.row]
                    cell.deleteBtn.isHidden = true
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .address:
            if let item = item as? OrderViewModalAddressItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: AddressDataTableViewCell.identifier) as? AddressDataTableViewCell {
                    cell.item = item.shipping
                    cell.arrowImage.isHidden = true
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .payment:
            if let item = item as? OrderViewModalPaymentItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: OrderPaymentTableViewCell.identifier) as? OrderPaymentTableViewCell {
                    cell.paymentMethodName.text = item.payment
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .shipping:
            if let item = item as? OrderViewModalShippingItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: OrderShippingTableViewCell.identifier) as? OrderShippingTableViewCell {
                    cell.shippingMethodName.text = item.shipping
                    cell.shippingPrice.text = item.price
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .billing:
            if let item = item as? OrderViewModalBillingAddressItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: AddressDataTableViewCell.identifier) as? AddressDataTableViewCell {
                    cell.item = item.billing
                    cell.arrowImage.isHidden = true
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .status:
            if let item = item as? OrderViewModalStatusItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: SellerOrderCheckStatusTableViewCell.identifier) as? SellerOrderCheckStatusTableViewCell {
                    cell.statusLabel.text = item.status
                    cell.selectionStyle = .none
                    cell.accessoryType = .disclosureIndicator
                    return cell
                }
            }
        case .messages:
            if let item = item as? OrderViewModalMessagesItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: OrderMessagesTableViewCell.identifier) as? OrderMessagesTableViewCell {
                    cell.item = item.messages[indexPath.row]
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .AddMessage:
            if let item = item as? OrderViewModalAddMessageItem {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                    cell.textLabel?.textAlignment = .right
                } else {
                    cell.textLabel?.textAlignment = .left
                }
                cell.textLabel?.text = item.messages
                cell.selectionStyle = .none
                cell.accessoryType = .disclosureIndicator
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.section]
        if item.type == OrderCases.status {
            moveDelegate?.moveController(id: "", name: "", dict: [:], jsonData: jsonData, index: 0, controller: AllController.sellerOrderStatus)
        } else if  item.type == OrderCases.AddMessage {
            let dict = ["products": products]
            moveDelegate?.moveController(id: "", name: "", dict: dict, jsonData: jsonData, index: 0, controller: AllController.addMessageController)
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let item = items[section]
        
        switch item.type {
        case .products:
            if let item = item as? OrderViewModalProductItem {
                return item.heading
            }
        case .pricedata:
            if let item = item as? OrderViewModalOrdersItem {
                return item.heading
            }
        case .address:
            if let item = item as? OrderViewModalAddressItem {
                return item.heading
            }
        case .payment:
            if let item = item as? OrderViewModalPaymentItem {
                return item.heading
                
            }
        case .shipping:
            if let item = item as? OrderViewModalShippingItem {
                return item.heading
                
            }
        case .billing:
            if let item = item as? OrderViewModalBillingAddressItem {
                return item.heading
                
            }
        case .status:
            
            if let item = item as? OrderViewModalStatusItem {
                return item.heading
                
            }
        case .voucher:
            if let item = item as? OrderViewModalAppliedVoucherItem {
                return item.heading
                
            }
        case .messages:
            if let item = item as? OrderViewModalMessagesItem {
                return item.heading
            }
        default:
            return nil
        }
        return nil
    }
    
}

extension OrderDetailViewModal: DownloadProduct {
    func StartDownload(item: ProductData) {
        downloadDelegate?.StartDownload(item: item)
    }
}
