//
//  OrderDetailDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON
import Photos
import SVProgressHUD

protocol DownloadProduct: class {
    func StartDownload(item: ProductData)
}

class OrderDetailDataViewController: UIViewController {
    
    @IBOutlet weak var orderTableView: UITableView!
    @IBOutlet weak var invoiceBtn: UIButton!
    @IBOutlet weak var invoiceBtnHeight: NSLayoutConstraint!
    
    var id: String!
    var reference: String!
    var controller: AllController!
    let OrderDetailViewModalObject = OrderDetailViewModal()
    var emptyView: EmptyView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = reference
        invoiceBtnHeight.constant = 0
        orderTableView.register(ProductOrderTableViewCell.nib, forCellReuseIdentifier: ProductOrderTableViewCell.identifier)
        orderTableView.register(OrderPriceView.nib, forCellReuseIdentifier: OrderPriceView.identifier)
        orderTableView.register(AddressDataTableViewCell.nib, forCellReuseIdentifier: AddressDataTableViewCell.identifier)
        orderTableView.register(OrderPaymentTableViewCell.nib, forCellReuseIdentifier: OrderPaymentTableViewCell.identifier)
        orderTableView.register(OrderShippingTableViewCell.nib, forCellReuseIdentifier: OrderShippingTableViewCell.identifier)
        orderTableView.register(SellerOrderCheckStatusTableViewCell.nib, forCellReuseIdentifier: SellerOrderCheckStatusTableViewCell.identifier)
        orderTableView.register(VouchersAppliedTableViewCell.nib, forCellReuseIdentifier: VouchersAppliedTableViewCell.identifier)
        orderTableView.register(OrderMessagesTableViewCell.nib, forCellReuseIdentifier: OrderMessagesTableViewCell.identifier)
        
        orderTableView.dataSource = OrderDetailViewModalObject
        orderTableView.delegate = OrderDetailViewModalObject
        
        OrderDetailViewModalObject.moveDelegate = self
        OrderDetailViewModalObject.downloadDelegate = self
        OrderDetailViewModalObject.controller = controller
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.makeRequest()
        let documentsUrl: URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        print(documentsUrl)
        let destinationFileUrl = documentsUrl.appendingPathComponent(id + "downloadedFile.pdf")
        print(documentsUrl)
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: destinationFileUrl.path) {
            self.invoiceBtn.setTitle("View Invoice", for: .normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeRequest() {
        var params = ""
        
        if controller == AllController.sellerOrder {
            params = GlobalConstants.MarketPlaceApis.sellerOrderDetails
            params += UrlParameters.fetchSellerID()!
        } else {
            params = GlobalConstants.ApiNames.orderDetails
        }
        params += "&id_order=" + id!
        params += UrlParameters.width
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                
                if let data = responseObject {
                    self.OrderDetailViewModalObject.getValue(jsonData: data, call: WhichApiCall.none) {
                        (data: Bool) in
                        if self.OrderDetailViewModalObject.invoiceAvailable {
                            self.invoiceBtnHeight.constant = 40
                            self.invoiceBtn.isHidden = false
                        } else {
                            self.invoiceBtnHeight.constant = 0
                            self.invoiceBtn.isHidden = true
                        }
                        
                        self.orderTableView.reloadData()
                    }
                }
            }
        }
    }
    
    @IBAction func invoiceBtnClicked(_ sender: UIButton) {
        // Create destination URL
        let documentsUrl: URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        print(documentsUrl)
        let destinationFileUrl = documentsUrl.appendingPathComponent(id + "downloadedFile.pdf")
        print(documentsUrl)
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: destinationFileUrl.path) {
            print("FILE AVAILABLE")
            if let view = UIStoryboard.loadCmsViewController() {
                view.controller = nil
                view.url = destinationFileUrl
                self.navigationController?.pushViewController(view, animated: true)
            }
        } else {
            //Create URL to the source file you want to download
            var stringUrl = GlobalConstants.AppCredentials.BASEURL + GlobalConstants.ApiNames.getpdf + "object=invoice"
            if let id_customer = UserDefaults.fetch(key: UserDefaults.Keys.id_customer) {
                stringUrl += "&id_customer=" + id_customer + "&id_order=" + id + "&ws_key=" + GlobalConstants.AppCredentials.WS_KEY
            }
            let fileURL = URL(string: stringUrl)
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url: fileURL!)
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    print(tempLocalUrl)
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    } catch (let writeError) {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print("Successfully downloaded. Status code: \(statusCode)")
                        self.move(url: destinationFileUrl)
                    }
                } else {
                    print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as Any)
                }
            }
            task.resume()
        }
    }
    
    func move(url: URL) {
        DispatchQueue.main.async {
            if let view = UIStoryboard.loadCmsViewController() {
                view.controller = nil
                view.url = url
                self.navigationController?.pushViewController(view, animated: true)
            }
        }
    }
}

extension OrderDetailDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        if controller == AllController.addMessageController {
            if let view  = UIStoryboard.loadAddMessageViewController() {
                if let products = dict["products"] as? [ProductData] {
                    view.product = products
                }
                view.id_order = self.id
                self.navigationController?.pushViewController(view, animated: true)
            }
        } else {
            if let view  = UIStoryboard.loadSellerStatusViewController() {
                view.jsonData = jsonData
                view.id = self.id
                view.statusId = self.OrderDetailViewModalObject.statusArray[0].id_order_state
                self.navigationController?.pushViewController(view, animated: true)
            }
        }
    }
}

extension OrderDetailDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest()
    }
}

extension OrderDetailDataViewController: DownloadProduct {
    func StartDownload(item: ProductData) {
        if let documentsDirectoryURL = try? FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) {
            let fileURL = documentsDirectoryURL.appendingPathComponent(item.display_filename ?? "")
            print(fileURL)
            if !FileManager.default.fileExists(atPath: fileURL.path) {
                //Create URL to the source file you want to download
                //http://suraj2kumar.com/d6/ps1731/api/mobikul/getvirtualitem?id_product=23&id_lang=1&ws_key=suraj2kumarattheratewebkuldotcom&id_order=122&id_currency=1&id_customer=90
                var stringUrl = GlobalConstants.AppCredentials.BASEURL + GlobalConstants.ApiNames.getvirtualitem
                stringUrl += "id_product=" + (item.id_product ?? "")
                stringUrl += "&id_lang=" + (UserDefaults.standard.value(forKey: "id_lang") as? String ?? "1")
                stringUrl += "&ws_key=" + GlobalConstants.AppCredentials.WS_KEY
                stringUrl += "&id_order=" + (item.id_order ?? "")
                stringUrl += "&id_currency=" + (UserDefaults.fetch(key: "id_currency") ?? "")
                stringUrl += "&id_customer=" + (UserDefaults.fetch(key: UserDefaults.Keys.id_customer) ?? "")
                print(stringUrl)
                //stringUrl = stringUrl.replacingOccurrences(of: "%2020", with: "%20").replacingOccurrences(of: "%2025", with: "%20")
                //print(stringUrl)
                let status = PHPhotoLibrary.authorizationStatus()
                switch status {
                case .authorized:
                    if  let url = URL(string: stringUrl) {
                        do {
                            let largeImageData = try Data(contentsOf: url)
                            print(largeImageData)
                            try largeImageData.write(to: fileURL)
                            if let bannerImage: UIImage = UIImage(data: largeImageData) {
                                UIImageWriteToSavedPhotosAlbum(bannerImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                            } else {
                                PHPhotoLibrary.shared().performChanges({
                                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: fileURL)
                                }) { saved, error in
                                    if saved {
                                        print("Your video was successfully saved")
                                    } else {
                                        print("Not Loaded To Photo Directory")
                                    }
                                }
                            }
                            let AC = UIAlertController(title: "success".localized, message: "yourfilehasbeensavedwouldyouliketosee".localized, preferredStyle: .alert)
                            let okBtn = UIAlertAction(title: "ok".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                if let view = UIStoryboard.loadCmsViewController() {
                                    view.controller = nil
                                    view.url = fileURL
                                    view.titleName = item.name
                                    self.navigationController?.pushViewController(view, animated: true)
                                }
                            })
                            let noBtn = UIAlertAction(title: "cancel".localized, style: .destructive, handler: nil)
                            AC.addAction(okBtn)
                            AC.addAction(noBtn)
                            self.present(AC, animated: true, completion: nil)
                        } catch {
                            print(error)
                        }
                    }
                case .denied, .restricted :
                    break
                case .notDetermined:
                    PHPhotoLibrary.requestAuthorization { status in
                        switch status {
                        case .authorized:
                            if  let url = URL(string: stringUrl) {
                                do {
                                    let largeImageData = try Data(contentsOf: url)
                                    try largeImageData.write(to: fileURL)
                                    if let bannerImage: UIImage = UIImage(data: largeImageData) {
                                        UIImageWriteToSavedPhotosAlbum(bannerImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
                                    } else {
                                        PHPhotoLibrary.shared().performChanges({
                                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: fileURL)
                                        }) { saved, error in
                                            if saved {
                                                print("Your video was successfully saved")
                                            } else {
                                                print("Not Loaded To Photo Directory")
                                            }
                                        }
                                    }
                                    let AC = UIAlertController(title: "success".localized, message: "yourfilehasbeensavedwouldyouliketosee".localized, preferredStyle: .alert)
                                    let okBtn = UIAlertAction(title: "ok".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                        if let view = UIStoryboard.loadCmsViewController() {
                                            view.controller = nil
                                            view.url = fileURL
                                            view.titleName = item.name
                                            self.navigationController?.pushViewController(view, animated: true)
                                        }
                                    })
                                    let noBtn = UIAlertAction(title: "cancel".localized, style: .destructive, handler: nil)
                                    AC.addAction(okBtn)
                                    AC.addAction(noBtn)
                                    self.present(AC, animated: true, completion: nil)
                                } catch {
                                    print(error)
                                }
                            }
                        case .denied, .restricted, .notDetermined:
                            break
                        }
                    }
                }
            } else {
                let AC = UIAlertController(title: "success".localized, message: "yourfilehasbeensavedwouldyouliketosee".localized, preferredStyle: .alert)
                let okBtn = UIAlertAction(title: "ok".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    if let view = UIStoryboard.loadCmsViewController() {
                        view.controller = nil
                        view.url = fileURL
                        view.titleName = item.name
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                })
                let noBtn = UIAlertAction(title: "cancel".localized, style: .destructive, handler: nil)
                AC.addAction(okBtn)
                AC.addAction(noBtn)
                self.present(AC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            print(error)
            print("Not Loaded To Photo Directory")
        } else {
            print("Saved To Photo Library")
        }
    }
}
