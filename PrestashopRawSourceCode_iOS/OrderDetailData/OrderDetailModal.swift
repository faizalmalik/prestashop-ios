//
//  OrderDetailModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

struct OrderDetailModal {

    private let cartPath = ["order"]
    private let ProductPath = ["order", "associations", "order_rows", "order_row"]
    private let orderMessages = ["order", "messages", "message"]
    private let sellerProductPath = ["products", "product"]
     var statusArray = [StatusHistory]()
        let statusPath = ["status_history", "history"]

    var products = [ProductData]()
     var messages = [OrderMessages]()
    var orderPrice: CartPriceData?

    var paymentName: String!
    var shippingName: String!
    var shippingPrice: String!
    var orderStatusName: String!
    var invoiceAvailable: Bool!

    private let cartAddressPath =  ["order", "associations", "delivery_address"]
    private let sellerAddressPath =  ["customer_details", "address_delivery"]
     private let billingAddressPath =  ["order", "associations", "invoice_address"]
     private let sellerbillingAddressPath =  ["customer_details", "address_invoice"]
    private let VoucherPath =  ["order", "associations", "discounts", "discount"]
    var addresses: AddressData!
    var billingAddresses: AddressData!
     var voucherArray = [VoucherData]()

    init?(data: JSON) {

        if data[ProductPath] != JSON.null {
            if let data  = data[ProductPath].array {
                self.products = data.map { ProductData( data: $0) }
            } else {
                self.products.append(ProductData(data: data[ProductPath]))
            }
        }
        if data[VoucherPath] != JSON.null {
            if let data  = data[VoucherPath].array {
                self.voucherArray = data.map { VoucherData( data: $0) }
            } else {
                print(data[VoucherPath])
                self.voucherArray.append(VoucherData(data: data[VoucherPath]))
            }
        } else {
            let Voucherpath =  [ "vouchers", "voucher"]
            if data[Voucherpath] != JSON.null {
                if let data  = data[Voucherpath].array {
                    self.voucherArray = data.map { VoucherData( data: $0) }
                } else {
                    self.voucherArray.append(VoucherData(data: data[Voucherpath]))
                }
            }
        }

        if data[sellerProductPath] != JSON.null {
            if let data  = data[sellerProductPath].array {
                self.products = data.map { ProductData( data: $0) }
            } else {
                self.products.append(ProductData(data: data[sellerProductPath]))
            }
        }
        
        if data[orderMessages] != JSON.null {
            if let data  = data[orderMessages].array {
                self.messages = data.map { OrderMessages( data: $0) }
            } else {
                self.messages.append(OrderMessages(data: data[orderMessages]))
            }
        }

        self.orderPrice = CartPriceData(data: data[cartPath])

        if data[cartAddressPath] != JSON.null {

            self.addresses = AddressData(data: data[cartAddressPath])

        }
        if data[sellerAddressPath] != JSON.null {

            self.addresses = AddressData(data: data[sellerAddressPath])

        }

        if data[billingAddressPath] != JSON.null {

            self.billingAddresses = AddressData(data: data[billingAddressPath])

        }

        if data[sellerbillingAddressPath] != JSON.null {

            self.billingAddresses = AddressData(data: data[sellerbillingAddressPath])

        }
        
        if data[statusPath] != JSON.null {
            if let data  = data[statusPath].array {
                self.statusArray = data.map { StatusHistory( data: $0) }
            } else {
                self.statusArray.append( StatusHistory( data: data[statusPath] ))
            }
            orderStatusName =  self.statusArray[0].ostate_name
        }
        print(data["invoice_number"].stringValue)
        if data["order"]["invoice_number"].stringValue == "0" || data["order"]["invoice_number"].stringValue.isEmpty {
            invoiceAvailable = false
        } else {
            invoiceAvailable = true
        }
        paymentName = data["order"]["payment"].stringValue
         shippingName = data["order"]["carrier_name"].stringValue
         shippingPrice = data["order"]["total_shipping"].stringValue

    }

}

struct OrderMessages {
    var comment: String!
    var from: String!
    var comment_date: String!
    init(data: JSON) {
        comment = data["comment"].stringValue
        from = data["from"].stringValue
        comment_date = data["comment_date"].stringValue
    }
    
}
