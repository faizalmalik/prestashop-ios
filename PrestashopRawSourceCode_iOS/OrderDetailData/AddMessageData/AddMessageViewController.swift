//
//  AddMessageViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

class AddMessageViewController: UIViewController {

    var product = [ProductData]()
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var commentFiels: UITextView!
    @IBOutlet weak var productSelectLabel: UILabel!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var products_lbl: UILabel!
    @IBOutlet weak var comment_lbl: UILabel!
    var productID: String!
    var id_order: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        commentFiels.textBorder()
        productView.textBorder()
        descriptionLabel.SetDefaultTextColor()
        sendBtn.SetAccentBackColor()
        productSelectLabel.text = product[0].name
        productID = product[0].id_product
        self.navigationItem.title = "addamessage".localized
        descriptionLabel.text = "addCommentaboutOrder".localized
        products_lbl.text = "products".localized
        comment_lbl.text = "comment".localized
        sendBtn.setTitle("send".localized, for: .normal)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func productsClicked(_ sender: UITapGestureRecognizer) {
        let picker = ActionSheetStringPicker(title: "products".localized, rows: product.map {$0.name }, initialSelection: 0, doneBlock: { _, indexes, _ in
            self.productSelectLabel.text = self.product[indexes].name
            self.productID = self.product[indexes].id_product
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: productView)
        picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
        picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
        picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
        picker?.show()
    }
    
    @IBAction func sendBtnClicked(_ sender: UIButton) {
        if commentFiels.text.isEmpty {
            ErrorChecking.warningView(message: "enterMessage".localized)
        } else {
            var dict = [String: Any]()
            dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            dict["id_order"] = id_order
            dict["id_product"] = productID
            dict["message"] = commentFiels.text
            self.makeRequest(dict: dict, call: WhichApiCall.none)
        }
    }
    
    func makeRequest(dict: [String: Any], call: WhichApiCall ) {
        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get
        
        params = GlobalConstants.ApiNames.addordermessage
       
        newDict[GlobalConstants.ApiNames.addordermessage.FetchApiName()] = dict
        verbs = .post
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.addWishlist, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                
                print("loginError".localized, error!)
                
            } else {
                
                if ErrorChecking.getData(data: responseObject) != JSON.null && ErrorChecking.getData(data: responseObject) != nil {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
