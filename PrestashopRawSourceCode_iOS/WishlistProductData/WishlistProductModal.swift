//
//  WishlistProductModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

struct WishlistProductModal {

    var wishArray = [ProductData]()

    private let wishPath =  ["response", "wishlistproduct", "product"]

    init?(data: JSON) {

        if data[wishPath] != JSON.null {
            if let data  = data[wishPath].array {
                self.wishArray = data.map { ProductData( data: $0) }
            } else {
                self.wishArray.append(ProductData(data: data[wishPath]))
            }
        }

    }
}
