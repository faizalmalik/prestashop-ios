//
//  WishlistProductTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import ActionSheetPicker_3_0

class WishlistProductTableViewCell: UITableViewCell {

    @IBOutlet weak var attributesLabel: UILabel!

    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var priorityLabel: UILabel!
    @IBOutlet weak var wishlistView: UIView!

    @IBOutlet weak var stepperView: UIStepper!

    var oldValue = Double()

    var id_product: String?
    var quantityValue: String?
    var id_product_attribute: String?
    var delegate: PassData?
    var oldQuantityValue: String?

    let priorityArray = [ "High", "Medium", "Low"]

    var  priority: String!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        wishlistView.layer.masksToBounds =  false
        wishlistView.backgroundColor = UIColor.white
        stepperView.minimumValue = 1.0
        stepperView.tintColor = GlobalConstants.Colors.accentColor
        priorityLabel.textColor = UIColor().HexToColor(hexString: "#FF5252")

        let tap = UITapGestureRecognizer(target: self, action: #selector(PriorityClicked(_:)))
      priorityLabel.isUserInteractionEnabled = true
        priorityLabel.addGestureRecognizer(tap)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: ProductData? {
        didSet {

            productImage.setImage(imageUrl: (item?.image_link!)!)
            productNameLabel.text = item?.name

            qtyLabel.text = String.init(format: "%@: %@", "qty".localized, (item?.quantity)!)
            if item?.priority == "0" {
                priorityLabel.text = String.init(format: "%@: %@", "priority".localized, "high".localized)
            } else if item?.priority == "1" {
                priorityLabel.text = String.init(format: "%@: %@", "priority".localized, "medium".localized)
            } else if item?.priority == "2" {
                priorityLabel.text = String.init(format: "%@: %@", "priority".localized, "low".localized)
            }

            priority = item?.priority
            qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: "qty".localized)
            priorityLabel.halfTextColorChange(fullText: priorityLabel.text!, changeText: "priority".localized)
            stepperView.value = Double(item?.quantity ?? "0")!
            attributesLabel.text = item?.attributes
            oldQuantityValue = item?.quantity
            self.id_product_attribute = item?.id_product_attribute
            self.id_product = item?.id_product

        }
    }

    @objc func PriorityClicked(_ sender: UITapGestureRecognizer) {
        let picker = ActionSheetStringPicker(title: "priority".localized, rows: priorityArray, initialSelection: (Int(item?.priority ?? "0") ?? 0), doneBlock: { _, indexes, _ in
            self.priorityLabel.text = String.init(format: "%@: %@", "priority".localized, self.priorityArray[indexes])
            self.priorityLabel.halfTextColorChange(fullText: self.priorityLabel.text!, changeText: "priority".localized)
            self.priority = String(indexes)
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: priorityLabel)
        picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
        picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
        picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
        picker?.show()
    }

    @IBAction func StepperAction(_ sender: UIStepper) {

        self.qtyLabel.text = String.init(format: "%@: %.0f", "qty".localized, sender.value )
        qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: "qty".localized)

        //        if sender.value > oldValue {
        //            delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call: WhichApiCall.updateCartPlus)
        //        }
        //        else {
        //            delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call:  WhichApiCall.updateCartMinus)
        //        }
    }
}
