//
//  WishlistProductDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class WishlistProductDataViewController: UIViewController {

    @IBOutlet weak var wishlistProductTableView: UITableView!
    fileprivate let WishlistProductViewModalObject = WishlistProductViewModal()
     var emptyView: EmptyView?

    var id: String!
    var name: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = name
        wishlistProductTableView.register(WishlistProductTableViewCell.nib, forCellReuseIdentifier: WishlistProductTableViewCell.identifier)
        wishlistProductTableView.dataSource = WishlistProductViewModalObject
        wishlistProductTableView.delegate = WishlistProductViewModalObject
        WishlistProductViewModalObject.moveControllerDelegate = self
        wishlistProductTableView.tableFooterView = UIView()
        WishlistProductViewModalObject.passDataDelegate = self
        WishlistProductViewModalObject.wishlistId = id
        // Do any additional setup after loading the view.
        self.makeRequest(dict: [:], call: WhichApiCall.none)
    }

    override func viewWillAppear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.black
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = "wishlistEmpty".localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-wishlist")
            emptyView?.button.isHidden = true
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.white
    }

    func makeRequest(dict: [String: Any], call: WhichApiCall ) {

        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get

        switch call {
        case .deleteWishlist:
            params = GlobalConstants.ApiNames.manageWishlist
            if let custId =  UrlParameters.fetchCustomerId() {
                params += custId
            }
            newDict[GlobalConstants.ApiNames.manageWishlist.FetchApiName()] = dict
            verbs = .post
        case .saveWishlist:
            params = GlobalConstants.ApiNames.manageWishlist
            if let custId =  UrlParameters.fetchCustomerId() {
                params += custId
            }
            newDict[GlobalConstants.ApiNames.manageWishlist.FetchApiName()] = dict
            verbs = .post
        case .getWishlists:
            params = GlobalConstants.ApiNames.getWishlists
            if let id_customer = UrlParameters.fetchCustomerId() {
                params += id_customer
            }
        case .moveWishlist:
            params = GlobalConstants.ApiNames.manageWishlist
            if let custId =  UrlParameters.fetchCustomerId() {
                params += custId
            }
            verbs = .post
            newDict[GlobalConstants.ApiNames.manageWishlist.FetchApiName()] = dict

        default:
            params = GlobalConstants.ApiNames.getWishlistProducts
            if let custId =  UrlParameters.fetchCustomerId() {
                params += custId
            }
            params += UrlParameters.width
            params += "&id_wishlist=" + id
        }

        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                self.emptyView?.dict = dict
                self.emptyView?.call = call                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                self.emptyView?.isHidden = true
                self.wishlistProductTableView.isHidden = false
                switch call {
                case .deleteWishlist:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.makeRequest(dict: [:], call: WhichApiCall.none)
                    }
                case .getWishlists:

                    if let data = responseObject {

                        let wishlistPath =  ["wishlists", "wishlist"]
                        var wishlistArray = [WishlistData]()
                        if data[wishlistPath] != JSON.null {
                            if let data  = data[wishlistPath].array {
                               wishlistArray = data.map { WishlistData( data: $0) }
                            } else {
                                wishlistArray.append(WishlistData(data: data[wishlistPath]))
                            }
                            if let index =  wishlistArray.index(where: { $0.id_wishlist == self.id }) {
                                wishlistArray.remove(at: index)
                            }
                            self.picker(data: wishlistArray, dict: dict)
                        }
                    }
                case .moveWishlist:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.makeRequest(dict: [:], call: WhichApiCall.none)
                    }
                  case .saveWishlist:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                            self.makeRequest(dict: [:], call: WhichApiCall.none)
                    }
                default:
                    self.WishlistProductViewModalObject.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                        if data {
                            self.emptyView?.isHidden = true
                            self.wishlistProductTableView.isHidden = false
                            self.wishlistProductTableView.reloadData()
                        } else {
                            self.emptyView?.isHidden = false
                            self.wishlistProductTableView.isHidden = true
                        }
                    }
                }
            }
        }
    }

    func picker(data: [WishlistData], dict: [String: Any]) {
        if data.count > 0 {
            let picker = ActionSheetStringPicker(title: "wishlists".localized, rows: data.map {$0.name!}, initialSelection: 0, doneBlock: { _, indexes, _ in
                var moveWishlistDict = [String: Any]()
                moveWishlistDict = dict
                moveWishlistDict["id_new_wishlist"] =  data[indexes].id_wishlist
                self.makeRequest(dict: moveWishlistDict, call: WhichApiCall.moveWishlist)
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: self.view)
            picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
            picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
            picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
            picker?.show()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
//    var moveWishlistDict = [String : Any]()
}

extension WishlistProductDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        if let view = UIStoryboard.loadProductViewController() {
            view.productId = id
            view.productName = name
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
}

extension WishlistProductDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        switch call {
        case .deleteWishlist:
            self.makeRequest(dict: dict, call: WhichApiCall.deleteWishlist)
        case .moveWishlist:
            self.makeRequest(dict: dict, call: WhichApiCall.getWishlists)
        case .saveWishlist:
            self.makeRequest(dict: dict, call: WhichApiCall.saveWishlist)
        default:
            print()
        }

    }
}

extension WishlistProductDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
    }
    
    func buttonPressed() {
        self.makeRequest(dict: (emptyView?.dict)!, call: (emptyView?.call)!)
    }
}
