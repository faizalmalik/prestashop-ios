//
//  WishlistProductViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

class WishlistProductViewModal: NSObject {
    
    var moveControllerDelegate: MoveController?
    var passDataDelegate: PassData?
    
    var wishlistId: String!
    
    var wishArray = [ProductData]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = WishlistProductModal(data: jsonData) else {
            return
        }
        wishArray.removeAll()
        if !data.wishArray.isEmpty {
            wishArray = data.wishArray
            completion(true)
        } else {
            completion(false)
        }
        
    }
}

extension WishlistProductViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: WishlistProductTableViewCell.identifier) as? WishlistProductTableViewCell {
            cell.item = wishArray[indexPath.row]
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        moveControllerDelegate?.moveController(id: wishArray[indexPath.row].id_product, name: wishArray[indexPath.row].name, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.addAddress)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let moreAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "move".localized, handler: { (_, indexPath) in
            var dict = [String: Any]()
            dict["id_product"] = self.wishArray[indexPath.row].id_product
            dict["id_product_attribute"] = self.wishArray[indexPath.row].id_product_attribute
            dict["operation"] = "move"
            dict["quantity"] =  self.wishArray[indexPath.row].quantity
            dict["priority"] =   self.wishArray[indexPath.row].priority
            dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            dict["id_wishlist"] = self.wishlistId
            self.passDataDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiCall.moveWishlist)
        })
        moreAction.backgroundColor = UIColor.lightGray
        
        let shareAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "save".localized, handler: { (_, indexPath) in
            var dict = [String: Any]()
            if let cell = tableView.cellForRow(at: indexPath) as? WishlistProductTableViewCell {
                dict["id_product"] = self.wishArray[indexPath.row].id_product
                dict["id_product_attribute"] = self.wishArray[indexPath.row].id_product_attribute
                dict["operation"] = "update"
                dict["quantity"] =  cell.stepperView.value
                dict["priority"] =   cell.priority
                dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
                dict["id_wishlist"] = self.wishlistId
                self.passDataDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiCall.saveWishlist)
            }
        })
        shareAction.backgroundColor = UIColor.green
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "delete".localized, handler: { (_, indexPath) in
            var dict = [String: Any]()
            dict["id_product"] = self.wishArray[indexPath.row].id_product
            dict["id_product_attribute"] = self.wishArray[indexPath.row].id_product_attribute
            dict["operation"] = "remove"
            dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            dict["id_wishlist"] = self.wishlistId
            self.passDataDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiCall.deleteWishlist)
        })
        deleteAction.backgroundColor = UIColor().HexToColor(hexString: "#FF5252")
        return [deleteAction, shareAction, moreAction]
        
    }
    
}
