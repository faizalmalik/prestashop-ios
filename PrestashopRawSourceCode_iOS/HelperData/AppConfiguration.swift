//
//  GlobalConstants.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import UIKit
import SwiftMessages
import SwiftyJSON

let SCREEN_WIDTH = ((UIApplication.shared.statusBarOrientation == .portrait) || (UIApplication.shared.statusBarOrientation == .portraitUpsideDown) ? UIScreen.main.bounds.size.width : UIScreen.main.bounds.size.height)

enum EnvironmentType {
    case mobikul, marketplace
}

struct  GlobalConstants {
    
    struct AppCredentials {
        
        #if MARKETPLACE
        //http://jewelbharat.com/api/mpsshop/gethomepage?ws_key=3LBHRHTXY4JW8F6CZBAHMM6ASSVP7R69&width=420&outputformat=json
        static let BASEURL: String = "http://jewelbharat.com/api/"
        static let WS_KEY: String = "3LBHRHTXY4JW8F6CZBAHMM6ASSVP7R69"
        
        static let environment: EnvironmentType = .marketplace
        #else
        
        static let BASEURL: String = "http://jewelbharat.com/api/"
        static let WS_KEY: String = "3LBHRHTXY4JW8F6CZBAHMM6ASSVP7R69"
        
        static let environment: EnvironmentType = .mobikul
        #endif
        
    }
    
    struct ApiNames {
        
        static let firebasetoken = "mpsshop/registergcmuser"
        static let homepageApi = "mpsshop/gethomepage?"
        
        static let myAccount = "mpsshop/mydashboard?"
        static let myMerchandise = "mpsshop/getordersreturn?"
        static let myCreditSlip = "mpsshop/getordersslip?"
        
        // order
        static let orderHistory = "mpsshop/getcustomerorder?"
        static let orderDetails = "mpsshop/getorder?"
        static let reOrder =  "mpsshop/createreorder?"
        
        // notifications
        static let getNotifications = "mpsshop/getsentnotification?"
        
        // requestList
        static let getRequestList = "mpsshop/getRealStockRequestList?"
        
        
        static let aboutUs = "mpsshop/getaboutuspage?"
        static let terms = "mpsshop/gettermsandcondition?"
        static let privacyPolicy = "mpsshop/getprivacypolicypage?"
        static let support  = "mpsshop/getcustomerservicepage?"
        
        //        cart
        static let cart = "mpsshop/getcustomercart?"
        static let addToCart = "mpsshop/addproducttocart?"
        static let deleteCart = "mpsshop/deletecartproduct?"
        static let updateCustomerCart = "mpsshop/updatecartcustomer?"
        static let applyVoucher = "mpsshop/managevoucher?"
        
        //product
        
        static let getProductData = "mpsshop/getproductpage"
        static let productCategory = "mpsshop/getcategorypage?"
        static let productAvailabilityCheck = "mpsshop/getRealTimeStockRequestFlag?"
        static let checkAvailability =    "mpsshop/shopToServerStockRequest?"
        static let getRequestCount  =  "mpsshop/getRealStockRequestCount?"
        static let updateRequest  = "mpsshop/updateRealTimeStockRequest?"

        
        // sign in
        
        static let login = "mpsshop/login?"
        static let forgotPassword = "mpsshop/forgotpassword?"
        static let socialLogin = "mpsshop/loginthroughsocialsite?"
        
        
        
        //Review
        
        static let addReview = "mpsshop/postreview?"
        
        // checkout
        
        static let getAddress = "mpsshop/getcustomeraddress?"
        static let orderReview = "mpsshop/getcartdetails?"
        static let getShpppingMethod = "mpsshop/getcarriers?"
        static let updateShippingMethod = "mpsshop/updatecartcarrier?"
        static let getPaymentMethods = "mpsshop/getpaymentmethods?"
        static let placeOrder = "mpsshop/createorder?"
        static let updateCartAddress = "mpsshop/updatecartaddress?"
        
        //address
        
        static let getCountries = "mpsshop/getcountrieswithstates?"
        static let saveAddress = "mpsshop/saveaddress?"//"addresses?"
        static let editAddress = "addresses/"
        static let getAddressFields = "mpsshop/getaddressform?"
        static let deleteaddress = "mpsshop/deleteaddress?"
        
        static let accountDetails = "mpsshop/getcustomerform?"
        static let checkCustomerPassword = "mpsshop/checkcustomerpassword?"
        static let customerExists = "mpsshop/customerexists?"
        static let savecustomer = "mpsshop/savecustomer?"
        
        //        wishlist
        static let getWishlists = "mpsshop/getwishlist?"
        static let getWishlistProducts = "mpsshop/getwishlistproducts?"
        static let manageWishlist = "mpsshop/managewishlist?"
        static let defaultWishlist  = "mpsshop/setwishlistdefault?"
        static let addWishlist = "mpsshop/addwishlist?"
        static let addRemoveWishlist = "mpsshop/addremovewishlistproduct?"
        static let getVouchers = "mpsshop/getallvouchers"
        
        //Contact us
        static let getContactUs = "mpsshop/getcontactuspage?"
        static let submitContactUs = "mpsshop/submitcontactus?"
        
        static let getAllProducts = "mpsshop/getmoreproduct?"
        
        static let guestLogin = "customers?"
        
        static let getpdf = "mpsshop/getpdf?"
        
        static let addordermessage = "mpsshop/addordermessage?"
        // MarketPlace Apis
        
        static let saveAttachment = "mpsshop/downloadproductattachment?"
        static let getvirtualitem = "mpsshop/getvirtualitem?"
    }
    
    struct MarketPlaceApis {
        
        static let getSellerReq = "mpsmarket/getsellerrequestinfo?"
        static let createSeller = "mpsmarket/createsellerrequest?"
        static let sellerOrder = "mpsmarket/getsellerorder?"
        static let sellerOrderDetails = "mpsmarket/getsellerorderdetails?"
        static let updatesellerorderstatus = "mpsmarket/updatesellerorderstatus?"
        static let getSellerCollection = "mpsmarket/getsellercollection?"
        static let getSellerProfile = "mpsmarket/getsellerprofile?"
        static let sellerDashBoard = "mpsmarket/getsellerdashboard?"
        static let getAllSellerList = "mpsmarket/getstoreseller?"
        static let contactseller = "mpsmarket/contactseller?"
    }
    
    struct AppSizes {
        static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
        static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    }
    
    struct Colors {
        static var colorChange = false
        static var gradientArray = [primaryColor, accentColor]
        static var primaryColor = UIColor().HexToColor(hexString: "#14334A")
        static let DefaultprimaryColor = UIColor().HexToColor(hexString: "#14334A")
        static let lineColor = UIColor().HexToColor(hexString: "#FFF176")
        static let priceColor = UIColor().HexToColor(hexString: "#00C853")
        static let defaultTextColor = UIColor().HexToColor(hexString: "#ADADB0")
        static var accentColor  = UIColor().HexToColor(hexString: "#989775")
        static var  DefaultaccentColor  = UIColor().HexToColor(hexString: "#989775")
        static let setBorderColor  = UIColor(red: 222/255.0, green: 225/255.0, blue: 227/255.0, alpha: 1.0)
        static var  unreadColor  = UIColor().HexToColor(hexString: "#0071F1")
        static var  linkColor  = UIColor().HexToColor(hexString: "#0000EE")
        static let reviewGradient =  [UIColor().HexToColor(hexString: "93BC4B"), UIColor().HexToColor(hexString: "9ED836")]
        
    }
    
    struct StoreData {
        static var languageArray = [Languages]()
        static var currencyArray = [Currencies]()
        static var cmsArray = [String]()
    }
    
    struct Permissiom {
        static var permissiomAlertPresented = false
    }
    
    struct RequestAction {
        
        static let Available = "Available"
        static let OutOfStock = "Out Of Stock"
        static let BackOrder = "Back Order"
        static let OnHold = "On Hold"

    }
    
}

extension UserDefaults {
    
    struct Keys {
        
        static let currencyCode = "id_currency"
        static let languageCode = "id_lang"
        static let id_customer = "id_customer"
        static let firstname = "firstname"
        static let email = "email"
        static let lastname = "lastname"
        static let id_guest = "id_guest"
        static let isLoggedIn = "isLoggedIn"
        static let isLoggedInAdmin = "isLoggedInAdmin"
        static let isGuestLoggedIn = "isGuestLoggedIn"
        static let id_cart = "id_cart"
        static let AppLanguageKey = "AppLanguage"
        static let cartCount = "nb_products"
        static let is_seller = "is_seller"
        static let id_seller = "id_seller"
        static let profile_picture_upload_url = "profile_picture_upload_url"
        static let customer_profile_image = "customer_profile_image"
        static let firebaseKey = "firebaseToken"
        static let id_admin = "id_admin"
        static let loginDict = "loginDict"
        static let dob = "dob"
        static let psgdprToken = "psgdpr_token"
        
    }
    
    class func set(value: String, key: String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    class func fetch(key: String) -> String? {
        if UserDefaults.standard.value(forKey: key) == nil {
            return nil
        } else {
            return  (UserDefaults.standard.value(forKey: key) as? String)
        }
    }
}

struct UrlParameters {
    static var width = "&width=" + String(format: "%.0f", GlobalConstants.AppSizes.SCREEN_WIDTH * UIScreen.main.scale)
    //static var id_customer = "id_customer=" + UserDefaults.fetch(key: UserDefaults.Keys.id_customer)!
    
    static func fetchCustomerId () -> String? {
        //if UserDefaults.fetch(key: UserDefaults.Keys.isLoggedIn) != nil && UserDefaults.fetch(key: UserDefaults.Keys.id_customer) != nil {
        if  UserDefaults.fetch(key: UserDefaults.Keys.id_customer) != nil {
            return String.init(format: "&\(UserDefaults.Keys.id_customer)=%@", UserDefaults.fetch(key: UserDefaults.Keys.id_customer)!)
        } else {
            return nil
        }
    }
    
    static func fetchSellerID() -> String? {
        //if UserDefaults.fetch(key: UserDefaults.Keys.isLoggedIn) != nil && UserDefaults.fetch(key: UserDefaults.Keys.id_customer) != nil {
        if  UserDefaults.fetch(key: UserDefaults.Keys.id_seller) != nil {
            return String.init(format: "&\(UserDefaults.Keys.id_seller)=%@", UserDefaults.fetch(key: UserDefaults.Keys.id_seller)!)
        } else {
            return nil
        }
    }
    
    static func fetchGuestId () -> String? {
        //if UserDefaults.fetch(key: UserDefaults.Keys.isLoggedIn) != nil && UserDefaults.fetch(key: UserDefaults.Keys.id_customer) != nil {
        if  UserDefaults.fetch(key: UserDefaults.Keys.id_guest) != nil {
            return String.init(format: "&\(UserDefaults.Keys.id_guest)=%@", UserDefaults.fetch(key: UserDefaults.Keys.id_guest)!)
        } else {
            return String.init(format: "&\(UserDefaults.Keys.id_guest)=0")
        }
    }
    
    static func fetchCartID () -> String? {
        if  UserDefaults.fetch(key: UserDefaults.Keys.id_cart) != nil {
            return String.init(format: "&\(UserDefaults.Keys.id_cart)=%@", UserDefaults.fetch(key: UserDefaults.Keys.id_cart)!)
        } else {
            return nil
        }
    }
}

struct ErrorChecking {
    
    static func getData (data: JSON?) -> JSON? {
        if let newData = data {
            if (newData["response"]["status"].stringValue == "1") {
                if newData["response"]["message"].string != nil {
                    ErrorChecking.successView(message: newData["response"]["message"].stringValue)
                }
                return newData
            } else if newData["errors"] == JSON.null  && newData["response"]["status"].string == nil {
                if newData["response"]["message"].string != nil {
                    ErrorChecking.successView(message: newData["response"]["message"].stringValue)
                }
                return newData
            } else {
                ErrorChecking.warningView(message: newData["response"]["message"].string ?? "someErrorOccur".localized)
                return JSON.null
            }
        } else {
            ErrorChecking.ErrorView(message: nil)
            return JSON.null
        }
    }
    
    static func warningView(message: String) {
        let success = MessageView.viewFromNib(layout: .messageView)
        success.configureTheme(.warning)
        success.configureDropShadow()
        //success.configureContent(title: "warning".localized, body: message, iconText: "😳")
        success.configureContent(title: "warning".localized, body: message)
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.duration = .seconds(seconds: 0.5)
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    static func successView(message: String) {
        let success = MessageView.viewFromNib(layout: .messageView)
        success.configureTheme(.success)
        success.configureDropShadow()
        success.configureContent(title: "success".localized, body: message)
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.duration = .seconds(seconds: 0.5)
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    static func ErrorView(message: String?) {
        let success = MessageView.viewFromNib(layout: .messageView)
        success.configureTheme(.error)
        success.configureDropShadow()
        success.configureContent(title: "error".localized, body: message ?? "someErrorOccur".localized)
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.duration = .seconds(seconds: 0.5)
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    static func NoInternetView() {
        let success = MessageView.viewFromNib(layout: .statusLine)
        success.configureTheme(.error)
        success.configureDropShadow()
        //success.backgroundColor = UIColor.black
        success.configureContent(body: "You must connect to Wi-fi or a Cellular Network")
        success.button?.isHidden = true
        var successConfig = SwiftMessages.defaultConfig
        successConfig.presentationStyle = .top
        successConfig.duration = .forever
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        SwiftMessages.show(config: successConfig, view: success)
    }
    
    static func isEmptyString(text: String!) -> Bool {
        if text == nil || text.count == 0 || text == "" || text.replace(string: " ", replacement: "") == "" {
            return true
        } else {
            return false
        }
    }
    
    static func checkPasswordcharLimit(limit: Int, text: String) -> Bool {
        return text.count < limit
    }
    
    static  func isValidEmail(testStr: String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return !emailTest.evaluate(with: testStr)
    }
    
    static func isStringContainDigits(text: String!) -> Bool {
        let decimalCharacters = CharacterSet.decimalDigits
        let decimalRange = text.rangeOfCharacter(from: decimalCharacters)
        if decimalRange != nil {
            return true
        } else {
            return false
        }
    }
    
    static func specialCharacters(text: String!) -> Bool {
//        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z].*+ ", options: [])
//        if regex.firstMatch(in: text, options: [], range: NSMakeRange(0, text.count)) != nil {
//            return true
//        } else {
//            return false
//        }
        do {
            let regex = try NSRegularExpression(pattern: ".*[^A-Za-z].*+ ", options: [])
            if regex.firstMatch(in: text, options: [], range: NSMakeRange(0, text.count)) != nil {
                return true
            } else {
                return false
            }
        } catch {
            return false
        }
    }
    
    static func isStringContainLetters(text: String!) -> Bool {
        let characters = CharacterSet.letters
        
        let characterRange = text.rangeOfCharacter(from: characters)
        
        if characterRange != nil {
            return true
        } else {
            return false
        }
    }
}

struct FetchDetails {
    static func fetchCustomerId () -> String? {
        if UserDefaults.fetch(key: UserDefaults.Keys.isLoggedIn) != nil && UserDefaults.fetch(key: UserDefaults.Keys.id_customer) != nil {
            return UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
        } else {
            return nil
        }
    }
    
    static func fetchSellerId () -> String? {
        if UserDefaults.fetch(key: UserDefaults.Keys.isLoggedIn) != nil && UserDefaults.fetch(key: UserDefaults.Keys.id_seller) != nil {
            return UserDefaults.fetch(key: UserDefaults.Keys.id_seller)
        } else {
            return nil
        }
    }
    
    static func fetchCartCount () -> String {
        if UserDefaults.fetch(key: UserDefaults.Keys.cartCount) != nil {
            return (UserDefaults.fetch(key: UserDefaults.Keys.cartCount) ?? "0")
        } else {
            return "0"
        }
    }
    
    static func isLoggedIn() -> Bool {
        if UserDefaults.fetch(key: UserDefaults.Keys.isLoggedIn) != nil {
            return true
        } else {
            return false
        }
    }
    
    static func isLoggedInAdmin() -> Bool {
        if UserDefaults.fetch(key: UserDefaults.Keys.isLoggedInAdmin) != nil {
            return true
        } else {
            return false
        }
    }
    
    static func isGuestLoggedIn() -> Bool {
        if UserDefaults.fetch(key: UserDefaults.Keys.isGuestLoggedIn) != nil {
            return true
        } else {
            return false
        }
    }
    
    static func is_seller() -> Bool {
        if UserDefaults.fetch(key: UserDefaults.Keys.is_seller) != nil {
            return true
        } else {
            return false
        }
    }
    
    static func fetchCustomerName() -> String {
        if UserDefaults.fetch(key: UserDefaults.Keys.firstname) != nil {
            return String.init(format: "%@ %@", UserDefaults.fetch(key: UserDefaults.Keys.firstname)!, UserDefaults.fetch(key: UserDefaults.Keys.lastname)!)
        } else {
            return  ""
        }
    }
    
    static func fetchEmail() -> String? {
        if UserDefaults.fetch(key: UserDefaults.Keys.email) != nil {
            return String.init(format: "%@ ", UserDefaults.fetch(key: UserDefaults.Keys.email )! )
        } else {
            return nil
        }
    }
}

struct WishlistContains {
    static var wishlistIds = [String]()
}

struct AddonsEnabled {
    
    static var wishlistEnable = false
    static var guestEnable = false
    static var enable_optin = false
    static var display_dob_field = true
    
    static func AddonsChecks(data: JSON) {
        
        if data["module_blockwishlist"] != JSON.null {
            if data["module_blockwishlist"].stringValue == "1" {
                AddonsEnabled.wishlistEnable = true
            } else {
                AddonsEnabled.wishlistEnable = false
            }
        }
    }
    
}
