//
//  AllController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import UIKit

enum AllController: String {
    case requestList = "requestListDataController"
    case notification = "notificationDataController"
    case home = "homeController"
    case categories
    case homeProductBlock
    case myAccount = "myAccountViewController"
    case MyPersonalDataViewController = "MyPersonalDataViewController"
    case orderHistory = "orderHistory"
    case cmsController = "cmsDataViewController"
    case cartController = "cartDataViewController"
    case languageController = "languageDataViewController"
    case currencyController = "currencyDataViewController"
    case productController = "productPageDataViewController"
    case addReview = "addReviewViewController"
    case checkoutController = "checkoutDataViewController"
    case orderPlace = "orderPlaceDataViewController"
    case addressController = "myAddress"
    case orderDetail = "orderDetailDataViewController"
    //case addAddress = "newAddressDataViewController"
    case addAddress = "AddAddressViewController"
    case myMerchandise = "myReturns"
    case myCreditSlip = "myCreditSlips"
    case myPersonalInfo = "myPersonalInfo"
    case myWishlists = "myWishlists"
    case myVouchers = "myVouchers"
    case wishlistProduct = "wishlistProductDataViewController"
    case addWishlist = "addWishlistDataViewController"
    case productCategory = "productCategoryDataViewController"
    case categoryController = "categoryController"
     case productTableCategory = "productCategoryTableViewController"
    case filterController = "filterDataViewController"
     case SearchController = "searchDataViewController"
    case signInController = "signInDataViewController"
    case signUpController = "signUpViewController"
    case zoomController = "zoomImageViewController"
    case contactUsController = "contactUsDataViewController"
    case becomeSellerController = "becomeSellerDataViewController"
    case sellerOrder = "sellerOrderViewController"
    case sellerOrderStatus = "sellerOrderChangeStatusViewController"
    case sellerCollection = ""
    case sellerProfile  = "sellerProfileDataViewController"
    case sellerDashboard = "sellerDashBoardDataViewController"
    case sellerList = "allSellerDataViewController"
    case changeDeliveryAddress
    case descriptionViewController = "descriptionViewController"
    case changeInvoiceAddress
    case guestController = "guestCheckoutViewController"
    case signoutController = "signoutDataViewController"
    case reviewViewController = "reviewsViewController"
    case chatDataController = "chatDataViewController"
    case launchController = "launchImageViewController"
    case browser
    case featureController = "fetureDataViewController"
    case chatWithSeller = "chatWithSellerViewController"
    case addMessageController = "addMessageViewController"
    case virtualProductDataViewController = "virtualProductDataViewController"
    case customizableProductViewController = "customizableProductViewController"
    case cartCustomizationViewController = "cartCustomizationViewController"
    case shopDataViewController = "shopDataViewController"
    case contactSellerDataViewController = "contactSellerDataViewController"
    case rootnav = "rootnav"
    case detect = "DetectorViewController"
}

private enum Storyboard: String {
    case main = "Main"
    case customer = "Customer"
    case product = "Product"
    case checkout = "Checkout"
     case marketplace = "Marketplace"
    case launchScreen = "LaunchScreen"
    // add enum case for each storyboard in your project
}

fileprivate extension UIStoryboard {

    static func loadFromMain(_ identifier: String) -> UIViewController {
        return load(from: .main, identifier: identifier)
    }

    static func loadFromCustomer(_ identifier: String) -> UIViewController {
        return load(from: .customer, identifier: identifier)
    }

    static func loadFromProduct(_ identifier: String) -> UIViewController {
        return load(from: .product, identifier: identifier)
    }

    static func loadFromCheckout(_ identifier: String) -> UIViewController {
        return load(from: .checkout, identifier: identifier)
    }

    static func loadFromMarketplace(_ identifier: String) -> UIViewController {
        return load(from: .marketplace, identifier: identifier)
    }
    
    static func loadFromLaunchScreen(_ identifier: String) -> UIViewController {
        return load(from: .launchScreen, identifier: identifier)
    }

    // optionally add convenience methods for other storyboards here ...

    // ... or use the main loading method directly when
    // instantiating view controller from a specific storyboard
    static func load(from storyboard: Storyboard, identifier: String) -> UIViewController {
        let uiStoryboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        return uiStoryboard.instantiateViewController(withIdentifier: identifier)
    }

}

extension UIStoryboard {
    // Main Storyboards

    class func loadRootViewController() -> UIViewController {
        return loadFromMain(AllController.rootnav.rawValue) as UIViewController
    }
    
    class func loadDetectorViewController() -> DetectorViewController? {
        return loadFromMain(AllController.detect.rawValue) as? DetectorViewController
    }
    
    class func MyPersonalDataViewController() -> MyPersonalDataViewController? {
        return loadFromCustomer(AllController.MyPersonalDataViewController.rawValue) as? MyPersonalDataViewController
    }
    
    class func loadHomeViewController() -> HomeController? {
        return loadFromMain(AllController.home.rawValue) as? HomeController

    }

    class func loadCmsViewController() -> CmsDataViewController? {
        return loadFromMain(AllController.cmsController.rawValue) as? CmsDataViewController

    }
    class func loadRequestListViewController() -> RequestListViewController? {
        return loadFromMain(AllController.requestList.rawValue) as? RequestListViewController
        
    }
    class func loadNotificationViewController() -> NotificationDataController? {
        return loadFromMain(AllController.notification.rawValue) as? NotificationDataController

    }

    class func loadLanguageViewController() -> LanguageDataViewController? {
        return loadFromMain(AllController.languageController.rawValue) as? LanguageDataViewController

    }

    class func loadCurrencyViewController() -> CurrencyDataViewController? {
        return loadFromMain(AllController.currencyController.rawValue) as? CurrencyDataViewController

    }

    class func loadCategoryViewController() -> CategoryController? {
        return loadFromMain(AllController.categoryController.rawValue) as? CategoryController

    }

    class func loadSearchViewController() -> SearchDataViewController? {
        return loadFromMain(AllController.SearchController.rawValue) as? SearchDataViewController

    }

    class func loadSignInViewController() -> SignInDataViewController? {
        return loadFromMain(AllController.signInController.rawValue) as? SignInDataViewController

    }

    class func loadSignUpViewController() -> SignUpViewController? {
        return loadFromMain(AllController.signUpController.rawValue) as? SignUpViewController

    }

    class func loadContactUsViewController() -> ContactUsDataViewController? {
        return loadFromMain(AllController.contactUsController.rawValue) as? ContactUsDataViewController

    }

    class func loadGuestViewController() -> GuestCheckoutViewController? {
        return loadFromMain(AllController.guestController.rawValue) as? GuestCheckoutViewController

    }

    // Customer Storyboards

    class func loadMyAccountViewController() -> MyAccountViewController? {
        return loadFromCustomer(AllController.myAccount.rawValue) as? MyAccountViewController

    }

    class func loadOrderHistoryViewController() -> OrderHistoryDataViewController? {
        return loadFromCustomer(AllController.orderHistory.rawValue) as? OrderHistoryDataViewController

    }

    class func loadOrderdetailViewController() -> OrderDetailDataViewController? {
        return loadFromCustomer(AllController.orderDetail.rawValue) as? OrderDetailDataViewController

    }

//    class func loadAddAddressViewController() -> NewAddressDataViewController? {
//        return loadFromCustomer(AllController.addAddress.rawValue) as? NewAddressDataViewController
//
//    }
    
    class func loadAddAddressViewController() -> AddAddressViewController? {
        return loadFromCustomer(AllController.addAddress.rawValue) as? AddAddressViewController
        
    }

    class func loadAddresslViewController() -> AddressDataViewController? {
        return loadFromCustomer(AllController.addressController.rawValue) as? AddressDataViewController

    }

    class func loadMyCreditViewController() -> MyCreditDataViewController? {
        return loadFromCustomer(AllController.myCreditSlip.rawValue) as? MyCreditDataViewController

    }

    class func loadMyMerchandiseViewController() -> MyMerchandiseDataController? {
        return loadFromCustomer(AllController.myMerchandise.rawValue) as? MyMerchandiseDataController

    }

    class func loadMyPersonalInfoViewController() -> MyPersonalInformationData? {
        return loadFromCustomer(AllController.myPersonalInfo.rawValue) as? MyPersonalInformationData

    }

    class func loadMyWishlistViewController() -> MyWishlistDataViewController? {
        return loadFromCustomer(AllController.myWishlists.rawValue) as? MyWishlistDataViewController

    }

    class func loadMyVoucherInfoViewController() -> MyVoucherDataViewController? {
        return loadFromCustomer(AllController.myVouchers.rawValue) as? MyVoucherDataViewController

    }

    class func loadWishlistProductViewController() -> WishlistProductDataViewController? {
        return loadFromCustomer(AllController.wishlistProduct.rawValue) as? WishlistProductDataViewController

    }

    class func loadAddWishlistProductViewController() -> AddWishlistDataViewController? {
        return loadFromCustomer(AllController.addWishlist.rawValue) as? AddWishlistDataViewController

    }
    
    class func loadAddMessageViewController() -> AddMessageViewController? {
        return loadFromCustomer(AllController.addMessageController.rawValue) as? AddMessageViewController
        
    }

    // Product Storyboards

    class func loadProductViewController() -> ProductPageDataViewController? {
        return loadFromProduct(AllController.productController.rawValue) as? ProductPageDataViewController

    }

    class func loadAddReviewViewController() -> AddReviewViewController? {
        return loadFromProduct(AllController.addReview.rawValue) as? AddReviewViewController

    }

    class func loadProductCategoryViewController() -> ProductCategoryDataViewController? {
        return loadFromProduct(AllController.productCategory.rawValue) as? ProductCategoryDataViewController

    }

    class func loadFilterViewController() -> FilterDataViewController? {
        return loadFromProduct(AllController.filterController.rawValue) as? FilterDataViewController

    }
    class func loadZoomViewController() -> ZoomImageViewController? {
        return loadFromProduct(AllController.zoomController.rawValue) as? ZoomImageViewController

    }

    class func loadDescriptionViewController() -> DescriptionViewController? {
        return loadFromProduct(AllController.descriptionViewController.rawValue) as? DescriptionViewController

    }

    class func loadSignOutViewController() -> SignoutDataViewController? {
        return loadFromProduct(AllController.signoutController.rawValue) as? SignoutDataViewController

    }
    
    class func loadReviewViewViewController() -> ReviewsViewController? {
        return loadFromProduct(AllController.reviewViewController.rawValue) as? ReviewsViewController
        
    }
    
    class func loadFeatureViewController() -> FetureDataViewController? {
        return loadFromProduct(AllController.featureController.rawValue) as? FetureDataViewController
        
    }
    
    class func loadVirtualProductViewController() -> VirtualProductDataViewController? {
        return loadFromProduct(AllController.virtualProductDataViewController.rawValue) as? VirtualProductDataViewController
        
    }
    
    class func loadCustomizableProductViewController() -> CustomizableProductViewController? {
        return loadFromProduct(AllController.customizableProductViewController.rawValue) as? CustomizableProductViewController
        
    }
    class func loadCartCustomizationViewController() -> CartCustomizationViewController? {
        return loadFromProduct(AllController.cartCustomizationViewController.rawValue) as? CartCustomizationViewController
        
    }

    // Checkout Storyboards
    class func loadCheckoutViewController() -> CheckoutDataViewController? {
        return loadFromCheckout(AllController.checkoutController.rawValue) as? CheckoutDataViewController

    }

    class func loadOrderPlaceViewController() -> OrderPlaceDataViewController? {
        return loadFromCheckout(AllController.orderPlace.rawValue) as? OrderPlaceDataViewController

    }

    // Marketplace Storyboards

    class func loadBecomeSellerViewController() -> BecomeSellerDataViewController? {
        return loadFromMarketplace(AllController.becomeSellerController.rawValue) as? BecomeSellerDataViewController

    }

    class func loadSellerStatusViewController() -> SellerOrderChangeStatusViewController? {
        return loadFromMarketplace(AllController.sellerOrderStatus.rawValue) as? SellerOrderChangeStatusViewController

    }

    class func loadSellerProfileViewController() -> SellerProfileDataViewController? {
        return loadFromMarketplace(AllController.sellerProfile.rawValue) as? SellerProfileDataViewController

    }

    class func loadSellerDashboardViewController() -> SellerDashBoardDataViewController? {
        return loadFromMarketplace(AllController.sellerDashboard.rawValue) as? SellerDashBoardDataViewController

    }

    class func loadSellerListViewController() -> AllSellerDataViewController? {
        return loadFromMarketplace(AllController.sellerList.rawValue) as? AllSellerDataViewController

    }
    
    class func loadChatDataViewController() -> ChatDataViewController? {
        return loadFromMarketplace(AllController.chatDataController.rawValue) as? ChatDataViewController
        
    }
    
    class func loadChatWithSellerViewController() -> ChatWithSellerViewController? {
        return loadFromMarketplace(AllController.chatWithSeller.rawValue) as? ChatWithSellerViewController
        
    }
    
    class func loadShopDataViewController() -> ShopDataViewController? {
         return loadFromMarketplace(AllController.shopDataViewController.rawValue) as? ShopDataViewController
    }
    
    class func loadContactSellerDataViewController() -> ContactSellerDataViewController? {
        return loadFromMarketplace(AllController.contactSellerDataViewController.rawValue) as? ContactSellerDataViewController
    }
    
    // Launch Screen
    
    class func loadLaunchDataViewController() -> LaunchImageViewController? {
        return loadFromLaunchScreen(AllController.launchController.rawValue) as? LaunchImageViewController
        
    }

}

enum CmsControllers: String {
    case aboutUs = "aboutUs"
    case termsCondition = "termsCondition"
    case support = "support"
    case privacyPolicy = "privacyPolicy"
}

enum WhichApiCall {
    case none
    case firebasetoken
    case getRequestList
    case Cart
    case BuyNow
    case updateCart
    case deleteCart
    case saveAddress
    case editAddress
    case checkCustomerPassword
     case customerExists
     case updateAccountDetails
    case saveWishlist
    case moveWishlist
    case ViewAllProductBlock
    case deleteWishlist
    case getWishlists
    case defaultWishlist
    case addToWishlist
    case applyFilter
    case applySort
    case getSearchSuggestion
    case searchPressed
    case uploadImage
    case wishlistClicked
    case shareClicked
    case HomeHeaderClicked
    case forgotPassword
    case submitContactUs
    case ViewAll
    case createSeller
    case updateCustomerCart
    case terms
    case applyVoucher
    case deleteVoucher
    case reOrder
    case guestLogin
    case social
    case login
    case next
    case previous
    case diffrentBilling
    case loadMoreProduct
    case pagination
    case deleteaddress
    case getAddressFields
    case productAvailabilityRequired
    case checkAvailability
    
    case requestCount
    case requestAction
}

enum CheckoutApiCall {
    case Checkout
    case getAddress
    case orderReview
    case updateShippingMethod
    case updatePaymentMethod
    case orderPlace
    case updateCartAddress
    case saveAddress
    case getShpppingMethod
    case getPaymentMethods
}
