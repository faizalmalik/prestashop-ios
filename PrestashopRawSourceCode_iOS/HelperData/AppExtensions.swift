//
//  AppExtensions.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import UIKit
import Kingfisher
import AudioToolbox


extension UIViewController {
    
    func showAlert(title : String, message : String, completion: @escaping ()->()){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { (action) in
            completion()
        }
        alertController.addAction(action)
        
       self.present(alertController, animated: true, completion: nil)
        
    }
}

//Removing emoji in string

extension Character {
    fileprivate func isEmoji() -> Bool {
        return Character(UnicodeScalar(UInt32(0x1d000))!) <= self && self <= Character(UnicodeScalar(UInt32(0x1f77f))!)
            || Character(UnicodeScalar(UInt32(0x2100))!) <= self && self <= Character(UnicodeScalar(UInt32(0x26ff))!)
    }
}

extension String {
    func stringByRemovingEmoji() -> String {
        return String(self.filter { !$0.isEmoji() })
    }
}

// Removing white space in string

extension String {
    func replace(string: String, replacement: String) -> String {
        return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
    }

    func removeWhitespace() -> String {
        return self.replace(string: " ", replacement: "%20")
    }
}

// localization extension

let AppLanguageKey = "AppLanguage"
let AppLanguageDefaultValue = "en"

var appLanguage: String {

    get {
        if let language = UserDefaults.standard.string(forKey: AppLanguageKey) {
            return language
        } else {
            UserDefaults.standard.setValue(AppLanguageDefaultValue, forKey: AppLanguageKey)
            return AppLanguageDefaultValue
        }
    }

    set(value) {
        UserDefaults.standard.setValue((value), forKey: AppLanguageKey)
    }

}

extension String {

    var localized: String {
        return localized(lang: appLanguage)
    }

    var localizeStringUsingSystemLang: String {
        return NSLocalizedString(self, comment: "")
    }

    func localized(lang: String?) -> String {

        if let lang = lang {
            if let path = Bundle.main.path(forResource: lang, ofType: "lproj") {
                let bundle = Bundle(path: path)
                return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
            }

        }

        return localizeStringUsingSystemLang
    }
}

extension UINavigationBar {
    func navigationChanges() {
        self.barTintColor = GlobalConstants.Colors.primaryColor
        self.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.tintColor = UIColor.white
        UINavigationBar.appearance().tintColor = UIColor.white
        UIBarButtonItem.appearance().tintColor = UIColor.white
    }
}

// color hexadecimal extension

extension UIColor {
    func HexToColor(hexString: String, alpha: CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }

    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
}

extension UIImageView {

    func setImage(imageUrl: String ) {
        ImageCache.default.maxDiskCacheSize = 30 * 1024 * 1024
        let Url = (imageUrl.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? imageUrl).replace(string: "%2520", replacement: "%20")
        if URL(string: Url) != nil && Url.count > 0 {
            let resource = ImageResource(downloadURL: URL(string: Url)!, cacheKey: Url)
            self.contentMode = .scaleAspectFit
            self.kf.setImage(with: resource, placeholder: UIImage(named: "placeholder"), options: [.transition(.fade(0.2))], progressBlock: nil, completionHandler: nil)
        } else {
            self.image = UIImage(named: "placeholder")
        }
    }
}

extension UILabel {
    func halfTextColorChange (fullText: String, changeText: String ) {
        let strNumber: NSString = fullText as NSString
        let range = (strNumber).range(of: changeText)
        let attribute = NSMutableAttributedString.init(string: fullText)
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: GlobalConstants.Colors.defaultTextColor, range: range)
        self.attributedText = attribute
    }

    func strike(text: String) {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: text)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        self.attributedText = attributeString
    }

    func SetDefaultTextColor() {
        self.textColor = GlobalConstants.Colors.defaultTextColor
    }
    func SetAccentTextColor() {
        self.textColor = GlobalConstants.Colors.accentColor
    }

}

extension UIView {
    func SetAccentBackColor() {
        self.backgroundColor = GlobalConstants.Colors.accentColor
    }
}

extension String {

    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func heightOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }
}

extension Bundle {

    static func loadView<T>(fromNib name: String, withType type: T.Type) -> T {
        if let view = Bundle.main.loadNibNamed(name, owner: nil, options: nil)?.first as? T {
            return view
        }

        fatalError("Could not load view with type " + String(describing: type))
    }
}

extension String {

    func FetchApiName() -> String {
        if  self.contains("mpmobikul/") {
            var newString = self.replace(string: "mpmobikul/", replacement: "")
            newString = newString.replace(string: "?", replacement: "")
            return newString
        }
        else if self.contains("mobikul/") {
            var newString = self.replace(string: "mobikul/", replacement: "")
            newString = newString.replace(string: "?", replacement: "")
            return newString
        }
        else {
            var newString = self.replace(string: "mpsshop/", replacement: "")
            newString = newString.replace(string: "?", replacement: "")
            return newString
        }
    }

}

extension UIView {
    func setViewMainColor() {
        self.backgroundColor = GlobalConstants.Colors.primaryColor
    }

        func shadowBorder() {
            self.layer.shadowColor = UIColor.lightGray.cgColor
            self.layer.shadowOpacity = 2
            self.layer.shadowOffset = CGSize.zero

            self.layer.cornerRadius = 5

        }

    func normalBorder() {
        self.layer.borderColor = UIColor().HexToColor(hexString: "#CFD8DC").cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5

    }

    func textBorder() {
        let borderColor: UIColor? = UIColor(red: 204.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
        self.layer.borderColor = borderColor?.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
    }

    func normalBorder1() {
        self.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5

    }

    func normalBorder2() {
        self.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        self.layer.borderWidth = 1

    }
    func applyBorder(colours: UIColor) {
        self.layer.borderColor = colours.cgColor
        self.layer.borderWidth = 1.0
    }

    func setCornerRadius() {
        self.clipsToBounds = true
        self.layer.cornerRadius = 5
    }
}

extension UITabBarController {

    func setCartCount (value: String) {

        if  value != "0" && !value.isEmpty {

            self.tabBar.items![3].badgeValue = value
        } else {
            self.tabBar.items![3].badgeValue = nil
        }

    }

}

//Device Identification
//struct Device {
//    // iDevice detection code
//    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
//    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
//    static let IS_RETINA           = UIScreen.main.scale >= 2.0
//
//    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
//    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
//    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
//    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
//
//    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
//    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
//    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
//    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
//    static let IS_IPHONE_XSERIES   = IS_IPHONE && SCREEN_MAX_LENGTH > 736
//    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH > 1125
//    static let IS_IPHONE_XR        = IS_IPHONE && SCREEN_MAX_LENGTH > 828
//    static let IS_IPHONE_XS        = IS_IPHONE && SCREEN_MAX_LENGTH > 1125
//    static let IS_IPHONE_XS_MAX    = IS_IPHONE && SCREEN_MAX_LENGTH > 1242
//}
//
//extension UIImageView {
//    func FetchLaunchImage() -> UIImage? {
//        if Device.IS_IPHONE_4_OR_LESS {
//            return UIImage(named: "LaunchImage-700@2x.png")
//        } else if Device.IS_IPHONE_5 {
//            return UIImage(named: "LaunchImage-700-568h@2x.png")
//        } else if Device.IS_IPHONE_6 {
//            return UIImage(named: "LaunchImage-800-667h@2x.png")
//        } else if Device.IS_IPHONE_6P {
//            return UIImage(named: "LaunchImage-800-Portrait-736h@3x.png")
//        } else if Device.IS_IPHONE_XSERIES {
//            return UIImage(named: "LaunchImage-1100-Portrait-2436h@3x.png")
//        } else if Device.IS_IPAD {
//            if (UIScreen.main.scale == 1) {
//                return UIImage(named: "LaunchImage-700-Portrait~ipad.png")
//            } else if (UIScreen.main.scale == 2) {
//                return UIImage(named: "LaunchImage-700-Portrait@2x~ipad.png")
//            }
//        }
//        return nil
//    }
//}

// RTL

let APPLE_LANGUAGE_KEY = "AppleLanguages"

/// L102Language

class L102Language {

    /// get current Apple language

    class func currentAppleLanguage() -> String {
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as? NSArray
        let current = langArray?.firstObject as? String ?? "en"
        return current
    }

    /// set @lang to be the first in Applelanguages list

    class func setAppleLAnguageTo(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set([lang, currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
}

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
//        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate) // For vibration
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func RemoveLastEnter() -> String {
        var str = self
        while str.last == "\n" {
            str = String(str.dropLast())
        }
        print(str)
        return str
    }
}

extension String {
    func emojiToImage() -> UIImage? {
        let size = CGSize(width: 30, height: 35)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.white.set()
        let rect = CGRect(origin: CGPoint(), size: size)
        UIRectFill(CGRect(origin: CGPoint(), size: size))
        (self as NSString).draw(in: rect, withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 30)])
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

extension UILabel {
    
    var substituteFontName: String {
        get { return self.font.fontName }
        set {
            if self.font.fontName.range(of: "Medium") == nil {
                self.font = UIFont(name: newValue, size: self.font.pointSize)
            }
        }
    }
    
    var substituteFontNameBold: String {
        get { return self.font.fontName }
        set {
            if self.font.fontName.range(of: "Medium") != nil {
                self.font = UIFont(name: newValue, size: self.font.pointSize)
            }
        }
    }
}

extension UIView {
    func applyGradient(colours: [UIColor]) {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        gradient.startPoint = CGPoint(x: 0.2, y: 1)
        gradient.endPoint = CGPoint(x: 0.8, y: 0)
        self.layer.addSublayer(gradient)
    }
    
    func applyWithAlphaGradient(colours: [UIColor]) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.withAlphaComponent(0.8).cgColor }
        gradient.startPoint = CGPoint(x: 0.2, y: 1)
        gradient.endPoint = CGPoint(x: 0.8, y: 0)
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradientAccordingToScreenWidth(colours: [UIColor]) {
        if let layers = self.layer.sublayers {
            for layer in layers where layer.name == "masklayer" {
                layer.removeFromSuperlayer()
            }
        }
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.name = "masklayer"
        gradient.frame.size.width = GlobalConstants.AppSizes.SCREEN_WIDTH
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradientToZeroLayer(colours: [UIColor]) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        self.layer.insertSublayer(gradient, at: 0)
    }
}

extension UINavigationBar {
    /// Applies a background gradient with the given colors
    func applyNavigationGradient( colors: [UIColor]) {
        var frameAndStatusBar: CGRect = self.bounds
        frameAndStatusBar.size.height += 20 // add 20 to account for the status bar
        
        setBackgroundImage(UINavigationBar.gradient(size: frameAndStatusBar.size, colors: colors), for: .default)
    }
    
    /// Creates a gradient image with the given settings
    static func gradient(size: CGSize, colors: [UIColor]) -> UIImage? {
        // Turn the colors into CGColors
        let cgcolors = colors.map { $0.cgColor }
        
        // Begin the graphics context
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        
        // If no context was retrieved, then it failed
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        // From now on, the context gets ended if any return happens
        defer { UIGraphicsEndImageContext() }
        
        // Create the Coregraphics gradient
        var locations: [CGFloat] = [0.0, 1.0]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: cgcolors as NSArray as CFArray, locations: &locations) else { return nil }
        
        // Draw the gradient
        context.drawLinearGradient(gradient, start: CGPoint(x: 0.0, y: 0.0), end: CGPoint(x: size.width, y: 0.0), options: [])
        
        // Generate the image (the defer takes care of closing the context)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension CAShapeLayer {
    func drawCircleAtLocation(location: CGPoint, withXRadius xradius: CGFloat, withYRadius yradius: CGFloat, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        let origin = CGPoint(x: location.x - xradius, y: location.y - yradius)
        path = UIBezierPath(ovalIn: CGRect(origin: origin, size: CGSize(width: xradius * 2, height: yradius * 2))).cgPath
    }
}

private var handle: UInt8 = 0

extension UIBarButtonItem {
    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }
    
    func addBadge(number: Int, withOffset offset: CGPoint = CGPoint.zero, andColor color: UIColor = UIColor.red, andFilled filled: Bool = true) {
        guard let view = self.value(forKey: "view") as? UIView else { return }
        
        badgeLayer?.removeFromSuperlayer()
        
        // Initialize Badge
        
        let badge = CAShapeLayer()
        let radius = CGFloat(7)
        let stringWidth = "\(number)".widthOfString(usingFont: UIFont.systemFont(ofSize: 11)) > radius ? "\(number)".widthOfString(usingFont: UIFont.systemFont(ofSize: 11)):radius
        let location = CGPoint(x: view.frame.width - (radius + offset.x), y: (radius + offset.y))
        if stringWidth > radius {
            badge.drawCircleAtLocation(location: location, withXRadius: stringWidth-4, withYRadius: radius, andColor: color, filled: filled)
        } else {
            badge.drawCircleAtLocation(location: location, withXRadius: radius, withYRadius: radius, andColor: color, filled: filled)
        }
        view.layer.addSublayer(badge)
        
        // Initialiaze Badge's label
        let label = CATextLayer()
        label.string = "\(number)"
        label.alignmentMode = CATextLayerAlignmentMode.center
        label.fontSize = 11
        if stringWidth > radius {
            label.frame = CGRect(origin: CGPoint(x: (location.x - 9), y: offset.y), size: CGSize(width: stringWidth, height: 16))
        } else {
            label.frame = CGRect(origin: CGPoint(x: (location.x - 4), y: offset.y), size: CGSize(width: stringWidth, height: 16))
        }
        label.foregroundColor = filled ? UIColor.white.cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)
        
        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func updateBadge(number: Int) {
        if let text = badgeLayer?.sublayers?.filter({ $0 is CATextLayer }).first as? CATextLayer {
            text.string = "\(number)"
        }
    }
    
    func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
}

struct Device {
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_XSERIES   = IS_IPHONE && SCREEN_MAX_LENGTH > 736
    
    static func FetchLaunchImage() -> UIImage? {
        if Device.IS_IPHONE_4_OR_LESS {
            return UIImage(named: "LaunchImage-700@2x.png")
        } else if Device.IS_IPHONE_5 {
            return UIImage(named: "LaunchImage-700-568h@2x.png")
        } else if Device.IS_IPHONE_6 {
            return UIImage(named: "LaunchImage-800-667h@2x.png")
        } else if Device.IS_IPHONE_6P {
            return UIImage(named: "LaunchImage-800-Portrait-736h@3x.png")
        } else if Device.IS_IPHONE_XSERIES {
            return UIImage(named: "LaunchImage-1100-Portrait-2436h@3x.png")
        } else if Device.IS_IPAD {
            if (UIScreen.main.scale == 1) {
                return UIImage(named: "LaunchImage-700-Portrait~ipad.png")
            } else if (UIScreen.main.scale == 2) {
                return UIImage(named: "LaunchImage-700-Portrait@2x~ipad.png")
            }
        }
        return nil
    }    
}
