//
//  NetworkManager.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import Alamofire
import SwiftyJSON
import SVProgressHUD
import UIKit

typealias ServiceResponse = (JSON?, Error?) -> Void

enum RequestType: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

class NetworkManager: NSObject, EmptyDelegate {
   static let baseURLString: String = GlobalConstants.AppCredentials.BASEURL
    //    var baseURLWithAPIKeyString: String = ""
    static var request: URLRequest?

    class  func fetchData(params: String, verb: RequestType, dict: [String: Any], controller: AllController, view: UIView, _ completion: @escaping ServiceResponse) {
        if controller != AllController.launchController {
            SVProgressHUD.show(withStatus: "loading".localized)
           
        }
        
        var baseURLWithAPIKeyString = "\(NetworkManager.baseURLString)\(params)"

        if(UserDefaults.standard.value(forKey: "id_lang") != nil) {
            baseURLWithAPIKeyString += "&id_lang="
            baseURLWithAPIKeyString +=  UserDefaults.fetch(key: UserDefaults.Keys.languageCode) ?? ""
        } else {
            baseURLWithAPIKeyString += "&id_lang=" + "1"

        }
        if let code = UserDefaults.standard.string(forKey: UserDefaults.Keys.currencyCode) {
            baseURLWithAPIKeyString += "&id_currency=" +  code
        } else {
            baseURLWithAPIKeyString += "&id_currency=" + "1"
        }
        baseURLWithAPIKeyString += "&ws_key=" + GlobalConstants.AppCredentials.WS_KEY

        if ( baseURLWithAPIKeyString.range(of: "/mpsshop") != nil || baseURLWithAPIKeyString.range(of: "/mobikul") != nil   || baseURLWithAPIKeyString.range(of: "/mpmobikul") != nil) {
            baseURLWithAPIKeyString += "&outputformat=json"

        } else {
            baseURLWithAPIKeyString += "&output_format=JSON"
        }
        if FetchDetails.isLoggedIn(), let id_customer = UrlParameters.fetchCustomerId() {
            baseURLWithAPIKeyString += id_customer
        }
        if FetchDetails.isGuestLoggedIn(), let id_customer = UrlParameters.fetchGuestId() {
            baseURLWithAPIKeyString += id_customer            
        }
        
        baseURLWithAPIKeyString = baseURLWithAPIKeyString.removeWhitespace()
        baseURLWithAPIKeyString = baseURLWithAPIKeyString.stringByRemovingEmoji()
        baseURLWithAPIKeyString = baseURLWithAPIKeyString.removeWhitespace()
        baseURLWithAPIKeyString = baseURLWithAPIKeyString.stringByRemovingEmoji()
        baseURLWithAPIKeyString = baseURLWithAPIKeyString.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        let requestURL: String = baseURLWithAPIKeyString
        print(requestURL)
        var request1 = URLRequest(url: URL(string: requestURL)!)
        request1.httpMethod = verb.rawValue
        request1.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        request1.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request1.addValue("application/json", forHTTPHeaderField: "Accept")
        if verb == .post  ||  verb == .put {

            if baseURLWithAPIKeyString.range(of: "/mpsshop") != nil || baseURLWithAPIKeyString.range(of: "/mobikul") != nil  || baseURLWithAPIKeyString.range(of: "/mpmobikul") != nil {
                var newDict = dict
                newDict["json"] = "1"
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: newDict, options: .prettyPrinted)
                    request1.httpBody = jsonData
                    print(verb.rawValue)
                    let jsonSortString: String = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
                    print(jsonSortString)
                } catch {
                    print("json not converted")
                }
            } else {
                let dict = ["": dict ] as [String: Any]
                let xmlWriter1 = XMLWriter()

                var xmlString = xmlWriter1.convertDictionary(toXML: dict, withStartElement: "prestashop", isFirstElement: true)
                xmlString = xmlString?.replacingOccurrences(of: "<>", with: "", options: .literal, range: nil)
                xmlString = xmlString?.replacingOccurrences(of: "</>", with: "", options: .literal, range: nil)
                print(xmlString!)
                request1.httpBody = xmlString?.data(using: String.Encoding.utf8)
            }
        }
        
        NetworkManager.requestForData(request: request1) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                 NetworkManager.request = request1
                print("Error logging you in", error!._code)
                if controller != AllController.launchController {
                  SVProgressHUD.dismiss()
                }
                  completion( nil, error)

            } else {
                if controller != AllController.launchController {
                    SVProgressHUD.dismiss()
                }
                completion(responseObject, nil)
            }
        }

    }
    
   class func requestForData (request: URLRequest, _ completion: @escaping ServiceResponse) {
    
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(request).responseJSON { (response) in
           
            switch response.result {
            case .success:
                let jsonData = JSON(response.data as Any)
                print(jsonData)
                completion(jsonData, nil)

            case .failure(let error):
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)")
                }
                completion( nil, error as NSError)
            }
        }
    
    }
    
    class  func fetchGetData(params: String, _ completion: @escaping ServiceResponse) {
        var baseURLWithAPIKeyString = params
        baseURLWithAPIKeyString = baseURLWithAPIKeyString.removeWhitespace()
        baseURLWithAPIKeyString = baseURLWithAPIKeyString.stringByRemovingEmoji()
        baseURLWithAPIKeyString = baseURLWithAPIKeyString.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
        let requestURL: String = baseURLWithAPIKeyString
        print(requestURL)
        var request1 = URLRequest(url: URL(string: requestURL)!)
        request1.httpMethod = "GET"
        request1.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        request1.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request1.addValue("application/json", forHTTPHeaderField: "Accept")
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        manager.request(request1).responseJSON { (response) in
            
            switch response.result {
            case .success:
                let jsonData = JSON(response.data as Any)
                print(jsonData)
                completion(jsonData, nil)
                
            case .failure(let error):
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)")
                }
                completion( nil, error as NSError)
            }
        }
    }

     static var delegate: EmptyDelegate?
    
    class func setView(view: UIView, internetError: Int) {
        for  subView in  view.subviews {
            subView.isHidden = true
        }
        
        var emptyView: EmptyView!
      
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = view.bounds

            view.addSubview(emptyView!)
        }
         emptyView.button.setTitle("tryAgain".localized, for: .normal)
        emptyView.delegate = NetworkManager()
        switch internetError {
        case 1:
            emptyView?.labelText.text = "internetConnection".localized
            emptyView?.image.image = #imageLiteral(resourceName: "nointernet")
            emptyView.button.isHidden = false
        case 2:
            emptyView?.labelText.text = "working".localized
            emptyView?.image.image = #imageLiteral(resourceName: "nointernet")
            emptyView.button.isHidden = true
            
        default:
            emptyView?.labelText.text = "someErorr".localized
            emptyView?.image.image = #imageLiteral(resourceName: "nointernet")
            emptyView.button.isHidden = false
        }
       
    }
    
    class  func fetchDataWithFormData(url: String, params: String, verb: RequestType, dict: [String: Any], controller: AllController, view: UIView, _ completion: @escaping ServiceResponse) {
        SVProgressHUD.show(withStatus: "loading".localized)
        var fileUrl = url +  "&outputformat=json"
        fileUrl = fileUrl.replace(string: "%2F", replacement: "/")
        fileUrl = fileUrl.replace(string: "%3A", replacement: ":")
        print(fileUrl)
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in dict {
                if let check = value as? String {
                    print(key, check)
                    multipartFormData.append(check.data(using: String.Encoding.utf8)!, withName: key)
                }
            }
        }, usingThreshold: UInt64.init(), to: fileUrl, method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    SVProgressHUD.dismiss()
                    let jsonData = JSON(response.data as Any)
                    print(jsonData)
                    completion(jsonData, nil)
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                print(error.localizedDescription)
                completion(nil, error)
            }
        }        
    }
}

extension NetworkManager {
    func buttonPressed() {
      print("hjekdfhs")
    }
}
