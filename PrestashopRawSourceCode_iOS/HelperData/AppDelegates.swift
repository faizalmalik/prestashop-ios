//
//  AppDelegates.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

protocol PassData: class {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall)
}

protocol MoveController: class {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController)
}

protocol test: class {
    func moveController2(tuple: TupleVar)
}

typealias TupleVar = (String, String, JSON, Int, AllController)

protocol PassCheckoutData: class {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: CheckoutApiCall)
}

protocol ChangeAddress: class {
    func addressChanged(addressId: String, controller: AllController)
}

protocol PassJsonData: class {
    func passJsonData(data: JSON) 
}
protocol gdprSwitchProtocol: class {
    func gdprAct()
}

protocol downloadGdprFileAct: class {
    func downloadfile(type: String)
}
