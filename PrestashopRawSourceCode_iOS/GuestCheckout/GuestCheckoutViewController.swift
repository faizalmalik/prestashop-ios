//
//  GuestCheckoutViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import Pastel

import ActionSheetPicker_3_0

class GuestCheckoutViewController: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var newsletterLabel: UILabel!
    @IBOutlet weak var specialOfferLabel: UILabel!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var offerSwitch: UISwitch!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var dobField: UITextField!
    
    var signUpDict = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationController?.navigationBar.navigationChanges()
        self.dobField.delegate = self
        //dobField.addTarget(self, action: #selector(myTargetFunction(_:)), for: UIControl.Event.touchDown)

        if !AddonsEnabled.enable_optin {
            specialOfferLabel.isHidden = true
            offerSwitch.isHidden = true
        }
        
        if let firstName =  UserDefaults.fetch(key: UserDefaults.Keys.firstname) {
            self.firstNameField.text = firstName
        }
        
        if let lastname =  UserDefaults.fetch(key: UserDefaults.Keys.lastname) {
            self.lastNameField.text = lastname
        }
        
        if let email =  UserDefaults.fetch(key: UserDefaults.Keys.email) {
            self.emailField.text = email
        }
        
        if let dob =  UserDefaults.fetch(key: UserDefaults.Keys.dob) {
            self.dobField.text = dob
        }
        
        self.stringsLocalized()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        view.backgroundColor = UIColor.clear
        mainView.backgroundColor = UIColor.clear
    }
    
    override func viewWillLayoutSubviews() {
        
        let pastelView = PastelView(frame: view.bounds)
        // Custom Direction
        //         pastelView.startPastelPoint = .topLeft
        //        pastelView.endPastelPoint = .bottomRight
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        // Custom Duration
        pastelView.animationDuration = 2.0
        // Custom Color
        pastelView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
    }
    
    @IBAction func backPress(_ sender: Any) {
        if FetchDetails.isGuestLoggedIn() {
            UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.isGuestLoggedIn)
            UserDefaults.standard.synchronize()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signUpClicked(_ sender: Any) {
        let someOptional: Bool = true
        switch someOptional {
        case ErrorChecking.isEmptyString(text: firstNameField.text):
            ErrorChecking.warningView(message: "firstNameValidate".localized)
            firstNameField.shake()
        case ErrorChecking.isEmptyString(text: lastNameField.text):
            ErrorChecking.warningView(message: "lastNameValidate".localized)
            lastNameField.shake()
        case ErrorChecking.isEmptyString(text: emailField.text):
            ErrorChecking.warningView(message: "emailEmptyValidate".localized)
            emailField.shake()
        case ErrorChecking.isValidEmail(testStr: emailField.text!):
            ErrorChecking.warningView(message: "emailValidate".localized)
            emailField.shake()
        default:
            print()
            signUpDict["id_gender"] = "1"
            signUpDict["active"] = "1"
            signUpDict["id_default_group"] = "2"
            signUpDict["firstname"] = firstNameField.text
            signUpDict["lastname"] = lastNameField.text
            signUpDict["email"] = emailField.text
            signUpDict["passwd"] = "test123"
            signUpDict["is_guest"] = "1"
            signUpDict["id_guest"] = UserDefaults.standard.string(forKey: UserDefaults.Keys.id_guest)
            signUpDict["optin"] = "0"
            signUpDict["id"] = UserDefaults.fetch(key: "id_customer")
            
            self.makeRequest(call: WhichApiCall.guestLogin)
        }
    }
    
    func makeRequest(call: WhichApiCall) {
        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = .get
        
        switch call {
        case .updateCart:
            //http://suraj2kumar.com/d6/ps1731/api/mobikul/updatecartcustomer?&id_cart=606&id_customer=146&id_lang=1&id_currency=1&ws_key=suraj2kumarattheratewebkuldotcom&outputformat=json&id_customer=146
            params =  GlobalConstants.ApiNames.updateCustomerCart
            params += UrlParameters.fetchCartID() ?? ""
            params += UrlParameters.fetchGuestId() ?? ""
            params += UrlParameters.fetchCustomerId() ?? ""
        case .guestLogin:
            params += GlobalConstants.ApiNames.savecustomer
            if let id = UserDefaults.fetch(key: "id_customer") {
                params += id
            }
            newDict[GlobalConstants.ApiNames.savecustomer.FetchApiName()] = signUpDict
            if UserDefaults.fetch(key: "id_customer") != nil {
                verbs = .put
            } else {
                verbs = .post
            }
        default:
            var emailText = ""
            if let email =  UserDefaults.fetch(key: UserDefaults.Keys.email) {
                emailText = email
            }
            if UserDefaults.fetch(key: "id_customer") != nil && emailText == self.emailField.text {
                self.makeRequest(call: WhichApiCall.guestLogin)
            } else {
                params =  GlobalConstants.ApiNames.customerExists
                params += "&email=" + emailField.text!
                verbs = .get
            }
        }
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.home, view: self.view) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                print("loginError".localized, error!)
            } else {
                switch call {
                case .updateCart:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.navigationController?.view.backgroundColor = GlobalConstants.Colors.primaryColor
                        self.navigationController?.navigationBar.isTranslucent = false
                        self.navigationController?.navigationBar.navigationChanges()
                        if let view = UIStoryboard.loadCheckoutViewController() {
                            self.navigationController?.pushViewController(view, animated: true)
                        }
                    }
                case .guestLogin:
                    if let data = responseObject?["response"], data != JSON.null, data["status"].stringValue == "1" {
                        UserDefaults.set(value: self.firstNameField.text!, key: UserDefaults.Keys.firstname)
                        
                        UserDefaults.set(value: self.lastNameField.text!, key: UserDefaults.Keys.lastname)
                        
                        UserDefaults.set(value: self.emailField.text!, key: UserDefaults.Keys.email)
                        UserDefaults.set(value: self.dobField.text!, key: UserDefaults.Keys.dob)
                        
                        if data["id"].string != nil {
                            UserDefaults.set(value: data["id"].stringValue, key: UserDefaults.Keys.id_customer)
                        }
                        if data["id_customer"].string != nil {
                            UserDefaults.set(value: data["id_customer"].stringValue, key: UserDefaults.Keys.id_customer)
                        }
                        UserDefaults.set(value: "1", key: UserDefaults.Keys.isGuestLoggedIn)
                        UserDefaults.standard.synchronize()
                        self.makeRequest(call: WhichApiCall.updateCart)
                        
                    } else {
                        ErrorChecking.warningView(message: responseObject?["response"]["message"].string ?? "Some error occurs while creating guest account")
                    }
                default:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.makeRequest(call: WhichApiCall.guestLogin)
                    }
                }
            }
        }
    }
    
    @objc func myTargetFunction( _ textField: UITextField) {
        self.dismiskeyboard()
        dobField.resignFirstResponder()
        dobField.endEditing(true)
        UIBarButtonItem.appearance().tintColor = UIColor.black
        textField.resignFirstResponder()
        
        let datePicker = ActionSheetDatePicker(title: "dateOfBirth".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: { _, value, _ in
            if let value = value as? Date {
                let calendar = Calendar.autoupdatingCurrent
                let components = calendar.dateComponents([.hour, .minute, .day, .month, .year], from: value)
                let dateFormatter = DateFormatter()
                dateFormatter.timeStyle = DateFormatter.Style.none
                dateFormatter.dateStyle = DateFormatter.Style.full
                self.dobField.text = dateFormatter.string(from: value)
                self.signUpDict["years"] = String(components.year ?? 0)
                self.signUpDict["months"] = String(components.month ?? 0 )
                self.signUpDict["days"] = String(components.day  ?? 0)
            }
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: textField)
        datePicker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
        datePicker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
        datePicker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
        datePicker?.maximumDate = Date()
        datePicker?.show()
    }
    
    func dismiskeyboard() {
        view.endEditing(true)
    }
    
    func stringsLocalized() {
        self.navigationItem.title = "guestLogin".localized
        firstNameLabel.text = "firstName".localized
        lastNameLabel.text = "lastName".localized
        emailLabel.text = "emailAddress".localized
        dobLabel.text = "dob".localized
        newsletterLabel.text = "recieveNewsletter".localized
        specialOfferLabel.text = "recieveOffer".localized
        signUpButton.setTitle("continue".localized, for: .normal)
    }
    
}
extension GuestCheckoutViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dobField {
            self.myTargetFunction(textField)
        }
        return false
    }
    
}
