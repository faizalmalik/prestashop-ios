//
//  ProductPageDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON
import FirebaseAnalytics

class ProductPageDataViewController: UIViewController {
    fileprivate let productPageViewModalObject = ProductPageViewModal()
    var productId: String!
    var selectedProduct: ProductData!
 var isCheckAvailabilityRequired = false
    
    var productName: String!
    var emptyView: EmptyView!
    @IBOutlet weak var shortDescription: UILabel!
    //    let interactor = Interactor()
    @IBOutlet weak var virtualView: VirtualTap!
    @IBOutlet weak var virtualViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomTableView: UITableView!
    @IBOutlet weak var featureViewHeight: NSLayoutConstraint!
    @IBOutlet weak var featureView: FeatureProductView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var descriptionView: MoreInformationView!
    
    @IBOutlet weak var descriptionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var productBasicViewHeight: NSLayoutConstraint!
    @IBOutlet weak var customizationView: ProductCustomizationView!
    @IBOutlet weak var customizationViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var qtyView: QuantityView!
    @IBOutlet weak var reviewHeight: NSLayoutConstraint!
    @IBOutlet weak var productBasicDetailView: ProductBasicDetails!
    @IBOutlet weak var cartView: BuyNowView!
    @IBOutlet weak var bottomCartView: BuyNowView!
    @IBOutlet weak var attributeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var attributeViews: UIView!
    @IBOutlet weak var productCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var productCollectionView: UICollectionView!
    
    @IBOutlet weak var reviewDetailView: ReviewView!
    @IBOutlet weak var pageControll: CHIPageControlFresno!
    @IBOutlet weak var cartBtn: UIBarButtonItem!
    
    var productDict = [String: Any]()
    var selectedIndex = 0
    var wishlistArray = [String]()
    let dispatchGroupProduct = DispatchGroup()
    
    var id_product_attribute = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        customizationView.moveDelegate = self
        let start = Date()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        bottomCartView.passDataDelegate = self
        cartView.passDataDelegate = self
        virtualView.moveDelegate = self
        self.navigationItem.title = productName
        productCollectionView.register(BannerCollectionCell.nib, forCellWithReuseIdentifier: BannerCollectionCell.identifier)
        bottomTableView.register(HomeProductBlockTableViewCell.nib, forCellReuseIdentifier: HomeProductBlockTableViewCell.identifier)
        
        productCollectionView.delegate = productPageViewModalObject
        productCollectionView.dataSource = productPageViewModalObject
        pageControll.radius = 4
        pageControll.tintColor = GlobalConstants.Colors.accentColor
        pageControll.currentPageTintColor = .black
        productPageViewModalObject.pageController = pageControll
        reviewDetailView.newControllerDelegate = self
        self.mainView.isHidden = true
        self.bottomCartView.isHidden = true
        productBasicDetailView.passDelegate = self
        productBasicDetailView.moveDelegate = self
        productPageViewModalObject.moveControllerDelegate = self
        descriptionView.moveDelegate = self
        featureView.moveDelegate = self
        
        DispatchQueue.global(qos: .userInteractive).async {
            
           
            self.dispatchGroupProduct.enter()
            self.makeRequest(dict: [:], call: WhichApiCall.none)

            if let products : [ProductData] = self.productDict["products"] as? [ProductData] {
                
                self.selectedProduct = products[self.selectedIndex]
                
                       //This api call is to check weather product availablity required or not
                            self.dispatchGroupProduct.enter()
                            self.makeRequest(dict: [:], call: WhichApiCall.productAvailabilityRequired)
                
            }
        

            self.dispatchGroupProduct.notify(queue: DispatchQueue.main, execute: {
                
                //if check availability required , restrict buy button and change add to cart button title
                if  self.isCheckAvailabilityRequired {
                    
                    self.cartView.buyNowBtn.isEnabled = false
                    self.bottomCartView.buyNowBtn.isEnabled = false
                    self.cartView.buyNowBtn.alpha = 0.3
                    self.bottomCartView.buyNowBtn.alpha = 0.3
                    self.cartView.cartBtn.setTitle("checkAvailability".localized, for: .normal)
                    self.bottomCartView.cartBtn.setTitle("checkAvailability".localized, for: .normal)
                }else{
                    self.cartView.buyNowBtn.isEnabled = true
                    self.bottomCartView.buyNowBtn.isEnabled = true
                    self.cartView.buyNowBtn.alpha = 1
                    self.bottomCartView.buyNowBtn.alpha = 1
                    self.cartView.cartBtn.setTitle("addCart".localized, for: .normal)
                    self.bottomCartView.cartBtn.setTitle("addCart".localized, for: .normal)
                }
                
            })
            
        }
        
        self.nextPreviousData()
        let end = Date()
        print("time taken \(end.timeIntervalSince(start))")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.white
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let count = Int(FetchDetails.fetchCartCount()), count != 0 {
            self.cartBtn.addBadge(number: count)
        }
    }
    
    func nextPreviousData() {
        productBasicDetailView.nextBtn.setTitle("next".localized, for: .normal)
        productBasicDetailView.previousBtn.setTitle("previous".localized, for: .normal)
        
        if let productArray = productDict["products"] as? [ProductData] {
            if productArray.count == 0  || productArray.count == 1 {
                productBasicDetailView.nextBtn.setTitleColor(UIColor.lightGray, for: .normal)
                productBasicDetailView.previousBtn.setTitleColor(UIColor.lightGray, for: .normal)
                productBasicDetailView.nextBtn.isUserInteractionEnabled = false
                productBasicDetailView.previousBtn.isUserInteractionEnabled = false
            }
            if selectedIndex + 1 == productArray.count {
                productBasicDetailView.nextBtn.setTitleColor(UIColor.lightGray, for: .normal)
                productBasicDetailView.nextBtn.isUserInteractionEnabled = false
            }
        } else {
            productBasicDetailView.nextBtn.setTitleColor(UIColor.lightGray, for: .normal)
            productBasicDetailView.previousBtn.setTitleColor(UIColor.lightGray, for: .normal)
            productBasicDetailView.nextBtn.isUserInteractionEnabled = false
            productBasicDetailView.previousBtn.isUserInteractionEnabled = false
        }
        
        if selectedIndex == 0 {
            productBasicDetailView.previousBtn.setTitleColor(UIColor.lightGray, for: .normal)
            productBasicDetailView.previousBtn.isUserInteractionEnabled = false
        }
        
    }
    
    func makeRequest(dict: [String: Any], call: WhichApiCall) {
        var newDict = [String: Any]()
        
        var verbs: RequestType = .get
        
        var params = ""
        switch call {
        case .BuyNow:
            params =  GlobalConstants.ApiNames.addToCart
            newDict[GlobalConstants.ApiNames.addToCart.FetchApiName()] = dict
            verbs = .post
        case .Cart:
            params =  GlobalConstants.ApiNames.addToCart
            newDict[GlobalConstants.ApiNames.addToCart.FetchApiName()] = dict
            verbs = .post
        case .wishlistClicked:
            params =  GlobalConstants.ApiNames.addRemoveWishlist
            newDict[GlobalConstants.ApiNames.addRemoveWishlist.FetchApiName()] = dict
            verbs = .post
        case .productAvailabilityRequired :
            params =  GlobalConstants.ApiNames.productAvailabilityCheck
            let productAttribute = selectedProduct.attributes.count > 0 ? selectedProduct.attributes! : "0"
            params += "id_product=\((productId ?? ""))&id_product_attribute=\(productAttribute)"
            verbs = .get
        case .checkAvailability :
            params =  GlobalConstants.ApiNames.checkAvailability
            let productAttribute = selectedProduct.attributes.count > 0 ? selectedProduct.attributes! : "0"
            params += "id_product=\((productId ?? ""))&id_product_attribute=\(productAttribute)"
            params += "&quantity=\(Int(qtyView.stepper.value))"
            
            if FetchDetails.is_seller(), let customerID =  UrlParameters.fetchCustomerId(){
                
                    params += customerID
                    params += "&id_real_stock_status=1&run_as=3&array_format=1"
                    
                
            }
            else if FetchDetails.isLoggedIn(), let customerID =  UrlParameters.fetchCustomerId(){
                
            
                    params += customerID
                    params += "&run_as=2"
                
            }
            else {
                if let customerID =  UrlParameters.fetchGuestId(){
                    params += customerID
                    params += "&run_as=1"
                    
                }
                else{
                    params += "id_guest=0&run_as=1"
                    
                    
                }
                
                
                
            }
            verbs = .get

            
        default:
            params =    GlobalConstants.ApiNames.getProductData
            params += UrlParameters.width
            params += "&id_product=" + (productId ?? "")
            verbs = .get
        }
        
        
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                if let jsonData =  DBManager.sharedInstance.fetchData(key: AllController.productController.rawValue + self.productId) {
                    self.loadProductData(data: jsonData)
                } else {
                    self.setEmptyData(code: error._code)
                    print("loginError".localized, error)
                    self.emptyView.dict = dict
                    self.emptyView.call = call
                }
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                
                switch call {
                case .BuyNow:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, let data = ErrorChecking.getData(data: responseObject) {
                        if data["response"]["id_cart"].string != nil {
                            UserDefaults.set(value: data["response"]["id_cart"].stringValue, key: UserDefaults.Keys.id_cart)
                        }
                        if data["response"]["id_guest"].string != nil {
                            UserDefaults.set(value: data["response"]["id_guest"].stringValue, key: UserDefaults.Keys.id_guest)
                        }
                        
                        if data["response"]["nbProducts"].string != nil {
                            UserDefaults.set(value: data["response"]["nbProducts"].stringValue, key: UserDefaults.Keys.cartCount)
                        }
                        if  self.tabBarController != nil {
                            self.tabBarController?.setCartCount(value: FetchDetails.fetchCartCount())
                        }
                        
                        self.tabBarController?.selectedIndex = 3
                        if self.tabBarController?.selectedIndex == 3 {
                            if let nav = self.tabBarController?.selectedViewController as? UINavigationController {
                                nav.popToRootViewController(animated: true)
                            }
                        }
                    }
                case .Cart:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, let data = ErrorChecking.getData(data: responseObject) {
                        Analytics.logEvent(AnalyticsEventAddToCart, parameters: [AnalyticsParameterItemID: self.productId as NSObject, AnalyticsParameterItemName: self.productName as NSObject])
                        
                        if data["response"]["id_cart"].string != nil {
                            UserDefaults.set(value: data["response"]["id_cart"].stringValue, key: UserDefaults.Keys.id_cart)
                        }
                        if data["response"]["id_guest"].string != nil {
                            UserDefaults.set(value: data["response"]["id_guest"].stringValue, key: UserDefaults.Keys.id_guest)
                        }
                        
                        if data["response"]["nbProducts"].string != nil {
                            UserDefaults.set(value: data["response"]["nbProducts"].stringValue, key: UserDefaults.Keys.cartCount)
                        }
                        if  self.tabBarController != nil {
                            self.tabBarController?.setCartCount(value: FetchDetails.fetchCartCount())
                        }
                        if let count = Int(FetchDetails.fetchCartCount()), count != 0 {
                            self.cartBtn.addBadge(number: count)
                        }
                    }
                case .wishlistClicked:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        Analytics.logEvent(AnalyticsEventAddToWishlist, parameters: [AnalyticsParameterItemID: self.productId as NSObject, AnalyticsParameterItemName: self.productName as NSObject])
                        if self.productPageViewModalObject.combinationsDataArray.count > 0 {
                            
                            if self.wishlistArray.contains(self.id_product_attribute) {
                                self.wishlistArray.remove(at: self.wishlistArray.index(where: {$0 == self.id_product_attribute})!)
                                UIView.animate(withDuration: 0.5, animations: {() -> Void in
                                    self.productBasicDetailView.wishlistButton.imageView?.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                                }, completion: {(_ finished: Bool) -> Void in
                                    UIView.animate(withDuration: 0.5, animations: {() -> Void in
                                        self.productBasicDetailView.wishlistButton.imageView?.transform = CGAffineTransform(scaleX: 1, y: 1)
                                        self.productBasicDetailView.wishlistButton.setImage(#imageLiteral(resourceName: "symbols-wishlist"), for: .normal)
                                    })
                                })
                            } else {
                                self.wishlistArray.append(self.id_product_attribute)
                                UIView.animate(withDuration: 0.5, animations: {() -> Void in
                                    self.productBasicDetailView.wishlistButton.imageView?.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                                }, completion: {(_ finished: Bool) -> Void in
                                    UIView.animate(withDuration: 0.5, animations: {() -> Void in
                                        self.productBasicDetailView.wishlistButton.imageView?.transform = CGAffineTransform(scaleX: 1, y: 1)
                                        self.productBasicDetailView.wishlistButton.setImage(#imageLiteral(resourceName: "wishlist-active"), for: .normal)
                                    })
                                })
                            }
                            
                        } else {
                            if self.productPageViewModalObject.added_in_wishlist == "1" {
                                self.productPageViewModalObject.added_in_wishlist = "0"
                                if let index = WishlistContains.wishlistIds.index(where: {$0 == self.productId}) {
                                    WishlistContains.wishlistIds.remove(at: index)
                                }
                                UIView.animate(withDuration: 0.5, animations: {() -> Void in
                                    self.productBasicDetailView.wishlistButton.imageView?.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                                }, completion: {(_ finished: Bool) -> Void in
                                    UIView.animate(withDuration: 0.5, animations: {() -> Void in
                                        self.productBasicDetailView.wishlistButton.imageView?.transform = CGAffineTransform(scaleX: 1, y: 1)
                                        self.productBasicDetailView.wishlistButton.setImage(#imageLiteral(resourceName: "symbols-wishlist"), for: .normal)
                                    })
                                })
                            } else {
                                if !WishlistContains.wishlistIds.contains(self.productId) {
                                    WishlistContains.wishlistIds.append(self.productId)
                                }
                                self.productPageViewModalObject.added_in_wishlist = "1"
                                
                                UIView.animate(withDuration: 0.5, animations: {() -> Void in
                                    self.productBasicDetailView.wishlistButton.imageView?.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                                }, completion: {(_ finished: Bool) -> Void in
                                    UIView.animate(withDuration: 0.5, animations: {() -> Void in
                                        self.productBasicDetailView.wishlistButton.imageView?.transform = CGAffineTransform(scaleX: 1, y: 1)
                                        self.productBasicDetailView.wishlistButton.setImage(#imageLiteral(resourceName: "wishlist-active"), for: .normal)
                                    })
                                })
                            }
                        }
                    }
                    
                case .productAvailabilityRequired:
                    
                  
                    if let data =  responseObject {
                        
                        if data["real_time_stock_flag"]["is_required"].string  == "1" {
                            
                            self.isCheckAvailabilityRequired = true
                        }
                        
                        print("data--",self.isCheckAvailabilityRequired)
                    }
                      self.dispatchGroupProduct.leave()
                    
                     case .checkAvailability :
                    
                        if let data =  responseObject {
                            
                            if data["shop_to_server_stock_response"]["status"].string  == "1" {
                                
                                self.showAlert(title: "Success", message: "Your request has been sent Successfully and Please wait for the merchant's response.", completion: {
                                    self.navigationController?.popViewController(animated: true)
                                })
                                
                            }
                            
                    }
                    
                default:
                    
                    self.dispatchGroupProduct.leave()
                    if let data =  responseObject {
                        
                        let item = Item()
                        
                        item.ID = AllController.productController.rawValue + self.productId
                        item.textString = data.rawString()!
                        DBManager.sharedInstance.addData(object: item)
                        self.loadProductData(data: data)
                        if self.productPageViewModalObject.isPackProduct {
                            self.bottomTableViewHeight.constant = SCREEN_WIDTH/2 + 60
                            self.bottomTableView.isHidden = false
                            self.bottomTableView.delegate = self.productPageViewModalObject
                            self.bottomTableView.dataSource = self.productPageViewModalObject
                            self.bottomTableView.reloadData()
                        } else {
                            self.bottomTableViewHeight.constant = 0
                            self.bottomTableView.isHidden = true
                        }
                    }
                }
            }
        }
    }
    
    func loadProductData(data: JSON) {
        self.productPageViewModalObject.getValue(jsonData: data) { (data: Bool) in
            self.shortDescription.text = self.productPageViewModalObject.shortDescription
            if self.productPageViewModalObject.featuresDataArray.count == 0 {
                self.featureViewHeight.constant = 0
                self.featureView.isHidden = true
            }
            Analytics.logEvent(AnalyticsEventViewItem, parameters: [AnalyticsParameterItemID: self.productId as NSObject, AnalyticsParameterItemName: self.productName as NSObject])
            self.mainView.isHidden = false
            self.productCollectionHeight.constant = SCREEN_WIDTH
            self.productCollectionView.reloadData()
            self.setDefaultValue(attributes: self.productPageViewModalObject.attributesDataArray)
            if  self.productPageViewModalObject.productDetail != nil {
                self.productDataSet(data: self.productPageViewModalObject.productDetail)
            }
            self.reviewData()
            if self.productPageViewModalObject.added_in_wishlist == "1" {
                if !WishlistContains.wishlistIds.contains(self.productId) {
                    WishlistContains.wishlistIds.append(self.productId)
                }
                self.productBasicDetailView.wishlistButton.setImage(#imageLiteral(resourceName: "wishlist-active"), for: .normal)
            } else {
                self.productBasicDetailView.wishlistButton.setImage(#imageLiteral(resourceName: "symbols-wishlist"), for: .normal)
            }
            if  self.productPageViewModalObject.isVirtual || self.productPageViewModalObject.attachmentDataArray.count > 0 {
                self.virtualView.isHidden = false
            } else {
                self.virtualView.isHidden = true
                self.virtualViewHeight.constant = 0
            }
            if !self.productPageViewModalObject.isCustomizable {
                self.customizationView.isHidden = true
                self.customizationViewHeight.constant = 0
            }
            if  self.productPageViewModalObject.wishlistEnabled {
                self.productBasicDetailView.wishlistButton.isHidden = false
                self.productBasicDetailView.wishlistWidth.constant = 38
            } else {
                self.productBasicDetailView.wishlistButton.isHidden = true
                self.productBasicDetailView.wishlistWidth.constant = 0
            }
            if self.productPageViewModalObject.productDescription.isEmpty {
                self.descriptionView.isHidden = true
                self.descriptionViewHeight.constant = 0
            } else {
                self.descriptionView.isHidden = false
                self.descriptionViewHeight.constant = 46
            }
            self.wishlistArray = (self.productPageViewModalObject.combinationsDataArray.filter {$0.added_in_wishlist == "1"}).map { $0.id_product_attribute }
            self.productBasicDetailView.layoutIfNeeded()
            self.view.layoutIfNeeded()
            if self.isVisible(view: cartView) {
                self.bottomCartView.isHidden = true
                self.cartView.isHidden = false
            } else {
                self.cartView.isHidden = true
                self.bottomCartView.isHidden = false
            }
            if self.productPageViewModalObject.displayAddtoCart != nil && !self.productPageViewModalObject.displayAddtoCart {
                self.cartView.isHidden = true
                self.bottomCartView.isHidden = true
            }
            print("self.wishlistArray", self.wishlistArray )
        }
    }
    
    func productDataSet(data: ProductBasicDetail) {
        
        productBasicDetailView.nameLabel.text = data.name
        productBasicDetailView.priceLabel.text = data.price
        productBasicDetailView.discountPriceLabel.text = data.price_pack
        productBasicDetailView.discountPriceLabel.strike(text: data.price_pack)
        
        productBasicDetailView.stockLabel.text = self.productPageViewModalObject.inStock
        
        if self.productPageViewModalObject.SellerAvailable {
            productBasicDetailView.sellerNameLabel.setTitle(productPageViewModalObject.sellerDetails.shop_name, for: .normal)
        } else {
            productBasicDetailView.sellerNameHeight.constant = 0
            productBasicViewHeight.constant = 153
            productBasicDetailView.sellerNameLabel.setTitle("", for: .normal)
        }
        if data.show_price {
            self.productBasicDetailView.priceLabel.isHidden = false
        } else {
            self.productBasicDetailView.priceLabel.isHidden = true
        }
        //        self.productBasicDetailView.layoutIfNeeded()
        //        self.productBasicViewHeight.constant = self.productBasicDetailView.priceLabel.frame.origin.y + self.productBasicDetailView.priceLabel.frame.size.height
    }
    
    func reviewData() {
        if  self.productPageViewModalObject.reviewEnabled {
            reviewDetailView.ratingLabel.text = String.init(format: "%@/5", self.productPageViewModalObject.average_review)
            reviewDetailView.starView.allowsHalfStars = true
            reviewDetailView.starView.value =  CGFloat(Float(self.productPageViewModalObject.average_review) ?? 0)
            reviewDetailView.totalReviews.text = String.init(format: "%i", self.productPageViewModalObject.reviewDataArray.count)
            
        } else {
            reviewHeight.constant = 0
            reviewDetailView.isHidden = true
        }
        //        reviewHeight
    }
    
    func setattributes (attributes: [AttributesData], selectedValues: [String: String]) {
        
        //        let varCombArray = fetchDefaultValues (productId : productId)
        
        if attributes.count > 0 {
            var intY: CGFloat = 8
            for i in 0..<attributes.count {
                
                let varLabel: UILabel = UILabel()
                varLabel.frame = CGRect(x: 8, y: intY, width: SCREEN_WIDTH - 16, height: 17)
                //                varLabel.font = GlobalData.fonts.small
                varLabel.SetDefaultTextColor()
                varLabel.text = attributes[i].attribute_group
                self.attributeViews.addSubview(varLabel)
                intY += 25
                if attributes[i].is_color_group == "1" {
                    let varScroll: UIScrollView = UIScrollView()
                    varScroll.frame = CGRect(x: 0, y: intY, width: SCREEN_WIDTH - 16, height: 50)
                    varScroll.tag = 100 + i
                    self.attributeViews.addSubview(varScroll)
                    var intX: CGFloat = 8
                    for j in 0..<attributes[i].attributeValues.count {
                        let circleView: UIButton = UIButton(frame: CGRect(x: intX, y: 4, width: 36, height: 36))
                        circleView.backgroundColor = UIColor().HexToColor(hexString: attributes[i].attributeValues[j].color!)
                        circleView.layer.cornerRadius = 18
                        circleView.tag = (i*1000) + j + 1000
                        circleView.shadowBorder1()
                        circleView.addTarget(self, action: #selector(self.buttonClicked(sender:)), for: .touchUpInside)
                        circleView.applyBorder(colours: GlobalConstants.Colors.setBorderColor)
                        varScroll.addSubview(circleView)
                        intX += 50
                        if let value =  selectedValues[attributes[i].id_attribute_group] {
                            if value == attributes[i].attributeValues[j].id_attribute {
                                circleView.applyBorder(colours: GlobalConstants.Colors.accentColor)
                                circleView.setTitleColor(GlobalConstants.Colors.accentColor, for: .normal)
                            }
                            
                        }
                        
                    }
                    intY += 52
                    varScroll.contentSize = CGSize(width: intX, height: 36)
                } else {
                    let varScroll: UIScrollView = UIScrollView()
                    varScroll.frame = CGRect(x: 0, y: intY, width: SCREEN_WIDTH - 16, height: 50)
                    varScroll.tag = 100 + i
                    self.attributeViews.addSubview(varScroll)
                    var intX: CGFloat = 8
                    for j in 0..<attributes[i].attributeValues.count {
                        
                        let buttonText = String.init(format: "  %@  ", attributes[i].attributeValues[j].name)
                        let stringSize = buttonText.widthOfString(usingFont: UIFont.systemFont(ofSize: 14))
                        let valueView: UIButton = UIButton(frame: CGRect(x: intX, y: 4, width: stringSize + 26, height: 36))
                        valueView.titleLabel?.font =  UIFont.systemFont(ofSize: 14)
                        valueView.setTitle(String.init(format: "  %@  ", buttonText), for: .normal)
                        valueView.tag = (i*1000) + j + 1000
                        valueView.backgroundColor = UIColor.white
                        valueView.shadowBorder1()
                        valueView.setTitleColor(GlobalConstants.Colors.defaultTextColor, for: .normal)
                        valueView.layer.cornerRadius = 18
                        valueView.addTarget(self, action: #selector(self.buttonClicked(sender:)), for: .touchUpInside)
                        valueView.applyBorder(colours: GlobalConstants.Colors.setBorderColor)
                        varScroll.addSubview(valueView)
                        
                        intX += stringSize + 34
                        
                        if let value =  selectedValues[attributes[i].id_attribute_group] {
                            if value == attributes[i].attributeValues[j].id_attribute {
                                valueView.applyBorder(colours: GlobalConstants.Colors.accentColor)
                                valueView.setTitleColor(GlobalConstants.Colors.accentColor, for: .normal)
                                
                                //                                self.ProductPrice.text =
                            }
                            
                        }
                        
                    }
                    intY += 52
                    varScroll.contentSize = CGSize(width: intX, height: 44)
                }
            }
            self.attributeViewHeight.constant = intY
            self.fetchProductDetails(selectedValues: selectedValues)
        } else {
            self.attributeViewHeight.constant = 0
        }
    }
    
    var selectedValues = [String: String]()
    
    func setDefaultValue (attributes: [AttributesData]) {
        if let defaultCode = self.productPageViewModalObject.defaultCode {
            let defaultCodeArray = defaultCode.split(separator: ",")
            
            for i in 0..<defaultCodeArray.count {
                
                let defaultCodeSubArray = defaultCodeArray[i].split(separator: ":")
                print( defaultCodeSubArray)
                if let index = productPageViewModalObject.attributesDataArray.index(where: {
                    $0.id_attribute_group == defaultCodeSubArray[0]
                }) {
                    if let internalIndex = productPageViewModalObject.attributesDataArray[index].attributeValues.index(where: {
                        $0.id_attribute == defaultCodeSubArray[1]
                        
                    }) {
                        selectedValues[productPageViewModalObject.attributesDataArray[index].id_attribute_group] = productPageViewModalObject.attributesDataArray[index].attributeValues[internalIndex].id_attribute
                    } else {
                        selectedValues.removeAll()
                        break
                    }
                } else {
                    selectedValues.removeAll()
                    break
                }
            }
            
            self.setattributes(attributes: productPageViewModalObject.attributesDataArray, selectedValues: selectedValues)
        } else {
            self.attributeViewHeight.constant = 0
        }
    }
    
    @objc func buttonClicked(sender: UIButton) {
        let superTag = ((sender.superview?.tag)! - 100)
        
        let attID = self.productPageViewModalObject.attributesDataArray[superTag].id_attribute_group
        
        for j in 0..<self.productPageViewModalObject.attributesDataArray[superTag].attributeValues.count {
            
            let attValues = self.productPageViewModalObject.attributesDataArray[superTag]
            
            if let temp: UIButton = self.view.viewWithTag(((superTag  * 1000) + j) + 1000) as? UIButton {
                let subTag = (sender.tag - 1000)%1000
                
                if sender.tag == temp.tag {
                    let valId =  attValues.attributeValues[subTag].id_attribute
                    selectedValues.removeValue(forKey: attID!)
                    
                    selectedValues[attID!] = valId
                    temp.applyBorder(colours: GlobalConstants.Colors.accentColor)
                    temp.setTitleColor(GlobalConstants.Colors.accentColor, for: .normal)
                    self.fetchProductDetails(selectedValues: selectedValues)
                } else {
                    temp.applyBorder(colours: GlobalConstants.Colors.setBorderColor)
                    temp.setTitleColor(GlobalConstants.Colors.defaultTextColor, for: .normal)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchProductDetails(selectedValues: [String: String]) {
        var makeCode = ""
        
        for (key, value) in selectedValues {
            makeCode += key + ":" + value + ","
        }
        if makeCode.last == "," {
            makeCode.removeLast()
        }
        
        if let index = productPageViewModalObject.combinationsDataArray.index(where: {
            $0.code.split(separator: ",").sorted() == makeCode.split(separator: ",").sorted()
        }) {
            self.productPageViewModalObject.added_in_wishlist = productPageViewModalObject.combinationsDataArray[index].added_in_wishlist
            
            self.productBasicDetailView.priceLabel.text = productPageViewModalObject.combinationsDataArray[index].price
            self.id_product_attribute = productPageViewModalObject.combinationsDataArray[index].id_product_attribute
            if self.wishlistArray.contains(self.id_product_attribute) {
                self.productBasicDetailView.wishlistButton.setImage(#imageLiteral(resourceName: "wishlist-active"), for: .normal)
            } else {
                self.productBasicDetailView.wishlistButton.setImage(#imageLiteral(resourceName: "symbols-wishlist"), for: .normal)
            }
        }
    }
    
    func isVisible(view: UIView) -> Bool {
        func isVisible(view: UIView, inView: UIView?) -> Bool {
            guard let inView = inView else { return true }
            let viewFrame = inView.convert(view.bounds, from: view)
            if viewFrame.intersects(inView.bounds) {
                return isVisible(view: view, inView: inView.superview)
            }
            return false
        }
        return isVisible(view: view, inView: view.superview)
    }
    
    @IBAction func tapCartBtn(_ sender: Any) {
        self.tabBarController?.selectedIndex = 3
        if self.tabBarController?.selectedIndex == 3 {
            if let nav = self.tabBarController?.selectedViewController as? UINavigationController {
                nav.popToRootViewController(animated: true)
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ProductPageDataViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.productPageViewModalObject.displayAddtoCart != nil && self.productPageViewModalObject.displayAddtoCart {
            if self.isVisible(view: cartView) {
                bottomCartView.isHidden = true
                self.cartView.isHidden = false
            } else {
                self.cartView.isHidden = true
                bottomCartView.isHidden = false
            }
        } else {
            self.cartView.isHidden = true
            self.bottomCartView.isHidden = true
        }
    }
}

extension ProductPageDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        print(controller)
        switch controller {
        case .zoomController:
            if let view = UIStoryboard.loadZoomViewController() {
                view.dict = dict
                view.currentPage = self.productPageViewModalObject.currentPage
                //             view.interactor = interactor
                let nav  = UINavigationController(rootViewController: view)
                self.present(nav, animated: true, completion: nil)
            }
        case .sellerProfile:
            if let view = UIStoryboard.loadShopDataViewController() {
                view.name = productPageViewModalObject.sellerDetails.shop_name
                view.custId = productPageViewModalObject.sellerDetails.id_seller
                view.seller_customer_id = productPageViewModalObject.sellerDetails.id_customer
                self.navigationController?.pushViewController(view, animated: true)
            }
            //            let view = UIStoryboard.loadSellerProfileViewController()
            //            view.title1 = productPageViewModalObject.sellerDetails.shop_name
            //            view.custId = productPageViewModalObject.sellerDetails.id_seller
            //            view.controller = AllController.productController
        //            self.navigationController?.pushViewController(view, animated: true)
        case .descriptionViewController:
            if let view = UIStoryboard.loadDescriptionViewController() {
                view.htmlData = self.productPageViewModalObject.productDescription
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .reviewViewController:
            if self.productPageViewModalObject.reviewDataArray.count > 0 {
                if let view = UIStoryboard.loadReviewViewViewController() {
                    view.reviewDataArray = self.productPageViewModalObject.reviewDataArray
                    self.navigationController?.pushViewController(view, animated: true)
                }
            }
        case .featureController:
            if let view = UIStoryboard.loadFeatureViewController() {
                view.featuresDataArray = self.productPageViewModalObject.featuresDataArray
                view.title1 = self.productName
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .productController:
            if let view = UIStoryboard.loadProductViewController() {
                view.productId = id
                view.productName = name
                view.productDict = dict
                view.selectedIndex = index
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .virtualProductDataViewController:
            if let view = UIStoryboard.loadVirtualProductViewController() {
                view.attachmentDataArray = self.productPageViewModalObject.attachmentDataArray
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .customizableProductViewController:
            if let view = UIStoryboard.loadCustomizableProductViewController() {
                view.custProducts = self.productPageViewModalObject.custProducts
                view.productId = self.productId
                view.custReqArray = self.productPageViewModalObject.custReqArray
                view.customization_controller_link = self.productPageViewModalObject.customization_controller_link
                view.delegate = self
                self.navigationController?.pushViewController(view, animated: true)
            }
        default:
            if let view = UIStoryboard.loadAddReviewViewController() {
                view.PName = productName
                view.templateId = productId
                self.navigationController?.pushViewController(view, animated: true)
            }
        }
    }
}

extension ProductPageDataViewController: PassData, CheckCustimisationSaved {
    
    func CustomisationSaved(data: Bool) {
        self.productPageViewModalObject.isCustomizable = false
    }
    
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        switch call {
            
            
        case .checkAvailability:
            if !self.productPageViewModalObject.isCustomizable {
                self.makeRequest(dict: [:], call: call)
            } else {
                ErrorChecking.warningView(message: "saveCustomisationFirst".localized)
            }
            
        case .BuyNow:
            if !self.productPageViewModalObject.isCustomizable {
                self.makeRequest(dict: self.CartDict(), call: call)
            } else {
                ErrorChecking.warningView(message: "saveCustomisationFirst".localized)
            }
        case .Cart:
            if !self.productPageViewModalObject.isCustomizable {
                self.makeRequest(dict: self.CartDict(), call: call)
            } else {
                ErrorChecking.warningView(message: "saveCustomisationFirst".localized)
            }
        case .wishlistClicked:
            
            if FetchDetails.isLoggedIn() {
                var dict = [String: Any]()
                
                dict["id_product_attribute"] = id_product_attribute
                dict ["id_customer"] =  UserDefaults.fetch(key: UserDefaults.Keys.id_customer) ?? ""
                dict["id_product"] = productId
                self.makeRequest(dict: dict, call: call)
            } else {
                if let view = UIStoryboard.loadSignInViewController() {
                    let nav  = UINavigationController(rootViewController: view)
                    self.present(nav, animated: true, completion: nil)
                }
            }
            
        case.shareClicked:
            self.activityInd()
        case .next:
            self.loadProductPage(index: selectedIndex + 1)
            
        //            self.navigationController?.pushViewController(view, animated: true)
        case .previous:
            
            self.loadProductPage(index: selectedIndex - 1)
        //            self.navigationController?.pushViewController(view, animated: true)
        default:
            print()
        }
    }
    
    func loadProductPage( index: Int) {
        let productArray = productDict["products"] as? [ProductData] ?? []
        
        if let view = UIStoryboard.loadProductViewController() {
            view.productId = productArray[index].id_product
            view.productName = productArray[index].name
            view.productDict = productDict
            view.selectedIndex = index
            hero.replaceViewController(with: view)
        }
    }
    
    func activityInd() {
        print(self.productPageViewModalObject.product_link)
        let vc = UIActivityViewController(activityItems: [self.productPageViewModalObject.product_link], applicationActivities: nil)
        if UI_USER_INTERFACE_IDIOM() == .phone {
            self.present(vc, animated: true, completion: nil)
        } else {
            UIBarButtonItem.appearance().tintColor = UIColor.black
            let popup = UIPopoverController(contentViewController: vc)
            popup.present(from: CGRect(x: CGFloat(self.view.frame.size.width / 2), y: CGFloat(self.view.frame.size.height / 4), width: CGFloat(0), height: CGFloat(0)), in: self.view, permittedArrowDirections: .any, animated: true)
        }
        
    }
    
    func CartDict() -> [String: Any] {
        var cartDict = [String: Any]()
        cartDict["quantity"] = Int(qtyView.stepper.value)
        cartDict["operation"] = "up"
        cartDict["id_customization"] = ""
        cartDict["id_address_delivery"] = "0"
        cartDict["id_product_attribute"] = id_product_attribute
        cartDict ["id_customer"] =  UserDefaults.fetch(key: UserDefaults.Keys.id_customer) ?? ""
        cartDict ["id_cart"] =  UserDefaults.fetch(key: UserDefaults.Keys.id_cart) ?? ""
        cartDict ["id_guest"] =  UserDefaults.fetch(key: UserDefaults.Keys.id_guest) ?? ""
        cartDict ["id_currency"] =  UserDefaults.fetch(key: "id_currency") ?? ""
        cartDict["id_product"] = productId
        return cartDict
    }
}

extension ProductPageDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest(dict: emptyView.dict, call: emptyView.call)
    }
}

//extension ProductPageDataViewController: UIViewControllerTransitioningDelegate {
//    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return DismissAnimator()
//    }
//
//    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
//
//        return interactor.hasStarted ? interactor : nil
//    }
//}
//
//class Interactor: UIPercentDrivenInteractiveTransition {
//    var hasStarted = false
//    var shouldFinish = false
//}
