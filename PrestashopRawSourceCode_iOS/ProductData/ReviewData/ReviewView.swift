//
//  ReviewView.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import HCSStarRatingView
import SwiftyJSON

class ReviewView: UIView {

    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var writeViewBtn: UIButton!
    @IBOutlet weak var totalReviews: UILabel!
    @IBOutlet weak var totalRating: UILabel!
    @IBOutlet weak var starView: HCSStarRatingView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var internalReviewView: UIView!

    var newControllerDelegate: MoveController?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
     override func awakeFromNib() {
        starView.isUserInteractionEnabled = false
        internalReviewView.isUserInteractionEnabled = true
        internalReviewView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(reviewViewTap)))
        writeViewBtn.setTitle("writeReview".localized, for: .normal)
        reviewLabel.text = "reviews".localized
    }
    
    @objc private func reviewViewTap() {
      
     newControllerDelegate?.moveController(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.reviewViewController)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let xibView = Bundle.main.loadNibNamed("ReviewView", owner: self, options: nil)?[0] as? UIView {
            xibView.frame = self.bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            internalReviewView.normalBorder()
            topView.normalBorder()
            internalReviewView.layer.cornerRadius = 0
            topView.layer.cornerRadius = 0
            self.addSubview(xibView)
        }
    }

    @IBAction func writeReviewClicked(_ sender: Any) {
        newControllerDelegate?.moveController(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.addReview)
        
    }

}
