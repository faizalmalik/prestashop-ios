//
//  QuantityView.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class QuantityView: UIView {
    
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override func awakeFromNib() {
        stepper.minimumValue = 1
        stepper.value = 1
        qtyLabel.text = "quantity".localized
        itemLabel.text = String.init(format: "%.0f %@", stepper.value, "item".localized)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let xibView = Bundle.main.loadNibNamed("QuantityView", owner: self, options: nil)?[0] as? UIView {
            xibView.frame = self.bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(xibView)
        }
    }
    
    @IBAction func steeperClicked(_ sender: UIStepper) {
        print(sender.value)
        if sender.value == 1 {
            itemLabel.text = String.init(format: "%.0f %@", sender.value, "item".localized)
        } else {
            itemLabel.text = String.init(format: "%.0f %@", sender.value, "items".localized)
        }
    }
    
}
