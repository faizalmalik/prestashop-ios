//
//  CustimizableTextFieldTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class CustimizableTextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var field: UITextField!
    @IBOutlet weak var TitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        field.delegate = self
        // Initialization code
    }
    
    var passDelegate: PassData?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    var item: CustomizableProductData! {
        didSet {
            TitleLabel.text = item.name
        }
    }
    
}

extension CustimizableTextFieldTableViewCell: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        var dict = [String: Any]()
        dict[item.id_customization_field] = textField.text
        passDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiCall.none)
    }
}
