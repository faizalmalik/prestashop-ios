//
//  CustomizableFileTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON
import SVProgressHUD
import Alamofire

class CustomizableFileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var imageType: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

     var passDelegate: PassData?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func editBtnClicked(_ sender: Any) {
        let alert = UIAlertController(title: "upload".localized, message: nil, preferredStyle: .actionSheet)
        
        let takeAction = UIAlertAction(title: "takeAPhoto".localized, style: .default, handler: take)
        let upload = UIAlertAction(title: "uploadFromLibrary".localized, style: .default, handler: uploadImage)
        let CancelAction = UIAlertAction(title: "cancel".localized, style: .cancel, handler: cancel)
        
        alert.addAction(takeAction)
        alert.addAction(upload)
        alert.addAction(CancelAction)
        if Device.IS_IPAD {
            alert.popoverPresentationController?.sourceView = self
            alert.popoverPresentationController?.sourceRect = CGRect(x: self.bounds.size.width / 2.0, y: self.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        }
        let window: UIWindow = UIApplication.shared.keyWindow!
        window.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func take(alertAction: UIAlertAction!) {
        
        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.allowsEditing = false
        picker.delegate = self
        let window: UIWindow = UIApplication.shared.keyWindow!
        window.rootViewController?.present(picker, animated: true, completion: nil)
//        present(picker, animated: true, completion: nil)
    }
    func uploadImage(alertAction: UIAlertAction!) {
        
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = false
        let window: UIWindow = UIApplication.shared.keyWindow!
        window.rootViewController?.present(picker, animated: true, completion: nil)
        
    }
    func cancel(alertAction: UIAlertAction!) {
        
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    var item: CustomizableProductData! {
        didSet {
            titleLabel.text = item.name
        }
    }
    
}
extension CustomizableFileTableViewCell: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage ,
            let imageData = image.jpegData(compressionQuality: 0.8) {
//            self.imageData = imageData
            self.imageType.image = image
            var dict = [String: Any]()
            dict[item.id_customization_field] = imageData
            passDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiCall.none)
        } else {
            print("Something went wrong")
        }
        
        let window: UIWindow = UIApplication.shared.keyWindow!
        window.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
