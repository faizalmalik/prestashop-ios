//
//  CustomizableProductViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import Alamofire
import SVProgressHUD

protocol CheckCustimisationSaved: class {
    func CustomisationSaved(data: Bool)
}

class CustomizableProductViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataDict = [String: Any]()
    var productId = ""
    var custProducts = [CustomizableProductData]()
    var custReqArray = [String]()
    var customization_controller_link: String!
    weak var delegate: CheckCustimisationSaved?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        print(custReqArray)
        self.navigationItem.title = "productcustomisation".localized
        tableView.register(CustimizableTextFieldTableViewCell.nib, forCellReuseIdentifier: CustimizableTextFieldTableViewCell.identifier)
        tableView.register(CustomizableFileTableViewCell.nib, forCellReuseIdentifier: CustomizableFileTableViewCell.identifier)
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveCustomizationClicked(_ sender: UIButton) {
        var value = 0
        
        for i in 0..<custReqArray.count {
            if !dataDict.keys.contains(custReqArray[i]) {
                if let index =  custProducts.index(where: { $0.id_customization_field == custReqArray[i] }) {
                    ErrorChecking.warningView(message: "\(custProducts[index].name!) " + "isrequriedField".localized)
                    
                }
                value = 1
                break
            } else {
                value = 0
            }
        }
        
        if  value == 0 {
            self.upload()
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func upload() {
        
        //         You can change your image name here, i use NSURL image and convert into string
        SVProgressHUD.show(withStatus: "loading".localized)
        var fileUrl = customization_controller_link +  "&outputformat=json"
        fileUrl = fileUrl.replace(string: "%2F", replacement: "/")
        fileUrl = fileUrl.replace(string: "%3A", replacement: ":")
        print(fileUrl)
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        let cartId =  (UserDefaults.standard.value(forKey: "id_cart") as? String) ?? ""
        let custId = (UserDefaults.standard.value(forKey: "id_customer") as? String) ?? ""
        let langId =  (UserDefaults.standard.value(forKey: "id_lang") as? String)!
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append((self.productId).data(using: String.Encoding.utf8)!, withName: "id_product")
            multipartFormData.append(cartId.data(using: String.Encoding.utf8)!, withName: "id_cart")
            multipartFormData.append(custId.data(using: String.Encoding.utf8)!, withName: "id_customer")
            multipartFormData.append(langId.data(using: String.Encoding.utf8)!, withName: "id_lang")
            
            for (key, value) in self.dataDict {
                if let check = value as? String {
                    multipartFormData.append(check.data(using: String.Encoding.utf8)!, withName: "\("textField")\(key)")
                }
                if let check = value as? Data {
                    multipartFormData.append(check, withName: "\("file")\(key)", fileName: "filename=imageName.jpg", mimeType: "image/jpeg")
                }
            }
            
        }, usingThreshold: UInt64.init(), to: fileUrl, method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    SVProgressHUD.dismiss()
                    print(String(data: response.data!, encoding: String.Encoding.utf8)!)
                    if let jsonData = try? JSON(data: response.data!) {
                        print(jsonData)
                        if jsonData["response"]["status"].stringValue == "1" {
                            ErrorChecking.successView(message: jsonData["response"]["message"].stringValue)
                            if jsonData["response"]["id_cart"].string != nil {
                                UserDefaults.set(value: jsonData["response"]["id_cart"].stringValue, key: UserDefaults.Keys.id_cart)
                            }
                            if jsonData["response"]["id_guest"].string != nil {
                                UserDefaults.set(value: jsonData["response"]["id_guest"].stringValue, key: UserDefaults.Keys.id_guest)
                            }
                            self.delegate?.CustomisationSaved(data: true)
                            self.navigationController?.popViewController(animated: true)
                        } else {
                            ErrorChecking.warningView(message: "someerroroccur".localized)
                        }
                    } else {
                        ErrorChecking.warningView(message: "someerroroccur".localized)
                    }
                    //                    if ErrorChecking.getData(data: jsonData) != JSON.null, let _ = ErrorChecking.getData(data: jsonData) {
                    //                            self.makeRequest(call:  WhichApiCall.submitContactUs, dict: dict)
                    //                    }
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                ErrorChecking.warningView(message: "someerroroccur".localized)
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
    
}
extension CustomizableProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return custProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = custProducts[indexPath.row].type
        
        switch type {
        case .textField?:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CustimizableTextFieldTableViewCell.identifier, for: indexPath) as? CustimizableTextFieldTableViewCell {
                cell.item = custProducts[indexPath.row]
                cell.selectionStyle = .none
                cell.passDelegate = self
                return cell
            }
        case .file?:
            if let cell = tableView.dequeueReusableCell(withIdentifier: CustomizableFileTableViewCell.identifier, for: indexPath) as? CustomizableFileTableViewCell {
                cell.item = custProducts[indexPath.row]
                cell.selectionStyle = .none
                cell.passDelegate = self
                return cell
            }
        case .none:
            break
        }
        return UITableViewCell()
    }
}

extension CustomizableProductViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        if let data = dict[Array(dict.keys)[0]] as? String {
            if data.replacingOccurrences(of: " ", with: "") != "" {
                dataDict.merge(dict: dict)
            }
        } else {
            dataDict.merge(dict: dict)
        }
        print(dict)
        print(dataDict)
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]) {
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}
