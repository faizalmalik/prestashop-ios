//
//  FetureDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit

class FetureDataViewController: UIViewController {

      var featuresDataArray = [FeaturesData]()
    let FeatureViewModalObject = FeatureViewModal()
    var title1: String!
    
    @IBOutlet weak var featureTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        featureTableView.register(FeatureTableViewCell.nib, forCellReuseIdentifier: FeatureTableViewCell.identifier)
        // Do any additional setup after loading the view.
        
        self.navigationItem.title = title1
        
        self.FeatureViewModalObject.getValue(jsonData: featuresDataArray) {
            (data: Bool) in
            print(data)
            if data {
                
                featureTableView.delegate = FeatureViewModalObject
                 featureTableView.dataSource = FeatureViewModalObject
              
                self.featureTableView.isHidden = false
                self.featureTableView.reloadData()
            } else {
               
                self.featureTableView.isHidden = true
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
