//
//  FeatureViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class FeatureViewModal: NSObject {
    var featuresDataArray = [FeaturesData]()
    
    func getValue( jsonData: [FeaturesData], completion: ((_ data: Bool) -> Void) ) {
       
        if !jsonData.isEmpty {
            self.featuresDataArray = jsonData
            completion(true)
        } else {
            completion(false)
        }
    }
}

extension FeatureViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return featuresDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: FeatureTableViewCell.identifier) as? FeatureTableViewCell {
        cell.item = featuresDataArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
        }
        return UITableViewCell()
    }
   
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "features".localized
    }
}
