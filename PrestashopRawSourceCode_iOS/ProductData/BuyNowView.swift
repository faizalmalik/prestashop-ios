//
//  BuyNowView.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

protocol AddToCartDelegate {
    
    func addToCart(cart: AddCart)
}

class BuyNowView: UIView {
    
    @IBOutlet weak var buyNowBtn: UIButton!
    @IBOutlet weak var cartBtn: UIButton!
    var cartDelegate: AddToCartDelegate?
    var passDataDelegate: PassData?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let xibView = Bundle.main.loadNibNamed("BuyNowView", owner: self, options: nil)?[0] as? UIView {
            xibView.frame = self.bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(xibView)
        }
    }
    
    override func awakeFromNib() {
        cartBtn.setTitle("addCart".localized, for: .normal)
        buyNowBtn.setTitle("buyNow".localized, for: .normal)
        buyNowBtn.SetAccentBackColor()
        cartBtn.SetAccentBackColor()
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    @IBAction func addToCartClicked(_ sender: UIButton) {
        
        if cartBtn.currentTitle == "addCart".localized {
             passDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.Cart)
        } else if  cartBtn.currentTitle == "checkAvailability".localized {
             passDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.checkAvailability)
        }
        else{
            
        }
        
    }
    @IBAction func buyNowClicked(_ sender: UIButton) {
        passDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.BuyNow)
        
    }
}

enum AddCart {
    case cart
    case buyNow
}
