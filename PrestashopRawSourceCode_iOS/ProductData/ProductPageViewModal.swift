//
//  ProductPageViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class ProductPageViewModal: NSObject {
    var productImages = [String]()
    var pageController: CHIPageControlFresno?
    var attributesDataArray = [AttributesData]()
    var combinationsDataArray = [CombinationsData]()
    var defaultCode: String!
    var productDetail: ProductBasicDetail!
    var reviewDataArray = [ReviewData]()
    var reviewEnabled: Bool!
    var wishlistEnabled: Bool!
    var average_review: String!
    var added_in_wishlist: String!
    var product_link: String!
    var moveControllerDelegate: MoveController?
    var currentPage = 0
    var SellerAvailable: Bool!
    var sellerDetails: ProductSellerDetails!
    var productDescription: String!
    var featuresDataArray = [FeaturesData]()
    var isPackProduct: Bool!
    var shortDescription: String!
    var packProducts = [ProductData]()
    var attachmentDataArray = [AttachmentData]()
    var custProducts = [CustomizableProductData]()
    var moveDelegate: MoveController?
    var isVirtual: Bool!
    var inStock: String!
    var customization_controller_link: String!
    var isCustomizable: Bool!
    var custReqArray = [String]()
    var displayAddtoCart: Bool!
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = ProductPageModal(data: jsonData ) else {
            return
        }
        
        if !data.productImages.isEmpty {
            self.productImages = data.productImages
        }
        pageController?.numberOfPages = productImages.count
        
        if !data.packProducts.isEmpty {
            self.packProducts = data.packProducts
        }
        
        if !data.attributesDataArray.isEmpty {
            self.attributesDataArray = data.attributesDataArray
        }
        
        if !data.featuresDataArray.isEmpty {
            self.featuresDataArray = data.featuresDataArray
        }
        
        if !data.combinationsDataArray.isEmpty {
            self.combinationsDataArray = data.combinationsDataArray
        }
        
        if !data.reviewDataArray.isEmpty {
            self.reviewDataArray = data.reviewDataArray
        }
        if !data.custProducts.isEmpty {
            self.custProducts = data.custProducts
        }
        if !data.custReqArray.isEmpty {
            self.custReqArray = data.custReqArray
        }
        if data.productDetail != nil {
            productDetail = data.productDetail
        }
        
        if !data.attachmentDataArray.isEmpty {
            self.attachmentDataArray = data.attachmentDataArray
        }
        
        self.added_in_wishlist = data.added_in_wishlist
        self.reviewEnabled =  data.reviewEnabled
        self.wishlistEnabled = data.wishlistEnabled
        self.average_review = data.average_review
        self.product_link = data.product_link
        self.isPackProduct = data.isPackProduct
        self.SellerAvailable = data.SellerAvailable
        self.sellerDetails = data.sellerDetails
        self.productDescription = data.description
        shortDescription = data.shortDescription
        defaultCode = data.defaultCode
        isCustomizable = data.isCustomizable
        self.inStock = data.inStock
        isVirtual = data.isVirtual
        customization_controller_link = data.customization_controller_link
        displayAddtoCart = data.displayAddtoCart
        completion(true)
    }
    
}

extension ProductPageViewModal: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionCell", for: indexPath as IndexPath) as? BannerCollectionCell {
            //        cell.backgroundColor =  GlobalData.Credentials.defaultBackgroundColor
            cell.bannerImage.setImage(imageUrl: productImages[indexPath.row])
            cell.setNeedsLayout()
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SCREEN_WIDTH, height: SCREEN_WIDTH )
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = ["image": productImages]
        moveControllerDelegate?.moveController(id: "", name: "", dict: dict, jsonData: JSON.null, index: indexPath.row, controller: AllController.zoomController)
    }
}

extension ProductPageViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("sfsjsd", packProducts)
        if let cell = tableView.dequeueReusableCell(withIdentifier: HomeProductBlockTableViewCell.identifier) as? HomeProductBlockTableViewCell {
            cell.tableCollectionView.reloadData()
            cell.viewAllBtn.isHidden = true
            cell.products = packProducts
            cell.titleLabel.text = "packproduct".localized
            cell.selectionStyle = .none
            cell.tableCollectionView.showsHorizontalScrollIndicator = false
            cell.moveDelegate = self
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (SCREEN_WIDTH/2.5 + 90)
    }
}

extension ProductPageViewModal: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageController?.set(progress: Int(scrollView.contentOffset.x / SCREEN_WIDTH), animated: true)
        currentPage = Int(scrollView.contentOffset.x / SCREEN_WIDTH)
    }
    
}
extension ProductPageViewModal: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        moveControllerDelegate?.moveController(id: id, name: name, dict: dict, jsonData: jsonData, index: index, controller: controller)
    }
}

//extension ProductPageViewModal : UITableViewDelegate , UITableViewDataSource {
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return tableArray.count
//    }
//
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
//        cell.textLabel?.text = tableArray[indexPath.row]
//        cell.accessoryType = .disclosureIndicator
//         cell.selectionStyle = .none
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return " "
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 0  &&  self.descExists == true{
//            delegate?.moveToController(id: (productData?.description!)!, name: "", dict: JSON.null, Controller: Controllers.description)
//        }
//        else {
//            delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.reviews)
//        }
//    }
//}
