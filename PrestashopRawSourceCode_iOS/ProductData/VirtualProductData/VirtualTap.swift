//
//  VirtualTap.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON

class VirtualTap: UIView {

    @IBOutlet var virtualView: UIView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var moveDelegate: MoveController?
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let xibView = Bundle.main.loadNibNamed("VirtualTap", owner: self, options: nil)?[0] as? UIView {
            xibView.frame = self.bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(xibView)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        virtualView.isUserInteractionEnabled = true
        virtualView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(statusViewTap)))
        
    }
    
    @objc private func statusViewTap() {
        moveDelegate?.moveController(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.virtualProductDataViewController)
    }

}
