//
//  VirtualProductDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import Photos
import SVProgressHUD
class VirtualProductDataViewController: UIViewController {
    var attachmentDataArray = [AttachmentData]()
    @IBOutlet weak var attachmentTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.title = "Attachments"
        attachmentTableView.register(VirtualDataTableViewCell.nib, forCellReuseIdentifier: VirtualDataTableViewCell.identifier)
        attachmentTableView.delegate = self
        attachmentTableView.dataSource = self
        attachmentTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension VirtualProductDataViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attachmentDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: VirtualDataTableViewCell.identifier) as? VirtualDataTableViewCell {
            cell.item = attachmentDataArray[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let documentsDirectoryURL = try? FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) {
            let fileURL = documentsDirectoryURL.appendingPathComponent(attachmentDataArray[indexPath.row].fileName ?? "")
            print(fileURL)
            if !FileManager.default.fileExists(atPath: fileURL.path) {
                //Create URL to the source file you want to download
                let stringUrl = GlobalConstants.AppCredentials.BASEURL + GlobalConstants.ApiNames.saveAttachment + UrlParameters.width + "&id_attachment=" + attachmentDataArray[indexPath.row].id_attachment + "&ws_key=" + GlobalConstants.AppCredentials.WS_KEY
                print(stringUrl)
                let status = PHPhotoLibrary.authorizationStatus()
                switch status {
                case .authorized:
                    if  let url = URL(string: stringUrl) {
                        do {
                            let largeImageData = try Data(contentsOf: url)
                            print(largeImageData)
                            try largeImageData.write(to: fileURL)
                            if let bannerImage: UIImage = UIImage(data: largeImageData) {
                                UIImageWriteToSavedPhotosAlbum(bannerImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                            } else {
                                PHPhotoLibrary.shared().performChanges({
                                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: fileURL)
                                }) { saved, error in
                                    if saved {
                                        print("Your video was successfully saved")
                                    } else {
                                        print("Not Loaded To Photo Directory")
                                    }
                                }
                            }
                            let AC = UIAlertController(title: "success".localized, message: "yourfilehasbeensavedwouldyouliketosee".localized, preferredStyle: .alert)
                            let okBtn = UIAlertAction(title: "ok".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                if let view = UIStoryboard.loadCmsViewController() {
                                    view.controller = nil
                                    view.url = fileURL
                                    view.titleName = self.attachmentDataArray[indexPath.row].name
                                    self.navigationController?.pushViewController(view, animated: true)
                                }
                            })
                            let noBtn = UIAlertAction(title: "cancel".localized, style: .destructive, handler: nil)
                            AC.addAction(okBtn)
                            AC.addAction(noBtn)
                            self.present(AC, animated: true, completion: nil)
                        } catch {
                            print(error)
                        }
                    }
                case .denied, .restricted :
                    break
                case .notDetermined:
                    PHPhotoLibrary.requestAuthorization { status in
                        switch status {
                        case .authorized:
                            if  let url = URL(string: stringUrl) {
                                do {
                                    let largeImageData = try Data(contentsOf: url)
                                    try largeImageData.write(to: fileURL)
                                    if let bannerImage: UIImage = UIImage(data: largeImageData) {
                                        UIImageWriteToSavedPhotosAlbum(bannerImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
                                    } else {
                                        PHPhotoLibrary.shared().performChanges({
                                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: fileURL)
                                        }) { saved, error in
                                            if saved {
                                                print("Your video was successfully saved")
                                            } else {
                                                print("Not Loaded To Photo Directory")
                                            }
                                        }
                                    }
                                    let AC = UIAlertController(title: "success".localized, message: "yourfilehasbeensavedwouldyouliketosee".localized, preferredStyle: .alert)
                                    let okBtn = UIAlertAction(title: "ok".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                                        if let view = UIStoryboard.loadCmsViewController() {
                                            view.controller = nil
                                            view.url = fileURL
                                            view.titleName = self.attachmentDataArray[indexPath.row].name
                                            self.navigationController?.pushViewController(view, animated: true)
                                        }
                                    })
                                    let noBtn = UIAlertAction(title: "cancel".localized, style: .destructive, handler: nil)
                                    AC.addAction(okBtn)
                                    AC.addAction(noBtn)
                                    self.present(AC, animated: true, completion: nil)
                                } catch {
                                    print(error)
                                }
                            }
                        case .denied, .restricted, .notDetermined:
                            break
                        }
                    }
                }
            } else {
                let AC = UIAlertController(title: "success".localized, message: "yourfilehasbeensavedwouldyouliketosee".localized, preferredStyle: .alert)
                let okBtn = UIAlertAction(title: "ok".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                    if let view = UIStoryboard.loadCmsViewController() {
                        view.controller = nil
                        view.url = fileURL
                        view.titleName = self.attachmentDataArray[indexPath.row].name
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                })
                let noBtn = UIAlertAction(title: "cancel".localized, style: .destructive, handler: nil)
                AC.addAction(okBtn)
                AC.addAction(noBtn)
                self.present(AC, animated: true, completion: nil)
            }
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            print(error)
            SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
            SVProgressHUD.setMinimumDismissTimeInterval(1.0)
//            SVProgressHUD.showError(withStatus: "Not Loaded To Photo Directory")
        } else {
            SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
            SVProgressHUD.setMinimumDismissTimeInterval(1.0)
//            SVProgressHUD.showSuccess(withStatus: "Saved To Photo Library")
        }
    }
    
}
