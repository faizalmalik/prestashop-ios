//
//  ProductBasicDetails.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class ProductBasicDetails: UIView {
    
    @IBOutlet weak var previousBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var wishlistWidth: NSLayoutConstraint!
    @IBOutlet weak var sellerNameHeight: NSLayoutConstraint!
    @IBOutlet weak var sellerNameLabel: UIButton!
    @IBOutlet weak var share: UIButton!
    @IBOutlet weak var wishlistButton: UIButton!
    @IBOutlet weak var discountPriceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var stockLabel: UILabel!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    var passDelegate: PassData?
    var moveDelegate: MoveController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let xibView = Bundle.main.loadNibNamed("ProductBasicDetails", owner: self, options: nil)?[0] as? UIView {
            xibView.frame = self.bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            wishlistButton.shadowBorder1()
            share.shadowBorder1()
            discountPriceLabel.strike(text: "")
            discountPriceLabel.isHidden = false
            self.addSubview(xibView)
        }
    }
    
    @IBAction func sellerNameClicked(_ sender: Any) {
        moveDelegate?.moveController(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.sellerProfile)
    }
    
    @IBAction func wishlistClicked(_ sender: UIButton) {
        passDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.wishlistClicked)
    }
    
    @IBAction func shareClicked(_ sender: Any) {
        passDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.shareClicked)
    }
    
    @IBAction func nextPressed(_ sender: UIButton) {
        passDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.next)
    }
    
    @IBAction func previousPressed(_ sender: UIButton) {
        passDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.previous)
    }
    
}
//
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = true
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 4
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    func shadowBorder1() {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 4
        self.layer.shadowOffset =  CGSize(width: 0, height: 2)
    }
    
    func shadowBorder2() {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 8
        self.layer.shadowOffset =  CGSize(width: 0, height: 2)
        
    }
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
