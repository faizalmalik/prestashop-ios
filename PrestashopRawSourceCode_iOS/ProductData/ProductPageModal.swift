//
//  ProductPageModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

struct ProductPageModal {
    var productImages = [String]()
    
    private let productImagePath = ["product", "image_links", "image_link"]
    private let attributePath = ["product", "attributes", "attribute_set", "attribute_name_value" ]
    private let CombinationPath = [ "product", "attributes", "combinations", "combination"  ]
    private let reviewPath = ["product", "reviews", "review"]
    private let featuresPath = ["product", "features", "feature"]
    private let attachmentPath = ["product", "attachments", "attachment"]
    private let customizablePath = ["product", "customizable_field", "fields"]
    private let displayAddToCartPath = ["product", "display_addtocart"]
    
    var attributesDataArray = [AttributesData]()
    var featuresDataArray = [FeaturesData]()
    var combinationsDataArray = [CombinationsData]()
    var attachmentDataArray = [AttachmentData]()
    var reviewDataArray = [ReviewData]()
    var defaultCode: String!
    var productDetail: ProductBasicDetail!
    var packProducts = [ProductData]()
    var custProducts = [CustomizableProductData]()
    var reviewEnabled: Bool!
    var customization_controller_link: String!
    var wishlistEnabled: Bool!
    var average_review: String!
    var added_in_wishlist: String!
    var product_link: String!
    var module_blockwishlist: String!
    var SellerAvailable: Bool!
    var sellerDetails: ProductSellerDetails!
    var description: String!
    var shortDescription: String!
    var isPackProduct: Bool!
    var isVirtual: Bool!
    var inStock: String!
    var isCustomizable: Bool!
    var custReqArray = [String]()
    var displayAddtoCart: Bool!
    
    init?(data: JSON) {
        if data[productImagePath] != JSON.null {
            if let array = data[productImagePath].array {
                self.productImages = array.map { $0["large"].stringValue }
            } else {
                self.productImages.append(data[productImagePath]["large"].stringValue)
            }
        }
        
        if data[customizablePath] != JSON.null {
            //isCustomizable = true
            if let array = data[customizablePath].array {
                self.custProducts = array.map {  CustomizableProductData( data: $0) }
            } else {
                self.custProducts.append(CustomizableProductData(data: data[customizablePath] ))
            }
            let arr = custProducts.filter({$0.required == "1"}).map({$0.id_customization_field!})
            custReqArray = arr
            print(arr)
            if custReqArray.count > 0 {
                isCustomizable = true
            } else {
                isCustomizable = false
            }
        } else {
            isCustomizable = false
        }
        
        if let data  = data["product"]["pack_products"]["product"] .array {
            self.packProducts = data.map { ProductData( data: $0) }
        } else {
            self.packProducts.append(ProductData(data: data["product"]["pack_products"]["product"] ))
        }
        
        if data[attributePath] != JSON.null {
            if let data  = data[attributePath].array {
                self.attributesDataArray = data.map { AttributesData( data: $0) }
            } else {
                self.attributesDataArray.append(AttributesData(data: data[attributePath]))
            }
        }
        
        if data[CombinationPath] != JSON.null {
            if let data  = data[CombinationPath].array {
                self.combinationsDataArray = data.map { CombinationsData( data: $0) }
            } else {
                self.combinationsDataArray.append(CombinationsData(data: data[CombinationPath]))
            }
        }
        
        if data[featuresPath] != JSON.null {
            if let data  = data[featuresPath].array {
                self.featuresDataArray = data.map { FeaturesData( data: $0) }
            } else {
                self.featuresDataArray.append(FeaturesData(data: data[featuresPath]))
            }
        }
        
        if data[attachmentPath] != JSON.null {
            if let data  = data[attachmentPath].array {
                self.attachmentDataArray = data.map { AttachmentData( data: $0) }
            } else {
                self.attachmentDataArray.append(AttachmentData(data: data[attachmentPath]))
            }
        }
        
        if let index =  combinationsDataArray.index(where: {
            $0.default_on == "1"
        }) {
            defaultCode = combinationsDataArray[index].code
        }
        
        if  data["product"] != JSON.null {
            productDetail = ProductBasicDetail(data: data["product"] )
        }
        
//        if  data["product"]["is_virtual"] != JSON.null {
//            if data["product"]["is_virtual"].stringValue == "1" {
//                isVirtual = true
//            } else {
//                isVirtual = false
//            }
//        }
        
        if attachmentDataArray.count > 0 {
            isVirtual = true
        } else {
            isVirtual = false
        }
        
        if data["stock_management"].stringValue == "1" && data["product"]["quantity"].intValue > 0 {
            inStock = "inStock".localized
        } else if data["stock_management"].stringValue == "0" {
            inStock = ""
        } else  if data["product"]["quantity"].intValue == 0 {
            inStock = "outStock".localized
        }
        
        if data["module_blockwishlist"].stringValue == "1"{
            wishlistEnabled = true
        } else {
            wishlistEnabled = false
        }
        
        if data["product"]["cache_is_pack"].stringValue == "1"{
            isPackProduct = true
        } else {
            isPackProduct = false
        }
        
        if data["module_productcomments"].stringValue == "1"{
            reviewEnabled = true
        } else {
            reviewEnabled = false
        }
        
        if data[reviewPath] != JSON.null {
            if let data  = data[reviewPath].array {
                self.reviewDataArray = data.map { ReviewData( data: $0) }
            } else {
                self.reviewDataArray.append(ReviewData(data: data[reviewPath]))
            }
        }
        
        if data["seller_details"]["id_customer"] != JSON.null {
            SellerAvailable = true
            sellerDetails = ProductSellerDetails(data: data["seller_details"])
        } else {
            SellerAvailable = false
        }
        
        self.average_review = data["product"]["average_review"].string ?? "0"
        customization_controller_link = data["customization_controller_link"].stringValue
        shortDescription = data["product"]["description_short"].stringValue.htmlToString
        self.added_in_wishlist = data["product"]["added_in_wishlist"].stringValue
        self.product_link = data["product_link"].stringValue
        description = data["product"]["description"].stringValue
        displayAddtoCart = data[displayAddToCartPath].stringValue == "1" ? true:false
    }
    
}

struct FeaturesData {
    
    var name: String!
    var value: String!
    var id_feature: String!
    init(data: JSON) {
        name = data["name"].stringValue
        value = data["value"].stringValue
        id_feature = data["id_feature"].stringValue
    }
    
}

struct AttachmentData {
    var name: String!
    var id_attachment: String!
    var file_size: String!
    var description: String!
    var fileName: String!
    init(data: JSON) {
        name = data["name"].stringValue
        id_attachment = data["id_attachment"].stringValue
        file_size = data["file_size"].stringValue
        description = data["description"].stringValue
        fileName = data["file_name"].stringValue
    }
}

struct ProductSellerDetails {

    var shop_name: String!
    var id_customer: String!
    var id_seller: String!
    init(data: JSON) {
        shop_name = data["shop_name"].stringValue
        id_seller = data["id_seller"].stringValue
        id_customer = data["id_customer"].stringValue
    }

}

struct ReviewData {
    var customer_name: String!
     var content: String!
     var date_add: String!
     var title: String!
    var grade: String!

    init(data: JSON) {
        customer_name = data["customer_name"].stringValue
        content = data["content"].stringValue
        date_add = data["date_add"].stringValue
        title = data["title"].stringValue
        grade = data["grade"].stringValue
    }
}

struct ProductBasicDetail {
    var name: String!
    var price: String!
    var show_price: Bool!
    var price_pack: String!
    init(data: JSON) {
         name = data["name"].stringValue
         price = data["price"].stringValue
        price_pack = data ["price_pack"].string ?? data["old_price"].stringValue
        if data["show_price"].stringValue == "1" {
            show_price = true
        } else {
            show_price = false
        }
    }
}

struct AttributesData {
    var attribute_group: String!
    var id_attribute_group: String!
    var is_color_group: String!
    private let attributeValuePath = ["attribute", "values"  ]
    var attributeValues = [AttributesValuesData]()
    init(data: JSON) {

        attribute_group = data["attribute_group"].stringValue
        id_attribute_group = data["id_attribute_group"].stringValue
        is_color_group = data["is_color_group"].stringValue

        if data[attributeValuePath] != JSON.null {
            if let data  = data[attributeValuePath].array {
                self.attributeValues = data.map { AttributesValuesData( data: $0) }
            } else {
                self.attributeValues.append(AttributesValuesData(data: data[attributeValuePath]))
            }
        }

    }

}

struct AttributesValuesData {

    var name: String!
    var id_attribute: String!
    var color: String!
    init(data: JSON) {
        name = data["name"].stringValue
        id_attribute = data["id_attribute"].stringValue
        color = data["color"].string ?? "#ffffff"
    }

}

struct CombinationsData {

    var id_product_attribute: String!
    var id_product: String!
    var price: String!
    var code: String!
    var image_link: String!
    var default_on: String!
    var added_in_wishlist: String!

    init(data: JSON) {
        id_product_attribute = data["id_product_attribute"].stringValue
        id_product = data["id_product"].stringValue
        price = data["price"].stringValue
        code = data["code"].stringValue
        default_on = data["default_on"].stringValue
        added_in_wishlist = data["added_in_wishlist"].stringValue
        
        if data["image_link"] != JSON.null {
            if let data  = data["image_link"].array {
                image_link = data[0]["large"].stringValue
            } else {
                image_link = data["image_link"]["large"].stringValue
            }
        } else {
            image_link = ""
        }
    }

}

struct CustomizableProductData {
    var id_customization_field: String!
    var type: CustomizationType!
    var required: String!
    var name: String!
    var id_lang: String!
    init(data: JSON) {
        id_customization_field = data["id_customization_field"].stringValue
        required = data["required"].stringValue
        if required == "1" {
            name = data["name"].stringValue != "" ? "\(data["name"].stringValue) *":"required".localized
        } else {
            name = data["name"].stringValue
        }
        id_lang = data["id_lang"].stringValue
        if data["type"].stringValue == "1" {
            type = .textField
        } else {
            type = .file
        }
    }
}

enum CustomizationType {
    case file
    case textField
}
