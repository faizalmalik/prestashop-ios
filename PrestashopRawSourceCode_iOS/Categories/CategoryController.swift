//
//  CategoryController.swift
//  Odoo application
//
//  Created by vipin sahu on 9/5/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class CategoryController: UIViewController {
    @IBOutlet weak var categoryTableView: UITableView!

    @IBAction func backpress(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }

    fileprivate let viewModel = CategoryMenuViewModel()
    var categoryData: JSON?
    var name: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
       self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = UIColor.white

        viewModel.reloadSections = { [weak self] (section: Int) in
            self?.categoryTableView?.beginUpdates()
            self?.categoryTableView?.reloadSections([section], with: .fade)
            self?.categoryTableView?.endUpdates()
        }
        viewModel.delegate = self
        categoryTableView?.estimatedRowHeight = 100
        categoryTableView?.rowHeight = UITableView.automaticDimension
        categoryTableView?.sectionHeaderHeight = 70
        categoryTableView?.separatorStyle = .none
        categoryTableView?.dataSource = viewModel
        categoryTableView?.delegate = viewModel
        categoryTableView?.register(CategoryHeadeViewCell.nib, forHeaderFooterViewReuseIdentifier: CategoryHeadeViewCell.identifier)
        self.categoryTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.navigationController?.navigationBar.navigationChanges()
        if((name?.count) ?? 0 > 0) {
            self.navigationItem.title = name
        } else {
//            self.navigationItem.title = categories.localized
        }

        self.navigationItem.title = "categories".localized

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
         viewModel.getValue(categoryData: categoryData ?? JSON.null)
        categoryTableView.reloadData()
    }
    
    override func viewWillLayoutSubviews() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func makeRequest( catId: String, completion: @escaping ((_ data: JSON) -> Void) ) {

        var params = "mobikul/getcategories?"
        params += "&id_category=" + catId
        params += UrlParameters.width
//        let networkManager = NetworkManager()
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.categories, view: self.view) { (responseObject: JSON?, error: Error?) in

            if error != nil {
                print("loginError".localized, error!)

            } else {
                completion(responseObject!)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CategoryController: SubCategoryDelegate {
    func passSubCategoryData(id: String, name: String, hasChild: Bool) {
        
        if hasChild {
            self.makeRequest(catId: id) {
                (_ data: JSON) in                
                if let view = UIStoryboard.loadCategoryViewController() {
                    view.categoryData = data
                    view.name = name
                    view.view.backgroundColor = .white
                    view.isHeroEnabled = true
                    self.navigationController?.pushViewController(view, animated: true)
                }
            }
        } else {
            if let view = UIStoryboard.loadProductCategoryViewController() {
                view.id = id
                view.name = name
                self.navigationController?.pushViewController(view, animated: true)
            }
        }
    }

}
