//
//  CategoryModel.swift
//  Odoo application
//
//  Created by vipin sahu on 9/7/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import Foundation
import SwiftyJSON

class CategoryMenuModel {
    var categories = [Category]()
    init?(data: JSON) {
        
        if let categories = data.array {
            self.categories = categories.map { Category(json: $0 ) }
        } else {
            if data["name"] != JSON.null {
                self.categories.append( Category(json: data ))
            } else {
                ErrorChecking.warningView(message: "nocategories".localized)
            }
        }
    }
}

class Category {
    var name: String?
    var CategoryId: String?
    var isCollapsed = true
    var SubcategoryArray = [SubCategoryData]()
    var json: JSON!
    init(json: JSON) {
        self.name = json["name"].stringValue
        self.CategoryId = json["id_category"].stringValue
        self.json = json
        if json["children"] != JSON.null {
            if let categories = json["children"].array {
                self.SubcategoryArray = categories.map { SubCategoryData(json: $0, check: false ) }
            } else {
                self.SubcategoryArray.append(SubCategoryData(json: json["children"], check: false))
            }
            
            if SubcategoryArray.count > 0 {
                let object = SubCategoryData(json: self.json, check: true)
                SubcategoryArray.insert(object, at: 0)
            }
        }
    }
}
    
class SubCategoryData {
    var name: String?
    var CategoryId: String?
    var containSubCategory: Bool
    var hasChild: Bool
    var jsonData: JSON?
    init(json: JSON, check: Bool) {
        self.name = check ? "\("viewAll".localized) \(json["name"].stringValue)" : "\(json["name"].stringValue)"
        self.CategoryId = json["id_category"].stringValue
        if json["children"].count > 0 {
            self.containSubCategory = true
            self.jsonData = json["children"]
        } else {
            self.containSubCategory = false
            self.jsonData = JSON.null
        }
        if json["has_child"].stringValue == "1" || json["children"].count > 0 {
            self.hasChild = check ? false:true
        } else {
            self.hasChild = false
        }
        
    }
}
