//
//  NewAddressModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

struct NewAddressModal {

    private let countryPath = ["countries", "country"]
     var countryArray = [CountryData]()
    
    var  reqData: AddressReqFields!

    init?(data: JSON) {

        if data[countryPath] != JSON.null {
            if let data  = data[countryPath].array {
                self.countryArray = data.map { CountryData( data: $0) }
            } else {
                self.countryArray.append(CountryData(data: data[countryPath]))
            }
        }
        reqData =   AddressReqFields(data: data["required_fields"])

    }
}

struct CountryData {

    var countryid: String?
    var countryName: String?
    var zip_code_format: String!
    var iso_code: String!
     private let statePath = ["states", "state"]
    var stateArray = [StateData]()
    init(data: JSON) {
        self.countryid = data["id_country"].stringValue
        self.countryName = data["name"].stringValue
        self.zip_code_format = data["zip_code_format"].stringValue
        self.iso_code = data["iso_code"].stringValue
        if data[statePath] != JSON.null {
            if let data  = data[statePath].array {
                self.stateArray = data.map { StateData( data: $0) }
            } else {
                self.stateArray.append(StateData(data: data[statePath]))
            }
        }
    }

}

struct StateData {
    var stateid: String?
    var stateName: String?
        init(data: JSON) {
            self.stateid = data["id_state"].stringValue
            self.stateName = data["name"].stringValue
        }

}

struct AddressReqFields {
    var homePhone: Bool = false
    var mobilePhone: Bool = false
     var company: Bool = false
     var streetAddress1: Bool = false
     var streetAddress2: Bool = false
    var city: Bool = false
    var other: Bool = false
    
    init(data: JSON) {
        if data["phone_config"].stringValue == "1" {
            if data["phone_mobile"].stringValue == "1" {
                mobilePhone = true
            } else {
                mobilePhone = false
            }
            if data["phone"].stringValue == "1" {
                homePhone = true
            } else {
                homePhone = false
            }
        } else {
            homePhone = false
            mobilePhone = false
            
        }
        if data["address1"].stringValue == "1" {
            streetAddress1 = true
        }
        
        if data["address2"].stringValue == "1" {
            streetAddress2 = true
        }
        
        if data["city"].stringValue == "1" {
            city = true
        }
        
        if data["other"].stringValue == "1" {
            other = true
        }
        
    }
  
}

struct AddressFieldData {
    
    var field_name: String!
    var display_name: String!
    var required: Bool!
    var valueEntered: String = ""
    
    init(data: JSON) {
        field_name = data["field_name"].stringValue
        display_name = data["display_name"].stringValue
        required = data["required"].boolValue
        if required {
            display_name += "*"
        }
        valueEntered = data["value"].stringValue
    }
    
}
