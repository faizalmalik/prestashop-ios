//
//  NewAddressDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import IQKeyboardManagerSwift
import ActionSheetPicker_3_0

protocol AddressUpdate {
    func addressupdated(addressId: String)
}

class NewAddressDataViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var citytextField: UITextField!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var streetAddress2Field: UITextField!
    @IBOutlet weak var streetAddress2Label: UILabel!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var companyField: UITextField!
    @IBOutlet weak var stateFieldLabel: UILabel!
    @IBOutlet weak var CountryFieldLabel: UILabel!
    @IBOutlet weak var contactLabel: UILabel!
     @IBOutlet weak var nameLabel: UILabel!
     @IBOutlet weak var phoneLabel: UILabel!
     @IBOutlet weak var addressLabel: UILabel!
     @IBOutlet weak var streetLabel: UILabel!
     @IBOutlet weak var zipLabel: UILabel!
     @IBOutlet weak var countryLabel: UILabel!
     @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var mobilePhnLabel: UILabel!

    @IBOutlet weak var mobilePhmField: UITextField!
    @IBOutlet weak var informationField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var stateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameField: UITextField!
     @IBOutlet weak var phoneField: UITextField!
     @IBOutlet weak var streetField: UITextField!
     @IBOutlet weak var zipField: UITextField!
     @IBOutlet weak var countryView: UIView!
     @IBOutlet weak var stateFieldView: UIView!
    @IBOutlet weak var saveButton: UIButton!
    
    var need_zip_code: Bool = false

    var delete = false

    var countryArray = [CountryData]()
     var countryRow = 0
    var countries = [JSON]()
    var countryId: String?
    var StateId: String?
    var stateRow  = 0
    var addressId: String?
    var delegate: AddressUpdate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        countryId = ""
        StateId = ""
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.stateView.isHidden = true
         contactLabel.SetDefaultTextColor()
         nameLabel.SetDefaultTextColor()
         phoneLabel.SetDefaultTextColor()
         addressLabel.SetDefaultTextColor()
         streetLabel.SetDefaultTextColor()
         zipLabel.SetDefaultTextColor()
         countryLabel.SetDefaultTextColor()
         stateLabel.SetDefaultTextColor()
        saveButton.SetAccentBackColor()
        mobilePhnLabel.SetDefaultTextColor()
        companyLabel.SetDefaultTextColor()
        informationLabel.SetDefaultTextColor()
         titleLabel.SetDefaultTextColor()
        lastNameLabel.SetDefaultTextColor()
        streetAddress2Label.SetDefaultTextColor()
        cityLabel.SetDefaultTextColor()
        self.navigationItem.title = "address".localized

        nameLabel.text = "firstName".localized
        lastNameLabel.text = "lastName".localized
        phoneLabel.text = "homePhone".localized
          mobilePhnLabel.text = "mobilePhone".localized
        addressLabel.text = "address".localized
        streetLabel.text = "streetAddress".localized
         streetAddress2Label.text = "streetAddress2".localized
         cityLabel.text = "city".localized
        zipLabel.text = "zip".localized
        countryLabel.text = "country".localized
        stateLabel.text = "state".localized
        informationLabel.text = "additionalInf".localized
        titleLabel.text = "titleInf".localized
        saveButton.setTitle("saveAddress".localized, for: .normal)
        companyLabel.text = "company".localized
        contactLabel.text = "contactInformation".localized
        zipField.keyboardType = .numberPad
        phoneField.keyboardType = .numberPad
         mobilePhmField.keyboardType = .numberPad

        let borderColor: UIColor? = UIColor(red: 204.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
        countryView.layer.borderColor = borderColor?.cgColor
        countryView.layer.borderWidth = 1.0
        countryView.layer.cornerRadius = 5.0
        stateFieldView.layer.borderColor = borderColor?.cgColor
        stateFieldView.layer.borderWidth = 1.0
        stateFieldView.layer.cornerRadius = 5.0
        stateViewHeight.constant = 0
//        countryView.applyBorder(colours: GlobalData.Credentials.textFieldBorder)
//        stateFieldView.applyBorder(colours: GlobalData.Credentials.textFieldBorder)

        if addressId != nil {
            self.makeRequest(dict: [:], call: WhichApiCall.editAddress, verb: RequestType.get)
        } else {
            
            self.makeRequest(dict: [:], call: WhichApiCall.none, verb: RequestType.get)
            self.nameField.text =   UserDefaults.standard.value(forKey: UserDefaults.Keys.firstname) as? String
            self.lastNameField.text =   UserDefaults.standard.value(forKey: UserDefaults.Keys.lastname) as? String
            
        }
    }

    override func viewWillAppear(_ animated: Bool) {
         UIBarButtonItem.appearance().tintColor = UIColor.black
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
    }

    override func viewWillDisappear(_ animated: Bool) {
         UIBarButtonItem.appearance().tintColor = UIColor.white
         IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
    }

    func makeRequest(dict: [String: Any], call: WhichApiCall, verb: RequestType) {
        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get
        switch call {
            
        case .saveAddress:
            params += GlobalConstants.ApiNames.saveAddress
            verbs = verb
            newDict[GlobalConstants.ApiNames.saveAddress.FetchApiName()] = dict
        case .editAddress:
            params += GlobalConstants.ApiNames.editAddress + addressId!
        default:
            params += GlobalConstants.ApiNames.getCountries
            verbs = .get
        }
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.addAddress, view: self.view) { (responseObject: JSON?, error: Error?) in
            
            if error != nil {
                
                print("loginError".localized, error!)
                
            } else {
                switch call {
                case .saveAddress:
                    
                    if ErrorChecking.getData(data: responseObject) != JSON.null, let data = ErrorChecking.getData(data: responseObject) {
                        self.delegate?.addressupdated(addressId: data["addresses"]["address"]["id"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                case .editAddress:
                    print()
                    
                    if let data = responseObject {
                        let newData = data["address"]
                        self.nameField.text = newData["firstname"].stringValue
                        self.lastNameField.text = newData["lastname"].stringValue
                        
                        self.phoneField.text = newData["phone"].stringValue
                        self.mobilePhmField.text = newData["phone_mobile"].stringValue
                        self.companyField.text = newData["company"].stringValue
                        self.streetField.text = newData["address1"].stringValue
                        self.streetAddress2Field.text = newData["address2"].stringValue
                        self.citytextField.text = newData["city"].stringValue
                        //                        self.CountryFieldLabel.text = newData["lastname"].stringValue
                        //                        self.stateFieldLabel.text = newData["lastname"].stringValue
                        
                        self.countryId = newData["id_country"].stringValue
                        self.StateId = newData["id_state"].stringValue
                        self.informationField.text = newData["other"].stringValue
                        self.titleTextField.text = newData["alias"].stringValue
                        self.zipField.text = newData["postcode"].stringValue
                        self.makeRequest(dict: [:], call: WhichApiCall.none, verb: RequestType.get)
                    }
                    
                default:
                    self.setCountries(jsonData: responseObject!) {
                        
                        (data: Bool) in
                        
                    }
                }
            }
        }
    }
    
    var newAddressModal: NewAddressModal!

     func setCountries( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = NewAddressModal(data: jsonData ) else {
            return
        }
        newAddressModal = data
        if !data.countryArray.isEmpty {
            self.countryArray = data.countryArray
             if let index = self.countryArray.index( where: { $0.countryid == countryId }) {
                countryRow = index
                 if let index = self.countryArray[countryRow].stateArray.index( where: { $0.stateid == StateId }) {
                     stateRow = index
                }
            }

              self.CountryFieldLabel.text = self.countryArray[countryRow].countryName!
                self.countryId = self.countryArray[countryRow].countryid!

            if self.countryArray[countryRow].stateArray.count > 0 {
                    self.stateFieldLabel.text = self.countryArray[countryRow].stateArray[stateRow].stateName!
                    self.StateId = self.countryArray[countryRow].stateArray[stateRow].stateid!
                    self.stateView.isHidden = false
                    self.stateViewHeight.constant = 72
                }

              completion(true)
        } else {
            completion(false)
        }

            if self.countries.isEmpty {

            } else {

            }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func CountryTap(_ sender: UITapGestureRecognizer) {
        if countryArray.count > 0 {
            let picker = ActionSheetStringPicker(title: "countries".localized, rows: countryArray.map {$0.countryName! }, initialSelection: countryRow, doneBlock: { _, indexes, values in
                self.countryId = self.countryArray[indexes].countryid!
                self.countryRow = indexes
                self.CountryFieldLabel.text = values! as? String
                self.stateRow = 0
                if self.countryArray[indexes].stateArray.count > 0 {
                    self.stateView.isHidden = false
                    self.stateViewHeight.constant = 72
                    self.stateFieldLabel.text = self.countryArray[indexes].stateArray[0].stateName!
                    self.StateId =  self.countryArray[indexes].stateArray[0].stateid!
                } else {
                    self.StateId = nil
                    self.stateFieldLabel.text = ""
                    self.stateView.isHidden = true
                    self.stateViewHeight.constant = 0
                }
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.view ?? self.view)
            picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
            picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
            picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
            picker?.show()
        } else {
            
        }

    }

    @IBAction func StateTap(_ sender: UITapGestureRecognizer) {
        print(stateRow)
        let picker = ActionSheetStringPicker(title: "states".localized, rows: countryArray[countryRow].stateArray.map {$0.stateName!}, initialSelection: stateRow, doneBlock: { _, indexes, values in
            self.stateFieldLabel.text = values! as? String
            self.stateRow = indexes
            self.StateId =  self.countryArray[self.countryRow].stateArray[indexes].stateid!
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.view ?? self.view)
        picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
        picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
        picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
        picker?.show()
    }

    @IBAction func saveClicked(_ sender: UIButton) {

        var value = 0
        var errorMessage = ""
        
        let zipCode = zipValidator(zipcode: self.countryArray[countryRow].zip_code_format, isoCode: self.countryArray[countryRow].iso_code)
        
        let zipTest = NSPredicate(format: "SELF MATCHES %@", zipCode)
        
        let reqField = newAddressModal.reqData!

        if (nameField.text?.isEmpty)! {
            value = 1
            errorMessage = "firstNameValidate".localized
            nameField.shake()
        } else if ErrorChecking.isStringContainDigits(text: nameField.text) {
            value = 1
            errorMessage = "firstNotValid".localized
             nameField.shake()
        } else if (lastNameField.text?.isEmpty)! {
            value = 1
            errorMessage = "lastNameValidate".localized
             lastNameField.shake()
        } else if ErrorChecking.isStringContainDigits(text: lastNameField.text) {
            value = 1
            errorMessage = "lastNotValid".localized
             lastNameField.shake()
        } else if reqField.homePhone && (phoneField.text?.isEmpty)! {
            value = 1
            errorMessage = "homeNotValid".localized
             phoneField.shake()
        } else if reqField.mobilePhone && (mobilePhmField.text?.isEmpty)! {
            value = 1
            errorMessage = "mobileReq".localized
             mobilePhmField.shake()
        } else if reqField.streetAddress1 && (streetField.text?.isEmpty)! && streetField.text!.isEmptyOrWhitespace() {
            value = 1
            errorMessage = "streetReq".localized
             streetField.shake()
        } else if reqField.streetAddress2 && (streetAddress2Field.text?.isEmpty)! && streetAddress2Field.text!.isEmptyOrWhitespace() {
            value = 1
            errorMessage = "streetValid".localized
             streetAddress2Field.shake()
        } else if reqField.city && (citytextField.text?.isEmpty)! {
            value = 1
            errorMessage = "cityReq".localized
             citytextField.shake()
        } else if need_zip_code  && (zipField.text?.isEmpty)! {
            value = 1
            errorMessage = "zipReq".localized
             zipField.shake()
        } else if zipCode.count > 0  && !zipTest.evaluate(with: zipField.text) {
           value = 1
            errorMessage = "invalidZip".localized
             zipField.shake()
        } else if reqField.other && (informationField.text?.isEmpty)! {
            value = 1
            errorMessage = "additionalReq".localized
             informationField.shake()
        } else if (titleTextField.text?.isEmpty)! {
            value = 1
            errorMessage = "titleReq".localized
             titleTextField.shake()
        }

        if value == 1 {
            ErrorChecking.warningView(message: errorMessage)
//            self.alert(message: errorMessage, title: "Error")
        } else {
            var dict = [String: Any]()

            dict["firstname"] = nameField.text
            dict["lastname"] = lastNameField.text
            dict["company"] = companyField.text!
            dict["postcode"] = zipField.text
            
            let streetFieldTrimmedString = streetField.text!.trimmingCharacters(in: .whitespaces)
            dict["address1"] = streetFieldTrimmedString
        
            let streetAddress2FieldTrimmedString = streetAddress2Field.text!.trimmingCharacters(in: .whitespaces)
            dict["address2"] = streetAddress2FieldTrimmedString
            
            dict["city"] = citytextField.text
            dict["phone"] = phoneField.text
            dict["phone_mobile"] = mobilePhmField.text
            dict["id_country"] = countryId
            dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            dict["alias"] = titleTextField.text
            dict["other"] = informationField.text

            if StateId != nil {
                if !(StateId?.isEmpty)! {
                    dict["id_state"] = StateId!
                }
            }
            if addressId != nil {
                dict["id"] = addressId
                 self.makeRequest(dict: dict, call: .saveAddress, verb: RequestType.put)
            } else {
                self.makeRequest(dict: dict, call: .saveAddress, verb: RequestType.post)
            }
        }

    }
    
    func zipValidator(zipcode: String, isoCode: String) -> (String) {
        
        if(zipcode.count > 0) {
            var value = zipcode
            value = value.replacingOccurrences(of: " ", with: "( |)")
            value = value.replacingOccurrences(of: "-", with: "(-|)")
            value = value.replacingOccurrences(of: "N", with: "[0-9]")
            value = value.replacingOccurrences(of: "L", with: "[a-zA-Z]")
            value = value.replacingOccurrences(of: "C", with: isoCode)
            return value
        } else {
            return ""
        }
        
    }

    @IBAction func DeleteClicked(_ sender: UIButton) {
//        self.makeRequest(dict: [:], call: .deleteAddress)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension String {
    func isEmptyOrWhitespace() -> Bool {
        
        if(self.isEmpty) {
            return true
        }
        
        return (self.trimmingCharacters(in: NSCharacterSet.whitespaces) == "")
    }
}
