//
//  AddAddressViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import IQKeyboardManagerSwift
import ActionSheetPicker_3_0

class AddAddressViewController: UIViewController {
    
    @IBOutlet weak var address_tableView: UITableView!
    @IBOutlet weak var saveAddressBtn: UIButton!
    
    var addAddessViewModel = AddAddessViewModel()
    var addressId: String?
    var newAddressModal: NewAddressModal!
    var countryArray = [CountryData]()
    var countryRow = 0
    var countries = [JSON]()
    var countryId: String?
    var StateId: String?
    var stateRow  = 0
    var delegate: AddressUpdate?
    var isStateHidden: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "address".localized
        saveAddressBtn.SetAccentBackColor()
        saveAddressBtn.setTitle("saveAddress".localized, for: .normal)
        
        self.makeRequest(dict: [:], call: WhichApiCall.getAddressFields, verb: RequestType.get)
        
    }
    
    func makeRequest(dict: [String: Any], call: WhichApiCall, verb: RequestType) {
        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get
        switch call {
        case .getAddressFields:
            params += GlobalConstants.ApiNames.getAddressFields
            if let address = addressId {
                params += "&id_address=\(address)"
            }            
            verbs = verb
            newDict[GlobalConstants.ApiNames.getAddressFields.FetchApiName()] = []
        case .saveAddress:
            params += GlobalConstants.ApiNames.saveAddress
            verbs = verb
            newDict[GlobalConstants.ApiNames.saveAddress.FetchApiName()] = dict
        case .editAddress:
            params += GlobalConstants.ApiNames.editAddress// + addressId!
            if let address = addressId {
                //params += "&id_address=\(address)"
                params += "\(address)?"
            }
        default:
            params += GlobalConstants.ApiNames.getCountries
            verbs = .get
        }
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.addAddress, view: self.view) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                print("loginError".localized, error!)
            } else {
                switch call {
                case .getAddressFields:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, let data = ErrorChecking.getData(data: responseObject) {
                        self.addAddessViewModel.getValue(jsonData: data, completion: { (check) in
                            if check {
                                self.address_tableView.register(AddressSelectionTableViewCell.nib, forCellReuseIdentifier: AddressSelectionTableViewCell.identifier)
                                self.address_tableView.register(AddressFieldTableViewCell.nib, forCellReuseIdentifier: AddressFieldTableViewCell.identifier)
                                self.addAddessViewModel.isStateHidden = self.isStateHidden
                                self.addAddessViewModel.delegate = self
                                self.address_tableView.delegate = self.addAddessViewModel
                                self.address_tableView.dataSource = self.addAddessViewModel
                                self.address_tableView.reloadData()
                            }
                        })
                        if self.addressId != nil {
                            self.makeRequest(dict: dict, call: .editAddress, verb: RequestType.put)
                        } else {
                            self.makeRequest(dict: [:], call: WhichApiCall.none, verb: RequestType.get)
                        }
                    }
                case .saveAddress:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, let data = ErrorChecking.getData(data: responseObject) {
                        self.delegate?.addressupdated(addressId: data["addresses"]["address"]["id_address"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                case .editAddress:
                    if let data = responseObject {
                        print(data)
                        if data["address"].dictionary != nil {
                            for i in 0..<self.addAddessViewModel.addressData.count {
                                let item = self.addAddessViewModel.addressData[i]
                                for editItem in (data["address"].dictionary?.keys)! {
                                    if item.field_name == editItem {
                                        switch editItem {
                                        case "id_country":
                                            self.countryId = data["address"][editItem].stringValue
                                        case "id_state":
                                            self.StateId = data["address"][editItem].stringValue
                                        default:
                                            self.addAddessViewModel.addressData[i].valueEntered = data["address"][editItem].stringValue
                                        }
                                    }
                                }
                            }
                            self.makeRequest(dict: [:], call: WhichApiCall.none, verb: RequestType.get)
                        }
                        self.address_tableView.reloadData()
                    }
                default:
                    self.setCountries(jsonData: responseObject!) {
                        (data: Bool) in
                        self.address_tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func setCountries( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = NewAddressModal(data: jsonData ) else {
            return
        }
        newAddressModal = data
        if !data.countryArray.isEmpty {
            self.countryArray = data.countryArray
            if let index = self.countryArray.index( where: { $0.countryid == countryId }) {
                countryRow = index
                if let index = self.countryArray[countryRow].stateArray.index( where: { $0.stateid == StateId }) {
                    stateRow = index
                }
            }
            //self.CountryFieldLabel.text = self.countryArray[countryRow].countryName!
            if let index = self.addAddessViewModel.addressData.index(where: {$0.field_name == "id_country"}) {
                self.addAddessViewModel.addressData[index].valueEntered = self.countryArray[countryRow].countryName!
            }
            self.countryId = self.countryArray[countryRow].countryid!
            if self.countryArray[countryRow].stateArray.count > 0 {
                //self.stateFieldLabel.text = self.countryArray[countryRow].stateArray[stateRow].stateName!
                if let index = self.addAddessViewModel.addressData.index(where: {$0.field_name == "id_state"}) {
                    self.addAddessViewModel.addressData[index].valueEntered = self.countryArray[countryRow].stateArray[stateRow].stateName!
                }
                self.StateId = self.countryArray[countryRow].stateArray[stateRow].stateid!
                self.isStateHidden = false
            } else {
                self.isStateHidden = true
            }
            self.addAddessViewModel.isStateHidden = self.isStateHidden
            completion(true)
        } else {
            completion(false)
        }
    }
    
    func zipValidator(zipcode: String, isoCode: String) -> (String) {
        if zipcode.count > 0 {
            var value = zipcode
            value = value.replacingOccurrences(of: " ", with: "( |)")
            value = value.replacingOccurrences(of: "-", with: "(-|)")
            value = value.replacingOccurrences(of: "N", with: "[0-9]")
            value = value.replacingOccurrences(of: "L", with: "[a-zA-Z]")
            value = value.replacingOccurrences(of: "C", with: isoCode)
            return value
        } else {
            return ""
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapSaveAddressBtn(_ sender: UIButton) {
        
        var value = 0
        var errorMessage = ""
        var dict = [String: Any]()
        let zipCode = zipValidator(zipcode: self.countryArray[countryRow].zip_code_format, isoCode: self.countryArray[countryRow].iso_code)
        let zipTest = NSPredicate(format: "SELF MATCHES %@", zipCode)
        
        if self.addAddessViewModel.addressData.count != 0 {
            for i in 0..<self.addAddessViewModel.addressData.count {
                let item = self.addAddessViewModel.addressData[i]
                if item.required {
                    switch item.field_name {
                    case "id_country":
                        let indexPath = IndexPath(row: i, section: 0)
                        if let cell = self.address_tableView.cellForRow(at: indexPath) as? AddressSelectionTableViewCell {
                            if item.valueEntered == "" || self.countryId == nil || self.countryId == "" {
                                value = 1
                                errorMessage = item.display_name + "isrquired".localized
                                ErrorChecking.warningView(message: errorMessage)
                                cell.value_view.shake()
                            } else {
                                dict[item.field_name] = self.countryId
                            }
                        } else {
                            if item.valueEntered == "" || self.countryId == nil || self.countryId == "" {
                                value = 1
                                errorMessage = item.display_name + "isrquired".localized
                                ErrorChecking.warningView(message: errorMessage)
                            } else {
                                dict[item.field_name] = self.countryId
                            }
                        }
                    case "id_state":
                        let indexPath = IndexPath(row: i, section: 0)
                        if let cell = self.address_tableView.cellForRow(at: indexPath) as? AddressSelectionTableViewCell {
                            if item.valueEntered == "" || self.StateId == nil || self.StateId == "" {
                                value = 1
                                errorMessage = item.display_name + "isrquired".localized
                                ErrorChecking.warningView(message: errorMessage)
                                cell.value_view.shake()
                            } else {
                                dict[item.field_name] = self.StateId
                            }
                        } else {
                            if item.valueEntered == "" || self.StateId == nil || self.StateId == "" {
                                value = 1
                                errorMessage = item.display_name + "isrquired".localized
                                ErrorChecking.warningView(message: errorMessage)
                            } else {
                                dict[item.field_name] = self.StateId
                            }
                        }
                    case "postcode":
                        let indexPath = IndexPath(row: i, section: 0)
                        if let cell = self.address_tableView.cellForRow(at: indexPath) as? AddressFieldTableViewCell {
                            if item.valueEntered == "" {
                                value = 1
                                errorMessage = item.display_name + "isrquired".localized
                                ErrorChecking.warningView(message: errorMessage)
                                cell.value_txtfld.shake()
                            } else if !zipTest.evaluate(with: item.valueEntered) && self.countryArray[countryRow].zip_code_format != ""{
                                value = 1
                                errorMessage = "Invalid Zip code"
                                ErrorChecking.warningView(message: errorMessage)
                                cell.value_txtfld.shake()
                            } else {
                                dict[item.field_name] = item.valueEntered
                            }
                        } else {
                            if item.valueEntered == "" {
                                value = 1
                                errorMessage = item.display_name + "isrquired".localized
                                ErrorChecking.warningView(message: errorMessage)
                            } else if !zipTest.evaluate(with: item.valueEntered) && self.countryArray[countryRow].zip_code_format != ""{
                                value = 1
                                errorMessage = "Invalid Zip code"
                                ErrorChecking.warningView(message: errorMessage)
                            } else {
                                dict[item.field_name] = item.valueEntered
                            }
                        }
                    default:
                        let indexPath = IndexPath(row: i, section: 0)
                        if let cell = self.address_tableView.cellForRow(at: indexPath) as? AddressFieldTableViewCell {
                            if item.valueEntered.replacingOccurrences(of: " ", with: "") == "" {
                                value = 1
                                errorMessage = item.display_name + "isrquired".localized
                                cell.value_txtfld.shake()
                                ErrorChecking.warningView(message: errorMessage)
                            } else {
                                dict[item.field_name] = item.valueEntered
                            }
                        } else {
                            if item.valueEntered.replacingOccurrences(of: " ", with: "") == "" {
                                value = 1
                                errorMessage = item.display_name + "isrquired".localized
                                ErrorChecking.warningView(message: errorMessage)
                            } else {
                                dict[item.field_name] = item.valueEntered
                            }
                        }
                    }
                    if value != 0 {
                        break
                    }
                } else {
                    switch item.field_name {
                    case "id_country":
                        dict[item.field_name] = self.countryId ?? ""
                    case "id_state":
                        dict[item.field_name] = self.StateId ?? ""
                    case "postcode":
                        if self.countryArray[countryRow].zip_code_format != "" {
                            let indexPath = IndexPath(row: i, section: 0)
                            if let cell = self.address_tableView.cellForRow(at: indexPath) as? AddressFieldTableViewCell {
                                if item.valueEntered != "" && !zipTest.evaluate(with: item.valueEntered) {
                                    value = 1
                                    errorMessage = "Invalid Zip code"
                                    ErrorChecking.warningView(message: errorMessage)
                                    cell.value_txtfld.shake()
                                } else {
                                    dict[item.field_name] = item.valueEntered
                                }
                            } else {
                                if item.valueEntered != "" && !zipTest.evaluate(with: item.valueEntered) {
                                    value = 1
                                    errorMessage = "Invalid Zip code"
                                    ErrorChecking.warningView(message: errorMessage)
                                } else {
                                    dict[item.field_name] = item.valueEntered
                                }
                            }
                        } else {
                            dict[item.field_name] = item.valueEntered
                        }
                    default:
                        dict[item.field_name] = item.valueEntered
                    }
                }
            }
        }
        
        print(dict)
        
        if value == 0 {
            if FetchDetails.isLoggedIn() || FetchDetails.isGuestLoggedIn() {
                dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            }
            if addressId != nil {
                dict["id_address"] = addressId
                self.makeRequest(dict: dict, call: .saveAddress, verb: RequestType.put)
            } else {
                self.makeRequest(dict: dict, call: .saveAddress, verb: RequestType.post)
            }
        }
    }
}

extension AddAddressViewController: OpenValuePicker {
    
    func SelectValue(index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        if let cell = self.address_tableView.cellForRow(at: indexPath) as? AddressSelectionTableViewCell {
            switch self.addAddessViewModel.addressData[index].field_name {
            case "id_country":
                if countryArray.count > 0 {
                    let picker = ActionSheetStringPicker(title: "countries".localized, rows: countryArray.map {$0.countryName! }, initialSelection: countryRow, doneBlock: { _, indexes, values in
                        self.countryId = self.countryArray[indexes].countryid!
                        self.countryRow = indexes
                        if let index = self.addAddessViewModel.addressData.index( where: { $0.field_name == "id_country" }) {
                            self.addAddessViewModel.addressData[index].valueEntered = (values as? String) ?? ""
                        }
                        if self.countryArray[indexes].stateArray.count > 0 {
                            self.isStateHidden = false
                            self.stateRow = 0
                            self.StateId =  self.countryArray[indexes].stateArray[0].stateid!
                            if let index = self.addAddessViewModel.addressData.index( where: { $0.field_name == "id_state" }) {
                                self.addAddessViewModel.addressData[index].valueEntered = self.countryArray[indexes].stateArray[0].stateName ?? ""
                            }
                        } else {
                            self.StateId = nil
                            self.isStateHidden = true
                        }
                        self.addAddessViewModel.isStateHidden = self.isStateHidden
                        self.address_tableView.reloadData()
                        return
                    }, cancel: { ActionStringCancelBlock in return }, origin: cell)
                    picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
                    picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
                    picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
                    picker?.show()
                }
            case "id_state":
                if self.countryArray[self.countryRow].stateArray.count > 0 {
                    let picker = ActionSheetStringPicker(title: "states".localized, rows: countryArray[countryRow].stateArray.map {$0.stateName!}, initialSelection: stateRow, doneBlock: { _, indexes, values in
                        if let index = self.addAddessViewModel.addressData.index( where: { $0.field_name == "id_state" }) {
                            self.addAddessViewModel.addressData[index].valueEntered = (values as? String) ?? ""
                        }
                        self.stateRow = indexes
                        self.StateId =  self.countryArray[self.countryRow].stateArray[indexes].stateid!
                        self.addAddessViewModel.isStateHidden = self.isStateHidden
                        self.address_tableView.reloadData()
                        return
                    }, cancel: { ActionStringCancelBlock in return }, origin: cell)
                    picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
                    picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
                    picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
                    picker?.show()
                }
            default:
                break
            }
        }
    }
}
