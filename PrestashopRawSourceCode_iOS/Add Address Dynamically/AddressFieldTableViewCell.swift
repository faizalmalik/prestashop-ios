//
//  AddressFieldTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class AddressFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var title_lbl: UILabel!
    @IBOutlet weak var value_txtfld: UITextField!
    
    weak var delegate: OpenValuePicker?
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        value_txtfld.applyBorder(colours: UIColor.lightGray)
        value_txtfld.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item: AddressFieldData! {
        didSet {
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                title_lbl.textAlignment = .right
                value_txtfld.textAlignment = .right
            } else {
                title_lbl.textAlignment = .left
                value_txtfld.textAlignment = .left
            }
            title_lbl.text = item.display_name
            value_txtfld.text = item.valueEntered
            if item.field_name == "phone" {
                value_txtfld.keyboardType = .numberPad
            } else {
                value_txtfld.keyboardType = .default
            }
        }
    }
    
    @IBAction func valueChanged(_ sender: UITextField) {
        if let value = sender.text {
            delegate?.PassValue!(index: sender.tag, value: value)
        }
    }
    
}
