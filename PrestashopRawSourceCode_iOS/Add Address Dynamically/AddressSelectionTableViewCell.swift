//
//  AddressSelectionTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class AddressSelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var title_lbl: UILabel!
    @IBOutlet weak var value_view: UIView!
    @IBOutlet weak var value_lbl: UILabel!
    @IBOutlet weak var value_btn: UIButton!
    
    weak var delegate: OpenValuePicker?
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        value_view.applyBorder(colours: UIColor.lightGray)
        value_view.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var item: AddressFieldData! {
        didSet {
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                title_lbl.textAlignment = .right
                value_lbl.textAlignment = .right
            } else {
                title_lbl.textAlignment = .left
                value_lbl.textAlignment = .left
            }
            title_lbl.text = item.display_name
            value_lbl.text = item.valueEntered
        }
    }
    
    @IBAction func tapValueBtn(_ sender: UIButton) {
        delegate?.SelectValue!(index: sender.tag)
    }
    
}
