//
//  AddAddessViewModel.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

@objc protocol OpenValuePicker: class {
    @objc optional func SelectValue(index: Int)
    @objc optional func PassValue(index: Int, value: String)
}

class AddAddessViewModel: NSObject {
    
    var addressData = [AddressFieldData]()
    var isStateHidden: Bool = false
    weak var delegate: OpenValuePicker?
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = AddAddressModel(data: jsonData) else {
            return
        }
        addressData.removeAll()
        if !data.addressData.isEmpty {
            addressData = data.addressData
            completion(true)
        } else {
            completion(false)
        }
    }
}

extension AddAddessViewModel: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addressData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        switch self.addressData[indexPath.row].field_name {
        case "id_country":
            if let cell = tableView.dequeueReusableCell(withIdentifier: AddressSelectionTableViewCell.identifier) as? AddressSelectionTableViewCell {
                cell.selectionStyle = .none
                cell.value_btn.tag = indexPath.row
                cell.delegate = delegate
                cell.item = self.addressData[indexPath.row]
                return cell
            }
        case "id_state":
            if let cell = tableView.dequeueReusableCell(withIdentifier: AddressSelectionTableViewCell.identifier) as? AddressSelectionTableViewCell, !isStateHidden {
                cell.selectionStyle = .none
                cell.value_btn.tag = indexPath.row
                cell.delegate = delegate
                cell.item = self.addressData[indexPath.row]
                return cell
            }
        default:
            if let cell = tableView.dequeueReusableCell(withIdentifier: AddressFieldTableViewCell.identifier) as? AddressFieldTableViewCell {
                cell.selectionStyle = .none
                cell.item = self.addressData[indexPath.row]
                cell.value_txtfld.tag = indexPath.row
                cell.delegate = self
                return cell
            }
        }        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.addressData[indexPath.row].field_name == "id_state" && isStateHidden {
            return 0
        } else {
            return UITableView.automaticDimension
        }
    }
    
}

extension AddAddessViewModel: OpenValuePicker {

    func PassValue(index: Int, value: String) {
        self.addressData[index].valueEntered = value
    }
}
