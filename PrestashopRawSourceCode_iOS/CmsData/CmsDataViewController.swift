//
//  CmsDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON

class CmsDataViewController: UIViewController {

    @IBOutlet weak var cmsWebView: UIWebView!
    @IBOutlet weak var shareBtn: UIBarButtonItem!
    var controller: CmsControllers?
    var emptyView: EmptyView!
    var url: URL!
    var titleName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        if controller != nil {
            shareBtn.isEnabled = false
            shareBtn.image = UIImage(named: "")
            self.makeRequest()
        } else if let url = url {
            shareBtn.isEnabled = true
            cmsWebView.scalesPageToFit = true
            self.cmsWebView.loadRequest(URLRequest(url: url))
        }
        self.navigationItem.title = titleName
        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
    }

    func makeRequest() {
        var params = ""
        
        switch controller {
        case CmsControllers.aboutUs?:
            self.navigationItem.title = "aboutUs".localized
            params += GlobalConstants.ApiNames.aboutUs
        case CmsControllers.privacyPolicy?:
            self.navigationItem.title = "privacyPolicy".localized
            params += GlobalConstants.ApiNames.privacyPolicy
        case CmsControllers.support?:
            self.navigationItem.title = "support".localized
            params += GlobalConstants.ApiNames.support
        case CmsControllers.termsCondition?:
            self.navigationItem.title = "termsCondition".localized
            
            params += GlobalConstants.ApiNames.terms
            
        case .none:
            print()
        }
        params += UrlParameters.width
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                
            } else {
                
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                if let data = responseObject {
                    var stringData = ""
                    if data["content"].stringValue == "0"{
                        stringData = "noData".localized
                    } else {
                        stringData = data["content"].stringValue + "<style>img{display: inline;height: auto;max-width: 100%;}</style>"
                    }
                    
                    self.cmsWebView.loadHTMLString(stringData, baseURL: nil)
                }
            }
        }
    }

@IBAction func ShareClicked(_ sender: Any) {
    do {
        let largeImageData = try Data(contentsOf: (url))
        let activityItems = [largeImageData]
        let activityController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        if UI_USER_INTERFACE_IDIOM() == .phone {
            self.present(activityController, animated: true, completion: { })
        } else {
            let popup = UIPopoverController(contentViewController: activityController)
            popup.present(from: CGRect(x: CGFloat(self.view.frame.size.width / 2), y: CGFloat(self.view.frame.size.height / 4), width: CGFloat(0), height: CGFloat(0)), in: self.view, permittedArrowDirections: .any, animated: true)
        }
    } catch {
        
    }
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CmsDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest()
    }
}
