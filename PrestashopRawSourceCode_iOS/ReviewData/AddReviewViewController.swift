//
//  AddReviewViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import HCSStarRatingView
import SwiftyJSON

class AddReviewViewController: UIViewController {
    var reviewStarValue = 0
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var reviewtextArea: UITextView!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var starView: HCSStarRatingView!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var youAreViewing: UILabel!

    var PName: String?
    var templateId: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        submitButton.SetAccentBackColor()
        youAreViewing.SetDefaultTextColor()
        ratingLabel.SetDefaultTextColor()
        nameLabel.SetDefaultTextColor()
        reviewLabel.SetDefaultTextColor()
        self.navigationItem.title = "addReview".localized
        starView.tintColor = UIColor().HexToColor(hexString: "FFB926")
        starView.isUserInteractionEnabled = true
        let borderColor: UIColor? = UIColor(red: 204.0 / 255.0, green: 204.0 / 255.0, blue: 204.0 / 255.0, alpha: 1.0)
        reviewtextArea.layer.borderColor = borderColor?.cgColor
        reviewtextArea.layer.borderWidth = 1.0
        reviewtextArea.layer.cornerRadius = 5.0
        productName.text = PName!
        starView.addTarget(self, action: #selector(self.didChangeValue1), for: .touchDown)
        youAreViewing.text = "youAreReviewing".localized
        ratingLabel.text = "rating".localized
        nameLabel.text = "title".localized
        reviewLabel.text = "review".localized
        submitButton.setTitle("submitReview".localized, for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func submitClicked(_ sender: Any) {

        self.dismissKeyboard()
        let someOptional: Bool = true
        
        switch someOptional {
        case reviewStarValue == 0:
            ErrorChecking.warningView(message: "enterrating".localized)
        case ErrorChecking.isEmptyString(text: nameTextField.text!):
            ErrorChecking.warningView(message: "entername".localized)
        case ErrorChecking.isEmptyString(text: reviewtextArea.text):
            ErrorChecking.warningView(message: "messageRequireField".localized)
        default:

        var reviewDict = [String: Any]()

        reviewDict["title"] = nameTextField.text!
        reviewDict["content"] = reviewtextArea.text
        reviewDict["grade"] = reviewStarValue
        reviewDict["id_customer"] = (UserDefaults.standard.value(forKey: "id_customer") as? String) ?? ""
        reviewDict["id_product"] = templateId
//
//        let dict = ["postreview" : reviewDict , "json" : "1" ] as [String : Any]

//        let dict = ["title" : nameTextField.text! , "detail" : reviewtextArea.text , "rate" : reviewStarValue , "template_id" : templateId!] as [String : Any]
        self.makeRequest(dict: reviewDict)
        }

    }

    @objc func didChangeValue1(_ sender: HCSStarRatingView) {
        reviewStarValue = Int(sender.value)
        print(reviewStarValue)
    }

    func makeRequest(dict: [String: Any]) {
        let params = GlobalConstants.ApiNames.addReview
//        let networkManager = NetworkManager()
         var newDict = [String: Any]()
          newDict[GlobalConstants.ApiNames.addReview.FetchApiName()] = dict
        NetworkManager.fetchData(params: params, verb: RequestType.post, dict: newDict, controller: AllController.addReview, view: self.view ) { (responseObject: JSON?, error: Error?) in

            if error != nil {
                print("loginError".localized)

            } else {
                if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }

    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
