//
//  ReviewsTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import HCSStarRatingView

class ReviewsTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var reviewByLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    @IBOutlet weak var msgLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        reviewByLabel.SetDefaultTextColor()
        ratingLabel.SetDefaultTextColor()
         ratingView.tintColor = UIColor().HexToColor(hexString: "FFB926")
        ratingView.isUserInteractionEnabled = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: ReviewData? {
        didSet {
           title.text = item?.title
            nameLabel.text = String.init(format: "%@ - %@", (item?.customer_name!)!, (item?.date_add)!)
            msgLabel.text = item?.content
            ratingView.value = CGFloat(Int(item?.grade ?? "0")!)
        }
    }

}
