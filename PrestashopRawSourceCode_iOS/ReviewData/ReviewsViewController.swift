//
//  ReviewsViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import  SwiftyJSON

class ReviewsViewController: UIViewController {

    var id: String?
    var reviewDataArray = [ReviewData]()
    fileprivate let reviewViewModalObject = ReviewViewModal()

    @IBOutlet weak var reviewViewController: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = "Reviews"
        reviewViewController.register(ReviewsTableViewCell.nib, forCellReuseIdentifier: ReviewsTableViewCell.identifier)
        reviewViewController.delegate = reviewViewModalObject
        reviewViewController.dataSource = reviewViewModalObject
        reviewViewController.estimatedRowHeight = 100
        reviewViewController.rowHeight = UITableView.automaticDimension
      self.show()
        // Do any additional setup after loading the view.
    }
    
    func show() {
        self.reviewViewModalObject.getValue(jsonData: reviewDataArray) {
                (data: Bool) in
                print(data)
                if data {
//                        self.emptyView?.isHidden = true
                    self.reviewViewController.isHidden = false
                    self.reviewViewController.reloadData()
                } else {
//                        self.emptyView?.isHidden = false
                    self.reviewViewController.isHidden = true
                }
            }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//    func makeRequest(){
//        let params = GlobalData.apiName.reviews
//        let networkManager = NetworkManager()
//        let dict = ["template_id" : Int(id!) ?? 0]
//        networkManager.fetchData(params: params,  login: false,  view: self.view, dict: dict , verbs: RequestType.post , controler: Controllers.reviews) { (responseObject:JSON?, error:Error?) in
//
//            if error != nil {
//                print("Error logging you in!")
//
//            } else {
//
//            }
//        }
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
