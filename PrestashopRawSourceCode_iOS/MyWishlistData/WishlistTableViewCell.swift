//
//  WishlistTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit

class WishlistTableViewCell: UITableViewCell {

    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var wishlistName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var setQtyLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        checkIcon.isHidden = true
        qtyLabel.text = "qty".localized.uppercased()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var item: WishlistData! {
        didSet {
            wishlistName.text = item.name
            setQtyLabel.text = item.nbProducts
            dateLabel.text = item.date_add
            if item.defaultValue == "1" {
                 checkIcon.isHidden = false
            } else {
                 checkIcon.isHidden = true
            }

        }
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
