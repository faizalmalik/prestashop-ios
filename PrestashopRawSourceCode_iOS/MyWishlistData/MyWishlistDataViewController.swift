//
//  MyWishlistDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
var emptyView: EmptyView!

class MyWishlistDataViewController: UIViewController {

    @IBOutlet weak var addMultiWishlist: UIButton!
    @IBOutlet weak var wishlistTableView: UITableView!
    fileprivate let WishlistViewModalObject = WishlistViewModal()
    var emptyView: EmptyView!
    
    var title1: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = title1
        wishlistTableView.register(WishlistTableViewCell.nib, forCellReuseIdentifier: WishlistTableViewCell.identifier)
        wishlistTableView.dataSource = WishlistViewModalObject
        wishlistTableView.delegate = WishlistViewModalObject
        wishlistTableView.tableFooterView = UIView()
        WishlistViewModalObject.moveControllerDelegate = self
        WishlistViewModalObject.passDataDelegate = self

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.makeRequest(dict: [:], call: WhichApiCall.none, id: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func makeRequest(dict: [String: Any], call: WhichApiCall, id: String) {
        
        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get
        
        switch call {
        case .deleteWishlist:
            params = GlobalConstants.ApiNames.manageWishlist
            if let custId =  UrlParameters.fetchCustomerId() {
                params += custId
            }
            newDict[GlobalConstants.ApiNames.manageWishlist.FetchApiName()] = dict
            verbs = .post
        case .defaultWishlist:
            params = GlobalConstants.ApiNames.defaultWishlist
            if let custId =  UrlParameters.fetchCustomerId() {
                params += custId
            }
            params += "&id_wishlist=" + id
            
            verbs = .get
            
        default:
            params = GlobalConstants.ApiNames.getWishlists
            if let id_customer = UrlParameters.fetchCustomerId() {
                params += id_customer
            }
            params += UrlParameters.width
        }
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                self.emptyView.dict = dict
                self.emptyView.call = call
                self.emptyView.params = id
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                
                switch call {
                case .deleteWishlist:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.makeRequest(dict: [:], call: WhichApiCall.none, id: "")
                    }
                    
                case .defaultWishlist:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.makeRequest(dict: [:], call: WhichApiCall.none, id: "")
                    }
                    
                default:
                    self.WishlistViewModalObject.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                        if data {
                            
                            self.wishlistTableView.reloadData()
                        } else {
                            
                        }
                    }
                }
            }
        }
    }

    @IBAction func addwishlistClicket(_ sender: Any) {

        if let view = UIStoryboard.loadAddWishlistProductViewController() {
            self.navigationController?.pushViewController(view, animated: true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyWishlistDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        switch call {
        case .deleteWishlist:
            self.makeRequest(dict: dict, call: WhichApiCall.deleteWishlist, id: id)
        case .defaultWishlist:
            //            moveWishlistDict = dict
            self.makeRequest(dict: [:], call: WhichApiCall.defaultWishlist, id: id)

        default:
            print()
        }

    }

}

extension MyWishlistDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        if let view = UIStoryboard.loadWishlistProductViewController() {
            view.id = id
            view.name = name
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
}

extension MyWishlistDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest(dict: emptyView.dict, call: emptyView.call, id: emptyView.params)
    }
}
