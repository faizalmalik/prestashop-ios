//
//  WishlistModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

struct WishlistModal {

    var wishlistArray = [WishlistData]()

    private let wishlistPath =  ["wishlists", "wishlist"]

    init?(data: JSON) {

        if data[wishlistPath] != JSON.null {
            if let data  = data[wishlistPath].array {
                self.wishlistArray = data.map { WishlistData( data: $0) }
            } else {
                self.wishlistArray.append(WishlistData(data: data[wishlistPath]))
            }
        }

    }
}

struct WishlistData {
    var id_wishlist: String!
    var name: String!
    var date_add: String!
    var defaultValue: String!
    var nbProducts: String!
    init(data: JSON) {
        self.id_wishlist = data["id_wishlist"].stringValue
        self.name = data["name"].stringValue
        self.date_add = data["date_add"].stringValue
        self.defaultValue = data["default"].stringValue
        self.nbProducts = data["nbProducts"].string ?? "0"
    }

}
