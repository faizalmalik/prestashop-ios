//
//  WishlistViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class WishlistViewModal: NSObject {

    var moveControllerDelegate: MoveController?
     var passDataDelegate: PassData?
     var wishlistArray = [WishlistData]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = WishlistModal(data: jsonData) else {
            return
        }
        wishlistArray.removeAll()
        if !data.wishlistArray.isEmpty {
            wishlistArray = data.wishlistArray
            completion(true)
        } else {
            completion(false)
        }

    }
}

extension WishlistViewModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: WishlistTableViewCell.identifier) as? WishlistTableViewCell {
            cell.item = wishlistArray[indexPath.row]
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishlistArray.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        moveControllerDelegate?.moveController(id: wishlistArray[indexPath.row].id_wishlist, name: wishlistArray[indexPath.row].name, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.addAddress)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

        let shareAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "default".localized, handler: { (_, indexPath) in
            self.passDataDelegate?.passData(id: self.wishlistArray[indexPath.row].id_wishlist, name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.defaultWishlist)

        })

        shareAction.backgroundColor = UIColor.green

        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "delete".localized, handler: { (_, indexPath) in
                var deleteDict = [String: Any]()
                deleteDict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
                deleteDict["operation"] = "delete_wishlist"
                deleteDict["id_wishlist"] = self.wishlistArray[indexPath.row].id_wishlist
                self.passDataDelegate?.passData(id: "", name: "", dict: deleteDict, jsonData: JSON.null, index: 0, call: WhichApiCall.deleteWishlist)
        })

        deleteAction.backgroundColor = UIColor().HexToColor(hexString: "#FF5252")
        return [deleteAction, shareAction]

    }

}
