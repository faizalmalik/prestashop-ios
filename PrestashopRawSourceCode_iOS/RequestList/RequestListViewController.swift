//
//  RequestListViewController.swift
//  PrestashopRawSourceCode_iOS
//
//  Created by Faizal on 6/24/19.
//  Copyright © 2019 com. All rights reserved.
//

import UIKit
import SwiftyJSON
import FirebaseAnalytics

class RequestListViewController: UIViewController {
    var emptyView: EmptyView?
    var quantity : Int = 0
    
   enum SellerRequestType {
    
        case stock
        case approve
    }
    
    @IBOutlet weak var headerView: UIView!
    
    fileprivate let requestListViewModalObject = RequestListViewModel()

    @IBOutlet weak var requestListTableView: UITableView!
    
   static var requestTypeSeller : SellerRequestType = .stock
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)

        requestListTableView.dataSource = requestListViewModalObject
        requestListTableView.delegate = requestListViewModalObject
        
        requestListViewModalObject.moveDelegate = self
        requestListTableView.register(RequestListTableViewCell.nib, forCellReuseIdentifier: RequestListTableViewCell.identifier)
        
        
        requestListTableView.tableFooterView = UIView()
        //If the user is seller then show the header view and register cell
        
        if FetchDetails.is_seller(){
            requestListTableView.tableHeaderView = headerView
            RequestListViewController.requestTypeSeller = .stock
            
             requestListTableView.register(ApproveTableViewCell.nib, forCellReuseIdentifier: ApproveTableViewCell.identifier)
        }
        requestListTableView.separatorStyle = .none
        requestListTableView.estimatedRowHeight = 100
        requestListTableView.rowHeight = UITableView.automaticDimension
        requestListTableView.showsVerticalScrollIndicator = false
        
        self.navigationItem.title = "Requests".localized
        requestListViewModalObject.passDataDelegate = self
        
        self.makeRequest(dict: [:], call: .getRequestList)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = "Empty Requests".localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-notification")
            emptyView?.button.isHidden = true
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeRequest(dict: [String: Any], call: WhichApiCall) {
        var params = ""
        var verbs: RequestType = .get
        var newDict = [String: Any]()

        switch call {
        case .requestAction:
            
            params = GlobalConstants.ApiNames.updateRequest
            params += dict["urlParam"] as! String
            verbs = .get
            
            if FetchDetails.is_seller(){
                if let customerID =  UrlParameters.fetchSellerID(){
                    
                    
                    switch(RequestListViewController.requestTypeSeller){
                        
                    case .stock :
                        //this case will not be executed
                        params += "\(UserDefaults.Keys.id_customer)=\(UserDefaults.fetch(key: UserDefaults.Keys.id_seller)!)"
                        
                        
                    case .approve :
                        params += customerID
                         params += "&run_as=3"
                        
                    }
                    
                }
            }
            else{
                //No need further action if logged in is not a seller
                return;
            }
            
            
            
            break
            
        case .getRequestList:
             params = GlobalConstants.ApiNames.getRequestList
            verbs = .get
             
            if FetchDetails.is_seller(){
                if let customerID =  UrlParameters.fetchSellerID(){
                   
                    
                    switch(RequestListViewController.requestTypeSeller){
                        
                    case .stock :
                         params += "\(UserDefaults.Keys.id_customer)=\(UserDefaults.fetch(key: UserDefaults.Keys.id_seller)!)"
                        params += "&id_real_stock_status=1&run_as=2&array_format=1"

                 
                    case .approve :
                        params += customerID
                        params += "&id_real_stock_status=1,2,3,4,5,6&run_as=3&&array_format=1"

                        
                    }
                    
                }
            }
            else if FetchDetails.isLoggedIn(){
                
                if let customerID =  UrlParameters.fetchCustomerId(){
                    params += customerID
                    params += "&id_real_stock_status=1&run_as=2"
                    
                }
            }
            else {
                if let customerID =  UrlParameters.fetchGuestId(){
                    params += customerID
                    params += "&id_real_stock_status=1&run_as=1"
                    
                }
                else{
                    params += "id_guest=0&id_real_stock_status=1&run_as=1"
                    
                    
                }
                
                
                
            }
            
            
        case .Cart :
            
            params =  GlobalConstants.ApiNames.addToCart
            newDict[GlobalConstants.ApiNames.addToCart.FetchApiName()] = dict
            verbs = .post
            
        default:
            break;
        }
        
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                
                print("loginError".localized, error!)
                
            } else {
                
                switch call {
                    
                case .Cart :
                    if ErrorChecking.getData(data: responseObject) != JSON.null, let data = ErrorChecking.getData(data: responseObject) {
//                        Analytics.logEvent(AnalyticsEventAddToCart, parameters: [AnalyticsParameterItemID: self.productId as NSObject, AnalyticsParameterItemName: self.productName as NSObject])
                        
                        if data["response"]["id_cart"].string != nil {
                            UserDefaults.set(value: data["response"]["id_cart"].stringValue, key: UserDefaults.Keys.id_cart)
                        }
                        if data["response"]["id_guest"].string != nil {
                            UserDefaults.set(value: data["response"]["id_guest"].stringValue, key: UserDefaults.Keys.id_guest)
                        }
                        
                        if data["response"]["nbProducts"].string != nil {
                            UserDefaults.set(value: data["response"]["nbProducts"].stringValue, key: UserDefaults.Keys.cartCount)
                        }
                        if  self.tabBarController != nil {
                            self.tabBarController?.setCartCount(value: FetchDetails.fetchCartCount())
                        }
//                        if let count = Int(FetchDetails.fetchCartCount()), count != 0 {
//                            self.cartBtn.addBadge(number: count)
//                        }
                    }
                    break
                    
                case .getRequestList :

                    self.requestListViewModalObject.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                        print(data)
                        if data {
                            self.emptyView?.isHidden = true
                            self.requestListTableView.isHidden = false
                            self.requestListTableView.reloadData()
                        } else {
                            self.emptyView?.isHidden = false
                            self.requestListTableView.isHidden = true
                        }
                    }
                    break
                    
                case .requestAction:
                    
                    self.showAlert(title: "Success", message: "Update Successfull", completion: {
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                    
                default:
                    break
                }
                
                
            }
        }
        
        
        
    }
    func CartDict(dic:[String: Any]) -> [String: Any] {
        var cartDict = [String: Any]()
        cartDict["quantity"] = dic["quantity"]
        cartDict["operation"] = "up"
        cartDict["id_customization"] = ""
        cartDict["id_address_delivery"] = "0"
        cartDict["id_product_attribute"] = dic["id_product_attribute"]
        cartDict ["id_customer"] =  UserDefaults.fetch(key: UserDefaults.Keys.id_customer) ?? ""
        cartDict ["id_cart"] =  UserDefaults.fetch(key: UserDefaults.Keys.id_cart) ?? ""
        cartDict ["id_guest"] =  UserDefaults.fetch(key: UserDefaults.Keys.id_guest) ?? ""
        cartDict ["id_currency"] =  UserDefaults.fetch(key: "id_currency") ?? ""
        cartDict["id_product"] = dic["id_product"]
        return cartDict
    }
    
    @IBAction func segmentControl(_ sender: UISegmentedControl) {
        

        if(sender.selectedSegmentIndex==0){
            RequestListViewController.requestTypeSeller = .stock
        }
        if(sender.selectedSegmentIndex==1){
            RequestListViewController.requestTypeSeller = .approve
        }

        requestListViewModalObject.requestData.removeAll()
        
        self.makeRequest(dict: [:], call: .getRequestList)

    }
    
}
extension RequestListViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        switch controller {
        case .productController:
            if let view = UIStoryboard.loadProductViewController() {
                view.productId = id
                view.productName = name
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .productCategory:
            if let view = UIStoryboard.loadProductCategoryViewController() {
                view.id = id
                view.name = name
                self.navigationController?.pushViewController(view, animated: true)
            }
        default:
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension RequestListViewController : PassData {
    func passData(id: String, name: String, dict: [String : Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        
        
        switch call {
        case .Cart:
            self.makeRequest(dict: self.CartDict(dic: dict), call: .Cart)
            
            break
            
        case .requestAction :
            
            self.makeRequest(dict: dict, call: .requestAction)
                
            break
            
        default:
            break
        }
        
    }
    
    
    
}
