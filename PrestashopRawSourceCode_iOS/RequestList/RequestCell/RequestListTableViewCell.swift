//
//  RequestListTableViewCell.swift
//  PrestashopRawSourceCode_iOS
//
//  Created by Faizal on 6/24/19.
//  Copyright © 2019 com. All rights reserved.
//

import UIKit
import SwiftyJSON
class RequestListTableViewCell: UITableViewCell{
    
      var passDataDelegate: PassData?
    
    @IBOutlet weak var prodName: UILabel!
    
    @IBOutlet weak var prodImage: UIImageView!
    
    @IBOutlet weak var inStockLabel: UILabel!
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    
    @IBOutlet weak var addToCart: UIButton!
    
    var item : RequestModel? {
        
        didSet{
            prodName.text = item?.product_name
            inStockLabel.text = "In Stock :" + (item?.quantity_instock ?? "0")
            countLabel.text = (item?.quantity_requested ?? "0") + " Item"
            stepper.value = Double(Int(item?.quantity_requested ?? "0" ) ?? 0)
            stepper.minimumValue = Double(Int(item?.minimum_quantity ?? "0" ) ?? 0)

            if Int(item?.quantity_requested ?? "0")  ?? 0 > 0 {
                addToCart.isHidden = false
            }
            else{
                addToCart.isHidden = true
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        addToCart.isHidden = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
        static var identifier: String {
            return String(describing: self)
        }
        
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    @IBAction func stepperAction(_ sender: UIStepper) {
        
        if Int(sender.value) <= Int(item?.quantity_requested ?? "0") ?? 0 {
            addToCart.isHidden = false
        }
        else{
            addToCart.isHidden = true

        }
        
        countLabel.text = "\(Int(sender.value))" + " Item"

    }
    

    @IBAction func addCartAction(_ sender: UIButton) {
        let dic : [String:String] = ["id_product":item?.id_product ?? "0","id_product_attribute":item?.id_product_attribute ?? "","quantity":"\(Int(stepper.value))"]
        
        
        passDataDelegate?.passData(id: "", name: "", dict: dic, jsonData: JSON.null, index: 0, call: .Cart)
        
    }
}
