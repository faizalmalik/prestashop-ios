/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

import Foundation
import SwiftyJSON
 
/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class RequestModel {
	public var id_product_attribute : String?
	public var id_real_stock_status : String?
	public var id_customer : String?
	public var id_customization : String?
	public var id_image : String?
	public var quantity_requested : String?
	public var id_mps_real_stock : String?
	public var date_upd : String?
	public var id_order : String?
	public var date_add : String?
	public var quantity_instock : String?
	public var id_product : String?
	public var id_seller : String?
	public var minimum_quantity : String?
	public var product_name : String?
	public var id_guest : String?
//    public var attribute_name : Attribute_name?
	public var date_exp : String?

    public var statusText : String {
        
        get{
            switch Int(self.id_real_stock_status ?? "0") {
            case 1:
                return "Pending"
            case 2:
                return "Available"
            case 3:
                return "Partially Available"
            case 4:
                return "Out Of Stock"
            case 5:
                return "Back Order"
            case 6:
                return "Hold"
            case 7:
                return "Expired"
                
            default:
                return ""
            }
        }
    }
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let product_1_list = Product_1.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of Product_1 Instances.
*/
//    public class func modelsFromDictionaryArray(array:NSArray) -> [RequestModel]
//    {
//        var models:[RequestModel] = []
//        for item in array
//        {
//            models.append(RequestModel(dictionary: item as! NSDictionary)!)
//        }
//        return models
//    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let product_1 = Product_1(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: Product_1 Instance.
*/
	required public init?(dictionary: JSON) {

		id_product_attribute = dictionary["id_product_attribute"].stringValue
		id_real_stock_status = dictionary["id_real_stock_status"].stringValue
		id_customer = dictionary["id_customer"].stringValue
		id_customization = dictionary["id_customization"].stringValue
		id_image = dictionary["id_image"].stringValue
		quantity_requested = dictionary["quantity_requested"].stringValue
		id_mps_real_stock = dictionary["id_mps_real_stock"].stringValue
		date_upd = dictionary["date_upd"].stringValue
		id_order = dictionary["id_order"].stringValue
		date_add = dictionary["date_add"].stringValue
		quantity_instock = dictionary["quantity_instock"].stringValue
		id_product = dictionary["id_product"].stringValue
		id_seller = dictionary["id_seller"].stringValue
		minimum_quantity = dictionary["minimum_quantity"].stringValue
		product_name = dictionary["product_name"].stringValue
		id_guest = dictionary["id_guest"].stringValue
//        if (dictionary["attribute_name"] != nil) { attribute_name = Attribute_name(dictionary: dictionary["attribute_name"] as! NSDictionary) }
		date_exp = dictionary["date_exp"].stringValue
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.id_product_attribute, forKey: "id_product_attribute")
		dictionary.setValue(self.id_real_stock_status, forKey: "id_real_stock_status")
		dictionary.setValue(self.id_customer, forKey: "id_customer")
		dictionary.setValue(self.id_customization, forKey: "id_customization")
		dictionary.setValue(self.id_image, forKey: "id_image")
		dictionary.setValue(self.quantity_requested, forKey: "quantity_requested")
		dictionary.setValue(self.id_mps_real_stock, forKey: "id_mps_real_stock")
		dictionary.setValue(self.date_upd, forKey: "date_upd")
		dictionary.setValue(self.id_order, forKey: "id_order")
		dictionary.setValue(self.date_add, forKey: "date_add")
		dictionary.setValue(self.quantity_instock, forKey: "quantity_instock")
		dictionary.setValue(self.id_product, forKey: "id_product")
		dictionary.setValue(self.id_seller, forKey: "id_seller")
		dictionary.setValue(self.minimum_quantity, forKey: "minimum_quantity")
		dictionary.setValue(self.product_name, forKey: "product_name")
		dictionary.setValue(self.id_guest, forKey: "id_guest")
//        dictionary.setValue(self.attribute_name?.dictionaryRepresentation(), forKey: "attribute_name")
		dictionary.setValue(self.date_exp, forKey: "date_exp")

		return dictionary
	}

}
