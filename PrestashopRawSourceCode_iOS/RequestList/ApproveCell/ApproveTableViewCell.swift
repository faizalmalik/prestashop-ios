//
//  ApproveTableViewCell.swift
//  PrestashopRawSourceCode_iOS
//
//  Created by Faizal on 7/31/19.
//  Copyright © 2019 com. All rights reserved.
//

import UIKit
import SwiftyJSON

class ApproveTableViewCell: UITableViewCell {
    
 let myPickerData = [String](arrayLiteral: GlobalConstants.RequestAction.Available, GlobalConstants.RequestAction.OutOfStock, GlobalConstants.RequestAction.BackOrder,GlobalConstants.RequestAction.OnHold)


   var passDataDelegate: PassData?
    let thePicker = UIPickerView()

    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var qtyRequested: UILabel!
    
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var minStock: UILabel!
    @IBOutlet weak var qtyInstock: UILabel!
    
    @IBOutlet weak var actionField: UITextField!
    
    var item : RequestModel? {
      didSet{
         productName.text = item?.product_name
         qtyInstock.text = "Qty Instock : " + (item?.quantity_instock ?? "0")
         qtyRequested.text = "Qty Requested : " + (item?.quantity_requested ?? "0")
         minStock.text = "Min Qty : " + (item?.minimum_quantity ?? "0")

        actionField.inputView = thePicker
        thePicker.delegate = self
        actionField.delegate = self
        status.text = item?.statusText
        }
        
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var identifier: String {
        return String(describing: self)
    }
    
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    @IBAction func submitAction(_ sender: UIButton) {
        
        guard actionField.text?.count ?? 0 > 0 else {
            
            return
            
        }
       
        var urlParam = "id_mps_real_stock=\(item?.id_mps_real_stock ?? "0")&quantity=\(item?.quantity_instock ?? "0")"
        
        
//https://jewelbharat.com/api/mpsshop/updateRealTimeStockRequest?id_mps_real_stock=1&quantity=2&run_as=3&id_seller=1

        switch actionField.text {
        case GlobalConstants.RequestAction.Available:
            urlParam += "&id_real_stock_status=2&"
            break
        case GlobalConstants.RequestAction.OutOfStock:
             urlParam += "&id_real_stock_status=4&"
            break
        case GlobalConstants.RequestAction.BackOrder:
             urlParam += "&id_real_stock_status=5&"
            break
        case GlobalConstants.RequestAction.OnHold:
             urlParam += "&id_real_stock_status=6&"
            
            break
        default:
            break
        }
        
        
        let dic : [String:String] = ["urlParam":urlParam]
        
         passDataDelegate?.passData(id: "", name: "", dict: dic, jsonData: JSON.null, index: 0, call: .requestAction)
    }
}

extension ApproveTableViewCell : UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
   
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return myPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return myPickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        actionField.text = myPickerData[row]
    }
    

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        actionField.text = myPickerData[0]
        return true
    }
}
