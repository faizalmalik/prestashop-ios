//
//  RequestListViewModel.swift
//  PrestashopRawSourceCode_iOS
//
//  Created by Faizal on 6/24/19.
//  Copyright © 2019 com. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class RequestListViewModel : NSObject {
    var passDataDelegate: PassData?

    
    var requestData :[RequestModel] = []
    var moveDelegate : MoveController!
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        
        if FetchDetails.is_seller(){
            
            switch(RequestListViewController.requestTypeSeller){
            case .stock :
                
                if let modelDic = jsonData["real_stock_list"]["id_real_stock_status_1"]["product"].array {
                    
                    
                    self.requestData = modelDic.map{RequestModel(dictionary: $0)!}
                    
                }
                completion(true)
            case .approve:
                
                
                for index in 1...6{
                    print(index)
                    if let modelDic = jsonData["real_stock_list"]["id_real_stock_status_\(index)"]["product"].array {
                        
                        
                        self.requestData += modelDic.map{RequestModel(dictionary: $0)!}
                        
                    }else if let modelDic = jsonData["real_stock_list"]["id_real_stock_status_\(index)"]["product"].dictionary {
                        
                        
                         self.requestData += modelDic.map{RequestModel(dictionary: $0.value)!}
                        
                    }
                    
                    
                 //   self.requestData
                }
                
               
                completion(true)
                
            }
            
        } else if jsonData["real_stock_list"]["id_real_stock_status_1"]["product"] != JSON.null {
            
            if let modelDic = jsonData["real_stock_list"]["id_real_stock_status_1"]["product"].array {
                
                
                self.requestData = modelDic.map{RequestModel(dictionary: $0)!}
                
            }
            completion(true)
            
            
        }
        else
        {
            completion(false)
        }
        
    }
}

extension RequestListViewModel : UITableViewDelegate,UITableViewDataSource,PassData{
    func passData(id: String, name: String, dict: [String : Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        
        passDataDelegate?.passData(id: id, name: name, dict: dict, jsonData: jsonData, index: index, call: call)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if FetchDetails.is_seller(){

            switch RequestListViewController.requestTypeSeller {
            case .stock:
                if let cell = tableView.dequeueReusableCell(withIdentifier: RequestListTableViewCell.identifier) as? RequestListTableViewCell {

                    cell.item = requestData[indexPath.row]

                    cell.passDataDelegate = self

                    return cell

                }
            case .approve:

                if let cell = tableView.dequeueReusableCell(withIdentifier: ApproveTableViewCell.identifier) as? ApproveTableViewCell {

                    cell.item = requestData[indexPath.row]
//
                    cell.passDataDelegate = self

                    return cell
                }
            }
          return UITableViewCell()

        }
        else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: RequestListTableViewCell.identifier) as? RequestListTableViewCell {
                
                cell.item = requestData[indexPath.row]
                
                cell.passDataDelegate = self
                
                return cell
            }
         return UITableViewCell()
            

        }

        
        
       
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    

}

