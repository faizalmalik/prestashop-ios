//
//  OrderPlaceDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import FirebaseAnalytics

class OrderPlaceDataViewController: UIViewController {
    @IBOutlet weak var orderDescriptionLabel: UILabel!
    @IBOutlet weak var orderSummaryView: UIView!
    @IBOutlet weak var orderIDlabel: UILabel!
    @IBOutlet weak var orderPlaced: UILabel!
    @IBOutlet weak var continueBtn: UIButton!
    
    var desc: String?
     var id: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
         self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
          self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        orderIDlabel.text = String.init(format: "%@: %@", "yourOrder".localized, id!)
        orderPlaced.text = "orderPlaced".localized
        continueBtn.setTitle("continueShopping".localized, for: .normal)
        Analytics.logEvent("Order Placed", parameters: [AnalyticsParameterItemID: id! as NSObject, AnalyticsParameterItemName: "" as NSObject])
        orderDescriptionLabel.text = desc?.htmlToString
//          orderDescriptionLabel.text = desc!
        continueBtn.SetAccentBackColor()
        self.navigationItem.title = "orderPlaced".localized
        orderSummaryView.normalBorder()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func orderSummaryPressed(_ sender: UIButton) {

    }

    @IBAction func ContinueShoppingPressed(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "back", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
