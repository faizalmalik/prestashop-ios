//
//  CheckoutDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import PaymentSDK
import Alamofire
import SVProgressHUD

class CheckoutDataViewController: UIViewController {

    @IBOutlet weak var line3: UIView!
    @IBOutlet weak var line1: UIView!
    @IBOutlet weak var line2: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var shippingBtn: UIButton!
    @IBOutlet weak var paymentBtn: UIButton!
    @IBOutlet weak var reviewBtn: UIButton!
    @IBOutlet weak var addressBtn: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    @IBOutlet weak var orderReviewView: CheckoutOrderReview!
    @IBOutlet weak var paymentView: CheckoutPayment!
    @IBOutlet weak var shippingView: CheckoutShipping!
    @IBOutlet weak var addressView: CheckoutAddress!
    @IBOutlet weak var firstViewHeight: NSLayoutConstraint!
    @IBOutlet weak var checkoutScrollView: UIScrollView!

    var emptyView: EmptyView?
    var termsOn = 1
    var id_address_delivery: String!
    var id_address_invoice: String!
    var cart_total_order: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
         self.performAction()
        shippingBtn.isUserInteractionEnabled = false
        paymentBtn.isUserInteractionEnabled = false
        self.setViews()
        self.localizedStrings()
    }

    func performAction() {
        addressView.PassCheckoutDataDelegate = self
        shippingView.PassCheckoutDataDelegate = self
        paymentView.PassCheckoutDataDelegate = self
        orderReviewView.PassCheckoutDataDelegate = self
        shippingView.moveDelegate = self
        shippingView.passData = self
        addressView.moveDelegate = self
        addressView.id_address_delivery = id_address_delivery
        addressView.id_address_invoice = id_address_invoice
    }

    func localizedStrings() {
        self.navigationItem.title = "checkout".localized
        addressLabel.text = "address".localized
        shippingLabel.text = "shipping".localized
        paymentLabel.text = "payment".localized
        reviewLabel.text = "review".localized
    }

    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.view.backgroundColor = GlobalConstants.Colors.primaryColor
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.navigationChanges()
        firstViewHeight.constant = UIScreen.main.bounds.size.height  - 80 - 64
        checkoutScrollView.isPagingEnabled = true
        checkoutScrollView.isScrollEnabled = false
        checkoutScrollView.contentSize = CGSize(width: 4*SCREEN_WIDTH, height: UIScreen.main.bounds.size.height - 80 - 64)
        if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
            topView.semanticContentAttribute = .forceRightToLeft
        } else {
            topView.semanticContentAttribute = .forceLeftToRight
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.makeRequest(dict: [:], call: CheckoutApiCall.getAddress)
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = "addressEmpty".localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-address")
            emptyView?.button.isHidden = false
            emptyView?.isHidden = true
            emptyView?.delegate = self
            emptyView?.button.setTitle("Add New Address", for: .normal)
            self.view.addSubview(emptyView!)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backPressed(_ sender: Any) {
        if FetchDetails.isGuestLoggedIn() {
            UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.isGuestLoggedIn)
            UserDefaults.standard.synchronize()
        }
        self.dismiss(animated: true, completion: nil)
    }

    func setViews() {
        let dimColor  = "#E5E5EE"
        let offset = self.checkoutScrollView.contentOffset.x
        if  offset == 0 {
            addressBtn.SetAccentBackColor()
            shippingBtn.backgroundColor = UIColor().HexToColor(hexString: dimColor)
             paymentBtn.backgroundColor = UIColor().HexToColor(hexString: dimColor)
            reviewBtn.backgroundColor = UIColor().HexToColor(hexString: dimColor)
            line1.backgroundColor = UIColor().HexToColor(hexString: dimColor)
            line2.backgroundColor = UIColor().HexToColor(hexString: dimColor)
            line3.backgroundColor = UIColor().HexToColor(hexString: dimColor)

        } else if   offset == SCREEN_WIDTH {
            addressBtn.SetAccentBackColor()
            shippingBtn.SetAccentBackColor()
            paymentBtn.backgroundColor = UIColor().HexToColor(hexString: dimColor)
            reviewBtn.backgroundColor = UIColor().HexToColor(hexString: dimColor)
            line1.backgroundColor = GlobalConstants.Colors.accentColor
            line2.backgroundColor = UIColor().HexToColor(hexString: dimColor)
            line3.backgroundColor = UIColor().HexToColor(hexString: dimColor)
        } else if   offset == 2*SCREEN_WIDTH {
            addressBtn.SetAccentBackColor()
            shippingBtn.SetAccentBackColor()
            paymentBtn.SetAccentBackColor()
            reviewBtn.backgroundColor = UIColor().HexToColor(hexString: dimColor)
            line1.backgroundColor = GlobalConstants.Colors.accentColor
            line2.backgroundColor = GlobalConstants.Colors.accentColor
            line3.backgroundColor = UIColor().HexToColor(hexString: dimColor)
        } else if   offset == 3*SCREEN_WIDTH {
            addressBtn.SetAccentBackColor()
            shippingBtn.SetAccentBackColor()
            paymentBtn.SetAccentBackColor()
            reviewBtn.SetAccentBackColor()
            line1.backgroundColor = GlobalConstants.Colors.accentColor
            line2.backgroundColor = GlobalConstants.Colors.accentColor
            line3.backgroundColor = GlobalConstants.Colors.accentColor
        }
    }

    @IBAction func AddressBtnClicked(_ sender: UIButton) {
          self.checkoutScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        shippingBtn.isUserInteractionEnabled = false
        paymentBtn.isUserInteractionEnabled = false

        self.setViews()
    }

    @IBAction func shippingClicked(_ sender: Any) {
        self.checkoutScrollView.setContentOffset(CGPoint(x: SCREEN_WIDTH, y: 0), animated: true)
        shippingBtn.isUserInteractionEnabled = true
        paymentBtn.isUserInteractionEnabled = false
        self.setViews()
    }

    @IBAction func paymentClicked(_ sender: Any) {
        self.checkoutScrollView.setContentOffset(CGPoint(x: 2*SCREEN_WIDTH, y: 0), animated: true)
        shippingBtn.isUserInteractionEnabled = true
        paymentBtn.isUserInteractionEnabled = true
        self.setViews()
    }
}

extension CheckoutDataViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.setViews()
    }
}

extension CheckoutDataViewController {
    func makeRequest(dict: [String: Any], call: CheckoutApiCall) {
        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get
        switch call {
        case .getAddress:
            params += GlobalConstants.ApiNames.getAddress
            if let custId =  UrlParameters.fetchCustomerId() {
                params += custId
            }
            verbs = .get
        case .updateCartAddress:
            params += GlobalConstants.ApiNames.updateCartAddress
            if let cartId =  UrlParameters.fetchCartID() {
                params += cartId
            }
            id_address_invoice = ((dict["id_address_invoice"] as? String) ?? "")
            id_address_delivery = ((dict["id_address_delivery"] as? String) ?? "" )
            params += "&id_address=" + id_address_delivery
            params += "&id_address_invoice=" + id_address_invoice
            verbs = .get
        case .getShpppingMethod:
            params += GlobalConstants.ApiNames.getShpppingMethod
            if let cartId =  UrlParameters.fetchCartID() {
                params += cartId
            }
            verbs = .get
        case .updateShippingMethod:
            params += GlobalConstants.ApiNames.updateShippingMethod
            verbs = .post
            newDict[GlobalConstants.ApiNames.updateShippingMethod.FetchApiName()] = dict
        case .getPaymentMethods:
            params += GlobalConstants.ApiNames.getPaymentMethods
            if let cartId =  UrlParameters.fetchCartID() {
                params += cartId
            }
            verbs = .get
        case .orderReview:
            params += GlobalConstants.ApiNames.orderReview
            if let custId =  UrlParameters.fetchCustomerId() {
                params += custId
            }
            if let cartId =  UrlParameters.fetchCartID() {
                params += cartId
            }
            params += UrlParameters.width
            verbs = .get
        case .orderPlace:
            params += GlobalConstants.ApiNames.placeOrder
            newDict[GlobalConstants.ApiNames.placeOrder.FetchApiName()] = dict
            verbs = .post
        default:
            print()
        }

        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.checkoutController, view: self.view) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                print("loginError".localized, error!)
            } else {
                switch call {
                case .orderReview:
                    self.orderReviewView.paymentName = self.paymentView.paymentMthd ?? ""
                    self.orderReviewView.shippingName = self.shippingView.name  ?? ""
                    self.orderReviewView.shippingPrice = self.shippingView.price  ?? ""
                    
                    self.orderReviewView.getValue(jsonData: responseObject!, call: WhichApiCall.none) { (data: Bool) in
                        print(data)
                        if data {
                            self.orderReviewView.chekoutOrderTableView.reloadData()
                            self.setViews()
                        }
                    }
                case .getAddress:
                    self.addressView.getValue(jsonData: responseObject! ) { (data: Bool) in
                        print(data)
                        if data {
                            self.emptyView?.isHidden = true
                            self.topView.isHidden = false
                            self.checkoutScrollView.isHidden = false
                            self.addressView.checkoutAddressTableView.reloadData()
                        } else {
                            self.topView.isHidden = true
                            self.checkoutScrollView.isHidden = true
                            self.emptyView?.isHidden = false
                        }
                    }
                case .updateCartAddress:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.checkoutScrollView.setContentOffset(CGPoint(x: SCREEN_WIDTH, y: 0), animated: true)
                        
                        self.makeRequest(dict: [:], call: CheckoutApiCall.getShpppingMethod)
                    }
                case .getShpppingMethod:
                    self.setViews()
                    self.shippingBtn.isUserInteractionEnabled = true
                    self.shippingView.getValue(jsonData: responseObject! ) { (data: Bool) in
                        print(data)
                        if data {
                            self.shippingView.checkoutShippingTableView.reloadData()
                        } else {
                            let ac = self.alertWithTitle(title: "Error", message: "noshippingmethod".localized, buttonTitle: "ok")
                            self.parent!.present(ac, animated: true, completion: nil)
                        }
                    }
                case .updateShippingMethod:
                    if let id = self.shippingView.id, id == "0" {
                        self.checkoutScrollView.setContentOffset(CGPoint(x: 2*SCREEN_WIDTH, y: 0), animated: true)
                        self.makeRequest(dict: [:], call: CheckoutApiCall.getPaymentMethods)
                    } else {
                        if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                            self.checkoutScrollView.setContentOffset(CGPoint(x: 2*SCREEN_WIDTH, y: 0), animated: true)
                            self.makeRequest(dict: [:], call: CheckoutApiCall.getPaymentMethods)
                        }
                    }
                case .getPaymentMethods:
                    self.paymentBtn.isUserInteractionEnabled = true
                    self.setViews()
                    self.paymentView.getValue(jsonData: responseObject! ) {(data: Bool) in
                        print(data)
                        if data {
                            self.paymentView.checkoutPaymentTableView.reloadData()
                        } else {
                            let ac = self.alertWithTitle(title: "Error", message: "nopaymentmethod".localized, buttonTitle: "ok")
                            self.parent!.present(ac, animated: true, completion: nil)
                        }
                    }
                case .orderPlace:
                    if let allData = responseObject {
                        if allData ["order"]["id_order"].string != nil {
                            UserDefaults.set(value: "0", key: UserDefaults.Keys.cartCount)
                            UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.id_cart)
                            UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.id_guest)
                            UserDefaults.standard.synchronize()
                            if let value = allData ["next_id_cart"].string {
                                UserDefaults.set(value: value, key: UserDefaults.Keys.id_cart)
                                UserDefaults.standard.synchronize()
                            }
                            if let view = UIStoryboard.loadOrderPlaceViewController() {
                                view.id = allData ["order"]["id_order"].stringValue
                                view.desc = allData ["order"]["order_message"].string ?? "orderDesc".localized
                                self.navigationController?.pushViewController(view, animated: true)
                            }
                        }
                    }
                default:
                    print()
                }
            }
        }
    }
    
    func alertWithTitle(title: String, message: String, buttonTitle: String) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle, style: .default, handler: new)
        alertController.addAction(action)
        return alertController
    }
    
    func new(alertAction: UIAlertAction!) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension CheckoutDataViewController: PassCheckoutData {
    
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: CheckoutApiCall) {
        switch call {
        case .updateCartAddress:
            self.makeRequest(dict: dict, call: call)
        case .updateShippingMethod:
            if termsOn == 1 {
                var dict = [String: Any]()
                dict["id_cart"] = UserDefaults.fetch(key: UserDefaults.Keys.id_cart)
                dict["id_address_delivery"] = id_address_delivery
                dict["id_carrier"] = shippingView.id
                dict["id_address_invoice"] = id_address_invoice
                self.makeRequest(dict: dict, call: CheckoutApiCall.updateShippingMethod)
            } else {
                ErrorChecking.warningView(message: "agreeterms".localized)
            }
        case .orderReview:
             self.checkoutScrollView.setContentOffset(CGPoint(x: 3*SCREEN_WIDTH, y: 0), animated: true)
            self.makeRequest(dict: [:], call: call)
        case .orderPlace:
            var dict = [String: Any]()
            if paymentView.name == "paytm" {
                self.beginPayment(data: self.paymentView.paymentConfigs)
            } else {
                dict["module"] = paymentView.name
                dict["payment"] = paymentView.paymentMthd
                dict["total_paid"] = self.cart_total_order ?? ""//shippingView.price
                dict["message"] = shippingView.message ?? ""
                dict["current_state"] = paymentView.orderState
                dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
                dict["id_cart"] =   UserDefaults.fetch(key: UserDefaults.Keys.id_cart)
                self.makeRequest(dict: dict, call: CheckoutApiCall.orderPlace)
            }
        default:
            print()
        }
    }
}

extension CheckoutDataViewController: EmptyDelegate {
    func buttonPressed() {
        if let view = UIStoryboard.loadAddAddressViewController() {
            view.delegate = self
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
}

extension CheckoutDataViewController: AddressUpdate {
    func addressupdated(addressId: String) {
         self.makeRequest(dict: [:], call: CheckoutApiCall.getAddress)
    }
}

extension CheckoutDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        switch controller {
        case .addAddress:
            if let view = UIStoryboard.loadAddAddressViewController() {
                view.addressId = id
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .changeDeliveryAddress:
            if let view = UIStoryboard.loadAddresslViewController() {
                view.controller = controller
                view.changeAddressDelegate = self
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .changeInvoiceAddress:
            if let view = UIStoryboard.loadAddresslViewController() {
                view.controller = controller
                view.changeAddressDelegate = self
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .cmsController:
            if let view = UIStoryboard.loadCmsViewController() {
                view.controller = CmsControllers.termsCondition
                self.navigationController?.pushViewController(view, animated: true)
            }
        default:
            print()
        }
    }
}

extension CheckoutDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        termsOn = index
    }
}

extension CheckoutDataViewController: ChangeAddress {
    
    func addressChanged(addressId: String, controller: AllController) {
        if controller == AllController.changeInvoiceAddress {
                self.addressView.id_address_invoice = addressId
              self.makeRequest(dict: [:], call: CheckoutApiCall.getAddress)
        }
        if controller == AllController.changeDeliveryAddress {
               self.addressView.id_address_delivery = addressId
              self.makeRequest(dict: [:], call: CheckoutApiCall.getAddress)
        }
    }
}

extension CheckoutDataViewController: PGTransactionDelegate {
    
    func beginPayment(data: Configs) {
        var txnController = PGTransactionViewController()
        var serv = PGServerEnvironment()
        var type :ServerType!
        
        if data.live_mode {
            serv = serv.createProductionEnvironment()
            type = .eServerTypeProduction
        } else {
            serv = serv.createStagingEnvironment()
            type = .eServerTypeStaging
        }
        let order = PGOrder(orderID: "", customerID: "", amount: "", eMail: "", mobile: "")
        if let EMAIL = data.EMAIL, let MOBILE_NO = data.MOBILE_NO, EMAIL != "", MOBILE_NO != "" {
            order.params = ["MID": data.MID ?? "",
                            "ORDER_ID": data.ORDER_ID ?? "",
                            "CUST_ID": data.CUST_ID ?? "",
                            "MOBILE_NO": MOBILE_NO ,
                            "EMAIL": EMAIL,
                            "CHANNEL_ID": data.CHANNEL_ID ?? "",
                            "WEBSITE": data.WEBSITE ?? "",
                            "TXN_AMOUNT": data.TXN_AMOUNT ?? "",
                            "INDUSTRY_TYPE_ID": data.INDUSTRY_TYPE_ID ?? "",
                            "CHECKSUMHASH": data.CHECKSUMHASH ?? "",
                            "CALLBACK_URL": data.CALLBACK_URL ?? ""]
        } else if let EMAIL = data.EMAIL, EMAIL != "" {
            order.params = ["MID": data.MID ?? "",
                            "ORDER_ID": data.ORDER_ID ?? "",
                            "CUST_ID": data.CUST_ID ?? "",
                            "EMAIL": EMAIL,
                            "CHANNEL_ID": data.CHANNEL_ID ?? "",
                            "WEBSITE": data.WEBSITE ?? "",
                            "TXN_AMOUNT": data.TXN_AMOUNT ?? "",
                            "INDUSTRY_TYPE_ID": data.INDUSTRY_TYPE_ID ?? "",
                            "CHECKSUMHASH": data.CHECKSUMHASH ?? "",
                            "CALLBACK_URL": data.CALLBACK_URL ?? ""]
        } else if let MOBILE_NO = data.MOBILE_NO, MOBILE_NO != "" {
            order.params = ["MID": data.MID ?? "",
                            "ORDER_ID": data.ORDER_ID ?? "",
                            "CUST_ID": data.CUST_ID ?? "",
                            "MOBILE_NO": MOBILE_NO ,
                            "CHANNEL_ID": data.CHANNEL_ID ?? "",
                            "WEBSITE": data.WEBSITE ?? "",
                            "TXN_AMOUNT": data.TXN_AMOUNT ?? "",
                            "INDUSTRY_TYPE_ID": data.INDUSTRY_TYPE_ID ?? "",
                            "CHECKSUMHASH": data.CHECKSUMHASH ?? "",
                            "CALLBACK_URL": data.CALLBACK_URL ?? ""]
        } else {
            order.params = ["MID": data.MID ?? "",
                            "ORDER_ID": data.ORDER_ID ?? "",
                            "CUST_ID": data.CUST_ID ?? "",
                            "CHANNEL_ID": data.CHANNEL_ID ?? "",
                            "WEBSITE": data.WEBSITE ?? "",
                            "TXN_AMOUNT": data.TXN_AMOUNT ?? "",
                            "INDUSTRY_TYPE_ID": data.INDUSTRY_TYPE_ID ?? "",
                            "CHECKSUMHASH": data.CHECKSUMHASH ?? "",
                            "CALLBACK_URL": data.CALLBACK_URL ?? ""]
        }
        print(order.params)
        txnController =  txnController.initTransaction(for: order) as! PGTransactionViewController
        txnController.title = "Paytm Payments"
        txnController.setLoggingEnabled(true)
        if(type != ServerType.eServerTypeNone) {
            txnController.serverType = type;
        } else {
            return
        }
        txnController.merchant = PGMerchantConfiguration.defaultConfiguration()
        txnController.delegate = self
        self.navigationController?.pushViewController(txnController, animated: true)
    }
    
    //this function triggers when transaction gets finished
    func didFinishedResponse(_ controller: PGTransactionViewController, response responseString: String) {
        var jsonData: [String:Any]!
        let msg : String = responseString
        var titlemsg : String = "PaymentFailed".localized
        if let data = responseString.data(using: String.Encoding.utf8) {
            do {
                if let jsonresponse = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] , jsonresponse.count > 0{
                    titlemsg = jsonresponse["STATUS"] as? String ?? ""
                    jsonData = jsonresponse
                }
            } catch {
                print("Something went wrong")
            }
        }
        if let dict = jsonData {
            self.paymentLinkVerification(dict: dict, url: self.paymentView.id_module ?? "", titlemsg: titlemsg)
        } else {
            let actionSheetController: UIAlertController = UIAlertController(title: titlemsg , message: msg, preferredStyle: .alert)
            let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel) {
                action -> Void in
                //controller.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }
            actionSheetController.addAction(cancelAction)
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
    
    //this function triggers when transaction gets cancelled
    func didCancelTrasaction(_ controller : PGTransactionViewController) {
        //controller.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    //Called when a required parameter is missing.
    func errorMisssingParameter(_ controller : PGTransactionViewController, error : NSError?) {
        //controller.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func paymentLinkVerification(dict: [String:Any], url: String, titlemsg: String) {
        NetworkManager.fetchDataWithFormData(url: url, params: "", verb: .post, dict: dict, controller: .checkoutController, view: self.view) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                print("loginError".localized, error!)
                let actionSheetController: UIAlertController = UIAlertController(title: titlemsg , message: "", preferredStyle: .alert)
                let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel) {
                    action -> Void in
                    self.dismiss(animated: true, completion: nil)
                }
                actionSheetController.addAction(cancelAction)
                self.present(actionSheetController, animated: true, completion: nil)
            } else {
                if let allData = responseObject {
                    if allData["response"] != JSON.null {
                        UserDefaults.set(value: "0", key: UserDefaults.Keys.cartCount)
                        UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.id_cart)
                        UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.id_guest)
                        UserDefaults.standard.synchronize()
                        if let value = allData["response"]["next_id_cart"].string, allData["response"]["error_heading"].string == nil {
                            UserDefaults.set(value: value, key: UserDefaults.Keys.id_cart)
                            UserDefaults.standard.synchronize()
                            if let view = UIStoryboard.loadOrderPlaceViewController() {
                                view.id = allData["response"]["id_order"].stringValue
                                view.desc = allData["response"]["message"].string ?? "orderDesc".localized
                                self.navigationController?.pushViewController(view, animated: true)
                            }
                        } else {
                            let actionSheetController: UIAlertController = UIAlertController(title: titlemsg , message: "", preferredStyle: .alert)
                            let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel) {
                                action -> Void in
                                self.dismiss(animated: true, completion: nil)
                            }
                            actionSheetController.addAction(cancelAction)
                            self.present(actionSheetController, animated: true, completion: nil)
                        }
                    } else {
                        let actionSheetController: UIAlertController = UIAlertController(title: titlemsg , message: "", preferredStyle: .alert)
                        let cancelAction : UIAlertAction = UIAlertAction(title: "OK", style: .cancel) {
                            action -> Void in
                            self.dismiss(animated: true, completion: nil)
                        }
                        actionSheetController.addAction(cancelAction)
                        self.present(actionSheetController, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}
