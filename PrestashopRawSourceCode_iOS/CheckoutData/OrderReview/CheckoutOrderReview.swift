//
//  CheckoutOrderReview.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class CheckoutOrderReview: UIView {
    
    @IBOutlet weak var chekoutOrderTableView: UITableView!
    @IBOutlet weak var continueBtn: UIButton!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    var PassCheckoutDataDelegate: PassCheckoutData?
    
    var paymentName: String!
    var shippingName: String!
    var shippingPrice: String!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let xibView = Bundle.main.loadNibNamed("CheckoutOrderReview", owner: self, options: nil)?[0] as? UIView {
            xibView.frame = self.bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(xibView)
        }
    }
    
    override func awakeFromNib() {
        continueBtn.setTitle("placeOrder".localized, for: .normal)
        continueBtn.SetAccentBackColor()
        chekoutOrderTableView.register(ProductOrderTableViewCell.nib, forCellReuseIdentifier: ProductOrderTableViewCell.identifier)
        chekoutOrderTableView.register(OrderPriceView.nib, forCellReuseIdentifier: OrderPriceView.identifier)
        chekoutOrderTableView.register(AddressDataTableViewCell.nib, forCellReuseIdentifier: AddressDataTableViewCell.identifier)
        chekoutOrderTableView.register(OrderPaymentTableViewCell.nib, forCellReuseIdentifier: OrderPaymentTableViewCell.identifier)
        chekoutOrderTableView.register(OrderShippingTableViewCell.nib, forCellReuseIdentifier: OrderShippingTableViewCell.identifier)
        chekoutOrderTableView.register(VouchersAppliedTableViewCell.nib, forCellReuseIdentifier: VouchersAppliedTableViewCell.identifier)
        chekoutOrderTableView.dataSource = self
        chekoutOrderTableView.delegate = self
    }
    
    @IBAction func continuePressed(_ sender: UIButton) {
        PassCheckoutDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: CheckoutApiCall.orderPlace)
    }
    
    var items = [OrderDetailViewItem]()
    func getValue( jsonData: JSON, call: WhichApiCall, completion: ((_ data: Bool) -> Void) ) {
        guard let data =  CheckoutOrderReviewModal(data: jsonData) else {
            return
        }
        items.removeAll()
        if !data.products.isEmpty {
            items.append(OrderViewModalProductItem(products: data.products))
            items.append(OrderViewModalOrdersItem(orderData: data.orderPrice!))
            if !data.voucherArray.isEmpty {
                items.append(OrderViewModalAppliedVoucherItem(vouchers: data.voucherArray))
            }
            items.append(OrderViewModalAddressItem(shipping: data.addresses))
            items.append(OrderViewModalPaymentItem(payment: paymentName))
            if shippingName != "" {
                items.append(OrderViewModalShippingItem(shipping: shippingName, price: shippingPrice))
            }
            completion(true)
        }
        
    }
    
}

extension CheckoutOrderReview: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        
        switch item.type {
        case .products:
            if let item = item as? OrderViewModalProductItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: ProductOrderTableViewCell.identifier) as? ProductOrderTableViewCell {
                    cell.item = item.products[indexPath.row]
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .pricedata :
            if let item = item as? OrderViewModalOrdersItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: OrderPriceView.identifier) as? OrderPriceView {
                    cell.item = item.orderData
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .voucher:
            if let item = item as? OrderViewModalAppliedVoucherItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: VouchersAppliedTableViewCell.identifier) as? VouchersAppliedTableViewCell {
                    cell.item = item.vouchers[indexPath.row]
                    cell.deleteBtn.isHidden = true
                    cell.selectionStyle = .none
                    return cell
                }
            }
            
        case .address:
            
            if let item = item as? OrderViewModalAddressItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: AddressDataTableViewCell.identifier) as? AddressDataTableViewCell {
                    cell.item = item.shipping
                    cell.arrowImage.isHidden = true
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .payment:
            if let item = item as? OrderViewModalPaymentItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: OrderPaymentTableViewCell.identifier) as? OrderPaymentTableViewCell {
                    cell.paymentMethodName.text = item.payment
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .shipping:
            if let item = item as? OrderViewModalShippingItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: OrderShippingTableViewCell.identifier) as? OrderShippingTableViewCell {
                    cell.shippingMethodName.text = item.shipping
                    cell.shippingPrice.text = item.price
                    cell.selectionStyle = .none
                    return cell
                }
            }
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let item = items[section]
        
        switch item.type {
        case .products:
            if let item = item as? OrderViewModalProductItem {
                return item.heading
            }
        case .pricedata:
            if let item = item as? OrderViewModalOrdersItem {
                return item.heading
            }
        case .address:
            if let item = item as? OrderViewModalAddressItem {
                return item.heading
            }
        case .payment:
            if let item = item as? OrderViewModalPaymentItem {
                return item.heading
                
            }
        case .shipping:
            if let item = item as? OrderViewModalShippingItem {
                return item.heading
                
            }
        case .billing:
            return nil
        case .status:
            return nil
        case .voucher:
            if let item = item as? OrderViewModalAppliedVoucherItem {
                return item.heading
                
            }
        default:
            return nil
        }
        return nil
    }
    
}

enum OrderCases {
    case pricedata
    case products
    case address
    case payment
    case shipping
    case billing
    case status
    case voucher
    case messages
    case AddMessage
}

protocol OrderDetailViewItem {
    var type: OrderCases { get }
    var rowCount: Int { get }
    var heading: String { get }
}

class OrderViewModalOrdersItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .pricedata
    }
    
    var rowCount: Int {
        return 1
    }
    
    var heading: String {
        return "orderDetail".localized
    }
    var orderData: CartPriceData?
    
    init(orderData: CartPriceData ) {
        self.orderData = orderData
    }
    
}

class OrderViewModalProductItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .products
    }
    
    var rowCount: Int {
        return products.count
    }
    
    var heading: String {
        return "reviewItems".localized
    }
    var products: [ProductData]
    
    init(products: [ProductData] ) {
        self.products = products
    }
    
}

class OrderViewModalAddressItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .address
    }
    
    var rowCount: Int {
        return 1
    }
    
    var heading: String {
        return "shippingAddress".localized
    }
    var shipping: AddressData?
    
    init(shipping: AddressData? ) {
        self.shipping = shipping
    }
    
}

class OrderViewModalBillingAddressItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .billing
    }
    
    var rowCount: Int {
        return 1
    }
    
    var heading: String {
        return "billingAddress".localized
    }
    var billing: AddressData?
    
    init(billing: AddressData? ) {
        self.billing = billing
    }
    
}

class OrderViewModalShippingItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .shipping
    }
    
    var rowCount: Int {
        return 1
    }
    
    var heading: String {
        return "shipping".localized
    }
    var shipping: String?
    var price: String?
    
    init(shipping: String?, price: String? ) {
        self.shipping = shipping
        self.price = price
    }
    
}

class OrderViewModalPaymentItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .payment
    }
    
    var rowCount: Int {
        return 1
    }
    
    var heading: String {
        return "payment".localized
    }
    var payment: String!
    
    init(payment: String? ) {
        self.payment = payment
    }
    
}

class OrderViewModalStatusItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .status
    }
    
    var rowCount: Int {
        return 1
    }
    
    var heading: String {
        return "status".localized
    }
    var status: String!
    
    init(status: String? ) {
        self.status = status
    }
    
}

class OrderViewModalAppliedVoucherItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .voucher
    }
    
    var rowCount: Int {
        return vouchers.count
    }
    
    var vouchers: [VoucherData]
    var heading: String {
        return "Vouchers".localized
    }
    
    init(vouchers: [VoucherData] ) {
        self.vouchers = vouchers
        
    }
}

class OrderViewModalMessagesItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .messages
    }
    
    var rowCount: Int {
        return messages.count
    }
    
    var messages: [OrderMessages]
    var heading: String {
        return "Messages".localized
    }
    
    init(messages: [OrderMessages] ) {
        self.messages = messages
        
    }
}

class OrderViewModalAddMessageItem: OrderDetailViewItem {
    
    var type: OrderCases {
        return .AddMessage
    }
    
    var rowCount: Int {
        return 1
    }
    
    var messages: String
    var heading: String {
        return "".localized
    }
    
    init(messages: String ) {
        self.messages = messages
        
    }
}
