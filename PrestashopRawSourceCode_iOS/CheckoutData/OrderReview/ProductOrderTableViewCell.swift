//
//  ProductOrderTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
//

import UIKit
import Photos
import SVProgressHUD

class ProductOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var downloadProductBtn: UIButton!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    var downloadDelegate: DownloadProduct?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        productLabel.SetDefaultTextColor()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: ProductData? {
        didSet {
            productImage.setImage(imageUrl: (item?.image_link )!)
            productLabel.text = "product".localized
            nameLabel.text = item?.name
            priceLabel.text = String.init(format: "%@: %@", "price".localized, (item?.price)!)
            qtyLabel.text = String.init(format: "%@: %@", "qty".localized, (item?.quantity)!)
            totalLabel.text = String.init(format: "%@: %@", "subTotal".localized, (item?.total)!)
            priceLabel.halfTextColorChange(fullText: priceLabel.text!, changeText: "price".localized)
            qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: "qty".localized)
            totalLabel.halfTextColorChange(fullText: totalLabel.text!, changeText: "subTotal".localized)
//            if let check = item?.is_download_item, check {
                downloadProductBtn.isHidden = false
//            } else {
//                downloadProductBtn.isHidden = true
//            }
        }
    }
    
    @IBAction func tapDownloadFileBtn(_ sender: UIButton) {
        if let item = self.item {
            downloadDelegate?.StartDownload(item: item)
        } else {
            
        }
    }
    
}
