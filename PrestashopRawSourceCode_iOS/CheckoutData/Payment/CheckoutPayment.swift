//
//  CheckoutPayment.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
class CheckoutPayment: UIView {
    
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var checkoutPaymentTableView: UITableView!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    @IBAction func continueClicked(_ sender: Any) {
        if paymentMthd != nil {
            PassCheckoutDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: CheckoutApiCall.orderReview)
        } else {
            ErrorChecking.warningView(message: "selectPaymentMethod".localized)
        }
    }
    
    var PassCheckoutDataDelegate: PassCheckoutData?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let xibView = Bundle.main.loadNibNamed("CheckoutPayment", owner: self, options: nil)?[0] as? UIView {
            xibView.frame = self.bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.addSubview(xibView)
        }
    }
    override func awakeFromNib() {
        continueBtn.SetAccentBackColor()
        checkoutPaymentTableView.register(PaymentDataTableViewCell.nib, forCellReuseIdentifier: PaymentDataTableViewCell.identifier)
        checkoutPaymentTableView.delegate = self
        checkoutPaymentTableView.dataSource = self
    }
    
    var id_module: String?
    var paymentMethods = [PaymentData]()
    var name: String?
    var price: String?
    var paymentMthd: String!
    var orderState: String!
    var paymentConfigs: Configs!
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = CheckoutPaymentModal(data: jsonData) else {
            return
        }
        
        if !data.paymentMethods.isEmpty {
            self.paymentMethods = data.paymentMethods
            id_module = paymentMethods[0].id_module
            paymentMthd = paymentMethods[0].display_name
            name = paymentMethods[0].name
            orderState = paymentMethods[0].id_order_state
            paymentConfigs = paymentMethods[0].paymentConfigs
            completion(true)
        } else {
            completion(false)
        }
    }
}

extension CheckoutPayment: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentMethods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let  cell = tableView.dequeueReusableCell(withIdentifier: PaymentDataTableViewCell.identifier, for: indexPath) as? PaymentDataTableViewCell {
            if  id_module != nil {
                cell.paymentID = id_module!
            }
            cell.item = paymentMethods[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "paymentMethod".localized
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        id_module = paymentMethods[indexPath.row].id_module
        name = paymentMethods[indexPath.row].name
        paymentMthd = paymentMethods[indexPath.row].display_name
        orderState = paymentMethods[indexPath.row].id_order_state
        paymentConfigs = paymentMethods[indexPath.row].paymentConfigs
        checkoutPaymentTableView.reloadData()
    }
    
}
