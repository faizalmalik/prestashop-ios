//
//  CheckoutPaymentTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
//

import UIKit

class PaymentDataTableViewCell: UITableViewCell {

    @IBOutlet weak var CheckboxImage: UIImageView!
    @IBOutlet weak var paymentMethodName: UILabel!
    @IBOutlet weak var additionalInfoLbl: UILabel!
    @IBOutlet weak var additionalInfoBottom: NSLayoutConstraint!
    
    var paymentID: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CheckboxImage.applyBorder(colours: GlobalConstants.Colors.accentColor)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }
    var item: PaymentData? {
        didSet {
            paymentMethodName.text = item?.display_name
            additionalInfoLbl.text = ""
            additionalInfoBottom.constant = 0
            if paymentID == item?.id_module {
                CheckboxImage.backgroundColor = GlobalConstants.Colors.accentColor
                CheckboxImage.image = #imageLiteral(resourceName: "check-icon24x24")
                if let additional_information = item?.additional_information, additional_information != "" {
                    additionalInfoBottom.constant = 8
                    additionalInfoLbl.text = additional_information.htmlToString.RemoveLastEnter()
                    additionalInfoLbl.SetDefaultTextColor()
                }
            } else {
                CheckboxImage.backgroundColor = UIColor.clear
                CheckboxImage.image = nil
            }
        }
    }

}
