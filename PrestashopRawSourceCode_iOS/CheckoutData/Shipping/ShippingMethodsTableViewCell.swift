//
//  ShippingMethodsTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class ShippingMethodsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var delayLabel: UILabel!
     @IBOutlet weak var priceLabel: UILabel!
     @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var CheckboxImage: UIImageView!
    var shippingId: String?

    override func awakeFromNib() {
        super.awakeFromNib()
         delayLabel.SetDefaultTextColor()
         priceLabel.SetDefaultTextColor()
         messageLabel.SetDefaultTextColor()
         CheckboxImage.applyBorder(colours: GlobalConstants.Colors.accentColor)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var item: ShippingData? {
        didSet {
            nameLabel.text = item?.name
            delayLabel.text = item?.delay
            priceLabel.text = item?.price_with_tax
            messageLabel.text = item?.display_message
            if shippingId == item?.id_carrier {
                CheckboxImage.backgroundColor = GlobalConstants.Colors.accentColor
                CheckboxImage.image = #imageLiteral(resourceName: "check-icon24x24")
            } else {
                CheckboxImage.backgroundColor = UIColor.clear
                CheckboxImage.image = nil
            }
        }
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
