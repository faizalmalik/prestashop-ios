//
//  ShippingFooterView.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class ShippingFooterView: UIView {

    @IBOutlet weak var messsageTextField: UITextField!
    @IBOutlet weak var agreeLabel: UILabel!
    @IBOutlet weak var leaveLabel: UILabel!
    @IBOutlet weak var shippingSwitch: UISwitch!
    @IBOutlet weak var termBtn: UIButton!
    var passData: PassData?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
        agreeLabel.text = "agreeCondition".localized
        termBtn.setTitle("viewTerms".localized, for: .normal)
        leaveLabel.text = "leaveMessage".localized
    }

    var moveDelegate: MoveController?

    @IBAction func viewTermsConditionTapped(_ sender: Any) {
        moveDelegate?.moveController(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.cmsController)
    }

    @IBAction func termsAndConditionSwitch(_ sender: UISwitch) {
        if shippingSwitch.isOn {
            passData?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 1, call: WhichApiCall.terms)
        } else {
            passData?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.terms)
        }
    }

}
