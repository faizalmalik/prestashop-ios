//
//  CheckoutShipping.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class CheckoutShipping: UIView {
    
    var moveDelegate: MoveController?
    var shippingSwitch: UISwitch!
    @IBOutlet weak var checkoutShippingTableView: UITableView!
    
    @IBOutlet weak var continueBtn: UIButton!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    var passData: PassData?
    @IBAction func continueClicked(_ sender: UIButton) {
        if id != nil {
            PassCheckoutDataDelegate?.passData(id: id!, name: "", dict: [:], jsonData: JSON.null, index: 0, call: CheckoutApiCall.updateShippingMethod)
        } else {
            ErrorChecking.warningView(message: "selectShippingMethod".localized)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let xibView = Bundle.main.loadNibNamed("CheckoutShipping", owner: self, options: nil)?[0] as? UIView {
            xibView.frame = self.bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(xibView)
        }
    }
    
    override func awakeFromNib() {
        continueBtn.SetAccentBackColor()
        continueBtn.setTitle("continue".localized, for: .normal)
        checkoutShippingTableView.delegate = self
        checkoutShippingTableView.dataSource = self
        checkoutShippingTableView.register(ShippingMethodsTableViewCell.nib, forCellReuseIdentifier: ShippingMethodsTableViewCell.identifier)
    }
    
    var PassCheckoutDataDelegate: PassCheckoutData?
    var shippingTableView =  UITableView()
    var message: String!
    var shippingMethods = [ShippingData]()
    var id: String?
    var name: String?
    var price: String?
    var footerView: ShippingFooterView?
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = CheckoutShippingModal(data: jsonData) else {
            return
        }
        
        print(data.shippingMethods)
        
        if !data.shippingMethods.isEmpty {
            self.shippingMethods = data.shippingMethods
            id = shippingMethods[0].id_carrier
            name = shippingMethods[0].name
            price = shippingMethods[0].price_with_tax
            completion(true)
        } else if data.isShippingRequired != nil && data.isShippingRequired == false {
            id = "0"
            name = ""
            price = ""
            PassCheckoutDataDelegate?.passData(id: id!, name: "", dict: [:], jsonData: JSON.null, index: 0, call: CheckoutApiCall.updateShippingMethod)
            completion(true)
        } else {
            completion(false)
        }
    }
}

extension CheckoutShipping: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shippingMethods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let  cell = tableView.dequeueReusableCell(withIdentifier: ShippingMethodsTableViewCell.identifier, for: indexPath) as? ShippingMethodsTableViewCell {
            if  id != nil {
                cell.shippingId = id!
            }
            cell.item = shippingMethods[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "shippingMethod".localized
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        id = shippingMethods[indexPath.row].id_carrier
        name = shippingMethods[indexPath.row].name
        price = shippingMethods[indexPath.row].price_with_tax
        checkoutShippingTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        footerView = (Bundle.main.loadNibNamed("ShippingFooterView", owner: self, options: nil)?[0] as? ShippingFooterView)!
        shippingSwitch = footerView?.shippingSwitch
        footerView?.moveDelegate = self
        footerView?.passData = self
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 170
    }
    
}

extension CheckoutShipping: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        moveDelegate?.moveController(id: id, name: name, dict: dict, jsonData: jsonData, index: index, controller: controller)
    }
}

extension CheckoutShipping: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        passData?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: index, call: WhichApiCall.terms)
    }
}
