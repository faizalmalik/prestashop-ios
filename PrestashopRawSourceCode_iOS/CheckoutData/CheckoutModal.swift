//
//  CheckoutModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

struct CheckoutAddressModal {

    private let addressPath = ["addresses", "address"]
   
    private let cartAddressPath =  ["address"]

    var addressArray = [AddressData]()
    init?(data: JSON) {
        if data[addressPath] != JSON.null {
            if let data  = data[addressPath].array {
                self.addressArray = data.map { AddressData( data: $0) }
            } else {
                self.addressArray.append(AddressData(data: data[addressPath]))
            }
        }

    }
}

struct CheckoutShippingModal {
    private let shippingPath = ["response", "address_row", "carriers"]
    private let carrierPath = ["response", "address_row", "carriers", "carrier_row"]
     private let carrierlistPath = ["response", "address_row", "carriers", "carrier_row", "carrier_list"]

    var shippingMethods = [ShippingData]()
    var isShippingRequired: Bool!
    init?(data: JSON) {

        if data[shippingPath] != JSON.null {
            if let data  = data[shippingPath].array {
                self.shippingMethods = data.map { ShippingData( data: $0) }
            } else {
                if let data  = data[carrierPath].array {
                    self.shippingMethods = data.map { ShippingData( data: $0) }
                } else {
                     if let data  = data[carrierlistPath].array {
                        self.shippingMethods = data.map { ShippingData( data: $0) }
                     } else {
                         self.shippingMethods.append(ShippingData(data: data[shippingPath]))
                    }                   
                }
            }
        }
        if data["response"]["address_row"] == JSON.null && data["response"]["status"].stringValue == "1" {
            isShippingRequired = false
        }
    }
}

struct CheckoutPaymentModal {
    private let paymentPath = ["payment_methods", "payment" ]
    var array = ["bankwire", "cheque", "cashondelivery", "awaiting_check", "awaiting_bankwire", "ps_cashondelivery", "awaiting_cod", "paytm", "ps_checkpayment", "ps_wirepayment"]
    var paymentMethods = [PaymentData]()
    init?(data: JSON) {

        if data[paymentPath] != JSON.null {
            if let data  = data[paymentPath].array {
                let val  = data.filter {  array.contains(PaymentData( data: $0).name) }
                if val.count > 0 {
                    self.paymentMethods = val.map { PaymentData( data: $0) }
                }
                //                self.paymentMethods = data.map { PaymentData( data: $0) }
            } else {
                self.paymentMethods.append(PaymentData(data: data[paymentPath]))
            }
        }

    }
}

struct CheckoutOrderReviewModal {

    private let cartPath = ["carts", "cart"]
    private let cartProductPath = ["carts", "cart", "products", "product"]
var voucherArray = [VoucherData]()
    var products = [ProductData]()
    var orderPrice: CartPriceData?

    private let cartAddressPath =  ["address"]
private let VoucherPath =  ["carts", "cart", "vouchers", "voucher_row"]
    var addresses: AddressData!

    init?(data: JSON) {

        if data[cartProductPath] != JSON.null {
            if let data  = data[cartProductPath].array {
                self.products = data.map { ProductData( data: $0) }
            } else {
                self.products.append(ProductData(data: data[cartProductPath]))
            }
        }
        
        if data[VoucherPath] != JSON.null {
            if let data  = data[VoucherPath].array {
                self.voucherArray = data.map { VoucherData( data: $0) }
            } else {
                print(data[VoucherPath])
                self.voucherArray.append(VoucherData(data: data[VoucherPath]))
            }
        }
        
        self.orderPrice = CartPriceData(data: data[cartPath])
        if data[cartAddressPath] != JSON.null {

                self.addresses = AddressData(data: data[cartAddressPath])

        }
    }
}

struct PaymentData {
    var display_name: String!
    var id_module: String!
    var name: String!
    var id_order_state: String!
    var code: String!
    var additional_information: String!
    var paymentConfigs: Configs!
    init(data: JSON) {
        self.display_name = data["display_text"].string ?? data["display_name"].stringValue
        self.id_module = data["id_module"].string ?? data["payment_validation_link"].stringValue
        self.name = data["name"].stringValue
        self.id_order_state = data["status"]["id_order_state"].string ?? "12"
        self.code = data["status"]["code"].stringValue
        self.additional_information = data["additional_information"].stringValue
        self.paymentConfigs = Configs(data: data["configs"])
    }
}

struct ShippingData {
    var delay: String!
    var id_carrier: String!
    var name: String!
    var display_message: String!
    var price_with_tax: String!

    init(data: JSON) {
        self.delay = data["carrier_row"]["delay"].string ?? data["delay"].stringValue
        self.id_carrier = data["carrier_row"]["id_carrier"].string ?? data["id_carrier"].stringValue
        self.name = data["carrier_row"]["name"].string ?? data["name"].stringValue
        self.display_message = data["carrier_row"]["display_message"].string ?? data["display_message"].stringValue
        self.price_with_tax = data["carrier_row"]["price_with_tax"].string ?? data["price_with_tax"].string ?? data["shipping_price"].string ?? data["carrier_row"]["shipping_price"].stringValue

    }
}

struct AddressData {
    var alias: String!
    var name: String!
    var completeAddress: String = ""
    var phone: String!
    var addressId: String!

    init(data: JSON) {
        self.alias = data["alias"].stringValue
        self.name = String.init(format: "%@ %@", data["firstname"].stringValue, data["lastname"].stringValue)

        if let value =  data["address1"].string {
            self.completeAddress.append(value)
        }

        if let value =  data["address2"].string {
            self.completeAddress.append(String.init(format: ", %@", value ))
        }
        if let value =  data["city"].string {
            self.completeAddress.append(String.init(format: ", %@", value ))
        }
        if let value =  data["postcode"].string {
            self.completeAddress.append(String.init(format: ", %@", value ))
        }
        if let value =  data["state"].string {
            self.completeAddress.append(String.init(format: ", %@", value ))
        }
        if let value =  data["country"].string {
            self.completeAddress.append(String.init(format: ", %@", value ))
        }

        self.addressId = data["id"].stringValue

        if let value =  data["phone"].string {
            self.phone = value
        } else  if let value =  data["phone_mobile"].string {
            self.phone = value
        }

    }

}

struct Configs {
    var CHANNEL_ID: String!
    var WEBSITE: String!
    var INDUSTRY_TYPE_ID: String!
    var CHECKSUMHASH: String!
    var ORDER_ID: String!
    var show_promo_code: String!
    var TXN_AMOUNT: String!
    var CALLBACK_URL: String!
    var CUST_ID: String!
    var MID: String!
    var EMAIL: String!
    var MOBILE_NO: String!
    var live_mode: Bool!
    init(data: JSON) {
        CHANNEL_ID = data["CHANNEL_ID"].stringValue
        WEBSITE = data["WEBSITE"].stringValue
        INDUSTRY_TYPE_ID = data["INDUSTRY_TYPE_ID"].stringValue
        CHECKSUMHASH = data["CHECKSUMHASH"].stringValue
        ORDER_ID = data["ORDER_ID"].stringValue
        show_promo_code = data["show_promo_code"].stringValue
        TXN_AMOUNT = data["TXN_AMOUNT"].stringValue
        CALLBACK_URL = data["CALLBACK_URL"].stringValue
        CUST_ID = data["CUST_ID"].stringValue
        MID = data["MID"].stringValue
        EMAIL = data["EMAIL"].stringValue
        MOBILE_NO = data["MOBILE_NO"].stringValue
        live_mode = data["live_mode"].stringValue == "1" ? true:false
    }
}
