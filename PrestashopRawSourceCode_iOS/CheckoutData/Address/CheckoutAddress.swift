//
//  CheckoutAddress.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import FirebaseAnalytics

class CheckoutAddress: UIView {
    
    @IBOutlet weak var checkoutAddressTableView: UITableView!    
    @IBOutlet weak var continueBtn: UIButton!
    
    var addressShow: AddressData!
    var billingAddress: AddressData!
    var id_address_invoice: String!
    var id_address_delivery: String!
    var SectionCount = 0
    var PassCheckoutDataDelegate: PassCheckoutData?
    var moveDelegate: MoveController?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let xibView = Bundle.main.loadNibNamed("CheckoutAddress", owner: self, options: nil)?[0] as? UIView {
            xibView.frame = self.bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(xibView)
        }
    }
    
    override func awakeFromNib() {
        continueBtn.SetAccentBackColor()
        Analytics.logEvent(AnalyticsEventBeginCheckout, parameters: [AnalyticsParameterItemName: (UserDefaults.standard.value(forKey: "email") ?? "" )])
        checkoutAddressTableView.register(AddressShowTableViewCell.nib, forCellReuseIdentifier: AddressShowTableViewCell.identifier)
        checkoutAddressTableView.register(DiffrentBillingTableViewCell.nib, forCellReuseIdentifier: DiffrentBillingTableViewCell.identifier)
        continueBtn.setTitle("continue".localized, for: .normal)
        checkoutAddressTableView.delegate = self
        checkoutAddressTableView.dataSource = self
    }
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = CheckoutAddressModal(data: jsonData) else {
            return
        }
        print("id_address_delivery", id_address_delivery)
        if !data.addressArray.isEmpty {
            if SectionCount == 0 {
                SectionCount = 3
            }
            if id_address_delivery == nil ||  id_address_delivery.count == 0 || id_address_delivery == "0"{
                addressShow = data.addressArray[0]
                billingAddress = data.addressArray[0]
                id_address_delivery = data.addressArray[0].addressId
                id_address_invoice = data.addressArray[0].addressId
            } else {
                if let index  = data.addressArray.index(where: {$0.addressId == id_address_delivery}) {
                    addressShow = data.addressArray[index]
                    id_address_delivery = data.addressArray[index].addressId
                    
                }
                if let index  = data.addressArray.index(where: {$0.addressId == id_address_invoice}) {
                    billingAddress = data.addressArray[index]
                    id_address_invoice = data.addressArray[index].addressId
                    
                }
            }
            if  id_address_invoice != nil &&  id_address_delivery.count > 0 && id_address_delivery != "0" {
                if let index  = data.addressArray.index(where: {$0.addressId == id_address_invoice}) {
                    billingAddress = data.addressArray[index]
                    id_address_invoice = data.addressArray[index].addressId
                    
                }
            }
            completion(true)
        } else {
            SectionCount = 0
            completion(false)
        }
    }
    
    @IBAction func continueClicked(_ sender: UIButton) {
        
        let dict: [String: Any] = ["id_address_delivery": id_address_delivery, "id_address_invoice": id_address_invoice]
        PassCheckoutDataDelegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: CheckoutApiCall.updateCartAddress)
    }
    
}

extension CheckoutAddress: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: AddressShowTableViewCell.identifier) as? AddressShowTableViewCell {
                cell.item = addressShow
                cell.selectionStyle = .none
                cell.changeBtn.tag = indexPath.section
                cell.delegate = self
                return cell
            }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: DiffrentBillingTableViewCell.identifier) as? DiffrentBillingTableViewCell {
                cell.selectionStyle = .none
                cell.passDataDelegate = self
                return cell
            }
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: AddressShowTableViewCell.identifier) as? AddressShowTableViewCell {
                cell.item = billingAddress
                cell.selectionStyle = .none
                cell.changeBtn.tag = indexPath.section
                cell.delegate = self
                return cell
            }
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return SectionCount
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "shippingAddress".localized
        case 2:
            return "billingAddress".localized
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            moveDelegate?.moveController(id: id_address_delivery, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.addAddress)
            
        }
        if indexPath.section == 2 {
            moveDelegate?.moveController(id: id_address_invoice, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.addAddress)
        }
    }
    
}

extension CheckoutAddress: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        moveDelegate?.moveController(id: id, name: name, dict: dict, jsonData: jsonData, index: index, controller: controller)
    }
}

extension CheckoutAddress: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        if index == 0 {
            id_address_invoice = id_address_delivery
            SectionCount = 2
            self.checkoutAddressTableView.reloadData()
        } else {
            SectionCount = 3
            self.checkoutAddressTableView.reloadData()
        }
    }
}
