//
//  DiffrentBillingTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON

class DiffrentBillingTableViewCell: UITableViewCell {

    @IBOutlet weak var diffBillingSwitch: UISwitch!
    @IBOutlet weak var diffAddressLabel: UILabel!
    var passDataDelegate: PassData!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        diffAddressLabel.text = "diffBillingAddress".localized
        // Initialization code
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func diffrenBillingAction(_ sender: UISwitch) {
        if diffBillingSwitch.isOn {
            passDataDelegate.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 1, call: WhichApiCall.diffrentBilling)
        } else {
            passDataDelegate.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.diffrentBilling)
        }
    }
}
