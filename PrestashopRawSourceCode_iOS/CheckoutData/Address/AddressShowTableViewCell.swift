//
//  AddressShowTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class AddressShowTableViewCell: UITableViewCell {

    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var aliasLabel: UILabel!
    @IBOutlet weak var completeAddressLabel: UILabel!
    @IBOutlet weak var changeBtn: UIButton!

    var delegate: MoveController?
    override func awakeFromNib() {
        super.awakeFromNib()
        changeBtn.setTitle("changeAddress".localized, for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    var item: AddressData? {
        didSet {
            aliasLabel.text = item?.alias
            nameLabel.text = item?.name
            phoneLabel.text = item?.phone
            completeAddressLabel.text = item?.completeAddress
        }
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    @IBAction func changeAddressClicked(_ sender: UIButton) {
        if sender.tag == 0 {
            delegate?.moveController(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.changeDeliveryAddress)
        } else {
             delegate?.moveController(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.changeInvoiceAddress)
        }
    }

    static var identifier: String {
        return String(describing: self)
    }
}
