//
//  ProductCategoryListCollectionViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import Kingfisher
import SwiftyJSON

class ProductCategoryListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var wishlistICOn: UIButton!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productOldPrice: UILabel!

     var delgate: PassData?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        productView.normalBorder()
        productOldPrice.SetDefaultTextColor()
        productPrice.textColor = GlobalConstants.Colors.priceColor
        productOldPrice.isHidden = true
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    @IBAction func WishlistPress(_ sender: UIButton) {
        delgate?.passData(id: (item?.id_product)!, name: (item?.id_product_attribute)!, dict: [:], jsonData: JSON.null, index: sender.tag, call: WhichApiCall.wishlistClicked)
    }
    
    var item: ProductData? {
        didSet {
            guard let item = item else {
                return
            }
            if item.added_in_wishlist == "1" {
                if  !WishlistContains.wishlistIds.contains(item.id_product) {
                     WishlistContains.wishlistIds.append(item.id_product)
                }
                wishlistICOn.setImage(#imageLiteral(resourceName: "wishlist-active"), for: .normal)
            } else {
                wishlistICOn.setImage(#imageLiteral(resourceName: "symbols-wishlist"), for: .normal)
            }
            
            if WishlistContains.wishlistIds.contains(item.id_product) {
                wishlistICOn.setImage(#imageLiteral(resourceName: "wishlist-active"), for: .normal)
            }
            
            if item.show_price {
                self.productPrice.isHidden = false
                self.productOldPrice.isHidden = false
            } else {
                self.productPrice.isHidden = true
                self.productOldPrice.isHidden = true
            }
            productImage.setImage(imageUrl: item.image_link ?? "")
            productName.text  = item.name
            productPrice.text = item.price
            productOldPrice.text = item.old_price
            productOldPrice.strike(text: item.old_price)
        }
    }
}
