//
//  ProductCategoryViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
//

import Foundation
import SwiftyJSON

class ProductCategoryViewModal: NSObject {
    var products = [ProductData]()
    var delegate: MoveController?
    var filterData = [FilterData]()
    var sortData = [SortData]()
    var passDataDelegate: PassData?
    var pagination: Pagination!
    var page = 1
    var isLoad = true
    var ViewType: Switching = .Grid
    
    var collectionView: UICollectionView!
    var moveDelegate: MoveController?
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = ProductCategoryModal(data: jsonData) else {
            return
        }
        if !data.products.isEmpty {
            products += (data.products)
            filterData = data.filterData
            sortData = data.sortData
            pagination = data.pagination
            completion(true)
        } else {
            products.removeAll()
            completion(false)
        }        
    }
}

extension ProductCategoryViewModal: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch ViewType {
        case .Grid:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCategoryCollectionViewCell.identifier, for: indexPath as IndexPath) as? ProductCategoryCollectionViewCell {
                
                cell.item = products[indexPath.row]
                cell.hero.modifiers = [.fade, .translate(y:20)]
                cell.productImage.hero.id  = "image_\(indexPath.item)"
                cell.productImage.hero.modifiers = [.arc]
                cell.productImage.isOpaque = true
                cell.wishlistICOn.tag = indexPath.row
                cell.backgroundColor = UIColor.white
                cell.delgate = self
                return cell
            }
        case .List:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCategoryListCollectionViewCell.identifier, for: indexPath as IndexPath) as? ProductCategoryListCollectionViewCell {
                
                cell.item = products[indexPath.row]
                cell.hero.modifiers = [.fade, .translate(y:20)]
                cell.productImage.hero.id  = "image_\(indexPath.item)"
                cell.productImage.hero.modifiers = [.arc]
                cell.productImage.isOpaque = true
                cell.wishlistICOn.tag = indexPath.row
                cell.backgroundColor = UIColor.white
                cell.delgate = self
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch ViewType {
        case .Grid:
            return CGSize(width: (SCREEN_WIDTH / 2 - 1 ), height: (SCREEN_WIDTH / 2) + 80)
        case .List:
            return CGSize(width: (SCREEN_WIDTH ), height: 151)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = ["products": products]
        moveDelegate?.moveController(id: products[indexPath.row].id_product!, name: products[indexPath.row].name!, dict: dict, jsonData: JSON.null, index: indexPath.row, controller: AllController.productController)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        for cell: UICollectionViewCell in collectionView.visibleCells {
            if let indexPath: IndexPath = collectionView.indexPath(for: cell) {
                if indexPath.row + 1 <  Int(pagination.totalproducts) ?? 0 && indexPath.row + 1 == products.count && isLoad {
                    page += 1
                    isLoad =  false
                    passDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: page, call: WhichApiCall.pagination)
                }
            }
        }
    }
}

enum Switching {
    case Grid
    case List
}

extension ProductCategoryViewModal: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: StoreProductTableViewCell.identifier) as? StoreProductTableViewCell {
            cell.item = products[indexPath.row]
            
            cell.hero.modifiers = [.fade, .translate(x:-100)]
            cell.productImage!.hero.id = "image_\(indexPath.item)"
            cell.productImage!.hero.modifiers = [.arc]
            cell.productImage!.isOpaque = true
            cell.backgroundColor = UIColor.white
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SCREEN_WIDTH/2.5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = ["products": products]
        moveDelegate?.moveController(id: products[indexPath.row].id_product!, name: products[indexPath.row].name!, dict: dict, jsonData: JSON.null, index: indexPath.row, controller: AllController.productController)
    }
}

extension UIAlertController {
    func addActions(actions: [UIAlertAction], preferred: String? = nil) {
        
        for action in actions {
            self.addAction(action)
            
            if let preferred = preferred, preferred == action.title {
                self.preferredAction = action
            }
        }
    }
}

extension ProductCategoryViewModal: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        passDataDelegate?.passData(id: id, name: name, dict: dict, jsonData: jsonData, index: index, call: call)
    }
}
