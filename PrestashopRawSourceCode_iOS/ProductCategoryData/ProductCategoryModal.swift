//
//  ProductCategoryModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class ProductCategoryModal {

    var products = [ProductData]()
    var filterData = [FilterData]()
     var sortData = [SortData]()
    
    var pagination: Pagination!

    let productPath = ["products", "allproducts", "product"]
    let filterPath = ["navigation_details", "filters"]
    let sortPath = ["sort_by", "sort_data"]
    let paginationPath = ["products", "pagination"]

    init?(data: JSON) {

        AddonsEnabled.AddonsChecks(data: data)
        if data[productPath] != JSON.null {
            if let data  = data[productPath].array {
                self.products = data.map { ProductData( data: $0) }
            } else {
                self.products.append( ProductData( data: data[productPath] ))
            }
        }

        if data[filterPath] != JSON.null {
            if let data  = data[filterPath].array {
                self.filterData = data.map { FilterData( data: $0) }
            } else {
                self.filterData.append( FilterData( data: data[filterPath] ))
            }
        }
        if data[sortPath] != JSON.null {
            if let data  = data[sortPath].array {
                self.sortData = data.map { SortData( data: $0) }
            } else {
                self.sortData.append( SortData( data: data[sortPath] ))
            }
        }
        
        if data[paginationPath] != JSON.null {
            pagination = Pagination(data: data[paginationPath])
        }
        
    }
}

struct Pagination {
    var totalproducts: String!
    var perpage: String!
    var showingproducts: String!
    init(data: JSON) {
        totalproducts = data["totalproducts"].stringValue
        perpage = data["perpage"].stringValue
        showingproducts = data["showingproducts"].stringValue
    }
    
}

struct FilterData {
    var attribute_group_name: String!
    var id_key: String!
    var attribute_group_rewrite: String!
    var slider: String!
     let filterPath = ["values", "value"]
     var filterValuesData = [FiltervaluesData]()
    init(data: JSON) {
        self.attribute_group_name = data["attribute_group_name"].stringValue
        self.id_key = data["id_key"].stringValue
        self.attribute_group_rewrite = data["attribute_group_rewrite"].stringValue
        self.slider = data["slider"].stringValue
        if data[filterPath] != JSON.null {
            if let data  = data[filterPath].array {
                self.filterValuesData = data.map { FiltervaluesData( data: $0) }
            } else {
                self.filterValuesData.append( FiltervaluesData( data: data[filterPath] ))
            }
        }
    }
}

struct FiltervaluesData {
    var id_value: String!
    var name: String!
    var link: String!
    var name_rewrite: String!
    var priceValues: String!
    init(data: JSON) {
        self.id_value = data["id_value"].stringValue
        self.name = data["name"].stringValue
        self.link = data["link"].stringValue
        self.name_rewrite = data["name_rewrite"].stringValue
         self.priceValues = data.stringValue
    }

}

struct SortData {
    var display_name: String!
    var value: String!

    init(data: JSON) {
        self.display_name = data["display_name"].stringValue
        self.value = data["value"].stringValue
       
    }

}
