//
//  ProductCategoryDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import Hero
import FirebaseAnalytics

class ProductCategoryDataViewController: UIViewController {

    @IBOutlet weak var listLabel: UILabel!
    
    @IBOutlet weak var filter_lbl: UILabel!
    @IBOutlet weak var sort_lbl: UILabel!
    
    @IBOutlet weak var listICon: UIImageView!
    @IBOutlet weak var listGridView: UIView!
    var id: String!
    var name: String!
    var filterDict = [String: [String]]()
     var emptyView: EmptyView!
      var custId: String!
    var ViewType: Switching = .Grid
    @IBOutlet weak var actionView: UIView!
    @IBOutlet weak var actionViewHeight: NSLayoutConstraint!

    @IBOutlet weak var productCollectionView: UICollectionView!
    fileprivate let productCategoryViewModalObject = ProductCategoryViewModal()
    var controller: AllController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        filter_lbl.text = "filter".localized
        sort_lbl.text = "sort".localized
        listLabel.text = "list".localized
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
         self.navigationItem.title = self.name
        navigationController?.isHeroEnabled = true
        productCollectionView.register(ProductCategoryCollectionViewCell.nib, forCellWithReuseIdentifier: ProductCategoryCollectionViewCell.identifier)
          productCollectionView.register(ProductCategoryListCollectionViewCell.nib, forCellWithReuseIdentifier: ProductCategoryListCollectionViewCell.identifier)
        productCollectionView.delegate = productCategoryViewModalObject
        productCollectionView.dataSource = productCategoryViewModalObject
        
        productCategoryViewModalObject.collectionView = productCollectionView
        productCategoryViewModalObject.moveDelegate = self
       productCategoryViewModalObject.passDataDelegate = self
        productCategoryViewModalObject.ViewType = ViewType
        // Do any additional setup after loading the view.
          self.makeRequest(dict: [:], call: WhichApiCall.none, index: 0, page: 1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = "productEmpty".localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-search-result")
            emptyView?.button.isHidden = true
            emptyView.tag = 556
            emptyView?.isHidden = true
           self.view.insertSubview(emptyView!, at: 0)
        }
//        if responsedata == nil {
//            self.makeRequest(dict: [:], call: WhichApiCall.none, index: 0, page: 1)
//        } else {
//            self.productCategoryViewModalObject.getValue(jsonData: responsedata!) {
//                (data: Bool) in
//               self.loadProductCategoryData(jsonData: responsedata!, call: self.call)
//            }
//        }
    }

    func makeRequest( dict: [String: Any], call: WhichApiCall, index: Int, page: Int) {
        
        var params = ""
        var newDict = [String: Any]()
        
        var verbs: RequestType = .get
        if controller == AllController.home {
            
            params =  GlobalConstants.ApiNames.getAllProducts
            params += "&object=" + id!
            params += UrlParameters.width
            params += "&p=" + String(page)
        } else {
            switch call {
            case .applyFilter:
                params =  GlobalConstants.ApiNames.productCategory
                params += UrlParameters.width
                params += "&id_category=" + id!
                
                params += "&selected_filters="
                var filter = ""
                for (key, value) in filterDict {
                    filter.append("/")
                    filter.append(key)
                    if key == "price" {
                        for i in 0..<value.count {
                            filter.append("-")
                            filter.append(value[i])
                            
                        }
                    } else {
                        for i in 0..<value.count {
                            filter.append(value[i])
                        }
                    }
                    
                }
                params += filter
            case .wishlistClicked:
                params =  GlobalConstants.ApiNames.addRemoveWishlist
                newDict[GlobalConstants.ApiNames.addRemoveWishlist.FetchApiName()] = dict
                verbs = .post
                
            default:
                if controller == AllController.sellerCollection {
                    params =  GlobalConstants.MarketPlaceApis.getSellerCollection
                    params += UrlParameters.width
                    if custId != nil {
                        params += "&id_seller=" + custId
                    } else if let sellerId = UrlParameters.fetchSellerID() {
                        params += sellerId
                    }
                    params += "&page=" + String(page)
                } else {
                    
                    if controller == AllController.SearchController {
                        params = "mobikul/getsearchdetails?"
                        params += UrlParameters.width
                        params += "&key=" + name.replacingOccurrences(of: " ", with: "_")
                    } else  if controller == AllController.homeProductBlock {
                        
                        params =  GlobalConstants.ApiNames.getAllProducts
                        params += "&object=" + "block_product"
                        params += "&id_product_block=" + id
                        params += UrlParameters.width
                    } else {
                        params =  GlobalConstants.ApiNames.productCategory
                        params += UrlParameters.width
                        params += "&id_category=" + id
                        if let custId = UrlParameters.fetchCustomerId() {
                            params += custId
                        }
                    }
                }
                
                for (key, value) in sortDict {
                    params += key
                    params += value as? String ?? ""
                }
                params += "&selected_filters="
                
                var filter = ""
                for (key, value) in filterDict {
                    filter.append("/")
                    filter.append(key)
                    if key == "price" {
                        for i in 0..<value.count {
                            filter.append("-")
                            filter.append(value[i])
                            
                        }
                    } else {
                        for i in 0..<value.count {
                            filter.append(value[i])
                        }
                    }
                    
                }
                params += filter
                
                params += "&p=" + String(page)
                
            }
        }
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.productCategory, view: self.view) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                
                if let jsonData =  DBManager.sharedInstance.fetchData(key: AllController.productCategory.rawValue + (self.id ?? self.custId ?? "")) {
                    print("jsonData", jsonData)
                    self.productCategoryViewModalObject.products.removeAll()
                    self.loadProductCategoryData(jsonData: jsonData, call: WhichApiCall.none)
                } else {
                    self.setEmptyData(code: error._code)
                    self.emptyView.dict = dict
                    self.emptyView.call = call
                    print("loginError".localized, error)
                }
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    print(subView.tag)
                    if subView.tag == 555 || subView.tag == 556 {
                        subView.isHidden = true
                    }
                }
                switch call {
                case .wishlistClicked:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        if let  pId = dict["id_product"] as? String {
                            let indexPath = IndexPath(row: index, section: 0 )
                            WishlistContains.wishlistIds = WishlistContains.wishlistIds.removeDuplicates()
                            if  WishlistContains.wishlistIds.contains(pId) {
                                if let index = WishlistContains.wishlistIds.index(where: {$0 == pId}) {
                                    print(WishlistContains.wishlistIds)
                                    WishlistContains.wishlistIds.remove(at: index)
                                    if let cell  = self.productCollectionView.cellForItem(at: indexPath) as? ProductCategoryCollectionViewCell {
                                        cell.wishlistICOn.setImage(#imageLiteral(resourceName: "symbols-wishlist"), for: .normal)
                                    }
                                    if let cell  = self.productCollectionView.cellForItem(at: indexPath) as? ProductCategoryListCollectionViewCell {
                                        cell.wishlistICOn.setImage(#imageLiteral(resourceName: "symbols-wishlist"), for: .normal)
                                    }
                                }
                            } else {
                                if !WishlistContains.wishlistIds.contains(pId) {
                                    WishlistContains.wishlistIds.append(pId)
                                }
                                if let cell  = self.productCollectionView.cellForItem(at: indexPath) as? ProductCategoryCollectionViewCell {
                                    cell.wishlistICOn.setImage(#imageLiteral(resourceName: "wishlist-active"), for: .normal)
                                }
                                if let cell  = self.productCollectionView.cellForItem(at: indexPath) as? ProductCategoryListCollectionViewCell {
                                    cell.wishlistICOn.setImage(#imageLiteral(resourceName: "wishlist-active"), for: .normal)
                                }
                            }
                        }
                    }
                default:
                    self.productCategoryViewModalObject.isLoad = true
                    if let data =  responseObject {
                        let item = Item()
                        item.ID = AllController.productCategory.rawValue + (self.id ?? self.custId ?? "")
                        item.textString = data.rawString()!
                        DBManager.sharedInstance.addData(object: item)
                        self.loadProductCategoryData(jsonData: data, call: call )
                        if call == WhichApiCall.applyFilter {
                            self.actionView.isHidden = false
                        }
                    }
                }
            }
        }
    }

    func loadProductCategoryData(jsonData: JSON, call: WhichApiCall) {
        if call == WhichApiCall.applySort ||  call == WhichApiCall.applyFilter {
            self.productCategoryViewModalObject.products.removeAll()
        }
        self.productCategoryViewModalObject.getValue(jsonData: jsonData) {
            (data: Bool) in
            if data {
                print(self.productCategoryViewModalObject.products.count)
                self.responsedata = jsonData
                self.productCollectionView.reloadData()
                self.emptyView?.isHidden = true
                 self.productCollectionView.isHidden = false
                self.emptyView?.button.isHidden = true
                 self.listGridView.isUserInteractionEnabled = true
            } else {
                if  self.controller == AllController.SearchController {
                    emptyView?.labelText.text = "searchEmpty".localized
                    emptyView?.image.image = #imageLiteral(resourceName: "empty-state-search-result")
                }
                self.emptyView?.isHidden = false
                print(call)
                if call != WhichApiCall.applyFilter {
                    self.actionView.isHidden = true
                }
                self.listGridView.isUserInteractionEnabled = false
                self.responsedata = JSON.null
                  self.productCollectionView.isHidden = true
                self.emptyView?.button.isHidden = true
                self.productCollectionView.reloadData()
            }
        }
    }
    
   var responsedata: JSON?
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    var sortDict = [String: Any]()

    @IBAction func sortClicked(_ sender: Any) {
        if productCategoryViewModalObject.sortData.count > 0 {
            let alertController = UIAlertController(title: "chooseOption".localized, message: nil, preferredStyle: .actionSheet)
            for action in productCategoryViewModalObject.sortData {
                alertController.addActions(actions: [
                    UIAlertAction(title: action.display_name, style: .default, handler: { (action) -> Void in
                        if let alertIndex = alertController.actions.index(of: action) {
                            print("actionIndex: \(alertIndex)")
                            //params += "&orderby=" + sortByArray[rowValue]
                            //params += "&orderway=" + sortWayArray[rowValue]
                            self.productCategoryViewModalObject.page = 1
                            let dict = ["&": self.productCategoryViewModalObject.sortData[alertIndex].value] as [String: Any]
                            self.sortDict = dict as Any as? [String: Any] ?? [:]
                            self.makeRequest(dict: [:], call: WhichApiCall.applySort, index: 0, page: 0)
                        }
                    })
                    ], preferred: "cancel".localized)
            }
            alertController.addActions(actions: [
                UIAlertAction(title: "cancel".localized, style: .cancel, handler: nil)
                ], preferred: "cancel".localized)
            alertController.popoverPresentationController?.sourceView = self.view
            alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
            present(alertController, animated: true, completion: nil)
        } else {
            ErrorChecking.warningView(message: "noSortAvailable".localized)
        }
    }

    @IBAction func ListTap(_ sender: UITapGestureRecognizer) {
        switch ViewType {
        case .Grid:
            listICon.image = #imageLiteral(resourceName: "grid")
            listLabel.text = "grid".localized
            ViewType = .List
            productCategoryViewModalObject.ViewType = ViewType
            productCollectionView.reloadData()
        case .List:
            listICon.image = #imageLiteral(resourceName: "list")
            listLabel.text = "list".localized
            ViewType = .Grid
            productCategoryViewModalObject.ViewType = ViewType
            productCollectionView.reloadData()
        }
    }
    
    @IBAction func filterTapped(_ sender: Any) {
        if productCategoryViewModalObject.filterData.count > 0 {
            if let view = UIStoryboard.loadFilterViewController() {
                view.filterData = productCategoryViewModalObject.filterData
                view.FilterDataExchangeDelegate = self
                view.filterDict = filterDict
                let nav = UINavigationController(rootViewController: view)
                self.present(nav, animated: true, completion: nil)
            }
        } else {
            ErrorChecking.warningView(message: "nofilterAvailable".localized)
        }
    }
}

extension ProductCategoryDataViewController: HeroViewControllerDelegate {
    func heroWillStartAnimatingTo(viewController: UIViewController) {
            productCollectionView!.hero.modifiers = [.cascade]
    }

    func heroWillStartAnimatingFrom(viewController: UIViewController) {
            productCollectionView!.hero.modifiers = [.cascade, .delay(0.2)]
    }
}

extension ProductCategoryDataViewController: FilterDataExchange {
    func filterDataExchange(dict: [String: [String]], call: WhichApiCall) {
        if call == WhichApiCall.applyFilter {
             filterDict = dict
            self.makeRequest(dict: filterDict, call: WhichApiCall.applyFilter, index: 0, page: 0)
        } else {
            filterDict = dict
        }
        print(filterDict)
    }
}

extension ProductCategoryDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        if let view = UIStoryboard.loadProductViewController() {
            view.productId = id
            view.productName = name
            view.productDict = dict
            view.selectedIndex = index
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
}

extension ProductCategoryDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        switch call {
        case .pagination:
            self.makeRequest(dict: [:], call: WhichApiCall.none, index: 0, page: index)
        default:
            if FetchDetails.isLoggedIn() {
                Analytics.logEvent(AnalyticsEventAddToWishlist, parameters: [AnalyticsParameterItemID: id as NSObject, AnalyticsParameterItemName: name as NSObject])
                var dict = [String: Any]()
                dict["id_product_attribute"] = name
                dict ["id_customer"] =  UserDefaults.fetch(key: UserDefaults.Keys.id_customer) ?? ""
                dict["id_product"] = id
                self.makeRequest(dict: dict, call: call, index: index, page: 0)
            } else {
                if let view = UIStoryboard.loadSignInViewController() {
                    let nav = UINavigationController(rootViewController: view)
                    self.present(nav, animated: true, completion: nil)
                }
            }
        }
    }
}

extension ProductCategoryDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
    }
    
    func buttonPressed() {
        self.makeRequest(dict: emptyView.dict, call: emptyView.call, index: 0, page: 0)
    }
}

extension Array where Element: Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        return result
    }
}
