//
//  ProductCategoryCollectionViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class ProductCategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var wishlistICOn: UIButton!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productOldPrice: UILabel!

    var delgate: PassData?
    var wishlistId: String?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        productView.normalBorder()
        productOldPrice.SetDefaultTextColor()
        productOldPrice.isHidden = true
        price.textColor = GlobalConstants.Colors.priceColor
        if AddonsEnabled.wishlistEnable {
            wishlistICOn.isHidden = false
        } else {
            wishlistICOn.isHidden = true
        }

    }
    @IBAction func WishlistPress(_ sender: UIButton) {

        delgate?.passData(id: (item?.id_product)!, name: (item?.id_product_attribute)!, dict: [:], jsonData: JSON.null, index: sender.tag, call: WhichApiCall.wishlistClicked)

    }

    var item: ProductData! {
        didSet {

            if item?.added_in_wishlist == "1" {
                if  !WishlistContains.wishlistIds.contains(item.id_product) {
                        WishlistContains.wishlistIds.append(item.id_product)
                }
                wishlistICOn.setImage(#imageLiteral(resourceName: "wishlist-active"), for: .normal)
            } else {
                wishlistICOn.setImage(#imageLiteral(resourceName: "symbols-wishlist"), for: .normal)
            }
            
            if WishlistContains.wishlistIds.contains(item.id_product) {
                 wishlistICOn.setImage(#imageLiteral(resourceName: "wishlist-active"), for: .normal)
            }

            if item.show_price {
                self.price.isHidden = false
                self.productOldPrice.isHidden = false
            } else {
                self.price.isHidden = true
                self.productOldPrice.isHidden = true
            }
            
            productName.text = item?.name
            price.text = item?.price
            productOldPrice.text = item?.old_price
            productOldPrice.strike(text: item?.old_price ?? "")
            
            let val = (item?.image_link!)!
            productImage.setImage(imageUrl: val)
        }
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
