//
//  StoreProductTableViewCell.swift
//  PrestashopHyperlocalMVVM
//
//  Created by temporary on 16/09/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import Kingfisher
import HCSStarRatingView

class StoreProductTableViewCell: UITableViewCell {

    @IBOutlet weak var starView: HCSStarRatingView!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

       productView.normalBorder()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: ProductData? {
        didSet {

            guard let item = item else {
                return
            }

            productName.text  = item.name
            productPrice.text = item.price
            productImage.setImage(imageUrl: item.image_link ?? "")
        }
    }
}
