//
//  MyVoucherViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class MyVoucherViewModal: NSObject {
    
    var moveControllerDelegate: MoveController?
    var controller: AllController!
    var passDataDelege: PassData?
    var voucherArray = [VoucherData]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = MyVoucherModal(data: jsonData) else {
            return
        }
        voucherArray.removeAll()
        if !data.voucherArray.isEmpty {
            voucherArray = data.voucherArray
            completion(true)
        } else {
            completion(false)
        }
        
    }
}

extension MyVoucherViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: VoucherDataTableViewCell.identifier) as? VoucherDataTableViewCell {
            cell.item = voucherArray[indexPath.row]
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return voucherArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if controller == AllController.cartController {
            passDataDelege?.passData(id: voucherArray[indexPath.row].code, name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.applyVoucher)
        } else {
            let pasteboard = UIPasteboard.general
            pasteboard.string = voucherArray[indexPath.row].code
            ErrorChecking.successView(message: "voucherCodeApplied".localized)
        }
    }
    
}
