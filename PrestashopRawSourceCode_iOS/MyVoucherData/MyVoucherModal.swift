//
//  MyVoucherModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

struct MyVoucherModal {

    var voucherArray = [VoucherData]()

    private let VoucherPath =  ["response", "myVouchers", "voucher"]

    init?(data: JSON) {

        if data[VoucherPath] != JSON.null {
            if let data  = data[VoucherPath].array {
                self.voucherArray = data.map { VoucherData( data: $0) }
            } else {
                self.voucherArray.append(VoucherData(data: data[VoucherPath]))
            }
        }
        
    }

}

struct VoucherData {
        var date_to: String!
        var code: String!
        var value: String!
        var cumulable: String!
        var minimal: String!
        var quantity_for_user: String!
        var id_cart_rule: String!

        init(data: JSON) {
           
            self.date_to = data["date_to"].stringValue
            self.code = data["code"].string ?? data["name"].string  ?? data["voucher_name"].stringValue
            self.value = data["value"].string ?? data["value_real"].string ?? data["total_price"].string ?? data["voucher_value"].stringValue
            self.cumulable = data["cumulable"].stringValue
            self.minimal = data["minimal"].stringValue
            self.quantity_for_user = data["quantity_for_user"].string ?? data["quantity"].stringValue
            self.id_cart_rule = data["id_cart_rule"].stringValue
        }

}
