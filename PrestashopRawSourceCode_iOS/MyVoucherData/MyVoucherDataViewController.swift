//
//  MyVoucherDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON

class MyVoucherDataViewController: UIViewController {

    @IBOutlet weak var applyVoucherBtn: UIButton!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var voucherTableView: UITableView!
    var emptyView: EmptyView!

    @IBOutlet weak var voucherField: UITextField!
    @IBOutlet weak var voucherView: UIView!
    @IBOutlet weak var voucherViewHeight: NSLayoutConstraint!
    @IBAction func ApplyVoucherClicked(_ sender: Any) {
        if !ErrorChecking.isEmptyString(text: voucherField.text) {
             self.makeRequest(code: voucherField.text!, call: .applyVoucher)
        } else {
            ErrorChecking.warningView(message: "enterYourVoucher".localized)
        }

    }
    var title1: String!
    var controller: AllController!

    fileprivate let MyVoucherViewModalObject = MyVoucherViewModal()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = title1
//        VoucherDataTableViewCell
        applyVoucherBtn.setTitle("applyVoucher".localized, for: .normal)
        codeLabel.text = "code".localized
        voucherField.placeholder = "enterVoucher".localized
        voucherTableView.register(VoucherDataTableViewCell.nib, forCellReuseIdentifier: VoucherDataTableViewCell.identifier)
        voucherTableView.dataSource = MyVoucherViewModalObject
        voucherTableView.delegate = MyVoucherViewModalObject
        voucherTableView.tableFooterView = UIView()
        MyVoucherViewModalObject.controller = controller
        MyVoucherViewModalObject.passDataDelege = self
        // Do any additional setup after loading the view.

    }

    func makeRequest(code: String, call: WhichApiCall) {
        var params = ""
        var verbs: RequestType = .get
        var newDict = [String: Any]()
        if call == .applyVoucher {
            params = GlobalConstants.ApiNames.applyVoucher
            verbs = .post
            var dict = [String: Any]()
            dict["id_cart"] = UserDefaults.fetch(key: UserDefaults.Keys.id_cart)
            dict["code"] = code
            dict["operation"] = "add"
            if FetchDetails.isLoggedIn() {
                dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            } else {
                dict["id_guest"] = UserDefaults.fetch(key: UserDefaults.Keys.id_guest)
            }
            newDict[GlobalConstants.ApiNames.applyVoucher.FetchApiName()] = dict
            
        } else {
            params = GlobalConstants.ApiNames.getVouchers
            if let id_customer = UrlParameters.fetchCustomerId() {
                params += id_customer
            } else {
                params += "&id_customer=0"
            }
            params += UrlParameters.width
        }
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                //                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                //                self.emptyView?.params = code
                //                self.emptyView.call = call
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                if call == .applyVoucher {
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    if self.controller == AllController.cartController {
                        self.voucherView.isHidden = false
                        self.voucherViewHeight.constant = 128
                        
                    } else {
                        self.voucherView.isHidden = true
                        self.voucherViewHeight.constant = 0
                    }
                    
                    self.MyVoucherViewModalObject.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                        if data {
                            
                            self.voucherTableView.reloadData()
                            self.voucherTableView.isHidden = false
                            self.emptyView?.isHidden = true
                        } else {
                            if self.controller == AllController.cartController {
                                self.emptyView?.isHidden = true
                            } else {
                                self.emptyView?.isHidden = false
                            }
                            self.voucherTableView.isHidden = true
                            
                        }
                    }
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        if controller == AllController.cartController {
            voucherView.isHidden = false
            voucherViewHeight.constant = 128

        } else {
            voucherView.isHidden = true
            voucherViewHeight.constant = 0
        }
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = "voucherEmpty".localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-Voucher")
            emptyView?.button.isHidden = true
            emptyView?.isHidden = true
            self.view.insertSubview(emptyView!, at: 0)
        }
          self.makeRequest(code: "", call: WhichApiCall.none)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyVoucherDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        self.makeRequest(code: id, call: call)
    }
}

extension MyVoucherDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest(code: emptyView!.params, call: emptyView.call)
    }
}
