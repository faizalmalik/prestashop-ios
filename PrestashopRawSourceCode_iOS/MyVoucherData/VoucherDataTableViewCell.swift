//
//  VoucherDataTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class VoucherDataTableViewCell: UITableViewCell {

    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var cummulativeLabel: UILabel!
    @IBOutlet weak var minmumLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: VoucherData! {
        didSet {
            codeLabel.text = String.init(format: "%@: %@", "code".localized, item.code)
             codeLabel.halfTextColorChange(fullText: codeLabel.text!, changeText: "code".localized)
             qtyLabel.text = String.init(format: "%@: %@", "qty".localized, item.quantity_for_user)
             qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: "qty".localized)
             valueLabel.text = String.init(format: "%@: %@", "value".localized, item.value)
             valueLabel.halfTextColorChange(fullText: valueLabel.text!, changeText: "value".localized)
             minmumLabel.text = String.init(format: "%@: %@", "minimum".localized, item.minimal)
             minmumLabel.halfTextColorChange(fullText: minmumLabel.text!, changeText: "minimum".localized)
             cummulativeLabel.text = String.init(format: "%@: %@", "cumulative".localized, item.cumulable)
             cummulativeLabel.halfTextColorChange(fullText: cummulativeLabel.text!, changeText: "cumulative".localized)
             expiryLabel.text = String.init(format: "%@: %@", "expiryDate".localized, item.date_to)
             expiryLabel.halfTextColorChange(fullText: expiryLabel.text!, changeText: "expiryDate".localized)
        }
    }

}
