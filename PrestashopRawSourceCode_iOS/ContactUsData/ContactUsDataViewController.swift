//
//  ContactUsDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0
import SVProgressHUD
import Alamofire

class ContactUsDataViewController: UIViewController {
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var subjectView: UIView!
    @IBOutlet weak var subjectSelectLabel: UILabel!

    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var orderField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var orderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var productInternalView: UIView!
    @IBOutlet weak var productselectLabel: UILabel!
    @IBOutlet weak var productViewHeight: NSLayoutConstraint!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var orderSelectLabel: UILabel!
    @IBOutlet weak var orderView: UIView!

    @IBOutlet weak var subjectLabel: UILabel!
    
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var attachMentLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    var file_upload_url: String!
    
    var gdprTrue = false
    @IBOutlet weak var contactGdprView: GdprView!
    
    @IBOutlet weak var gdprHeight: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = "contactUs".localized
        subjectView.textBorder()
        orderView.textBorder()
        productInternalView.textBorder()
        messageTextView.textBorder()
        self.makeRequest(call: WhichApiCall.none, dict: [:])

        emailField.text = UserDefaults.standard.value(forKey: UserDefaults.Keys.email) as? String
        self.stringsLocalized()
        // Do any additional setup after loading the view.
    }
    func stringsLocalized() {
       
        subjectLabel.text = "Subject".localized
        orderLabel.text = "order".localized
        emailLabel.text = "emailAddress".localized
        productLabel.text = "products".localized
        messageLabel.text = "message".localized
        attachMentLabel.text = "attachmentLabel".localized
        sendBtn.setTitle("send".localized, for: .normal)
        editBtn.setTitle("edit".localized, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.black
    }

    override func viewWillDisappear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.white
    }

    var contactArray = [ContactData]()
    var contactOrderArray = [ContactOrderData]()

    func makeRequest( call: WhichApiCall, dict: [String: Any]) {
        var params =  ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get
        
        switch call {
        case .submitContactUs:
            params =  GlobalConstants.ApiNames.submitContactUs
            newDict[GlobalConstants.ApiNames.submitContactUs.FetchApiName()] = dict
            verbs = .post
        default:
            params =  GlobalConstants.ApiNames.getContactUs
            if let custId =  UrlParameters.fetchCustomerId() {
                params += custId
            }
            params += UrlParameters.width
        }
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.contactUsController, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                
                print("loginError".localized, error!)
                
            } else {
                
                switch call {
                case .submitContactUs:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.navigationController?.popViewController(animated: true)
                    }
                default:
                    guard let data = ContactUsModal(data: responseObject!) else {
                        return
                    }
                    // MARK: - gdpr condition
                    
                    if responseObject!["consent_box"].stringValue == "" {
                        self.contactGdprView.isHidden = true
                        self.gdprHeight.constant = 0
                        self.gdprTrue = false
                    } else {
                        self.contactGdprView.isHidden = false
                        self.gdprHeight.constant = 70
                        self.gdprTrue = true
                        self.contactGdprView.gdprDataLbl.textColor = UIColor.black
                        self.contactGdprView.gdprDataLbl.text = responseObject!["consent_box"].stringValue
                    }
                    
                    if !data.contactArray.isEmpty {
                        self.contactArray = data.contactArray
                        self.subjectSelectLabel.text = self.contactArray[0].name
                        self.subjectID = self.contactArray[0].id_contact
                    }
                    
                    self.file_upload_url = data.file_upload_url
                    
                    if !data.contactOrderArray.isEmpty {
                        self.contactOrderArray = data.contactOrderArray
                        self.orderField.isHidden = true
                        self.orderSelectLabel.text = self.contactOrderArray[0].oreder_reference
                        self.orderId = self.contactOrderArray[0].id_order
                        self.productselectLabel.text = self.contactOrderArray[self.orderSelectPath].contactProductArray[0].label
                        self.productId = self.contactOrderArray[self.orderSelectPath].contactProductArray[0].value
                        
                    } else {
                        self.orderView.isHidden = true
                        self.orderViewHeight.constant = 0
                        self.productView.isHidden = true
                        self.productViewHeight.constant = 0
                        self.orderField.isHidden = false
                    }
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    var subjectID: String!
    var orderId: String!
    var productId: String!
    var orderSelectPath = 0
    var subjectSelectPath = 0
    var productSelectPath = 0
    var message = ""

    @IBAction func productTap(_ sender: Any) {
        if contactOrderArray.count > 0 {
            let picker = ActionSheetStringPicker(title: "products".localized, rows: contactOrderArray[orderSelectPath].contactProductArray.map {$0.label}, initialSelection: productSelectPath, doneBlock: { _, indexes, _ in
                self.productselectLabel.text = self.contactOrderArray[self.orderSelectPath].contactProductArray[indexes].label
                self.productId = self.contactOrderArray[self.orderSelectPath].contactProductArray[indexes].value
               self.productSelectPath = indexes
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: orderView)
            picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
            picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
            picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
            picker?.show()
        } else {
            ErrorChecking.warningView(message: "noproducts".localized)
        }
    }

    @IBAction func subjectTap(_ sender: UITapGestureRecognizer) {
         if contactArray.count > 0 {
            let picker = ActionSheetStringPicker(title: "subjects".localized, rows: contactArray.map {$0.name }, initialSelection: subjectSelectPath, doneBlock: { _, indexes, _ in
                self.subjectSelectLabel.text = self.contactArray[indexes].name
                self.subjectID = self.contactArray[indexes].id_contact
                self.subjectSelectPath = indexes
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: subjectView)
            picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
            picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
            picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
            picker?.show()
        } else {
            ErrorChecking.warningView(message: "nosubjects".localized)
        }
    }

    @IBAction func orderTap(_ sender: Any) {
         if contactOrderArray.count > 0 {
            let picker = ActionSheetStringPicker(title: "orders".localized, rows: contactOrderArray.map {$0.oreder_reference }, initialSelection: orderSelectPath, doneBlock: { _, indexes, _ in
                self.orderSelectLabel.text = self.contactOrderArray[indexes].oreder_reference
                self.orderId = self.contactOrderArray[indexes].id_order
                self.orderSelectPath = indexes
                self.productselectLabel.text = self.contactOrderArray[self.orderSelectPath].contactProductArray[0].label
                self.productId = self.contactOrderArray[self.orderSelectPath].contactProductArray[0].value
                  self.productSelectPath = 0
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: orderView)
            picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
            picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
            picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
            picker?.show()
        } else {
            ErrorChecking.warningView(message: "noorders".localized)
        }
    }

    @IBAction func saveClicked(_ sender: UIButton) {
        message = self.messageTextView.text ?? ""
        let someOptional: Bool = true
        var contactDict = [String: Any]()

        switch someOptional {
        case ErrorChecking.isEmptyString(text: emailField.text):
            ErrorChecking.warningView(message: "emailEmptyValidate".localized)
        case ErrorChecking.isValidEmail(testStr: emailField.text!):
            ErrorChecking.warningView(message: "emailValidate".localized)
        case ErrorChecking.isEmptyString(text: message):
            ErrorChecking.warningView(message: "messageRequireField".localized)
        default:
            if contactOrderArray.count == 0  &&  ErrorChecking.isEmptyString(text: orderField.text) {
                ErrorChecking.warningView(message: "enterOrderDetails".localized)
            } else {
                if contactOrderArray.count == 0 {
                    contactDict["id_order"] = orderField.text
                } else {
                     contactDict["id_order"] = orderId
                     contactDict["id_product"] = productId
                }
                contactDict["id_subject"] = subjectID
                contactDict["email"] = emailField.text
                contactDict["message"] = message
                if  self.imageData != nil {
                    self.upload(data: self.imageData, dict: contactDict)
                } else {
                    if self.gdprTrue == true {
                        if contactGdprView.gdprSwitch.isOn {
                            
                            self.makeRequest(call: WhichApiCall.submitContactUs, dict: contactDict)
                            
                        } else {
                            ErrorChecking.warningView(message: "Please check the terms and condition".localized)
                        }
                    } else {
                        self.makeRequest(call: WhichApiCall.submitContactUs, dict: contactDict)
                        
                    }
                }
            }
        }

    }

    @IBAction func EditClicked(_ sender: UIButton) {

        let alert = UIAlertController(title: "upload".localized, message: nil, preferredStyle: .actionSheet)

        let takeAction = UIAlertAction(title: "takeAPhoto".localized, style: .default, handler: take)
        let upload = UIAlertAction(title: "uploadFromLibrary".localized, style: .default, handler: uploadImage)
        let CancelAction = UIAlertAction(title: "cancel".localized, style: .cancel, handler: cancel)

        alert.addAction(takeAction)
        alert.addAction(upload)
        alert.addAction(CancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)

        self.present(alert, animated: true, completion: nil)
    }

    func take(alertAction: UIAlertAction!) {

        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.allowsEditing = false
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    func uploadImage(alertAction: UIAlertAction!) {

        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = false
        present(picker, animated: true, completion: nil)

    }
    func cancel(alertAction: UIAlertAction!) {

    }

    var imageData: Data!

}

extension ContactUsDataViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            let imageData = image.jpegData(compressionQuality: 0.8)
            self.imageData = imageData
            self.contactImage.image = image

        } else {
            print("Something went wrong")
        }

        self.dismiss(animated: true, completion: nil)
    }

    func upload( data: Data, dict: [String: Any]) {

        // You can change your image name here, i use NSURL image and convert into string
        SVProgressHUD.show(withStatus: "loading".localized)
        var fileUrl = file_upload_url
        fileUrl = fileUrl?.replace(string: "%2F", replacement: "/")
        fileUrl = fileUrl?.replace(string: "%3A", replacement: ":")
        fileUrl?.append("?outputformat=json")

        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append((self.orderId ?? "").data(using: String.Encoding.utf8)!, withName: "orderId")
            multipartFormData.append((self.message).data(using: String.Encoding.utf8)!, withName: "message")
            //multipartFormData.append(data, withName: "upload_logo", fileName: "filename=imageName.jpg", mimeType: "image/jpeg")
            multipartFormData.append(data, withName: "contactFile", fileName: "filename=imageName.jpg", mimeType: "image/jpeg")

        }, usingThreshold: UInt64.init(), to: fileUrl!, method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    SVProgressHUD.dismiss()
                        print(String(data: response.data!, encoding: String.Encoding.utf8)!)
                    if let jsonData = try? JSON(data: response.data!) {
                        print(jsonData)
                        if jsonData != JSON.null {
                            var dictWithFileName = dict
                            dictWithFileName["file_name"] = jsonData["response"]["file_name"].stringValue
                            self.makeRequest(call: WhichApiCall.submitContactUs, dict: dictWithFileName)
                        } else {
                            self.makeRequest(call: WhichApiCall.submitContactUs, dict: dict)
                        }
                    } else {
                        self.makeRequest(call: WhichApiCall.submitContactUs, dict: dict)
                    }
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }

}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
