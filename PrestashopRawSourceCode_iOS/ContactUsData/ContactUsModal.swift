//
//  ContactUsModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

struct ContactUsModal {

    var contactArray = [ContactData]()
    var contactOrderArray = [ContactOrderData]()

    private let contactPath =  ["contacts", "contact"]
     private let contactOrderPath =  ["orders", "order"]
    var file_upload_url: String!

    init?(data: JSON) {

        if data[contactPath] != JSON.null {
            if let data  = data[contactPath].array {
                self.contactArray = data.map { ContactData( data: $0) }
            } else {
                self.contactArray.append(ContactData(data: data[contactPath]))
            }
        }

        if data[contactOrderPath] != JSON.null {
            if let data  = data[contactOrderPath].array {
                self.contactOrderArray = data.map { ContactOrderData( data: $0) }
            } else {
                self.contactOrderArray.append(ContactOrderData(data: data[contactOrderPath]))
            }
        }

        self.file_upload_url = data["file_upload_url"].stringValue

    }
}

struct ContactData {
    var id_contact: String!
    var name: String!
    var description: String!

    init(data: JSON) {
        self.id_contact = data["id_contact"].stringValue
        self.name = data["name"].stringValue
        self.description = data["description"].stringValue

    }

}

struct ContactOrderData {
    var id_order: String!
    var oreder_reference: String!

    var contactProductArray = [ContactProductData]()
    let contactProductPath =  ["products", "product"]
    init(data: JSON) {
        self.id_order = data["id_order"].stringValue
        self.oreder_reference = data["oreder_reference"].stringValue

        if data[contactProductPath] != JSON.null {
            if let data  = data[contactProductPath].array {
                self.contactProductArray = data.map { ContactProductData( data: $0) }
            } else {
                self.contactProductArray.append(ContactProductData(data: data[contactProductPath]))
            }
        }
    }
}

struct ContactProductData {
    var value: String!
    var label: String!

    init(data: JSON) {
        self.value = data["value"].stringValue
        self.label = data["label"].stringValue
    }
}
