//
//  CartModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

struct CartModal {

   private let cartPath = ["carts", "cart"]
   private let cartProductPath = ["carts", "cart", "products", "product"]
    var voucherEnabled = false

    var voucherArray = [VoucherData]()

    private let VoucherPath =  ["carts", "cart", "vouchers", "voucher_row"]

    var products = [ProductData]()
    var cartPrice: CartPriceData?

    init?(data: JSON) {

        if let value = data[cartPath][UserDefaults.Keys.id_cart].string, value != "0" {
            UserDefaults.set(value: value, key: UserDefaults.Keys.id_cart)
            
        }
        
        if let value = data[cartPath][UserDefaults.Keys.id_guest].string, value != "0" {
            UserDefaults.set(value: value, key: UserDefaults.Keys.id_guest)
        } else {
            UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.id_guest)
        }
        
        if data[cartProductPath] != JSON.null {
            if let data  = data[cartProductPath].array {
                self.products = data.map { ProductData( data: $0) }
            } else {
                self.products.append(ProductData(data: data[cartProductPath]))
            }
        }
        if data[VoucherPath] != JSON.null {
            if let data  = data[VoucherPath].array {
                self.voucherArray = data.map { VoucherData( data: $0) }
            } else {
                print(data[VoucherPath])
                self.voucherArray.append(VoucherData(data: data[VoucherPath]))
            }
        }
        print( self.voucherArray)
          self.cartPrice = CartPriceData(data: data[cartPath])

        if data["carts"]["cart"]["voucher_enabled"].stringValue == "1" {
            voucherEnabled = true
        } else {
            voucherEnabled = false
        }

    }
}

struct CartPriceData {
    var product_total: String!
    var shipping_cost: String!
    var total: String!
    var id_address_delivery: String!
    var id_address_invoice: String!
    var carrier_name: String!
    var id_carrier: String!
    var total_value_real_with_currency: String!
    var show_tax: String!
    var total_tax: String!
    var cart_total_order: String!
    
    init(data: JSON) {
        self.cart_total_order = data["cart_total_order"].stringValue
        self.product_total = data["product_total"].string ?? data["total_products_wt"].stringValue
        self.shipping_cost = data["shipping_cost"].string ?? data["total_shipping"].stringValue
        self.total = data["total"].string ?? data["total_paid"].stringValue
        self.id_address_invoice = data["id_address_invoice"].stringValue
        self.id_address_delivery = data["id_address_delivery"].stringValue
        self.carrier_name = data["carrier_name"].stringValue
        self.id_carrier = data["id_carrier"].stringValue
        self.show_tax = data["show_tax"].stringValue
        self.total_tax = data["total_tax"].stringValue
        self.total_value_real_with_currency = data["vouchers"]["voucher"]["total_value_real_with_currency"].stringValue
    }
}

struct CartCustomizationData {
    var id_customization: String!
    var picture = [String]()
    var text_label =  [String]()
    init(data: JSON) {
        id_customization = data["id_customization"].stringValue
        if let  array = data["picture"].array {
            picture = array.map({ $0.stringValue })
        } else {
            self.picture.append( data["picture"].stringValue)
        }
        
        if let  array = data["text_label"].array {
            text_label = array.map({ $0.stringValue })
        } else {
            self.text_label.append( data["text_label"].stringValue)
        }
    }
}
