//
//  CartCustomizationViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import Kingfisher

class CartCustomizationViewController: UIViewController {

    var customizationData = [CartCustomizationData]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.title = "Product Customization"
        print(customizationData)
        tableView.register(CustomizableFileTableViewCell.nib, forCellReuseIdentifier: CustomizableFileTableViewCell.identifier)
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CartCustomizationViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return customizationData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customizationData[section].picture.count +  customizationData[section].text_label.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  indexPath.row < customizationData[indexPath.section].picture.count {
            if let cell = tableView.dequeueReusableCell(withIdentifier: CustomizableFileTableViewCell.identifier, for: indexPath) as? CustomizableFileTableViewCell {
                cell.imageType.setImage(imageUrl: customizationData[indexPath.section].picture[indexPath.row])
                cell.titleLabel.text = nil
                cell.selectionStyle = .none
                cell.editBtn.isHidden = true
                return cell
            }
        } else {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                cell.textLabel?.textAlignment = .right
            } else {
                cell.textLabel?.textAlignment = .left
            }
            if  customizationData[indexPath.section].picture.count > 0 {
                cell.textLabel?.text = customizationData[indexPath.section].text_label[indexPath.row - (customizationData[indexPath.section].picture.count)]
            } else {
                cell.textLabel?.text = customizationData[indexPath.section].text_label[indexPath.row]
            }
            return cell
        }
        return UITableViewCell()
    }
}
