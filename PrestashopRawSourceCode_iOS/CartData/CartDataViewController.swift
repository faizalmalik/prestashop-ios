//
//  CartDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class CartDataViewController: UIViewController {
    
    @IBOutlet weak var chckoutBtn: UIButton!
    @IBOutlet weak var cartTableView: UITableView!
    fileprivate let cartViewModalObject = CarViewModal()
    var emptyView: EmptyView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        chckoutBtn.SetAccentBackColor()
        chckoutBtn.setTitle("proceedToCheckout".localized, for: .normal)
        self.navigationController?.navigationBar.navigationChanges()
        self.navigationItem.title = "cart".localized
        cartTableView.register(CartProductTableViewCell.nib, forCellReuseIdentifier: CartProductTableViewCell.identifier)
        cartTableView.register(CartPriceTableViewCell.nib, forCellReuseIdentifier: CartPriceTableViewCell.identifier)
        cartTableView.register(VouchersAppliedTableViewCell.nib, forCellReuseIdentifier: VouchersAppliedTableViewCell.identifier)
        
        cartTableView.separatorStyle = .none
        cartTableView.dataSource = cartViewModalObject
        cartTableView.delegate = cartViewModalObject
        cartViewModalObject.delegate = self
        cartViewModalObject.passDataDelgate = self
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.makeRequest(dict: [:], call: WhichApiCall.none)
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = "cartEmpty".localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-cart")
            emptyView?.tag = 556
            emptyView?.button.isHidden = true
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
    }
    
    override func viewWillLayoutSubviews() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func makeRequest(dict: [String: Any], call: WhichApiCall) {
        
        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get
        
        switch call {
        case .deleteCart:
            params += GlobalConstants.ApiNames.deleteCart
            newDict[GlobalConstants.ApiNames.deleteCart.FetchApiName()] = dict
            verbs = .post
            
        case .updateCart:
            params += GlobalConstants.ApiNames.addToCart
            newDict[GlobalConstants.ApiNames.addToCart.FetchApiName()] = dict
            verbs = .post
        case . deleteVoucher:
            params = GlobalConstants.ApiNames.applyVoucher
            verbs = .post
            newDict[GlobalConstants.ApiNames.applyVoucher.FetchApiName()] = dict
            
        default:
            params = GlobalConstants.ApiNames.cart
            if let id_customer = UrlParameters.fetchCustomerId() {
                params += id_customer
            } else {
                if let id_guest = UrlParameters.fetchGuestId() {
                    params += id_guest
                } else {
                    params += "&id_guest=0"
                }
            }
            //            params += UrlParameters.id_customer
            params += UrlParameters.width
            verbs = .get
        }
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                self.emptyView.dict = dict
                self.emptyView.call = call
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 || subView.tag == 556 {
                        subView.isHidden = true
                    }
                }
                
                switch call {
                case .deleteCart:
                    if ErrorChecking.getData(data: responseObject) != JSON.null {
                        
                        self.makeRequest(dict: [:], call: WhichApiCall.none)
                    }
                    
                case .updateCart:
                    if ErrorChecking.getData(data: responseObject) != JSON.null {
                        self.makeRequest(dict: [:], call: WhichApiCall.none)
                    }
                case . deleteVoucher:
                    if ErrorChecking.getData(data: responseObject) != JSON.null {
                        self.makeRequest(dict: [:], call: WhichApiCall.none)
                    }
                    
                default:
                    
                    self.cartViewModalObject.items.removeAll()
                    UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.id_cart)
                    //UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.id_guest)
                    UserDefaults.standard.synchronize()
                    self.cartViewModalObject.getValue(jsonData: responseObject! ) {
                        
                        (data: Bool) in
                        print(data)
                        //
                        if data {
                            
                            if responseObject!["carts"]["cart"]["nb_total_products"].string != nil {
                                UserDefaults.set(value: responseObject!["carts"]["cart"]["nb_total_products"].stringValue, key: UserDefaults.Keys.cartCount)
                            }
                            if  self.tabBarController != nil {
                                self.tabBarController?.setCartCount(value: FetchDetails.fetchCartCount())
                            }
                            
                            self.cartTableView.reloadData()
                            self.chckoutBtn.isHidden = false
                            self.emptyView?.isHidden = true
                        } else {
                            
                            UserDefaults.set(value: "0", key: UserDefaults.Keys.cartCount)
                            
                            if  self.tabBarController != nil {
                                self.tabBarController?.setCartCount(value: FetchDetails.fetchCartCount())
                            }
                            UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.id_cart)
                            UserDefaults.standard.removeObject(forKey: UserDefaults.Keys.id_guest)
                            UserDefaults.standard.synchronize()
                            self.chckoutBtn.isHidden = true
                            self.emptyView?.isHidden = false
                            self.emptyView?.labelText.text = "cartEmpty".localized
                            self.emptyView?.image.image = #imageLiteral(resourceName: "empty-state-cart")
                            self.emptyView?.button.isHidden = true
                            self.cartTableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func chckoutPressed(_ sender: UIButton) {
        
        if FetchDetails.isLoggedIn() {
            if let view = UIStoryboard.loadCheckoutViewController() {
                view.id_address_delivery = self.cartViewModalObject.cartPrice?.id_address_delivery
                view.id_address_invoice = self.cartViewModalObject.cartPrice?.id_address_invoice
                view.cart_total_order = self.cartViewModalObject.cartPrice?.cart_total_order
                let nav  = UINavigationController(rootViewController: view)
                self.present(nav, animated: true, completion: nil)
            }
        } else {
            self.showAlert()
        }
        
    }
    
    func showAlert() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takeAction = UIAlertAction(title: "checkoutnewcustomer".localized, style: .default, handler: new)
        let upload = UIAlertAction(title: "checkoutexistingcustomer".localized, style: .default, handler: existing)
        let guestAction = UIAlertAction(title: "checkoutguestcustomer".localized, style: .default, handler: guest)
        let CancelAction = UIAlertAction(title: "cancel".localized, style: .cancel, handler: cancel)
        
        alert.addAction(takeAction)
        alert.addAction(upload)
        if AddonsEnabled.guestEnable {
            alert.addAction(guestAction)
        }
        alert.addAction(CancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func new(alertAction: UIAlertAction!) {
        if let view = UIStoryboard.loadSignUpViewController() {
            view.isDismiss = true
            let nav  = UINavigationController(rootViewController: view)
            self.present(nav, animated: true, completion: nil)
        }
    }
    func existing(alertAction: UIAlertAction!) {
        if let view = UIStoryboard.loadSignInViewController() {
            view.isDismiss = true
            let nav  = UINavigationController(rootViewController: view)
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    func cancel(alertAction: UIAlertAction!) {
        
    }
    
    func guest(alertAction: UIAlertAction!) {
        if let view = UIStoryboard.loadGuestViewController() {
            let nav  = UINavigationController(rootViewController: view)
            self.present(nav, animated: true, completion: nil)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CartDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        self.makeRequest(dict: dict, call: call)
    }
}

extension CartDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        print(controller)
        switch controller {
        case .myVouchers:
            if let view = UIStoryboard.loadMyVoucherInfoViewController() {
                view.title1 = "vouchers".localized
                view.controller = AllController.cartController
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .productController:
            if let view = UIStoryboard.loadProductViewController() {
                view.productId = id
                view.productName = name
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .cartCustomizationViewController:
            if let view = UIStoryboard.loadCartCustomizationViewController() {
                if let data = dict["data"] as? [CartCustomizationData] {
                    view.customizationData = data
                }
                self.navigationController?.pushViewController(view, animated: true)
            }
        default:
            break
        }
        
    }
}

extension CartDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
    }
    
    func buttonPressed() {
        self.makeRequest(dict: emptyView.dict, call: emptyView.call)
    }
}
