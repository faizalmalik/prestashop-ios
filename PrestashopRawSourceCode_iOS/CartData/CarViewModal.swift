//
//  CarViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class CarViewModal: NSObject {

    var passDataDelgate: PassData?
    var delegate: MoveController?

         var cartPrice: CartPriceData?
      var items = [CartViewModalItem]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data =  CartModal(data: jsonData) else {
            return
        }
        items.removeAll()
        if !data.products.isEmpty {
            items.append(CartViewModalProductsItem(products: data.products, heading: "items".localized))

            if data.voucherEnabled {
                items.append(CartViewModalVoucherItem(voucher: "applyVoucher".localized, heading: ""))
            }
             if !data.voucherArray.isEmpty {
                  items.append(CartViewModalAppliedVoucherItem(vouchers: data.voucherArray, heading: "appliedVoucher".localized))
            }
            cartPrice = data.cartPrice!
            items.append(CartViewModalPriceItem(cartPrice: data.cartPrice!, heading: "orderSummary".localized))
            completion(true)
        } else {
            completion(false)
        }

    }
}

extension CarViewModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return items[section].rowCount
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        
        switch item.type {
        case .products:
            if let item = item as? CartViewModalProductsItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: CartProductTableViewCell.identifier) as? CartProductTableViewCell {
                    cell.item = item.products[indexPath.row]
                    cell.selectionStyle = .none
                    cell.moveDelegate = self
                    cell.delegate = self
                    return cell
                }
            }
        case .price :
            if let item = item as? CartViewModalPriceItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: CartPriceTableViewCell.identifier) as? CartPriceTableViewCell {
                    cell.item = item.cartPrice
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .voucher:
            if let item = item as? CartViewModalVoucherItem {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                    cell.textLabel?.textAlignment = .right
                } else {
                    cell.textLabel?.textAlignment = .left
                }
                cell.textLabel?.text = item.voucher
                cell.accessoryType = .disclosureIndicator
                return cell
            }
        case .appliedVoucher:
            if let item = item as? CartViewModalAppliedVoucherItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: VouchersAppliedTableViewCell.identifier) as? VouchersAppliedTableViewCell {
                    cell.item = item.vouchers[indexPath.row]
                    cell.passDataDelegate = self
                    cell.selectionStyle = .none
                    return cell
                }
            }
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.section]

        if  item.type == .products {
             if let item = item as? CartViewModalProductsItem {
                delegate?.moveController(id: item.products[indexPath.row].id_product!, name: item.products[indexPath.row].name!, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productController)

            }
        } else if item.type == .voucher {
              delegate?.moveController(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.myVouchers)

        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let item = items[section]

        switch item.type {
        case .products:
            if let item = item as? CartViewModalProductsItem {
                return item.heading
            }
        case .price :
            if let item = item as? CartViewModalPriceItem {
               return item.heading
            }

        case .voucher:
            if let item = item as? CartViewModalVoucherItem {
                return item.heading
            }
        case .appliedVoucher:
            if let item = item as? CartViewModalAppliedVoucherItem {
                return item.heading
            }
        }
         return nil
    }

}

enum CartType {
    case products
    case price
    case voucher
    case appliedVoucher
}

protocol CartViewModalItem {
    var type: CartType { get }
    var rowCount: Int { get }
}

class CartViewModalProductsItem: CartViewModalItem {
    var type: CartType {
        return .products
    }

    var rowCount: Int {
        return products.count
    }

    var products: [ProductData]
    var heading: String!

    init(products: [ProductData], heading: String ) {
        self.products = products
          self.heading = heading
    }
}

class CartViewModalPriceItem: CartViewModalItem {
    var type: CartType {
        return .price
    }

    var rowCount: Int {
        return 1
    }
     var heading: String!
    var cartPrice: CartPriceData?

    init(cartPrice: CartPriceData, heading: String ) {
        self.cartPrice = cartPrice
        self.heading = heading
    }
}

class CartViewModalVoucherItem: CartViewModalItem {
    var type: CartType {
        return .voucher
    }

    var rowCount: Int {
        return 1
    }
     var heading: String!
    var voucher: String!

    init(voucher: String, heading: String ) {
        self.voucher = voucher
        self.heading = heading
    }
}

class CartViewModalAppliedVoucherItem: CartViewModalItem {
    var type: CartType {
        return .appliedVoucher
    }

    var rowCount: Int {
        return vouchers.count
    }

    var vouchers: [VoucherData]
    var heading: String!

    init(vouchers: [VoucherData], heading: String ) {
        self.vouchers = vouchers
        self.heading = heading
    }
}

extension CarViewModal: PassData {

    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        passDataDelgate?.passData(id: id, name: name, dict: dict, jsonData: jsonData, index: index, call: call)
    }

}

extension CarViewModal: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        delegate?.moveController(id: id, name: name, dict: dict, jsonData: jsonData, index: index, controller: controller)
    }

}
