//
//  CartPriceTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
//

import UIKit
import SwiftyJSON

class CartPriceTableViewCell: UITableViewCell {

    @IBOutlet weak var subtotaltext: UILabel!
    @IBOutlet weak var subtotalprice: UILabel!
    @IBOutlet weak var taxtext: UILabel!
    @IBOutlet weak var taxprice: UILabel!
    @IBOutlet weak var totaltext: UILabel!
    @IBOutlet weak var totalprice: UILabel!
     @IBOutlet weak var emptyButton: UIButton!
    @IBOutlet weak var vouchertext: UILabel!
    @IBOutlet weak var voucherprice: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var taxPriceLabel: UILabel!
    
    var delegate: PassData?

    override func awakeFromNib() {
        super.awakeFromNib()
        subtotaltext.text = String.init(format: "%@:", "subTotal".localized)
        taxtext.text = String.init(format: "%@:", "shippingCost".localized)
        vouchertext.text = String.init(format: "%@:", "voucherDiscount".localized)
        totaltext.text = String.init(format: "%@:", "total".localized)

        vouchertext.SetDefaultTextColor()
       subtotaltext.SetDefaultTextColor()
        totaltext.SetDefaultTextColor()
         taxtext.SetDefaultTextColor()
        taxLabel.SetDefaultTextColor()
//        emptyButton.setTitleColor(GlobalConstants.Colors.accentColor, for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }
    @IBAction func emptyPressed(_ sender: Any) {
//        delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 1, call: WhichApiUse.cartEmpty)
    }

    var item: CartPriceData! {
        didSet {
           subtotalprice.text = item?.product_total
            taxprice.text = item?.shipping_cost
            totalprice.text = item?.total
            voucherprice.text = item?.total_value_real_with_currency
            if item.show_tax == "1" {
                taxLabel.text = String.init(format: "%@:", "Tax".localized)
                taxPriceLabel.text = item.total_tax
            } else {
                taxLabel.text = ""
                taxPriceLabel.text = ""
            }
        }
    }

}
