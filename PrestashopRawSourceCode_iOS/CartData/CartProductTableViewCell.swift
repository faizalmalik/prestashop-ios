//
//  CartProductTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
//

import UIKit
import SwiftyJSON

class CartProductTableViewCell: UITableViewCell {

    @IBOutlet weak var productCustomizationBtn: UIButton!
    @IBOutlet weak var attributesLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var updatebutton: UIButton!
    @IBOutlet weak var removeButton: UIButton!

    @IBOutlet weak var stepperView: UIStepper!

    var oldValue = Double()

    var id_product: String?
    var quantityValue: String?
    var id_product_attribute: String?
    var delegate: PassData?
    var moveDelegate: MoveController?
    var oldQuantityValue: String?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cartView.layer.masksToBounds =  false
        cartView.backgroundColor = UIColor.white
        stepperView.minimumValue = 1.0
        stepperView.tintColor = GlobalConstants.Colors.accentColor
        updatebutton.setTitle("update".localized, for: .normal)
        removeButton.setTitle("remove".localized, for: .normal)
        cartView.normalBorder()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: ProductData! {
        didSet {
            
//            if item.show_price {
//                self.priceLabel.isHidden = false
//                self.subTotal.isHidden = false
//            } else {
//                self.priceLabel.isHidden = true
//                 self.subTotal.isHidden = true
//            }
            if item.customizationData.count > 0 {
                productCustomizationBtn.isHidden = false
            } else {
                productCustomizationBtn.isHidden = true
            }

            productImage.setImage(imageUrl: (item?.image_link!)!)
            productNameLabel.text = item?.name
            priceLabel.text = String.init(format: "%@: %@", "price".localized, (item?.price)!)
            qtyLabel.text = String.init(format: "%@: %@", "qty".localized, (item?.quantity)!)
             subTotal.text = String.init(format: "%@: %@", "subTotal".localized, (item?.total)!)
              priceLabel.halfTextColorChange(fullText: priceLabel.text!, changeText: "price".localized)
              qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: "qty".localized)
              subTotal.halfTextColorChange(fullText: subTotal.text!, changeText: "subTotal".localized)
            stepperView.value = Double(item?.quantity ?? "0")!
            attributesLabel.text = item?.attributes
              oldQuantityValue = item?.quantity
            self.id_product_attribute = item?.id_product_attribute
            self.id_product = item?.id_product

        }
    }

    @IBAction func StepperAction(_ sender: UIStepper) {

        self.qtyLabel.text = String.init(format: "%@: %.0f", "qty".localized, sender.value )
        qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: "qty".localized)

//        if sender.value > oldValue {
//            delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call: WhichApiCall.updateCartPlus)
//        }
//        else {
//            delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: sender.tag, call:  WhichApiCall.updateCartMinus)
//        }
    }
    @IBAction func productBtnClicked(_ sender: UIButton) {
        var dict = [String: Any]()
        dict["data"] = item.customizationData
        moveDelegate?.moveController(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, controller: AllController.cartCustomizationViewController)
        
    }
    
    @IBAction func plusPressed(_ sender: UIButton) {

    }

    @IBAction func minusPressed(_ sender: UIButton) {

    }

    @IBAction func updatePressed(_ sender: UIButton) {

            if self.id_product != nil && self.id_product_attribute != nil {
                var dict = [String: Any]()
                dict["id_product"] = self.id_product
                dict["id_product_attribute"] = self.id_product_attribute
                dict["id_address_delivery"] = "0"
                dict["id_customization"] = ""
                dict["id_cart"] = UserDefaults.fetch(key: UserDefaults.Keys.id_cart)
                dict["id_guest"] = ""
                if Int(oldQuantityValue ?? "0") ?? 0 > Int(self.stepperView.value) {
                    dict["operator"] = "down"
                    dict["quantity"] = String(format: "%i", Int(oldQuantityValue ?? "0")! - Int(self.stepperView.value))
                } else {
                    dict["operator"] = "up"
                    dict["quantity"] = String(format: "%i", (  Int(self.stepperView.value) - Int(oldQuantityValue ?? "0")!) )
                }
                if dict["quantity"] as? String == "0" {
                      ErrorChecking.warningView(message: "appendProductQuantity".localized)
                } else {
                    delegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiCall.updateCart)
                }
            }

    }

    @IBAction func removePressed(_ sender: UIButton) {
        if self.id_product != nil && self.id_product_attribute != nil {
            var dict = [String: Any]()
            dict["id_product"] = self.id_product
            dict["id_product_attribute"] = self.id_product_attribute
            dict["id_address_delivery"] = "0"
            dict["id_customization"] = item.customizationData.count > 0 ? item.customizationData[0].id_customization : ""
            dict["id_cart"] = UserDefaults.fetch(key: UserDefaults.Keys.id_cart)

            delegate?.passData(id: "", name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiCall.deleteCart)
        }
    }

}
