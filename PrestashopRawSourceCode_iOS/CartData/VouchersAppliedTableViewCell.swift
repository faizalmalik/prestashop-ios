//
//  VouchersAppliedTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class VouchersAppliedTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var codeLabel: UILabel!
     @IBOutlet weak var qtyLabel: UILabel!
     @IBOutlet weak var priceLabel: UILabel!
    var passDataDelegate: PassData?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: VoucherData! {
        didSet {
           print("hello", item)
            codeLabel.text = item.code

            priceLabel.text = String.init(format: "%@: %@", "price".localized, (item?.value)!)
            qtyLabel.text = String.init(format: "%@: %@", "qty".localized, (item?.quantity_for_user)!)

            priceLabel.halfTextColorChange(fullText: priceLabel.text!, changeText: "price".localized)
            qtyLabel.halfTextColorChange(fullText: qtyLabel.text!, changeText: "qty".localized)

        }
    }
    @IBAction func deleteClicked(_ sender: UIButton) {
        var dict = [String: Any]()
        dict["id_cart"] = UserDefaults.fetch(key: UserDefaults.Keys.id_cart)
        //dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
        if FetchDetails.isLoggedIn() {
            dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
        } else {
            dict["id_guest"] = UserDefaults.fetch(key: UserDefaults.Keys.id_guest)
        }
        dict["id_cart_rule"] = item.id_cart_rule
        dict["operation"] = "remove"
        passDataDelegate?.passData(id: item.id_cart_rule, name: "", dict: dict, jsonData: JSON.null, index: 0, call: WhichApiCall.deleteVoucher)
    }

}
