//
//  OrderHistoryModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class OrderHistoryModel {

    var orders = [OrderHistoryData]()
      private let orderPath = ["orders", "order"]
    private let mp_ordersPath = ["mp_orders", "order"]
    var MpOrders = [OrderHistoryData]()
    init?(data: JSON) {

        if data[orderPath] != JSON.null {
            if let data  = data[orderPath].array {
                self.orders = data.map { OrderHistoryData( data: $0) }
            } else {
                self.orders.append(OrderHistoryData(data: data[orderPath]))
            }
        }

        if data[mp_ordersPath] != JSON.null {
            if let data  = data[mp_ordersPath].array {
                self.orders = data.map { OrderHistoryData( data: $0) }
            } else {
                self.orders.append(OrderHistoryData(data: data[mp_ordersPath]))
            }
        }

    }
}

class OrderHistoryData {
    var reference: String?
    var address: String?
    var total_paid: String?
    var date_upd: String?
    var current_state_name: String!
    var id: String?
    var subtotal: String?
    var taxes: String?
    var grantTotal: String?
    var payment: String?
    var status_color: String!
    init(data: JSON) {
        self.reference = data["reference"].stringValue
        self.address = data["shipping_address"].stringValue
        self.total_paid = data["total_paid"].stringValue
        self.date_upd = data["date_upd"].string ?? data["date_add"].stringValue
        self.current_state_name = data["current_state_name"].string ?? data["order_status"].stringValue
        self.id = data["id"].string ?? data["id_order"].stringValue
        self.payment = data["payment"].string ?? data["payment_mode"].stringValue

        // For order Review

        self.subtotal = data["subtotal"]["value"].stringValue
        self.taxes = data["tax"]["value"].string ?? data["amount_tax"].stringValue
        self.grantTotal = data["grandtotal"]["value"].stringValue
        self.status_color = data["status_color"].string ?? data["current_state_color"].stringValue
    }
}
