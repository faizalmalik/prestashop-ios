//
//  OrderHistoryDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON

class OrderHistoryDataViewController: UIViewController {
  fileprivate let OrderHistoryViewModalObject = OrderHistoryViewModal()
    @IBOutlet weak var orderTableView: UITableView!
       var emptyView: EmptyView?
    var controller: AllController?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        orderTableView.delegate = OrderHistoryViewModalObject
        orderTableView.dataSource = OrderHistoryViewModalObject
        OrderHistoryViewModalObject.moveController = self
        self.navigationItem.title = "orderHistory".localized
        orderTableView.register(OrderHistoryViewCell.nib, forCellReuseIdentifier: OrderHistoryViewCell.identifier)
         orderTableView.register(SellerOrderTableViewCell.nib, forCellReuseIdentifier: SellerOrderTableViewCell.identifier)
       OrderHistoryViewModalObject.controller = controller
        orderTableView.tableFooterView = UIView()
        OrderHistoryViewModalObject.passDataDelegate = self

        self.makeRequest(id: "", call: WhichApiCall.none)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func makeRequest(id: String, call: WhichApiCall) {
        
        var params = ""
        switch call {
        case .reOrder:
            params = GlobalConstants.ApiNames.reOrder
            if let id_customer = UrlParameters.fetchCustomerId() {
                params += id_customer
            }
            params += UrlParameters.width
            params += "&id_order=" + id
            
        default:
            if controller == AllController.sellerOrder {
                params = GlobalConstants.MarketPlaceApis.sellerOrder
            } else {
                params = GlobalConstants.ApiNames.orderHistory
            }
            if let id_customer = UrlParameters.fetchCustomerId() {
                params += id_customer
            }
            if let id_seller = UrlParameters.fetchSellerID() {
                params += id_seller
            }
            params += UrlParameters.width
        }
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                
                print("loginError".localized, error!)
                
            } else {
                
                switch call {
                case .reOrder:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.tabBarController?.selectedIndex = 3
                    }
                default:
                    self.OrderHistoryViewModalObject.getValue(jsonData: responseObject!) {
                        (data: Bool) in
                        if data {
                            self.emptyView?.isHidden = true
                            self.orderTableView.isHidden = false
                            self.orderTableView.reloadData()
                        } else {
                            self.emptyView?.isHidden = false
                            self.orderTableView.isHidden = true
                            
                        }
                    }
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = "orderEmpty".localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-order")
//            emptyView?.button.setTitle("browseCategories".localized, for: .normal)
            emptyView?.isHidden = true
            emptyView?.button.isHidden = true
//            emptyView?.delegate = self
            self.view.addSubview(emptyView!)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OrderHistoryDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        if let view = UIStoryboard.loadOrderdetailViewController() {
            view.id = id
            view.reference = name
            view.controller =  self.controller
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
}

extension OrderHistoryDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        self.makeRequest(id: id, call: call)
    }
}
