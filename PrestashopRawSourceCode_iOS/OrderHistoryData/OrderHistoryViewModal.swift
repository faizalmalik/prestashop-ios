//
//  OrderHistoryViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import UIKit
import SwiftyJSON

class OrderHistoryViewModal: NSObject {
    
    var moveController: MoveController?
    var controller: AllController?
    var passDataDelegate: PassData?
    var orders = [OrderHistoryData]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = OrderHistoryModel(data: jsonData) else {
            return
        }
        
        if !data.orders.isEmpty {
            orders = data.orders
            completion(true)
        } else {
            completion(false)
        }
        
    }
}

extension OrderHistoryViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if controller == AllController.sellerOrder {
            if let cell = tableView.dequeueReusableCell(withIdentifier: SellerOrderTableViewCell.identifier) as? SellerOrderTableViewCell {
                cell.item = orders[indexPath.row]
                cell.selectionStyle = .none
                cell.moveController = self
                
                cell.separatorInset = .zero
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: OrderHistoryViewCell.identifier) as? OrderHistoryViewCell {
                cell.item = orders[indexPath.row]
                cell.selectionStyle = .none
                cell.moveController = self
                cell.passDataDelegate = self
                cell.separatorInset = .zero
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        moveController?.moveController(id: orders[indexPath.row].id!, name: orders[indexPath.row].reference!, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.orderDetail)
        //        delegate?.moveToController(id: orders[indexPath.row].orderId!, name: orders[indexPath.row].name!, dict: JSON.null, Controller: Controllers.orderDetails)
    }
    
}

extension OrderHistoryViewModal: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        moveController?.moveController(id: id, name: name, dict: dict, jsonData: jsonData, index: index, controller: controller)
    }
}

extension OrderHistoryViewModal: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        passDataDelegate?.passData(id: id, name: name, dict: dict, jsonData: jsonData, index: index, call: call)
    }
}
