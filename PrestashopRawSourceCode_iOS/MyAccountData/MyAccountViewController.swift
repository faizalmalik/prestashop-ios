//
//  MyAccountViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class MyAccountViewController: UIViewController {

    @IBOutlet weak var myAccounttableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = "myAccount".localized
        // Do any additional setup after loading the view.
        myAccounttableView.tableFooterView = UIView()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.makeRequest()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func makeRequest() {
        var params = GlobalConstants.ApiNames.myAccount
        if let id_customer = UrlParameters.fetchCustomerId() {
            params += id_customer
        }
        params += UrlParameters.width
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                
                print("loginError".localized, error!)
                
            } else {
                
                if let data = responseObject {
                    if data["response"]["gdpr_info"].count > 0 {
                        self.gdprData = data["response"]["gdpr_info"]
                    }
                    if data["response"][UserDefaults.Keys.id_seller].string != nil    &&  data["response"][UserDefaults.Keys.id_seller].stringValue != "0" {
                        UserDefaults.set(value: data["response"][UserDefaults.Keys.id_seller].stringValue, key: UserDefaults.Keys.id_seller)
                    }
                    self.perFormAction(data: data)
                }
            }
        }
    }

    var sectionCount = 1
    var mobikulArray = [JSON]()
    var mpArray = [JSON]()
    var gdprData: JSON = ""
    func perFormAction(data: JSON) {

        let mobikulPath = ["response", "ps_tabs", "tab_row"]
        let mpPath = ["response", "mp_tabs", "tab_row"]
        self.mobikulArray.removeAll()
        self.mpArray.removeAll()
        if data[mobikulPath] != JSON.null {
            if let data  = data[mobikulPath].array {
                self.mobikulArray = data.map { $0 }
            } else {
                self.mobikulArray.append(data[mobikulPath])
            }
        }

        if data[mpPath] != JSON.null {
            sectionCount = 2
            if let data  = data[mpPath].array {
                self.mpArray = data.map { $0 }
            } else {
                self.mpArray.append(data[mpPath])
            }
        }
        myAccounttableView.reloadData()

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyAccountViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return mobikulArray.count
        case 1:
            return mpArray.count
        default:
            return 0
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                cell.textLabel?.textAlignment = .right
            } else {
                cell.textLabel?.textAlignment = .left
            }
            //        cell.imageView?.image = profileImageArray[indexPath.row]
            cell.textLabel?.text = mobikulArray[indexPath.row]["name"].stringValue
            cell.accessoryType = .disclosureIndicator
            return cell
        case 1:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                cell.textLabel?.textAlignment = .right
            } else {
                cell.textLabel?.textAlignment = .left
            }
            //        cell.imageView?.image = profileImageArray[indexPath.row]
            cell.textLabel?.text = mpArray[indexPath.row]["name"].stringValue
              cell.textLabel?.numberOfLines = 0
            cell.textLabel?.sizeToFit()
            cell.accessoryType = .disclosureIndicator
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            switch mobikulArray[indexPath.row]["code"].stringValue {
            case "orderHistory":
                
                //                let chatView = ChatDataViewController()
                //                chatView.senderId = "5"
                //                chatView.senderDisplayName = "dksk"
                //////                chatView.displayName = persons[indexPath.row]
                ////                //        chatView.messages = makeNormalConversation()
                //                let chatNavigationController = UINavigationController(rootViewController: chatView)
                //                present(chatNavigationController, animated: true, completion: nil)
                
                //                let view = UIStoryboard.loadChatDataViewController()
                if let view = UIStoryboard.loadOrderHistoryViewController() {
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "myAddress":
                if let view = UIStoryboard.loadAddresslViewController() {
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "myCreditSlips":
                if let view = UIStoryboard.loadMyCreditViewController() {
                    view.title1 = mobikulArray[indexPath.row]["name"].stringValue
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "myReturns":
                if let view = UIStoryboard.loadMyMerchandiseViewController() {
                    view.title1 = mobikulArray[indexPath.row]["name"].stringValue
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "myPersonalInfo":
                if let view = UIStoryboard.loadMyPersonalInfoViewController() {
                    view.title1 = mobikulArray[indexPath.row]["name"].stringValue
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "myWishlists":
                if let view = UIStoryboard.loadMyWishlistViewController() {
                    view.title1 = mobikulArray[indexPath.row]["name"].stringValue
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "myVouchers":
                if let view = UIStoryboard.loadMyVoucherInfoViewController() {
                    view.title1 = mobikulArray[indexPath.row]["name"].stringValue
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "myFirstAddress":
                if let view = UIStoryboard.loadAddAddressViewController() {
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "myPersonalData":
                if let view = UIStoryboard.MyPersonalDataViewController() {
                    view.data = self.gdprData
                    self.navigationController?.pushViewController(view, animated: true)
                }
            default:
                print()
            }
        } else {
            switch mpArray[indexPath.row]["code"].stringValue {
                
            case "requestSeller":
                if let view = UIStoryboard.loadBecomeSellerViewController() {
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "sellerOrder":
                if let view = UIStoryboard.loadOrderHistoryViewController() {
                    view.controller = AllController.sellerOrder
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "sellerCollection":
                //                let view = UIStoryboard.loadProductCategoryViewController()
                //                view.controller = AllController.sellerCollection
                if let view = UIStoryboard.loadShopDataViewController() {
                    view.name = mpArray[indexPath.row]["name"].stringValue
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "sellerProfile":
                if let view = UIStoryboard.loadSellerProfileViewController() {
                    view.title1 = mpArray[indexPath.row]["name"].stringValue
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "sellerDashboard":
                if let view = UIStoryboard.loadSellerDashboardViewController() {
                    view.title1 = mpArray[indexPath.row]["name"].stringValue
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case "chat":
                let chatView = ChatDataViewController()
                chatView.senderId = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
                chatView.senderDisplayName = String.init(format: "%@ %@", UserDefaults.fetch(key: UserDefaults.Keys.firstname)!, UserDefaults.fetch(key: UserDefaults.Keys.lastname)!)
                chatView.name = String.init(format: "%@ %@", UserDefaults.fetch(key: UserDefaults.Keys.firstname)!, UserDefaults.fetch(key: UserDefaults.Keys.lastname)!)
                chatView.email = (UserDefaults.fetch(key: UserDefaults.Keys.email)) ?? ""
                chatView.buyer_unread = 0
                chatView.notificationKey = ""
                let chatNavigationController = UINavigationController(rootViewController: chatView)
                present(chatNavigationController, animated: true, completion: nil)
            case "myPersonalData":
                if let view = UIStoryboard.MyPersonalDataViewController() {
                    view.data = self.gdprData
                    self.navigationController?.pushViewController(view, animated: true)
                }
            default:
                print()
            }
        }
    }
}
