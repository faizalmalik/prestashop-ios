//
//  ZoomImageViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit

class ZoomImageViewController: UIViewController {

//    var interactor:Interactor? = nil
    var dict = [String: Any]()
    var imageArray = [String]()

    @IBOutlet weak var zoomCollectionVFiew: UICollectionView!
    @IBOutlet weak var croosbtn: UIBarButtonItem!
    @IBOutlet weak var imageController: UIImageView!
    var currentPage = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.view.backgroundColor = UIColor.clear
        zoomCollectionVFiew.isPagingEnabled = true
        imageArray = (dict["image"] as? [String]) ?? []
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear

    }
//    @IBAction func panGestureClicked(_ sender: UIPanGestureRecognizer) {
//
//        let percentThreshold:CGFloat = 0.9
//
//        // convert y-position to downward pull progress (percentage)
//        let translation = sender.translation(in: view)
//        let verticalMovement = translation.y / view.bounds.height
//        let downwardMovement = fmaxf(Float(verticalMovement), 1.0)
//        let downwardMovementPercent = fminf(downwardMovement, 1.0)
//        let progress = CGFloat(downwardMovementPercent)
//
//        guard let interactor = interactor else { return }
//
//        switch sender.state {
//        case .began:
//            interactor.hasStarted = true
//            dismiss(animated: true, completion: nil)
//        case .changed:
//            interactor.shouldFinish = progress > percentThreshold
//            interactor.update(progress)
//        case .cancelled:
//            interactor.hasStarted = false
//            interactor.cancel()
//        case .ended:
//            interactor.hasStarted = false
//            interactor.shouldFinish
//                ? interactor.finish()
//                : interactor.cancel()
//        default:
//            break
//        }
//    }
    
    override func viewDidAppear(_ animated: Bool) {
//        let indexPath1: IndexPath?
      let  indexPath1 = IndexPath.init(row: currentPage, section: 0)
        zoomCollectionVFiew.scrollToItem(at: indexPath1, at: .right, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func barButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension ZoomImageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as? CustomCellCollectionViewCell {
            cell.imagee.isUserInteractionEnabled = true
            cell.imagee.setImage(imageUrl: imageArray[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SCREEN_WIDTH, height: (UIScreen.main.bounds.size.height ) )

    }

}
