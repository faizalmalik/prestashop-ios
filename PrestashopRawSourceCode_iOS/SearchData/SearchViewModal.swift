//
//  SearchViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class SearchViewModal: NSObject {
     var searchSuggestion = [SearchSuggestionProductData]()
      var delegate: MoveController?

     func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        searchSuggestion.removeAll()
        guard let data = SearchModal(data: jsonData) else {
            return
        }

        if !data.searchSuggestion.isEmpty {
            self.searchSuggestion = data.searchSuggestion
            completion(true)
        } else {
            completion(false)
        }

    }
}

extension SearchViewModal: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchSuggestion.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = UITableViewCell(style: .default, reuseIdentifier: "cell")
        if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
            cell.textLabel?.textAlignment = .right
        } else {
            cell.textLabel?.textAlignment = .left
        }
        cell.textLabel?.text = searchSuggestion[indexPath.row].name
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if searchSuggestion.count > 0 {
            return "searchSuggestion".localized
        }
        return nil
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.moveController(id: searchSuggestion[indexPath.row].id!, name: searchSuggestion[indexPath.row].name!, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productController)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIApplication.shared.keyWindow?.rootViewController?.view?.endEditing(true)
    }
    
}
