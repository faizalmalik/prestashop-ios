//
//  SearchModal.swift
/**
* Webkul Software.
    *
* @Mobikul
* @PrestashopMobikulAndMarketplace
* @author Webkul
* @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
* @license https://store.webkul.com/license.html
*/

import Foundation
import SwiftyJSON

class SearchModal {

    var searchSuggestion = [SearchSuggestionProductData]()
    init?(data: JSON) {

        let suggestionPath = ["suggestions", "suggestion"]

        if data[suggestionPath] != JSON.null {
            if let data  = data[suggestionPath].array {
                self.searchSuggestion = data.map { SearchSuggestionProductData( json: $0)! }
            } else {
                self.searchSuggestion.append( SearchSuggestionProductData( json: data[suggestionPath] )! )
            }
        }
    }
}

class SearchSuggestionProductData {

    var name: String?
    var id: String?

     init?(json: JSON) {
        name = json["name"].stringValue
        id = json["id_product"].stringValue
    }
}
