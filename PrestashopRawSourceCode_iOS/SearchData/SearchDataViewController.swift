//
//  SearchDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON
import Alamofire
import Speech

class SearchDataViewController: UIViewController {
    
    var emptyView: EmptyView?
    
    @IBOutlet weak var searchBar: UISearchBar!
    var apiCall: WhichApiCall?
    fileprivate let searchViewModalObject = SearchViewModal()
    @IBOutlet weak var seaechTableView: UITableView!
    var yourData = ""
    var isButtonEnabled = false
    var templateImage: UIImage!
    let cameraBtn = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.configureCancelBarButton()
        self.navigationController?.navigationBar.navigationChanges()
        searchBar.showsCancelButton = false
        self.navigationItem.titleView = searchBar
        searchBar.placeholder = "search".localized
        seaechTableView.delegate = searchViewModalObject
        seaechTableView.dataSource = searchViewModalObject
        self.seaechTableView.tableFooterView = UIView()
        self.seaechTableView.isHidden = true
        searchViewModalObject.delegate = self
        searchBar.subviews[0].subviews.compactMap() { $0 as? UITextField }.first?.tintColor = UIColor.black
        
        if let searchTextField = searchBar.value(forKey: "_searchField") as? UITextField, let clearButton = searchTextField.value(forKey: "_clearButton") as? UIButton {
            // Create a template copy of the original button image
            //templateImage =  clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
            // Set the template image copy as the button image
            //clearButton.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
            // Finally, set the image color
            //clearButton.tintColor = .lightGray
            
            cameraBtn.frame = CGRect(origin: CGPoint(x: UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" ? 64:searchBar.frame.size.width - 105, y: (searchBar.frame.size.height - clearButton.frame.size.height)/2), size: clearButton.frame.size)
            cameraBtn.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
            cameraBtn.tintColor = .lightGray
            searchBar.addSubview(cameraBtn)
            cameraBtn.addTarget(self, action: #selector(OpenCamera), for: .touchUpInside)
            //            searchTextField.clearButtonMode = .always
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        searchBar.becomeFirstResponder()
        if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
            if let searchTextField = searchBar.value(forKey: "_searchField") as? UITextField {
                searchTextField.textAlignment = .right
            }
        } else {
            if let searchTextField = searchBar.value(forKey: "_searchField") as? UITextField {
                searchTextField.textAlignment = .left
            }
        }
        //        if emptyView == nil {
        //            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
        //            emptyView?.frame = self.view.bounds
        //            emptyView?.labelText.text = "searchEmpty".localized
        //            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-search-result")
        //            emptyView?.button.isHidden = true
        //            emptyView?.isHidden = true
        //            self.view.addSubview(emptyView!)
        //        }
    }
    
    override func viewWillLayoutSubviews() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func makeRequest() {
        
        var params = ""
        switch apiCall {
        case .getSearchSuggestion?:
            params = "mobikul/getsuggestion?"
            params += UrlParameters.width
            params += "&key=" + searchBar.text!
        case .searchPressed?:
            params = "mobikul/getsearchdetails?"
            params += UrlParameters.width
            params += "&key=" + searchBar.text!
        default:
            print("return")
        }
        
        if apiCall == WhichApiCall.getSearchSuggestion {
            
        }
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.SearchController, view: self.view) { (responseObject: JSON?, error: Error?) in
            
            if let error = error {
                //                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                
                switch self.apiCall {
                case .getSearchSuggestion?:
                    self.searchViewModalObject.getValue(jsonData: responseObject! ) {
                        (data: Bool) in
                        if self.searchViewModalObject.searchSuggestion.count > 0 {
                            self.seaechTableView.isHidden = false
                            self.emptyView?.button.isHidden = true
                            self.seaechTableView.reloadData()
                        } else {
                            self.searchViewModalObject.searchSuggestion.removeAll()
                            self.seaechTableView.isHidden = true
                            self.seaechTableView.reloadData()
                            self.emptyView?.button.isHidden = false
                        }
                    }
                case .searchPressed?:
                    self.dismissKeyboard()
                    //                    let nextController = UIStoryboard.loadProductCategoryViewController()
                    //                    nextController.responsedata = responseObject
                    //                    nextController.name = self.searchBar.text
                    //                    nextController.controller = AllController.SearchController
                    //
                //                    self.navigationController?.pushViewController(nextController, animated: true)
                default :
                    print("returm")
                }
            }
        }
    }
    
    func configureCancelBarButton() {
        let cancelButton = UIBarButtonItem()
        cancelButton.title = "cancel".localized
        cancelButton.action = #selector(self.ipadCancelButton)
        cancelButton.target = self
        self.navigationItem.setRightBarButton(cancelButton, animated: true)
    }
    
    @objc func ipadCancelButton(sender: UIBarButtonItem) {
        self.dismissKeyboard()
        self.dismiss(animated: false, completion: nil)
        self.tabBarController?.selectedIndex = 0
    }
    
    func dismissKeyboard() {
        searchBar.resignFirstResponder()
        view.endEditing(true)
    }
    
    @objc func OpenCamera() {
        self.view.endEditing(true)
        self.searchBar.resignFirstResponder()
        let alert = UIAlertController(title: "chooseaction".localized, message: nil, preferredStyle: .actionSheet)
        let TextDetection = UIAlertAction(title: "detecttext".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let view = UIStoryboard.loadDetectorViewController() {
                view.detectorType = .text
                view.delegate = self
                view.title = "detecttext".localized
                self.navigationController?.pushViewController(view, animated: true)
            }
        })
        let ImageDetection = UIAlertAction(title: "detectimage".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let view = UIStoryboard.loadDetectorViewController() {
                view.detectorType = .image
                view.delegate = self
                view.title = "detectimage".localized
                self.navigationController?.pushViewController(view, animated: true)
            }
        })
        alert.addAction(TextDetection)
        alert.addAction(ImageDetection)
        let cancel = UIAlertAction(title: "cancel".localized, style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(cancel)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width - 110, y: 64, width: 1.0, height: 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension SearchDataViewController: SuggestionDataHandlerDelegate {
    func suggestedData(data: String) {
        self.searchBar.text = data
        self.cameraBtn.isHidden = true
        if let nextController = UIStoryboard.loadProductCategoryViewController() {
            nextController.name = self.searchBar.text
            nextController.controller = AllController.SearchController
            self.navigationController?.pushViewController(nextController, animated: true)
        }
    }
}

extension SearchDataViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismiss(animated: false, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBar.text == "" {
            cameraBtn.isHidden = false
            //            if let searchTextField = searchBar.value(forKey: "_searchField") as? UITextField, let clearButton = searchTextField.value(forKey: "_clearButton") as? UIButton {
            //                clearButton.setImage(#imageLiteral(resourceName: "camera"), for: .normal)
            //                clearButton.tintColor = .lightGray
            //                clearButton.addTarget(self, action: #selector(OpenCamera), for: .touchUpInside)
            //                searchTextField.clearButtonMode = .always
            //                clearButton.isHidden = false
            //            }
        } else {
            cameraBtn.isHidden = true
            //            if let searchTextField = searchBar.value(forKey: "_searchField") as? UITextField, let clearButton = searchTextField.value(forKey: "_clearButton") as? UIButton {
            //                clearButton.setImage(templateImage, for: .normal)
            //                clearButton.tintColor = .lightGray
            //                clearButton.removeTarget(self, action: #selector(OpenCamera), for: .touchUpInside)
            //                searchTextField.clearButtonMode = .always
            //                clearButton.isHidden = false
            //            }
        }
        
        if let text = searchBar.text {
            if text.count > 2 {
                let sessionManager = Alamofire.SessionManager.default
                sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in
                    dataTasks.forEach { $0.cancel() }
                    uploadTasks.forEach { $0.cancel() }
                    downloadTasks.forEach { $0.cancel() }
                }
                apiCall = WhichApiCall.getSearchSuggestion
                self.makeRequest()
            } else {
                self.searchViewModalObject.searchSuggestion.removeAll()
                self.seaechTableView.reloadData()
            }
        } else {
            self.searchViewModalObject.searchSuggestion.removeAll()
            self.seaechTableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            if text.count > 0 {
                self.dismissKeyboard()
                self.dismissKeyboard()
                if let nextController = UIStoryboard.loadProductCategoryViewController() {
                    nextController.name = self.searchBar.text
                    nextController.controller = AllController.SearchController
                    
                    self.navigationController?.pushViewController(nextController, animated: true)
                }
                //                apiCall = WhichApiCall.searchPressed
                //                self.makeRequest()
            }
        }
    }
}

extension SearchDataViewController: MoveController {
    
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        self.dismissKeyboard()
        if let nextController = UIStoryboard.loadProductViewController() {
            nextController.productName = name
            nextController.productId = id
            self.navigationController?.pushViewController(nextController, animated: true)
        }
    }
    
}

extension SearchDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest()
    }
}
