//
//  ViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import PermissionScope
import FirebaseAnalytics
var CategoryArray: JSON?
import Firebase
import UserNotifications
import CoreML
import Kingfisher
import FirebaseMessaging

@objcMembers
class HomeController: UIViewController {
    
    @IBOutlet weak var notificationButton: MIBadgeButton!
    @IBOutlet weak var requestButton: MIBadgeButton!
//    @IBOutlet weak var badgeLabel: UILabel!
    @IBOutlet weak var searchProductLabel: UILabel!
    @IBOutlet weak var appName: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var cameraBtn: UIButton!
    
    fileprivate let HomeViewModalObject = HomeViewModal()
    var emptyView: EmptyView?
    var refreshControl: UIRefreshControl!
    var yourData = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.cameraBtn.tintColor = .lightGray
        self.launchScreen()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        let cache = KingfisherManager.shared.cache
        cache.maxMemoryCost = 30 * 1024 * 1024
        cache.maxDiskCacheSize = 30 * 1024 * 1024
        cache.maxCachePeriodInSecond = 60 * 60 * 24 * 3
        self.tabBarController?.delegate = self
        self.navigationController?.navigationBar.navigationChanges()
        self.navigationController?.navigationBar.isHidden = true
        homeTableView.delegate = HomeViewModalObject
        homeTableView.dataSource = HomeViewModalObject
        HomeViewModalObject.passDataDelegate = self
        HomeViewModalObject.reloadSections = { [weak self] (section: Int) in
            self?.homeTableView?.beginUpdates()
            self?.homeTableView?.reloadSections([section], with: .fade)
            self?.homeTableView?.endUpdates()
        }
        self.registerCell()
        self.setObservers()
        self.addRefreshController()
        homeTableView.separatorStyle = .none
        HomeViewModalObject.homeTableView = homeTableView
        Analytics.logEvent(AnalyticsEventAppOpen, parameters: [AnalyticsParameterItemID: (UserDefaults.standard.value(forKey: "id_customer") ?? "" ), AnalyticsParameterItemName: (UserDefaults.standard.value(forKey: "email") ?? "Guest" )])
        Messaging.messaging().subscribe(toTopic: "/topics/global")
        self.homeTableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: -45, right: 0)
        self.removeTabbarItemsText()
        
        
        self.setupHeaderButtons()
    }
    
    func addRefreshController() {
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "pullToRefresh".localized)
        refreshControl.addTarget(self, action: #selector(self.refresh), for: UIControl.Event.valueChanged)
        homeTableView.addSubview(refreshControl)
    }
    
    func setObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.pushNotificationReceived), name: NSNotification.Name(rawValue: "pushNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.universalLinkReceived), name: NSNotification.Name(rawValue: "universalLink"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.DTouch), name: NSNotification.Name(rawValue: "3DTouch"), object: nil)
    }
    
    func registerCell() {
        HomeViewModalObject.moveDelegate = self
        homeTableView.register(HomeCategoryCell.nib, forCellReuseIdentifier: HomeCategoryCell.identifier)
        homeTableView.register(HomeTableViewCell.nib, forCellReuseIdentifier: HomeTableViewCell.identifier)
        homeTableView.register(HomeProductBlockTableViewCell.nib, forCellReuseIdentifier: HomeProductBlockTableViewCell.identifier)
        homeTableView.register(HomeProductTableViewCell.nib, forCellReuseIdentifier: HomeProductTableViewCell.identifier)
        homeTableView.register(HomeBannerTableViewCell.nib, forCellReuseIdentifier: HomeBannerTableViewCell.identifier)
    }
    func setupHeaderButtons(){
        requestButton.badgeString = nil
        requestButton.badgeTextColor = UIColor.white
        requestButton.badgeEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 2)
        
        notificationButton.badgeString = nil
        notificationButton.badgeTextColor = UIColor.white
        notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 2)
        
        
    }
    func refresh(sender: AnyObject) {
        GlobalConstants.Permissiom.permissiomAlertPresented = false
        self.makeRequest(call: WhichApiCall.none)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func launchScreen() {
        if let view = UIStoryboard.loadLaunchDataViewController() {
            view.delegate = self
            self.present(view, animated: false, completion: {
                
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.reloadInputViews()
        if GlobalConstants.AppCredentials.environment == EnvironmentType.marketplace {
            appName.text = "appNameMp".localized
        } else {
            appName.text = "appName".localized
        }
        searchProductLabel.text = "searchProducts".localized
        self.navigationController?.navigationBar.isHidden = true
        if  self.tabBarController != nil {
            self.tabBarController?.setCartCount(value: FetchDetails.fetchCartCount())
        }
        self.tabBarController?.tabBar.isHidden = false
        self.setBadge()
        self.setupRequestBadge()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillLayoutSubviews() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.topView.applyGradientAccordingToScreenWidth(colours: GlobalConstants.Colors.gradientArray)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func setBadge() {
        
        if UserDefaults.standard.value(forKey: "badge") == nil {

            notificationButton.badgeString = nil
        } else {
            if let value = UserDefaults.standard.value(forKey: "badge") as? Int {
                if value == 0 {
                    notificationButton.badgeString = nil
                } else {
                   
                    notificationButton.badgeString = String.init(format: "%i", value)
                   
                }
            } else {
                notificationButton.badgeString = nil
            }
        }
    }
    func setupRequestBadge(){
        

        self.makeRequest(call: WhichApiCall.requestCount)

    }
    func removeTabbarItemsText() {
        if let items = tabBarController?.tabBar.items {
            for item in items {
                item.title = ""
                item.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
            }
        }
    }
    
    func presentSimplePermissionsAlert() {
        if !GlobalConstants.Permissiom.permissiomAlertPresented {
            self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
            GlobalConstants.Permissiom.permissiomAlertPresented = true
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().delegate = self
            } else {
                // Fallback on earlier versions
            }
            let permissionScope = PermissionScope()
            permissionScope.enableLocation = "enableLocation".localized
            permissionScope.allowedLocation = "allowedLocation".localized
            permissionScope.deniedLocation = "deniedLocation".localized
            permissionScope.allowNotification = "allowNotification".localized
            permissionScope.allowedNotification = "allowedNotification".localized
            permissionScope.deniedNotification = "deniedNotification".localized
            permissionScope.allowPhotos = "allowPhotos".localized
            permissionScope.allowedPhotos = "allowedPhotos".localized
            permissionScope.deniedPhotos = "deniedPhotos".localized
            permissionScope.bodyLabel.text = "permissionBody".localized
            permissionScope.closeButton.setTitle("close".localized, for: .normal)
            permissionScope.closeButton.frame = CGRect(x: permissionScope.closeButton.frame.origin.x - 20, y: permissionScope.closeButton.frame.origin.y, width: permissionScope.closeButton.frame.width + 20, height: permissionScope.closeButton.frame.size.height)
            permissionScope.headerLabel.text = "permissionHeading".localized
            
            // Set up permissions
            //permissionScope.addPermission(LocationWhileInUsePermission(),message: "locationPermission".localized)
            
            permissionScope.addPermission(NotificationsPermission(notificationCategories: nil), message: "notiPermission".localized )
            
            permissionScope.addPermission(PhotosPermission(), message: "cameraPermission".localized )
            
            // Show permission dialog with callbacks
            permissionScope.show({ finished, results in
                print("Permissions state changed, results: \(results)")
                if PermissionScope().statusLocationInUse() == .authorized {
                }
                if PermissionScope().statusNotifications() == .authorized {
                    if #available(iOS 10.0, *) {
                        UNUserNotificationCenter.current().delegate = self
                    } else {
                        // Fallback on earlier versions
                    }
                    Messaging.messaging().delegate = self
                }
            }, cancelled: { results in
                print("User closed the permission alert, results: \(results)")
                if PermissionScope().statusLocationInUse() == .authorized {
                }
                if PermissionScope().statusNotifications() == .authorized {
                    if #available(iOS 10.0, *) {
                        UNUserNotificationCenter.current().delegate = self
                    } else {
                        // Fallback on earlier versions
                    }
                    Messaging.messaging().delegate = self
                }
            })
        }
    }
    
    @IBAction func requestList(_ sender: MIBadgeButton) {
        self.navigationController?.navigationBar.isHidden = false
        if let notii = UIStoryboard.loadRequestListViewController() {
            self.navigationController?.pushViewController(notii, animated: true)
        }
        
    }
    @IBAction func tapNavigationNotification(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        if let notii = UIStoryboard.loadNotificationViewController() {
            self.navigationController?.pushViewController(notii, animated: true)
        }
    }
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
        self.HomeViewModalObject.items.removeAll()
        self.homeTableView.reloadData()
        self.makeRequest(call: WhichApiCall.none)

    }
    
    @IBAction func btnClicked(_ sender: UIButton) {
        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            return
        }
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = .camera
        cameraPicker.allowsEditing = false
        present(cameraPicker, animated: true)
    }
    
    @IBAction func tapCameraBtn(_ sender: Any) {
        openCameraAction()
    }
    
    @IBAction func NotificationPressed(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        if let notii = UIStoryboard.loadNotificationViewController() {
            self.navigationController?.pushViewController(notii, animated: true)
        }
    }
    
    @IBAction func SearchViewTapped(_ sender: UITapGestureRecognizer) {
        self.tabBarController?.selectedIndex = 1
    }
}

extension HomeController: SuggestionDataHandlerDelegate {
    
    func openCameraAction() {
        let alert = UIAlertController(title: "chooseaction".localized, message: nil, preferredStyle: .actionSheet)
        let TextDetection = UIAlertAction(title: "detecttext".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.navigationController?.navigationBar.isHidden = false
            if let view = UIStoryboard.loadDetectorViewController() {
                view.detectorType = .text
                view.delegate = self
                view.title = "detecttext".localized
                self.navigationController?.pushViewController(view, animated: true)
            }
        })
        let ImageDetection = UIAlertAction(title: "detectimage".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.navigationController?.navigationBar.isHidden = false
            if let view = UIStoryboard.loadDetectorViewController() {
                view.detectorType = .image
                view.delegate = self
                view.title = "detectimage".localized
                self.navigationController?.pushViewController(view, animated: true)
            }
        })
        alert.addAction(TextDetection)
        alert.addAction(ImageDetection)
        let cancel = UIAlertAction(title: "cancel".localized, style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(cancel)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width - 110, y: 64, width: 1.0, height: 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    func doSearchAction() {
        self.tabBarController?.selectedIndex = 1
    }
    
    func notificationAction() {
        self.navigationController?.navigationBar.isHidden = false
        if let notii = UIStoryboard.loadNotificationViewController() {
            self.navigationController?.pushViewController(notii, animated: true)
        }
    }
    
    func suggestedData(data: String) {
        self.navigationController?.navigationBar.isHidden = false
        if let nextController = UIStoryboard.loadProductCategoryViewController() {
            nextController.name = data
            nextController.controller = AllController.SearchController
            self.navigationController?.pushViewController(nextController, animated: true)
        }
    }
}

extension HomeController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        picker.dismiss(animated: true)
        guard let image = info["UIImagePickerControllerOriginalImage"] as? UIImage else {
            return
        }
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 299, height: 299), true, 2.0)
        image.draw(in: CGRect(x: 0, y: 0, width: 299, height: 299))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
        var pixelBuffer: CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(newImage.size.width), Int(newImage.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
        guard status == kCVReturnSuccess else {
            return
        }
        CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer!)
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext(data: pixelData, width: Int(newImage.size.width), height: Int(newImage.size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer!), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue) //3
        context?.translateBy(x: 0, y: newImage.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        UIGraphicsPushContext(context!)
        newImage.draw(in: CGRect(x: 0, y: 0, width: newImage.size.width, height: newImage.size.height))
        UIGraphicsPopContext()
        CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
    }
    
}

extension HomeController: MoveController {
    
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        self.navigationController?.navigationBar.isHidden = false
        switch controller {
        case .productController:
            if let view = UIStoryboard.loadProductViewController() {
                view.productId = id
                view.productName = name
                view.productDict = dict
                view.selectedIndex = index
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .productCategory:
            if let view = UIStoryboard.loadProductCategoryViewController() {
                view.id = id
                view.name = name
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .browser:
            if let url = URL(string: id) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:])
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        default:
            break
        }
    }
}

extension HomeController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        //self.tabBarController?.tabBar.isHidden = false
        if let vc = viewController as? UINavigationController {
            vc.popToRootViewController(animated: false)
        }
    }
}

extension  HomeController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        self.navigationController?.navigationBar.isHidden = false
        if let view = UIStoryboard.loadProductCategoryViewController() {
            view.id = id
            view.name = name
            if call == WhichApiCall.ViewAll {
                view.controller = AllController.home
            } else {
                view.controller = AllController.homeProductBlock
            }
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
}

// For push notification

extension HomeController {
    
    func pushNotificationReceived(_ note: Notification) {
        self.navigationController?.navigationBar.isHidden = false
        guard let type = note.userInfo?["gcm.notification.type"] as? String else {
            return
        }
        if type == "product" {
            guard let id = note.userInfo?["gcm.notification.id_page"] as? String else {
                return
            }
            guard let name = note.userInfo?["gcm.notification.product_name"] as? String else {
                return
            }
            self.movingFromExternalData(id: id, name: name, controller: AllController.productController)
        }
        
        if type == "category" {
            guard let id = note.userInfo?["gcm.notification.id_page"] as? String else {
                return
            }
            guard let name = note.userInfo?["gcm.notification.category_name"] as? String else {
                return
            }
            self.movingFromExternalData(id: id, name: name, controller: AllController.productCategory)
        }
    }
    
    func universalLinkReceived(_ note: Notification) {
    }
    
    func movingFromExternalData(id: String, name: String, controller: AllController) {
        switch controller {
        case .productController:
            self.navigationController?.navigationBar.isHidden = false
            if let view = UIStoryboard.loadProductViewController() {
                view.productId = id
                view.productName = name
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .productCategory:
            self.navigationController?.navigationBar.isHidden = false
            if let view = UIStoryboard.loadProductCategoryViewController() {
                view.id = id
                view.name = name
                self.navigationController?.pushViewController(view, animated: true)
            }
        default:
            print()
        }
    }
}

// 3D touch

extension HomeController {
    func DTouch (_ note: Notification) {
        let value  =  note.object as? String
        switch value {
        case "Search"?:
            self.tabBarController?.selectedIndex = 2
        case "MarketPlace"?:
            self.navigationController?.navigationBar.isHidden = false
            if let view = UIStoryboard.loadSellerListViewController() {
                self.navigationController?.pushViewController(view, animated: true)
            }
        case "Profile"?:
            self.tabBarController?.selectedIndex = 4
        case "Notification"?:
            self.navigationController?.navigationBar.isHidden = false
            if let notii = UIStoryboard.loadNotificationViewController() {
                self.navigationController?.pushViewController(notii, animated: true)
            }
        case "Cart"?:
            self.tabBarController?.selectedIndex = 3
        default:
            break
        }
    }
}

extension HomeController: EmptyDelegate {
    
    func setEmptyData(code: Int) {
        self.tabBarController?.tabBar.isHidden = true
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
    }
    
    func buttonPressed() {
        //self.makeRequest()
    }
}

extension HomeController: PassJsonData, SetPermission {
    
    func SetPermissionRequest() {
        self.presentSimplePermissionsAlert()
    }
    
    func passJsonData(data: JSON) {
        self.tabBarController?.tabBar.isHidden = false
        if UserDefaults.standard.value(forKey: UserDefaults.Keys.languageCode) != nil {
            self.presentSimplePermissionsAlert()
        }
        self.HomeViewModalObject.delegate = self
        self.HomeViewModalObject.getValue(jsonData: data) {
            [weak self] (data: Bool) in
            self?.navigationController?.navigationBar.navigationChanges()
            self?.topView.applyGradientAccordingToScreenWidth(colours: GlobalConstants.Colors.gradientArray)
            self?.homeTableView.reloadData()
        }
    }
    
    func makeRequest(call: WhichApiCall) {
        
        switch call {
        case .none:
            var params = GlobalConstants.ApiNames.homepageApi
            params += UrlParameters.width
            NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) {
                [weak self] (responseObject: JSON?, error: Error?) in
                if error != nil {
                } else {
                    self?.tabBarController?.tabBar.isHidden = false
                    self?.refreshControl.endRefreshing()
                    if let data =  responseObject {
                        CategoryArray = data["menu_category"]["root_category"]
                        self?.HomeViewModalObject.getValue(jsonData: data) {
                            [weak self] (data: Bool) in
                            self?.topView.applyGradientAccordingToScreenWidth(colours: GlobalConstants.Colors.gradientArray)
                            self?.homeTableView.reloadData()
                        }
                    }
                }
            }
            
        case .requestCount:
            
            var params = GlobalConstants.ApiNames.getRequestCount
            if FetchDetails.is_seller() {
                params += UrlParameters.fetchSellerID() ?? ""
                
                params += "&run_as=3"
                
            }else if FetchDetails.isLoggedIn() {
           
                params += UrlParameters.fetchCustomerId() ?? ""
                params += "&run_as=2"

            }
            else{
                //Guest Login case
                if let guesId = UrlParameters.fetchGuestId(){
                    params +=  guesId
                    params += "&run_as=1"

                }else{
                    params += "id_guest=0&run_as=1"
                }
                
                
            }
     
            
            NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) {
                [weak self] (responseObject: JSON?, error: Error?) in
                if error != nil {
//                    print(error)

                } else {
                    //real_stock_count //count_real_stock_status_1
                    
                    if let count = responseObject?["real_stock_count"]["count_real_stock_status_1"]{
                        
                        DispatchQueue.main.async {
                            self?.requestButton.badgeString = count.intValue > 0 ? "\(count)" : ""
                            
                        }

                    }
//                    print(responseObject)
                    
                    
                    
                    
                }
            }
            
            
            
        default:
            
            return
            
            
        }
        
    }    
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}
