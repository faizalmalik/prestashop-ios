//
//  HomeTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON

class HomeTableViewCell: UITableViewCell {
    @IBOutlet weak var tableCollectionView: UICollectionView!
    var banners = [Banners]()
    var collectionType: HomeType?
    var categories = [AllCategories]()
    var products = [ProductData]()
    var moveDelegate: MoveController?
    
    @IBOutlet weak var pageController: CHIPageControlFresno!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        tableCollectionView.showsHorizontalScrollIndicator = false
        tableCollectionView.register(BannerCollectionCell.nib, forCellWithReuseIdentifier: BannerCollectionCell.identifier)
        tableCollectionView.register(HomeCategoryCollectionViewCell.nib, forCellWithReuseIdentifier: HomeCategoryCollectionViewCell.identifier)
        tableCollectionView.register(HomeProductCollectionViewCell.nib, forCellWithReuseIdentifier: HomeProductCollectionViewCell.identifier)
        tableCollectionView.delegate = self
        tableCollectionView.dataSource = self
        
        pageController.radius = 4
        pageController.tintColor = GlobalConstants.Colors.accentColor
        pageController.currentPageTintColor = .black
        pageController.center = CGPoint(x: SCREEN_WIDTH/2, y: 20/2)
        pageController.isHidden = true
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}

extension HomeTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionType {
        case .banner?:
            pageController.numberOfPages = banners.count
            return banners.count
        case .categories?:
            pageController.numberOfPages = 0
            return categories.count
        case .products?:
            pageController.numberOfPages = 0
            return products.count
        default:
            pageController.numberOfPages = 0
            return 0
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionType {
        case .banner?:
            pageController.isHidden = false
            return
                self.configureBannerCell(collectionView: collectionView, indexPath: indexPath)
        case .categories?:
            pageController.isHidden = true
            return self.configureCategoryCell(collectionView: collectionView, indexPath: indexPath )
        case .products?:
            pageController.isHidden = true
            return self.configureProductCell(collectionView: collectionView, indexPath: indexPath )
        default:
            return UICollectionViewCell()
        }
        
    }
    
    func configureBannerCell (collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BannerCollectionCell.identifier, for: indexPath as IndexPath) as? BannerCollectionCell {
            cell.bannerItem = banners[indexPath.row]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func configureCategoryCell (collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCategoryCollectionViewCell.identifier, for: indexPath as IndexPath) as? HomeCategoryCollectionViewCell {
            cell.item = categories[indexPath.row]
            return cell
        }
        return UICollectionViewCell()
    }
    func configureProductCell (collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeProductCollectionViewCell.identifier, for: indexPath as IndexPath) as? HomeProductCollectionViewCell {
            cell.productItem = products[indexPath.row]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionType {
        case .banner?:
            if banners[indexPath.row].page == "category" {
                moveDelegate?.moveController(id: banners[indexPath.row].id_page, name: "Category", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productCategory)
            } else  if banners[indexPath.row].page == "product" {
                moveDelegate?.moveController(id: banners[indexPath.row].id_page, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productController)
            } else  if banners[indexPath.row].page == "custom_link" {
                moveDelegate?.moveController(id: banners[indexPath.row].url, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.browser)
            }
            
        case .categories?:
            moveDelegate?.moveController(id: categories[indexPath.row].id_category, name: categories[indexPath.row].name, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productCategory)
        case .products?:
            print()
        default:
            print()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionType {
        case .banner?:
            return CGSize(width: SCREEN_WIDTH, height: SCREEN_WIDTH / 2)
        case .categories?:
            return CGSize(width: 100, height: 100)
        case .products?:
            return CGSize(width: SCREEN_WIDTH / 2 - 1, height: SCREEN_WIDTH / 2 + 52)
        default:
            return CGSize(width: 0, height: 0)
        }
        
    }
}
extension HomeTableViewCell: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageController.set(progress: Int(scrollView.contentOffset.x / SCREEN_WIDTH), animated: true)
    }
    
    @objc func scrollAutomatically(_ timer1: Timer) {
        if collectionType == HomeType.banner {
            if let coll  = tableCollectionView {
                for cell in coll.visibleCells {
                    let indexPath: IndexPath? = coll.indexPath(for: cell)
                    if (indexPath?.row)! < (banners.count - 1) {
                        let indexPath1: IndexPath?
                        indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                        
                        coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                    } else {
                        let indexPath1: IndexPath?
                        indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                        coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                    }
                }
            }
        }
    }
}
