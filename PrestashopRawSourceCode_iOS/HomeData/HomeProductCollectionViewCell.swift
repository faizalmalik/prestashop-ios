//
//  HomeProductCollectionViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class HomeProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productView: UIView!
    @IBOutlet weak var viewMoreImage: UIImageView!
    @IBOutlet weak var productOldPrice: UILabel!
    @IBOutlet weak var oldPriceHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        productView.normalBorder()
        productPrice.textColor = GlobalConstants.Colors.priceColor
        productOldPrice.SetDefaultTextColor()
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var productItem: ProductData! {
        didSet {
            if productItem.show_price {
                self.productPrice.isHidden = false
                self.productOldPrice.isHidden = false
                if productItem.old_price != "" {
                    oldPriceHeight.constant = 17
                } else {
                    oldPriceHeight.constant = 0
                }
            } else {
                self.productPrice.isHidden = true
                self.productOldPrice.isHidden = true
                oldPriceHeight.constant = 0
            }
            self.productImage.setImage(imageUrl: productItem.image_link!)
            self.productName.text = productItem.name
            self.productPrice.text = productItem.price
            self.productOldPrice.text = productItem?.old_price
            self.productOldPrice.strike(text: productItem?.old_price ?? "")
        }

    }

}
