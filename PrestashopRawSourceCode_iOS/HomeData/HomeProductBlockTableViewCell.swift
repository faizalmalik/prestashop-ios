//
//  HomeProductBlockTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class HomeProductBlockTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableCollectionView: UICollectionView!
    var products = [ProductData]()
    @IBOutlet weak var viewAllBtn: UIButton!
    var passDataDelgate: PassData?
    var moveDelegate: MoveController?
    var id_product_block: String!
    var title: String!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tableCollectionView.register(HomeProductCollectionViewCell.nib, forCellWithReuseIdentifier: HomeProductCollectionViewCell.identifier)
        tableCollectionView.delegate = self
        tableCollectionView.dataSource = self
        viewAllBtn.SetAccentBackColor()
        viewAllBtn.setTitle("viewAll".localized, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    @IBAction func viewAllBtnClicked(_ sender: UIButton) {
        passDataDelgate?.passData(id: id_product_block, name: title, dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.ViewAllProductBlock)
    }
}

extension HomeProductBlockTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("products", products.count)
            return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          print("products", products.count)
        return self.configureProductCell(collectionView: collectionView, indexPath: indexPath )

    }

    func configureProductCell (collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeProductCollectionViewCell.identifier, for: indexPath as IndexPath) as? HomeProductCollectionViewCell {
            cell.productItem = products[indexPath.row]
            cell.viewMoreImage.isHidden = true
            return cell
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           let dict = ["products": products]
        moveDelegate?.moveController(id: products[indexPath.row].id_product, name: products[indexPath.row].name, dict: dict, jsonData: JSON.null, index: indexPath.row, controller: AllController.productController)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

            return CGSize(width: SCREEN_WIDTH / 2.5 - 1, height: SCREEN_WIDTH / 2.5 + 52)

    }
}
