//
//  HomeViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

class HomeViewModal: NSObject {
    var items = [HomeViewModalItem]()
    var selectedValue = 0
    var selectionCount = 0
    
    var reloadSections: ((_ section: Int) -> Void)?
    
    var homeTableView: UITableView!
    var moveDelegate: MoveController?
    var passDataDelegate: PassData?
    weak var delegate: SetPermission?
    var homeProHeaderView: HomeProductHeadingView?
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data =  HomeModal(data: jsonData, delegate: self.delegate) else {
            return
        }
        items.removeAll()
        
        if !data.categories.isEmpty {
            let categories = HomeViewModalCategoryItem(categories: data.categories)
            self.items.append(categories)
        }
        
        if !data.banners.isEmpty {
            let banners = HomeViewModalbannerItem(banners: data.banners)
            self.items.append(banners)
        }
        
        if !data.allproductsArray.isEmpty {
            let products = HomeViewModalProductItem(products: data.allproductsArray)
            self.items.append(products)
        }
        
        if !data.productBlocks.isEmpty {
            let products = HomeViewModalProductBlockItem(products: data.productBlocks)
            self.items.append(products)
        }
        
        if !data.bottomBanners.isEmpty {
            let products = HomeViewModalBottombannerItem(banners: data.bottomBanners)
            self.items.append(products)
        }        
        completion(true)
    }
}

extension HomeViewModal: UITableViewDelegate, UITableViewDataSource, SetPermission {
    
    func SetPermissionRequest() {
        delegate?.SetPermissionRequest()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].tableRowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        switch item.type {
        case .banner:
            if let item = item as? HomeViewModalbannerItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.identifier) as? HomeTableViewCell {
                    cell.selectionStyle = .none
                    cell.tableCollectionView.reloadData()
                    cell.tableCollectionView.isPagingEnabled = true
                    cell.collectionType = HomeType.banner
                    cell.banners = item.banners
                    cell.moveDelegate = self
                    return cell
                }
            }
        case .categories:
            if let item = item as? HomeViewModalCategoryItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: HomeCategoryCell.identifier) as? HomeCategoryCell {
                    cell.selectionStyle = .none
                    cell.homeCategoryCollection.tag = indexPath.section
                    cell.categories = item.categories
                    cell.moveDelegate = self
                    if item.categories.count == 0 {
                        cell.collection_height.constant = 0
                        cell.homeCategoryCollection.reloadData()
                    } else {
                        cell.collection_height.constant = 190
                        cell.homeCategoryCollection.reloadData()
                    }
                    cell.backgroundColor = UIColor.clear
                    cell.homeCategoryCollection.backgroundColor = UIColor.white
                    cell.homeCategoryCollection.reloadData()
                    cell.layoutIfNeeded()
                    if let lay = cell.gradientView.layer.sublayers {
                        for layers in lay {
                            layers.removeFromSuperlayer()
                        }
                    }
                    cell.gradientView.layoutIfNeeded()
                    cell.gradientView.applyGradientAccordingToScreenWidth(colours: GlobalConstants.Colors.gradientArray)
                    return cell
                }
            }
        case .products:
            if let item = item as? HomeViewModalProductItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: HomeProductTableViewCell.identifier) as? HomeProductTableViewCell {
                    cell.selectionStyle = .none
                    cell.homeCollectionView.reloadData()
                    cell.homeCollectionView.reloadData()
                    cell.moveDelegate = self
                    cell.link_rewrite = item.products[selectedValue].link_rewrite
                    cell.name = item.products[selectedValue].title
                    cell.passDataDelgate = self
                    cell.selectionStyle = .none
                    cell.products = item.products[selectedValue].productArray
                    let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                    swipeRight.direction = UISwipeGestureRecognizer.Direction.right
                    cell.addGestureRecognizer(swipeRight)
                    
                    let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
                    swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
                    cell.addGestureRecognizer(swipeLeft)
                    cell.moveDelegate = self
                    return cell
                }
            }
        case .productBlocks:
            if let item = item as? HomeViewModalProductBlockItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: HomeProductBlockTableViewCell.identifier) as? HomeProductBlockTableViewCell {
                    cell.selectionStyle = .none
                    cell.tableCollectionView.reloadData()
                    cell.products = item.products[indexPath.row].productArray
                    cell.titleLabel.text = item.products[indexPath.row].title
                    cell.selectionStyle = .none
                    cell.id_product_block = item.products[indexPath.row].id_product_block
                    cell.title = item.products[indexPath.row].title
                    cell.passDataDelgate = self
                    cell.tableCollectionView.showsHorizontalScrollIndicator = false
                    cell.moveDelegate = self
                    return cell
                }
            }
        case .bottomBanners:
            if let item = item as? HomeViewModalBottombannerItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerTableViewCell.identifier) as? HomeBannerTableViewCell {
                    cell.selectionStyle = .none
                    cell.bannerImage.setImage(imageUrl: item.banners[indexPath.row].image_link)
                    return cell
                }
            }
        }
        return UITableViewCell()
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                if  selectedValue > 0 {
                    self.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: selectedValue - 1, call: WhichApiCall.HomeHeaderClicked)
                    print("Swiped left")
                }
            case UISwipeGestureRecognizer.Direction.down:
                print("Swiped down")
            case UISwipeGestureRecognizer.Direction.left:
                print(selectionCount, selectedValue + 1)
                if  selectedValue + 1 <  selectionCount {
                    self.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: selectedValue + 1, call: WhichApiCall.HomeHeaderClicked)
                    print("Swiped left")
                }
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let item = items[section]
        switch item.type {
        case .banner:
            return nil
        case .products:
            selectionCount = items[section].collectionRowCount
            let headerView = (Bundle.main.loadNibNamed("HomeProductHeadingView", owner: self, options: nil)?[0] as? HomeProductHeadingView)!
            if let item = items[section] as? HomeViewModalProductItem {
                headerView.passDataDelegate = self
                headerView.products = item.products
                headerView.selectedValue = selectedValue
                headerView.setViewMainColor()
                homeProHeaderView = headerView
            }
            return headerView
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let item = items[section]
        switch item.type {
        case .products:
            return 50
        default:
            return CGFloat.leastNonzeroMagnitude
        }
    }
//    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if section < items.count-1 {
//            return CGFloat.leastNonzeroMagnitude
//        }
//        return CGFloat(15*items.count-1)
//    }
//    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = items[indexPath.section]
        switch item.type {
        case .banner:
            return SCREEN_WIDTH/2
        case .categories:
            if let item = item as? HomeViewModalCategoryItem, item.categories.count > 0 {
                if CGFloat(item.categories.count * 80) > 2 * SCREEN_WIDTH {
                    return 206
                } else {
                    return 111
                }
            }
            return 0
        case .products:
            if let item = item as? HomeViewModalProductItem {
                if item.products[selectedValue].productArray.count >= 5 {
                    return (SCREEN_WIDTH/2 + 62) * CGFloat(Int((item.products[selectedValue].productArray.count) / 2) + 1)
                } else {
                    if item.products[selectedValue].productArray.count % 2 == 0 {
                        return (SCREEN_WIDTH/2 + 62) * CGFloat(Int((item.products[selectedValue].productArray.count) / 2) )
                    } else {
                        return (SCREEN_WIDTH/2 + 62) * CGFloat(Int((item.products[selectedValue].productArray.count + 1) / 2) )
                    }
                }
            } else {
                return 0
            }
        case .productBlocks:
            return (SCREEN_WIDTH/2.5 + 100)
            
        case .bottomBanners:
            return SCREEN_WIDTH/2
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.section]
        if item.type == HomeType.bottomBanners {
            if let item = item as? HomeViewModalBottombannerItem {
                if item.banners[indexPath.row].page == "category" && item.banners[indexPath.row].id_page != "" {
                    moveDelegate?.moveController(id: item.banners[indexPath.row].id_page, name: "Category", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productCategory)
                } else  if item.banners[indexPath.row].page == "product" && item.banners[indexPath.row].id_page != "" {
                    moveDelegate?.moveController(id: item.banners[indexPath.row].id_page, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productController)
                } else  if item.banners[indexPath.row].page == "custom_url" {
                    moveDelegate?.moveController(id: item.banners[indexPath.row].url, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.browser)
                }
            }
        }
    }
}

extension HomeViewModal: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {

        if call == WhichApiCall.ViewAll || call == WhichApiCall.ViewAllProductBlock {
            passDataDelegate?.passData(id: id, name: name, dict: dict, jsonData: jsonData, index: index, call: call)
        } else {

            guard let sectionValue = items.index(where: {
                $0.type == HomeType.products
            }) else {
                return
            }
            if selectedValue != index {
                selectedValue = index
                reloadSections?(sectionValue)
                let indexToScrollTo = IndexPath(item: selectedValue, section: 0)
                homeProHeaderView?.headingCollectionView.scrollToItem(at: indexToScrollTo, at: .left, animated: true)
            }
        }

    }
}

enum HomeType {
    case banner
    case products
    case categories
    case productBlocks
    case bottomBanners
}

struct HomeViewModalbannerItem: HomeViewModalItem {
    var type: HomeType {
        return .banner
    }

    var tableRowCount: Int {
        return 1
    }

    var collectionRowCount: Int {
        return banners.count
    }

    var banners: [Banners]

    init(banners: [Banners] ) {
        self.banners = banners
    }
}

struct HomeViewModalCategoryItem: HomeViewModalItem {
    var type: HomeType {
        return .categories
    }

    var tableRowCount: Int {
        return 1
    }

    var collectionRowCount: Int {
        return categories.count
    }

    var categories: [AllCategories]

    init(categories: [AllCategories] ) {
        self.categories = categories
    }
}

struct HomeViewModalProductItem: HomeViewModalItem {
    var type: HomeType {
        return .products
    }

    var tableRowCount: Int {
        return 1
    }

    var collectionRowCount: Int {
        return products.count
    }

    var products: [AllProducts]

    init(products: [AllProducts] ) {
        self.products = products
    }
}

struct HomeViewModalProductBlockItem: HomeViewModalItem {
    var type: HomeType {
        return .productBlocks
    }

    var tableRowCount: Int {
        return products.count
    }

    var collectionRowCount: Int {
        return products.count
    }

    var products: [AllProductsBlockData]

    init(products: [AllProductsBlockData] ) {
        self.products = products

    }
}

struct HomeViewModalBottombannerItem: HomeViewModalItem {
    var type: HomeType {
        return .bottomBanners
    }
    
    var tableRowCount: Int {
        return banners.count
    }
    
    var collectionRowCount: Int {
        return 1
    }
    
    var banners: [Banners]
    
    init(banners: [Banners] ) {
        self.banners = banners
    }
}

protocol HomeViewModalItem {
    var type: HomeType { get }
    var tableRowCount: Int { get }
    var collectionRowCount: Int { get }
}

extension HomeViewModal: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        moveDelegate?.moveController(id: id, name: name, dict: dict, jsonData: jsonData, index: index, controller: controller)
    }
}
