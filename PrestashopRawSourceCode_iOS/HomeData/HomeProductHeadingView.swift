//
//  HomeProductHeadingCollectionViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class HomeProductHeadingView: UIView {
    @IBOutlet weak var headingCollectionView: UICollectionView!

    var products = [AllProducts]()
    var selectedValue = 0

    var passDataDelegate: PassData?
  override func awakeFromNib() {
        super.awakeFromNib()

        headingCollectionView.register(HomeProductHeadingCollectionViewCell.nib, forCellWithReuseIdentifier: HomeProductHeadingCollectionViewCell.identifier)
            headingCollectionView.delegate = self
            headingCollectionView.dataSource = self

    }
}

extension HomeProductHeadingView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return products.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return  self.configureBannerCell(collectionView: collectionView, indexPath: indexPath)
    }

    func configureBannerCell (collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeProductHeadingCollectionViewCell.identifier, for: indexPath as IndexPath) as? HomeProductHeadingCollectionViewCell {
            cell.headingLabel.text = String.init(format: "  %@  ", products[indexPath.row].title)
            if indexPath.row == selectedValue {
                cell.colorView.backgroundColor = GlobalConstants.Colors.accentColor
                cell.headingLabel.font = UIFont.boldSystemFont(ofSize: 17)
                
            } else {
                cell.colorView.backgroundColor = UIColor.clear
                cell.headingLabel.font = UIFont.systemFont(ofSize: 17)
            }
            cell.backgroundColor = UIColor.white
            cell.headingLabel.textColor = GlobalConstants.Colors.accentColor
            return cell
        }
        return UICollectionViewCell()
    }
    
//    override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
//        return cell.contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize)
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = SCREEN_WIDTH/2 - 36
        if width < (String.init(format: "  %@  ", products[indexPath.row].title)).widthOfString(usingFont: UIFont.boldSystemFont(ofSize: 17)) {
            width = (String.init(format: "  %@  ", products[indexPath.row].title)).widthOfString(usingFont: UIFont.boldSystemFont(ofSize: 17)) + 20
        }
        return CGSize(width: width, height: 60)
        //return CGSize(width:SCREEN_WIDTH/2 - 36, height: 60)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        headingCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        passDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: indexPath.row, call: WhichApiCall.HomeHeaderClicked)
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

//        if let coll  = headingCollectionView {
//            let indexToScrollTo = IndexPath(item: selectedValue, section: 0)
//            coll.scrollToItem(at: indexToScrollTo, at: .left, animated: true)
//        }
    }
}
