//
//  HomeCategoryCell.swift
//  Odoo application
//
//  Created by vipin sahu on 8/31/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
import SwiftyJSON

class HomeCategoryCell: UITableViewCell {
    
    @IBOutlet weak var homeCategoryCollection: UICollectionView!
    @IBOutlet weak var collection_height: NSLayoutConstraint!
    @IBOutlet weak var gradientView: UIView!
    
    var viewModel  = HomeViewModal()
    var categories = [AllCategories]()
    weak var moveDelegate: MoveController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        homeCategoryCollection.register(HomeCategoryCollectionViewCell.nib, forCellWithReuseIdentifier: HomeCategoryCollectionViewCell.identifier)
        homeCategoryCollection.setNeedsLayout()
        homeCategoryCollection.sizeToFit()
        homeCategoryCollection.applyBorder(colours: GlobalConstants.Colors.setBorderColor)
        selectionStyle = .none
        homeCategoryCollection.delegate = self
        homeCategoryCollection.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

extension HomeCategoryCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCategoryCollectionViewCell", for: indexPath as IndexPath) as? HomeCategoryCollectionViewCell {
            cell.backgroundColor = UIColor.clear
            cell.item = categories[indexPath.row]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        moveDelegate?.moveController(id: categories[indexPath.row].id_category, name: categories[indexPath.row].name, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productCategory)
    }
}
