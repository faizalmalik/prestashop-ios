//
//  HomeCategoryCollectionViewCell.swift
//  Odoo application
//
//  Created by vipin sahu on 8/31/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class HomeCategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        categoryImage.clipsToBounds = true
        categoryImage.layer.cornerRadius = 25
        categoryImage.layer.borderWidth = 1
        categoryImage.layer.borderColor = GlobalConstants.Colors.primaryColor.cgColor
    }

    var item: AllCategories? {
        didSet {

            guard let item = item else {
                return
            }
            categoryLabel.text = item.name
            categoryImage.setImage(imageUrl: item.img_icon ?? "")
        }
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
