//
//  HomeProductTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON

class HomeProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var homeCollectionView: UICollectionView!
    
    var link_rewrite: String!
    var name: String!
    
    var passDataDelgate: PassData?
    
    var products = [ProductData]()
    var moveDelegate: MoveController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        homeCollectionView.register(BannerCollectionCell.nib, forCellWithReuseIdentifier: BannerCollectionCell.identifier)
        homeCollectionView.register(HomeCategoryCollectionViewCell.nib, forCellWithReuseIdentifier: HomeCategoryCollectionViewCell.identifier)
        homeCollectionView.register(HomeProductCollectionViewCell.nib, forCellWithReuseIdentifier: HomeProductCollectionViewCell.identifier)
        homeCollectionView.delegate = self
        homeCollectionView.dataSource = self
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}

extension HomeProductTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if products.count >= 5 {
            return products.count + 1
        } else {
            return products.count
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return self.configureProductCell(collectionView: collectionView, indexPath: indexPath )
        
    }
    
    func configureProductCell (collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeProductCollectionViewCell.identifier, for: indexPath as IndexPath) as? HomeProductCollectionViewCell {
            cell.layoutSubviews()
            if indexPath.row < products.count {
                cell.productPrice.isHidden = false
                cell.productImage.image = nil
                cell.productItem = products[indexPath.row]
                cell.productImage.layer.cornerRadius = 0
                cell.productImage.applyBorder(colours: UIColor.clear)
                cell.viewMoreImage.isHidden  = true
            } else {
                cell.viewMoreImage.isHidden  = false
                cell.productImage.image = nil
                cell.viewMoreImage.image = #imageLiteral(resourceName: "view-more")
                cell.productPrice.isHidden = true
                cell.productName.text = "viewAll".localized
                cell.oldPriceHeight.constant = 0
                cell.viewMoreImage.applyBorder(colours: GlobalConstants.Colors.accentColor)
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < products.count {
            let dict = ["products": products]
            moveDelegate?.moveController(id: products[indexPath.row].id_product, name: products[indexPath.row].name, dict: dict, jsonData: JSON.null, index: indexPath.row, controller: AllController.productController)
        } else {
            passDataDelgate?.passData(id: link_rewrite, name: name, dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.ViewAll)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SCREEN_WIDTH / 2 - 4, height: SCREEN_WIDTH / 2 + 52)        
    }
}
