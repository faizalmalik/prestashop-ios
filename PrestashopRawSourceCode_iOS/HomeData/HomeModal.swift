//
//  HomeModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

struct HomeModal {
    
    var allproductsArray = [AllProducts]()
    var productBlocks = [AllProductsBlockData]()
    var banners = [Banners]()
    var bottomBanners = [Banners]()
    var categories = [AllCategories]()
    private let languagePath = ["languages", "language"]
    private let currencyPath = ["currencies", "currency"]
    var theme_color: String!
    //weak var delegate: SetPermission?
    
    init?(data: JSON, delegate: SetPermission?) {
        
        GlobalConstants.StoreData.cmsArray.removeAll()
        if data["is_customer_active"].stringValue == "0" {
            for key in UserDefaults.standard.dictionaryRepresentation().keys {
                if key == UserDefaults.Keys.languageCode || key == UserDefaults.Keys.currencyCode || key == UserDefaults.Keys.profile_picture_upload_url || key == AppLanguageKey || key == UserDefaults.Keys.loginDict || key == UserDefaults.Keys.firebaseKey {
                } else {
                    UserDefaults.standard.removeObject(forKey: key.description)
                }
            }
        }
        
        if data["cms_tabs"]["tab"] != JSON.null {
            if let data  = data["cms_tabs"]["tab"].array {
                let array = data.map { $0.stringValue }
                if array.contains("About Us") {
                    GlobalConstants.StoreData.cmsArray.append(CmsControllers.aboutUs.rawValue)
                }
                if array.contains("Terms and Conditions") {
                    GlobalConstants.StoreData.cmsArray.append(CmsControllers.termsCondition.rawValue)
                }
                if array.contains("Customer Service") {
                    GlobalConstants.StoreData.cmsArray.append(CmsControllers.support.rawValue)
                }
                if array.contains("Privacy Policy") {
                    GlobalConstants.StoreData.cmsArray.append(CmsControllers.privacyPolicy.rawValue)
                }
            } else {
                let data  = data["cms_tabs"]["tab"].stringValue
                if data.contains("About Us") {
                    GlobalConstants.StoreData.cmsArray.append(CmsControllers.aboutUs.rawValue)
                }
                if data.contains("Terms and Conditions") {
                    GlobalConstants.StoreData.cmsArray.append(CmsControllers.termsCondition.rawValue)
                }
                if data.contains("Customer Service") {
                    GlobalConstants.StoreData.cmsArray.append(CmsControllers.support.rawValue)
                }
                if data.contains("Privacy Policy") {
                    GlobalConstants.StoreData.cmsArray.append(CmsControllers.privacyPolicy.rawValue)
                }
            }
        }
        
        if let theme =  data["theme_color"].string {
            switch theme {
            case  "deep_purple_and_pink":
                if GlobalConstants.Colors.primaryColor != UIColor().HexToColor(hexString: "#673AB7") {
                    GlobalConstants.Colors.colorChange = true
                }
                GlobalConstants.Colors.primaryColor = UIColor().HexToColor(hexString: "#673AB7")
                GlobalConstants.Colors.accentColor = UIColor().HexToColor(hexString: "#FF4081")
            case  "blue_and_orange":
                if GlobalConstants.Colors.primaryColor != UIColor().HexToColor(hexString: "#3F51B5") {
                    GlobalConstants.Colors.colorChange = true
                }
                GlobalConstants.Colors.primaryColor = UIColor().HexToColor(hexString: "#3F51B5")
                GlobalConstants.Colors.accentColor = UIColor().HexToColor(hexString: "#FF9800")
            case  "teal_green_and_red":
                if GlobalConstants.Colors.primaryColor != UIColor().HexToColor(hexString: "#009688") {
                    GlobalConstants.Colors.colorChange = true
                }
                GlobalConstants.Colors.primaryColor = UIColor().HexToColor(hexString: "#009688")
                GlobalConstants.Colors.accentColor = UIColor().HexToColor(hexString: "#FF5252")
            case  "light_blue_and_red":
                if GlobalConstants.Colors.primaryColor != UIColor().HexToColor(hexString: "#03A9F4") {
                    GlobalConstants.Colors.colorChange = true
                }
                GlobalConstants.Colors.primaryColor = UIColor().HexToColor(hexString: "#03A9F4")
                GlobalConstants.Colors.accentColor = UIColor().HexToColor(hexString: "#FF5252")
            case  "orange_and_teal_green":
                if GlobalConstants.Colors.primaryColor != UIColor().HexToColor(hexString: "#FF5722") {
                    GlobalConstants.Colors.colorChange = true
                }
                GlobalConstants.Colors.primaryColor = UIColor().HexToColor(hexString: "#FF5722")
                GlobalConstants.Colors.accentColor = UIColor().HexToColor(hexString: "#009688")
            default:
                if GlobalConstants.Colors.primaryColor != GlobalConstants.Colors.DefaultprimaryColor {
                    GlobalConstants.Colors.colorChange = true
                }
                GlobalConstants.Colors.primaryColor =  GlobalConstants.Colors.DefaultprimaryColor
                GlobalConstants.Colors.accentColor =  GlobalConstants.Colors.DefaultaccentColor
            }
        } else {
            if GlobalConstants.Colors.primaryColor != GlobalConstants.Colors.DefaultprimaryColor {
                GlobalConstants.Colors.colorChange = true
            }
            GlobalConstants.Colors.primaryColor =  GlobalConstants.Colors.DefaultprimaryColor
            GlobalConstants.Colors.accentColor =  GlobalConstants.Colors.DefaultaccentColor
        }
        
        if GlobalConstants.Colors.colorChange {
            GlobalConstants.Colors.gradientArray = [GlobalConstants.Colors.primaryColor, GlobalConstants.Colors.accentColor]
            UITabBar.appearance().barTintColor = UIColor.black
            UITabBar.appearance().tintColor = GlobalConstants.Colors.accentColor
            if #available(iOS 10.0, *) {
                UITabBar.appearance().unselectedItemTintColor = UIColor.white
            } else {
                // Fallback on earlier versions
            }
            UINavigationBar.appearance().tintColor = UIColor.white
            UINavigationBar.appearance().barTintColor = UIColor.white
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            UINavigationBar.appearance().applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        }
        
        if data["guest_checkout"] != JSON.null {
            if data["guest_checkout"].stringValue == "1" {
                AddonsEnabled.guestEnable = true
            } else {
                AddonsEnabled.guestEnable = false
            }
        }
        
        if data["display_dob_field"] != JSON.null {
            if data["display_dob_field"].stringValue == "1" {
                AddonsEnabled.display_dob_field = true
            } else {
                AddonsEnabled.display_dob_field = false
            }
        }
        
        if data["enable_shop"] != JSON.null {
            if data["enable_shop"].stringValue == "1" {
                AddonsEnabled.enable_optin = true
            } else {
                AddonsEnabled.enable_optin = false
            }
        }
        
        if data["banners"]["banner"] != JSON.null {
            if let data  = data["banners"]["banner"].array {
                self.bottomBanners = data.map { Banners( data: $0) }
            } else {
                self.bottomBanners.append(Banners(data: data["banners"]["banner"]))
            }
        }
        
        if data["mod_product_sliders"]["mod_product_slider"] != JSON.null {
            if let data  = data["mod_product_sliders"]["mod_product_slider"].array {
                self.allproductsArray = data.map { AllProducts( data: $0) }
            } else {
                self.allproductsArray.append(AllProducts(data: data["mod_product_sliders"]["mod_product_slider"]))
            }
        }
        
        if data["product_block"]["block"] != JSON.null {
            if let data  = data["product_block"]["block"].array {
                self.productBlocks = data.map { AllProductsBlockData( data: $0) }
            } else {
                self.productBlocks.append(AllProductsBlockData(data: data["product_block"]["block"]))
            }
        }
        
        if data["slider"]["slide"] != JSON.null {
            if let data  = data["slider"]["slide"].array {
                self.banners = data.map { Banners( data: $0) }
            } else {
                self.banners.append( Banners(data: data["slider"]["slide"]))
            }
        }
        
        if data["menu_category"]["root_category"] != JSON.null {
            if let data  = data["menu_category"]["root_category"].array {
                self.categories = data.map { AllCategories( data: $0) }
            } else {
                self.categories.append( AllCategories(data: data["menu_category"]["root_category"]))
            }
        }
        
        if data[languagePath] != JSON.null {
            GlobalConstants.StoreData.languageArray.removeAll()
            if let data  = data[languagePath].array {
                GlobalConstants.StoreData.languageArray = data.map { Languages( data: $0) }
            } else {
                GlobalConstants.StoreData.languageArray.append(Languages(data: data[languagePath]))
            }
        }
        
        if data[currencyPath] != JSON.null {
            GlobalConstants.StoreData.currencyArray.removeAll()
            if let data  = data[currencyPath].array {
                GlobalConstants.StoreData.currencyArray = data.map { Currencies( data: $0) }
            } else {
                GlobalConstants.StoreData.currencyArray.append(Currencies(data: data[currencyPath]))
            }
        }
        
        if let currencyCode  = data["id_currency_default"].string {
            if UserDefaults.standard.value(forKey: UserDefaults.Keys.currencyCode) == nil {
                UserDefaults.set(value: currencyCode, key: UserDefaults.Keys.currencyCode)
            }
        }
        
        if let languageCode = data["id_lang_default"].string, let iSOCode = data["iso_lang_default"].string {
            if UserDefaults.standard.value(forKey: UserDefaults.Keys.languageCode) == nil {
                GlobalConstants.Colors.colorChange = false
                UserDefaults.set(value: languageCode, key: UserDefaults.Keys.languageCode)
                UserDefaults.set(value: iSOCode, key: AppLanguageKey)
                if iSOCode == "ar" {
                    L102Language.setAppleLAnguageTo(lang: "ar")
                    UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
                    UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
                    UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                    let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                    rootviewcontroller.rootViewController = UIStoryboard.loadRootViewController()
                } else if iSOCode != "en" {
                    L102Language.setAppleLAnguageTo(lang: "en")
                    UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
                    UITabBar.appearance().semanticContentAttribute =  .forceLeftToRight
                    UITabBar.appearance().semanticContentAttribute = .forceLeftToRight
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                    let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                    rootviewcontroller.rootViewController = UIStoryboard.loadRootViewController()
                } else {
                    delegate?.SetPermissionRequest()
                    L102Language.setAppleLAnguageTo(lang: "en")
                    UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
                    UITabBar.appearance().semanticContentAttribute =  .forceLeftToRight
                    UITabBar.appearance().semanticContentAttribute = .forceLeftToRight
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                    if GlobalConstants.Colors.colorChange {
                        GlobalConstants.Colors.colorChange = false
                        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                        rootviewcontroller.rootViewController = UIStoryboard.loadRootViewController()
                    }
                }
            } else if GlobalConstants.Colors.colorChange {
                GlobalConstants.Colors.colorChange = false
                let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
                rootviewcontroller.rootViewController = UIStoryboard.loadRootViewController()
            }
        } else if GlobalConstants.Colors.colorChange {
            GlobalConstants.Colors.colorChange = false
            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
            rootviewcontroller.rootViewController = UIStoryboard.loadRootViewController()
        }
        UserDefaults.set(value: data["profile_picture_upload_url"].stringValue, key: UserDefaults.Keys.profile_picture_upload_url)
    }
}

struct AllProducts {
    var title: String!
    var link_rewrite: String!
    var productArray = [ProductData]()
    init(data: JSON) {
        title = data["title"].stringValue
        if data["mod_product_list"]["mod_product"] != JSON.null {
            if let data  = data["mod_product_list"]["mod_product"] .array {
                self.productArray = data.map { ProductData( data: $0) }
            } else {
                self.productArray.append(ProductData(data: data["mod_product_list"]["mod_product"] ))
            }
        }
        link_rewrite = data["link_rewrite"].stringValue
    }
}

struct AllProductsBlockData {
    var title: String!
     var id_product_block: String!
    var productArray = [ProductData]()
    init(data: JSON) {
        title = data["title"].stringValue
        id_product_block = data["id_product_block"].stringValue
        if data["products"]["product"] != JSON.null {
            if let data  = data["products"]["product"] .array {
                self.productArray = data.map { ProductData( data: $0) }
            } else {
                self.productArray.append(ProductData(data: data["products"]["product"] ))
            }
        }
    }
}

struct ProductData {
    var id_product: String!
    var quantity: String!
    var price: String!
    var old_price: String!
    var id_product_attribute: String!
    var name: String!
    var image_link: String!
    var display_addtocart: String!
    var total: String!
    var attributes: String!
    var priority: String!
    var added_in_wishlist: String!
    var show_price: Bool!
    var pack_quantity: String!
    var customizationData = [CartCustomizationData]()
    //virtual prododuct
    var is_download_item: Bool!
    var id_order: String!
    var display_filename: String!
    init(data: JSON) {
        id_product = data["id_product"].string ?? data["product_id"].string ?? data["id_ps_product"].string ?? data["id"].stringValue
        quantity = data["quantity"].string ?? data["cart_quantity"].string  ?? data["product_quantity"].stringValue
        price = data["price"].string  ?? data["unit_price_tax_incl"].string ?? data["total_price_tax_incl"].stringValue
        old_price = data["old_price"].stringValue
        id_product_attribute = data["id_product_attribute"].stringValue
        name = data["name"].string ?? data["product_name"].stringValue
        image_link = data["image_link"].stringValue
        display_addtocart = data["display_addtocart"].stringValue
        total = data ["total"].string  ?? data["total_price_tax_incl"].stringValue
        attributes = data["attributes"].string ?? data["attributes_small"].stringValue
        priority = data["priority"].stringValue
        added_in_wishlist = data["added_in_wishlist"].stringValue
        if data["show_price"].stringValue == "1" {
            show_price = true
        } else {
            show_price = false
        }

        if data["customizations"]["customization"] != JSON.null {
            if let data  = data["customizations"]["customization"] .array {
                self.customizationData = data.map { CartCustomizationData( data: $0) }
            } else {
                self.customizationData.append(CartCustomizationData(data: data["customizations"]["customization"] ))
            }
            
        }
        pack_quantity = data["pack_quantity"].stringValue
        if !pack_quantity.isEmpty {
            price += " x " + pack_quantity
        }
        is_download_item = data["is_download_item"].stringValue == "1" ? true:false
        id_order = data["id_order"].stringValue
        display_filename = data["display_filename"].stringValue
    }
}

struct Banners {
    var page: String!
    var id_page: String!
    var id_product_attribute: String!
    var url: String!
    var image_link: String!

     init(data: JSON) {
         page = data["page"].string ?? data["category_name"].stringValue
         id_page = data["id_page"].stringValue
         id_product_attribute = data["id_product_attribute"].stringValue
         url = data["url"].stringValue
         image_link = data["image_link"].stringValue
    }
}

struct Currencies {
    var id_currency: String!
    var name: String!
    var iso_code: String!
    var sign: String!

    init(data: JSON) {
        id_currency = data["id_currency"].stringValue
        name = data["name"].stringValue
        iso_code = data["iso_code"].stringValue
        sign = data["sign"].stringValue
    }
}

struct Languages {
    var id_lang: String!
    var name: String!
    var iso_code: String!
    var icon: String!

    init(data: JSON) {
        id_lang = data["id_lang"].stringValue
        name = data["name"].stringValue
        iso_code = data["iso_code"].stringValue
        icon = data["icon"].stringValue
    }
}

struct AllCategories {
    var id_category: String!
    var name: String!
    var img_icon: String!
    var subCategoryArray = [SubCategories]()
    init(data: JSON) {
        id_category = data["id_category"].stringValue
        name = data["name"].stringValue
        img_icon = data["img_icon"].stringValue

        if data["children"] != JSON.null {
            if let data  = data["children"].array {
                self.subCategoryArray = data.map { SubCategories( data: $0) }
            } else {
                self.subCategoryArray.append(SubCategories(data: data["children"]))
            }
        }
    }
}

struct SubCategories {
    var id_category: String!
    var name: String!

    init(data: JSON) {
        id_category = data["id_category"].stringValue
        name = data["name"].stringValue
    }
}

enum ThemeColors: String {
    case deep_purple_and_pink = "deep_purple_and_pink"
    case blue_and_orange = "blue_and_orange"
    case teal_green_and_red = "teal_green_and_red"
    case light_blue_and_red = "light_blue_and_red"
    case orange_and_teal_green = "orange_and_teal_green"
}

protocol SetPermission: class {
    func SetPermissionRequest()
}
