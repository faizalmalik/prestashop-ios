

#import <Foundation/Foundation.h>



@interface XMLWriter : NSObject  {
    
}



- (NSString*)convertDictionaryToXML:(NSDictionary*)dictionary withStartElement:(NSString*)startElement isFirstElement:(BOOL) isFirstElement;


@end
