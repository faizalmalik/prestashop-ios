//
//  LanguageDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class LanguageDataViewController: UIViewController {

    @IBOutlet weak var languageTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = "languages".localized
        languageTableView.dataSource = self
        languageTableView.delegate = self
        languageTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LanguageDataViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GlobalConstants.StoreData.languageArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text  = GlobalConstants.StoreData.languageArray[indexPath.row].name
        if  UserDefaults.fetch(key: UserDefaults.Keys.languageCode) == GlobalConstants.StoreData.languageArray[indexPath.row].id_lang {
            cell.accessoryType = .checkmark
        }
        cell.imageView?.setImage(imageUrl: GlobalConstants.StoreData.languageArray[indexPath.row].icon)
        let itemSize = CGSize(width: 16, height: 11)
        UIGraphicsBeginImageContextWithOptions(itemSize, false, UIScreen.main.scale)
        let imageRect = CGRect(x: 0.0, y: 0.0, width: itemSize.width, height: itemSize.height)
        cell.imageView?.image!.draw(in: imageRect)
        cell.imageView?.image! = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
             cell.textLabel?.textAlignment = .right
        } else {
             cell.textLabel?.textAlignment = .left
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           UserDefaults.set(value: GlobalConstants.StoreData.languageArray[indexPath.row].id_lang!, key: UserDefaults.Keys.languageCode)
          UserDefaults.set(value: GlobalConstants.StoreData.languageArray[indexPath.row].iso_code!, key: AppLanguageKey)

        let languageCode = GlobalConstants.StoreData.languageArray[indexPath.row].iso_code!
        if languageCode == "ar" {
            L102Language.setAppleLAnguageTo(lang: "ar")
            if #available(iOS 9.0, *) {
                UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
                UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
                self.tabBarController?.tabBar.semanticContentAttribute = .forceRightToLeft
                UIView.appearance().semanticContentAttribute = .forceRightToLeft

            } else {
                // Fallback on earlier versions
            }

        } else {

            L102Language.setAppleLAnguageTo(lang: "en")
            if #available(iOS 9.0, *) {
                UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
                UITabBar.appearance().semanticContentAttribute =  .forceLeftToRight
                 self.tabBarController?.tabBar.semanticContentAttribute = .forceLeftToRight
                UIView.appearance().semanticContentAttribute = .forceLeftToRight

            } else {
                // Fallback on earlier versions
            }

        }

        //self.performSegue(withIdentifier: "reload", sender: self)
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = UIStoryboard.loadRootViewController()
    }
}
