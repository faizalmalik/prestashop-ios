//
//  LaunchImageViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON
import RealmSwift

class LaunchImageViewController: UIViewController {

    @IBOutlet weak var appNAme: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var mainView: UIView!
    var delegate: PassJsonData?
    @IBOutlet weak var launchview: UIImageView!
    var emptyView: EmptyView?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        if GlobalConstants.AppCredentials.environment == EnvironmentType.marketplace {
            if Device.IS_IPHONE_4_OR_LESS {
                launchview.image = UIImage(named: "LaunchImageMp-700@2x.png")
            } else if Device.IS_IPHONE_5 {
                launchview.image = UIImage(named: "LaunchImageMp-700-568h@2x.png")
            } else if Device.IS_IPHONE_6 {
                launchview.image = UIImage(named: "LaunchImageMp-800-667h@2x.png")
            } else if Device.IS_IPHONE_6P {
                launchview.image = UIImage(named: "LaunchImageMp-800-Portrait-736h@3x.png")
            } else if Device.IS_IPHONE_XSERIES {
                launchview.image = UIImage(named: "LaunchImageMp-1100-Portrait-2436h@3x.png")
            } else if Device.IS_IPAD {
                if (UIScreen.main.scale == 1) {
                    launchview.image = UIImage(named: "LaunchImageMp-700-Portrait~ipad.png")
                } else if (UIScreen.main.scale == 2) {
                    launchview.image = UIImage(named: "LaunchImageMp-700-Portrait@2x~ipad.png")
                }
            }
        } else {
            if Device.IS_IPHONE_4_OR_LESS {
                launchview.image = UIImage(named: "LaunchImage-700@2x.png")
            } else if Device.IS_IPHONE_5 {
                launchview.image = UIImage(named: "LaunchImage-700-568h@2x.png")
            } else if Device.IS_IPHONE_6 {
                launchview.image = UIImage(named: "LaunchImage-800-667h@2x.png")
            } else if Device.IS_IPHONE_6P {
                launchview.image = UIImage(named: "LaunchImage-800-Portrait-736h@3x.png")
            } else if Device.IS_IPHONE_XSERIES {
                launchview.image = UIImage(named: "LaunchImage-1100-Portrait-2436h@3x.png")
            } else if Device.IS_IPAD {
                if (UIScreen.main.scale == 1) {
                    launchview.image = UIImage(named: "LaunchImage-700-Portrait~ipad.png")
                } else if (UIScreen.main.scale == 2) {
                    launchview.image = UIImage(named: "LaunchImage-700-Portrait@2x~ipad.png")
                }
            }
            launchview.image = Device.FetchLaunchImage()
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.view.backgroundColor = GlobalConstants.Colors.primaryColor
        // Do any additional setup after loading the view.
        self.makeRequest()
    }
    
//    override func viewWillLayoutSubviews() {
//        if GlobalConstants.AppCredentials.environment == EnvironmentType.marketplace {
//            appNAme.text = "appNameMp".localized
//            appNAme.alpha = 0.0
//
//            // fade in
//            UIView.animate(withDuration: 1.0, animations: {
//                self.appNAme.alpha = 1.0
//            }) { (finished) in
//                // fade out
//                UIView.animate(withDuration: 1.5, animations: {
//                    self.appNAme.alpha = 0.0
//                })
//
//            }
//
////            UIView.animate(withDuration: 1.0, delay: 0.0, options: [], animations: {() -> Void in
////                self.appNAme.frame.origin.x = -SCREEN_WIDTH
////            }, completion: {(_ finished: Bool) -> Void in
////                self.appNAme.frame.origin.x = 0
////            })
////
//        }
//        else {
//             appNAme.text = "appName".localized
//            appNAme.alpha = 0.0
//
//            // fade in
//            UIView.animate(withDuration: 1.0, animations: {
//                self.appNAme.alpha = 1.0
//            }) { (finished) in
//                // fade out
//                UIView.animate(withDuration: 1.5, animations: {
//                    self.appNAme.alpha = 0.0
//                })
//
//
//            }
//
////            UIView.animate(withDuration: 1.0, delay: 0.0, options: [], animations: {() -> Void in
////                self.appNAme.frame.origin.x = -SCREEN_WIDTH
////            }, completion: {(_ finished: Bool) -> Void in
////                self.appNAme.frame.origin.x = 0
////            })
////
//        }
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeRequest() {
        var params = GlobalConstants.ApiNames.homepageApi
        params += UrlParameters.width
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.launchController, view: self.view ) {
            
            [weak self] (responseObject: JSON?, error: Error?) in
            if let error = error {
                if let jsonData =  DBManager.sharedInstance.fetchData(key: AllController.launchController.rawValue) {
                    CategoryArray = jsonData["menu_category"]["root_category"]
                    self?.delegate?.passJsonData(data: jsonData)
                    self?.dismisWithAnimation()
                } else {
                    self?.setEmptyData(code: error._code)
                }
                print("loginError".localized, error)
                
            } else {
                self?.tabBarController?.tabBar.isHidden = false
                for  subView in  (self?.view.subviews)! {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
               
                if let data =  responseObject {
                     CategoryArray = data["menu_category"]["root_category"]
                    let item = Item()
                    item.ID = AllController.launchController.rawValue
                    item.textString = data.rawString()!
                    DBManager.sharedInstance.addData(object: item)
                    self?.delegate?.passJsonData(data: data)
                    self?.dismisWithAnimation()
                    
                }
            }
        }
    }
    
    func dismisWithAnimation() {
         self.dismiss(animated: false, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LaunchImageViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        self.tabBarController?.tabBar.isHidden = true
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
    }
    
    func buttonPressed() {
        self.makeRequest()
    }
}

//NSString *launchImageName;
//if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//{
//    if ([UIScreen mainScreen].bounds.size.height == 480) launchImageName = @"LaunchImage-700@2x.png"; // iPhone 4/4s, 3.5 inch screen
//    if ([UIScreen mainScreen].bounds.size.height == 568) launchImageName = @"LaunchImage-700-568h@2x.png"; // iPhone 5/5s, 4.0 inch screen
//    if ([UIScreen mainScreen].bounds.size.height == 667) launchImageName = @"LaunchImage-800-667h@2x.png"; // iPhone 6, 4.7 inch screen
//    if ([UIScreen mainScreen].bounds.size.height == 736) launchImageName = @"LaunchImage-800-Portrait-736h@3x.png"; // iPhone 6+, 5.5 inch screen
//}
//else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//{
//    if ([UIScreen mainScreen].scale == 1) launchImageName = @"LaunchImage-700-Portrait~ipad.png"; // iPad 2
//    if ([UIScreen mainScreen].scale == 2) launchImageName = @"LaunchImage-700-Portrait@2x~ipad.png"; // Retina iPads
//}
//self.launchImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:launchImageName]];

//var allPngImageNames = Bundle.main.paths(forResourcesOfType: "png", inDirectory: nil)
//for imgName: String in allPngImageNames {
//    // Find launch images
//    if imgName.contains("LaunchImage") {
//        print(imgName)
//        var img = UIImage(named: imgName)
//        // Has image same scale and dimensions as our current device's screen?
//        if img?.scale == UIScreen.main.scale && (img?.size.equalTo(UIScreen.main.bounds.size))! {
//            print("Found launch image for current device \(img?.description ?? "")")
//            break
//        }
//    }
//}
