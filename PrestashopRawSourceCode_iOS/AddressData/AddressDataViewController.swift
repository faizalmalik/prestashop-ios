//
//  AddressDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import IQKeyboardManagerSwift

class AddressDataViewController: UIViewController {
    
    @IBOutlet weak var newAddressBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var addAddressBtn: UIButton!
    @IBOutlet weak var addressTableView: UITableView!
    fileprivate let AddressViewModalObject = AddressViewModal()
    var emptyView: EmptyView?
    var controller: AllController!
    var changeAddressDelegate: ChangeAddress?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        addressTableView.delegate = AddressViewModalObject
        addressTableView.dataSource = AddressViewModalObject
        AddressViewModalObject.moveControllerDelegate = self
        AddressViewModalObject.passDataDelegate = self
        AddressViewModalObject.controller = controller
        self.navigationItem.title = "myAddress".localized
        addAddressBtn.setTitle("addNewAddress".localized, for: .normal)
        addressTableView.register(AddressDataTableViewCell.nib, forCellReuseIdentifier: AddressDataTableViewCell.identifier)
        addressTableView.tableFooterView = UIView()
        self.makeRequest(call: WhichApiCall.none, id: "")
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = "addressEmpty".localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-address")
            emptyView?.button.isHidden = false
            emptyView?.isHidden = true
            emptyView?.delegate = self
            emptyView?.button.setTitle("addNewAddress".localized, for: .normal)
            self.view.addSubview(emptyView!)
        }
    }
    
    @IBAction func AddAddressClicked(_ sender: Any) {
        if let view = UIStoryboard.loadAddAddressViewController() {
            view.delegate = self
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
    
    func makeRequest(call: WhichApiCall, id: String) {
        var params = ""
        switch call {
        case .deleteaddress:
            params = GlobalConstants.ApiNames.deleteaddress
            params += "&id_address=" + id
            if let carID =  UrlParameters.fetchCartID() {
                params += carID
            }
        default:
            params =  GlobalConstants.ApiNames.getAddress
        }
        if let custId =  UrlParameters.fetchCustomerId() {
            params += custId
        }
        params += UrlParameters.width
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                
                print("loginError".localized, error!)
                
            } else {
                
                switch call {
                case .deleteaddress:
                    if ErrorChecking.getData(data: responseObject) != JSON.null && ErrorChecking.getData(data: responseObject) != nil {
                        self.makeRequest(call: WhichApiCall.none, id: "")
                    }
                default:
                    self.AddressViewModalObject.getValue(jsonData: responseObject!) { (data: Bool) in
                        if data {
                            
                            self.addressTableView.reloadData()
                            self.emptyView?.isHidden = true
                        } else {
                            self.emptyView?.isHidden = false
                            
                        }
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AddressDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        self.makeRequest(call: WhichApiCall.deleteaddress, id: id)
    }
}

extension AddressDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        print(id)
        switch controller {
        case .changeDeliveryAddress:
            changeAddressDelegate?.addressChanged(addressId: id, controller: AllController.changeDeliveryAddress)
            self.navigationController?.popViewController(animated: true)
        case .changeInvoiceAddress:
            changeAddressDelegate?.addressChanged(addressId: id, controller: AllController.changeInvoiceAddress)
            self.navigationController?.popViewController(animated: true)
        default:
            if let view = UIStoryboard.loadAddAddressViewController() {
                view.addressId = id
                view.delegate = self
                self.navigationController?.pushViewController(view, animated: true)
            }
        }
        
    }
}
extension AddressDataViewController: AddressUpdate {
    func addressupdated(addressId: String) {
        self.makeRequest(call: WhichApiCall.none, id: "")
    }
}

extension AddressDataViewController: EmptyDelegate {
    func buttonPressed() {
        if let view = UIStoryboard.loadAddAddressViewController() {
            view.delegate = self
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
}
