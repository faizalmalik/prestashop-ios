//
//  AddressViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class AddressViewModal: NSObject {

    var moveControllerDelegate: MoveController?
    var passDataDelegate: PassData?
    var addresses = [AddressData]()
    var controller: AllController!
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = AddressModal(data: jsonData) else {
            return
        }
        addresses.removeAll()
        if !data.addresses.isEmpty {
            addresses = data.addresses
            completion(true)
        } else {
            completion(false)
        }

    }
}

extension AddressViewModal: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: AddressDataTableViewCell.identifier) as? AddressDataTableViewCell {
            cell.item = addresses[indexPath.row]
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresses.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Delete", handler: { (_, indexPath) in
            self.passDataDelegate?.passData(id: self.addresses[indexPath.row].addressId, name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.deleteaddress)
        })
        deleteAction.backgroundColor = UIColor().HexToColor(hexString: "#FF5252")
        return [deleteAction]        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if controller != nil {
            moveControllerDelegate?.moveController(id: addresses[indexPath.row].addressId, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: controller)
        } else {
             moveControllerDelegate?.moveController(id: addresses[indexPath.row].addressId, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.addAddress)
        }
    }

}
