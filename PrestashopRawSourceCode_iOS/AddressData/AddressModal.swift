//
//  AddressModal.swift
//  NewTheme V4
//
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

struct AddressModal {

    var addresses = [AddressData]()

    private let addressPath =  ["addresses", "address"]

    init?(data: JSON) {

        if data[addressPath] != JSON.null {
            if let data  = data[addressPath].array {
                self.addresses = data.map { AddressData( data: $0) }
            } else {
                self.addresses.append(AddressData(data: data[addressPath]))
            }
        }

    }
}

struct AddAddressModel {
    
    private let addressPath =  ["address_fields", "field"]
    var addressData = [AddressFieldData]()
    
    init?(data: JSON) {
        if data[addressPath] != JSON.null {
            if let data  = data[addressPath].array {
                self.addressData = data.map { AddressFieldData( data: $0) }
            } else {
                self.addressData.append(AddressFieldData(data: data[addressPath]))
            }
        }
    }
}
