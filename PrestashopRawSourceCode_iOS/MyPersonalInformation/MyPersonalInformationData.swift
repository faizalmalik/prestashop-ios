//
//  MyAccountDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class MyPersonalInformationData: UIViewController {
    var gdprTrue = false
    @IBOutlet weak var gdprHeight: NSLayoutConstraint!
    @IBOutlet weak var gdprViewPersonal: GdprView!
    @IBOutlet weak var cnfrmPasswordFiels: UITextField!
    @IBOutlet weak var cnfrmPasswordLabel: UILabel!
    @IBOutlet weak var newPasswordField: UITextField!
    @IBOutlet weak var newPasswordLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var newsletterLabel: UILabel!
    @IBOutlet weak var specialOfferLabel: UILabel!
    @IBOutlet weak var lastNameField: UITextField!
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var dobField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var offerSwitch: UISwitch!
    @IBOutlet weak var newsletterSwitch: UISwitch!
    @IBOutlet weak var SpecialOfferSwitch: UISwitch!
    @IBOutlet weak var save_btn: UIButton!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    var secure_key: String!
    var email: String!
    var title1: String!
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.title = title1
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        dobField.delegate = self
        self.stringsLocalized()
        //dobField.addTarget(self, action: #selector(myTargetFunction(_:)), for: UIControl.Event.touchDown)
        // Do any additional setup after loading the view.
        
        passwordField.isSecureTextEntry = true
        newPasswordField.isSecureTextEntry = true
        cnfrmPasswordFiels.isSecureTextEntry = true
        
        if !AddonsEnabled.enable_optin {
            specialOfferLabel.isHidden = true
            offerSwitch.isHidden = true
        }
        
        if !AddonsEnabled.display_dob_field {
            dobLabel.isHidden = true
            dobField.isHidden = true
        }
        
        self.makeRequest(id: "", call: WhichApiCall.none, dict: [:])
    }
    
    func stringsLocalized() {
        firstNameLabel.text = "firstName".localized + "*"
        lastNameLabel.text = "lastName".localized + "*"
        emailLabel.text = "emailAddress".localized + "*"
        passwordLabel.text = "password".localized + "*"
        newPasswordLabel.text = "newPassword".localized
        cnfrmPasswordLabel.text = "comfirmPassword".localized
        dobLabel.text = "dob".localized
        newsletterLabel.text = "recieveNewsletter".localized
        specialOfferLabel.text = "recieveOffer".localized
        save_btn.setTitle("save".localized, for: .normal)
    }
    
    func applyGradient(colours: [UIColor]) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.view.bounds
        gradient.colors = colours.map { $0.cgColor }
        self.view.layer.insertSublayer(gradient, at: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func myTargetFunction( _ textField: UITextField) {
        self.dismiskeyboard()
        dobField.resignFirstResponder()
        dobField.endEditing(true)
        UIBarButtonItem.appearance().tintColor = UIColor.black
        textField.resignFirstResponder()
        
        let datePicker = ActionSheetDatePicker(title: "dateOfBirth".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: { _, value, _ in
            if let value = value as? Date {
                let calendar = Calendar.autoupdatingCurrent
                let components = calendar.dateComponents([.hour, .minute, .day, .month, .year], from: value)
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                self.dobField.text = dateFormatter.string(from: value)
                dateFormatter.timeStyle = DateFormatter.Style.none
                dateFormatter.dateStyle = DateFormatter.Style.full
                self.detailsDict["years"] = String(components.year ?? 0)
                self.detailsDict["months"] = String(components.month ?? 0 )
                self.detailsDict["days"] = String(components.day  ?? 0)
            }
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: textField)
        datePicker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
        datePicker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
        datePicker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
        datePicker?.maximumDate = Date()
        datePicker?.show()
    }
    
    func dismiskeyboard() {
        view.endEditing(true)
    }
    
    func makeRequest( id: String, call: WhichApiCall, dict: [String: Any] ) {
        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get
        switch call {
        case .checkCustomerPassword:
            params =  GlobalConstants.ApiNames.checkCustomerPassword
            if let customerId = UrlParameters.fetchCustomerId() {
                params += customerId
            }
            verbs = .post
            newDict[GlobalConstants.ApiNames.checkCustomerPassword.FetchApiName()] = passDict
        case .customerExists:
            params =  GlobalConstants.ApiNames.customerExists
            params += "&email=" +  emailField.text!
        case .updateAccountDetails:
            params =  GlobalConstants.ApiNames.savecustomer
            if let customerId = UserDefaults.fetch(key: UserDefaults.Keys.id_customer) {
                params += customerId
            }
            newDict["savecustomer"] = detailsDict
            verbs = .put
        default:
            params =  GlobalConstants.ApiNames.accountDetails
            if let customerId = UserDefaults.fetch(key: UserDefaults.Keys.id_customer) {
                params += customerId
            }
            verbs = .get
        }
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.myPersonalInfo, view: self.view) { (responseObject: JSON?, error: Error?) in
            
            if error != nil {
                print("loginError".localized)
            } else {
                print(call)
                switch call {
                case .checkCustomerPassword:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        if self.emailField.text != self.email {
                            self.makeRequest(id: "", call: WhichApiCall.customerExists, dict: [:])
                        } else {
                            self.makeRequest(id: "", call: WhichApiCall.updateAccountDetails, dict: [:])
                        }
                    }
                case .customerExists:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        self.makeRequest(id: "", call: WhichApiCall.updateAccountDetails, dict: [:])
                    }
                case .updateAccountDetails:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        UserDefaults.set(value: self.firstNameField.text!, key: UserDefaults.Keys.firstname)
                        UserDefaults.set(value: self.lastNameField.text!, key: UserDefaults.Keys.lastname)
                        UserDefaults.set(value: self.emailField.text!, key: UserDefaults.Keys.email)
                        self.navigationController?.popViewController(animated: true)
                    }
                default:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, let data = ErrorChecking.getData(data: responseObject) {
                        let allData = data["customer"] != JSON.null ? data["customer"]:data["customer_details"]
                        self.firstNameField.text = allData["firstname"].stringValue
                        self.lastNameField.text = allData["lastname"].stringValue
                        self.emailField.text = allData["email"].stringValue
                        self.dobField.text = allData["birthday"].string ?? "\(allData["years"].stringValue)-\(allData["months"].stringValue)-\(allData["days"].stringValue)"
                        self.secure_key = allData["secure_key"].stringValue
                        self.email = allData["email"].stringValue
                        
                        // MARK: - gdpr condition
                        
                        if responseObject!["consent_box"].stringValue == "" {
                            self.gdprViewPersonal.isHidden = true
                            self.gdprHeight.constant = 0
                            self.gdprTrue = false
                            self.mainViewHeight.constant = 700
                        } else {
                            self.gdprViewPersonal.isHidden = false
                            self.gdprTrue = true
                            self.gdprViewPersonal.gdprDataLbl.textColor = UIColor.black
                            self.gdprViewPersonal.gdprDataLbl.text = responseObject!["consent_box"].stringValue
                            self.gdprHeight.constant = responseObject!["consent_box"].stringValue.height(withConstrainedWidth: self.view.frame.size.width - 50, font: UIFont.systemFont(ofSize: 15.0)) + 40
                            self.mainViewHeight.constant = 650 + responseObject!["consent_box"].stringValue.height(withConstrainedWidth: self.view.frame.size.width - 50, font: UIFont.systemFont(ofSize: 15.0)) + 40
                            
                        }
                        if allData["newsletter"].stringValue == "0" {
                            self.newsletterSwitch.setOn(false, animated: false)
                        }
                        if allData["optin"].stringValue == "0" {
                            self.SpecialOfferSwitch.setOn(false, animated: false)
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func saveClicked(_ sender: UIButton) {
        
        let someOptional: Bool = true
        
        switch someOptional {
        case ErrorChecking.isEmptyString(text: firstNameField.text):
            ErrorChecking.warningView(message: "firstNameValidate".localized)
            firstNameField.shake()
        case ErrorChecking.isEmptyString(text: lastNameField.text):
            ErrorChecking.warningView(message: "lastNameValidate".localized)
            lastNameField.shake()
        case ErrorChecking.isEmptyString(text: emailField.text):
            ErrorChecking.warningView(message: "emailEmptyValidate".localized)
            emailField.shake()
        case ErrorChecking.isValidEmail(testStr: emailField.text!):
            ErrorChecking.warningView(message: "emailValidate".localized)
            emailField.shake()
        case ErrorChecking.isEmptyString(text: passwordField.text):
            ErrorChecking.warningView(message: "passwordRequiredField".localized)
            passwordField.shake()
        case (!(newPasswordField.text?.isEmpty)! && ErrorChecking.checkPasswordcharLimit(limit: 5, text: newPasswordField.text!)):
            ErrorChecking.warningView(message: "passwordCharLimit".localized)
            newPasswordField.shake()
        case (!(cnfrmPasswordFiels.text?.isEmpty)! && ErrorChecking.checkPasswordcharLimit(limit: 5, text: cnfrmPasswordFiels.text!)):
            ErrorChecking.warningView(message: "passwordCharLimit".localized)
            cnfrmPasswordFiels.shake()
        case newPasswordField.text! != cnfrmPasswordFiels.text!:
            ErrorChecking.warningView(message: "matchPassword".localized)
            cnfrmPasswordFiels.shake()
            //case (!(newPasswordField.text?.isEmpty)! || !(cnfrmPasswordFiels.text?.isEmpty)!) && newPasswordField.text != cnfrmPasswordFiels.text:
        //ErrorChecking.warningView(message: "samepasswordmatcherror".localized)
        default:
            
            if( newsletterSwitch.isOn ) {
                detailsDict ["newsletter"] = "1"
            } else {
                detailsDict ["newsletter"] = "0"
            }
            if( SpecialOfferSwitch.isOn ) {
                detailsDict ["optin"] = "1"
            } else {
                detailsDict ["optin"] = "0"
            }
            
            detailsDict["email"] = emailField.text
            
            detailsDict["firstname"] = firstNameField.text
            detailsDict["id_lang"] = UserDefaults.standard.value(forKey: "id_lang")
            detailsDict["id_shop_group"] = "1"
            if((newPasswordField.text?.count)! > 0) {
                detailsDict["new_passwd"] = newPasswordField.text
                detailsDict["confirm_passwd"] = newPasswordField.text
                detailsDict["passwd"] = passwordField.text
                
            } else {
                detailsDict["passwd"] = passwordField.text
            }
            detailsDict["active"] = "1"
            detailsDict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            detailsDict["lastname"] = lastNameField.text
            //            detailsDict["secure_key"] = secure_key
            detailsDict["birthday"] = dobField.text
            
            //            let dict1 = ["id" : "3"]
            //            let dict = ["group" : dict1]
            //            detailsDict ["groups"] = dict
            detailsDict ["id_gender"] = "1"
            
            //            passDict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            //            passDict["psw"] =  passwordField.text
            print(detailsDict)
            
            if self.gdprTrue == true {
                if gdprViewPersonal.gdprSwitch.isOn {
                    
                    self.makeRequest(id: "", call: WhichApiCall.updateAccountDetails, dict: detailsDict)
                    
                } else {
                    ErrorChecking.warningView(message: "Please check the terms and condition".localized)
                }
            } else {
                self.makeRequest(id: "", call: WhichApiCall.updateAccountDetails, dict: detailsDict)
                
            }
        }
        
    }
    var passDict = [String: Any]()
    var detailsDict = [String: Any]()
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension MyPersonalInformationData: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dobField {
            self.myTargetFunction(textField)
        }
        return false
    }
    
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
