//
//  MyCreditModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

struct MyCreditModal {

    var creditArray = [CreditData]()

    private let CreditPath =  ["response", "credit_slips", "credit_slip"]

    init?(data: JSON) {

        if data[CreditPath] != JSON.null {
            if let data  = data[CreditPath].array {
                self.creditArray = data.map { CreditData( data: $0) }
            } else {
                self.creditArray.append(CreditData(data: data[CreditPath]))
            }
        }

    }
}

struct CreditData {
    var id_order_slip: String!
    var id_order: String!
    var date_add: String!
    init(data: JSON) {
        self.id_order = data["id_order"].stringValue
         self.id_order_slip = data["id_order_slip"].stringValue
         self.date_add = data["date_add"].stringValue
    }

}
