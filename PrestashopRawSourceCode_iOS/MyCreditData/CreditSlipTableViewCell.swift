//
//  CreditSlipTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class CreditSlipTableViewCell: UITableViewCell {

    @IBOutlet weak var dateIssueLabel: UILabel!
    @IBOutlet weak var orderIdTextLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var returnIdLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dateIssueLabel.SetDefaultTextColor()
        orderIdTextLabel.SetDefaultTextColor()
        orderIdTextLabel.text = "orderId".localized
        dateIssueLabel.text = "dateofIssue".localized
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var item: CreditData! {
        didSet {
            returnIdLabel.text = String.init(format: "%@: %@", "returnId".localized, item.id_order_slip)
            orderIdLabel.text = item.id_order
            dateLabel.text = item.date_add
            returnIdLabel.halfTextColorChange(fullText: returnIdLabel.text!, changeText: "returnId".localized)
        }
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
