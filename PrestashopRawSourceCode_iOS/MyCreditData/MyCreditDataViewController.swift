//
//  MyCreditDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class MyCreditDataViewController: UIViewController {
    var title1: String?
    var emptyView: EmptyView!
    @IBOutlet weak var creditTableview: UITableView!

    fileprivate let MyCreditViewModalObject = MyCreditViewModal()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = title1
        // Do any additional setup after loading the view.
        creditTableview.register(CreditSlipTableViewCell.nib, forCellReuseIdentifier: CreditSlipTableViewCell.identifier)
        creditTableview.dataSource = MyCreditViewModalObject
        creditTableview.delegate = MyCreditViewModalObject
        creditTableview.tableFooterView = UIView()
        self.makeRequest()
    }

    func makeRequest() {
        var params =  GlobalConstants.ApiNames.myCreditSlip
        if let custId =  UrlParameters.fetchCustomerId() {
            params += custId
        }
        params += UrlParameters.width
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                
                self.MyCreditViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    if data {
                        self.emptyView?.isHidden = true
                        self.creditTableview.reloadData()
                    } else {
                        
                        self.emptyView?.isHidden = false
                    }
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = self.view.bounds
            emptyView?.labelText.text = "creditEmpty".localized
            emptyView?.image.image = #imageLiteral(resourceName: "empty-state-CreditSlip")
            emptyView?.button.isHidden = true
            emptyView?.isHidden = true
            self.view.addSubview(emptyView!)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyCreditDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest()
    }
}
