//
//  MyCreditViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class MyCreditViewModal: NSObject {
    
    var moveControllerDelegate: MoveController?
    
    var creditArray = [CreditData]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = MyCreditModal(data: jsonData) else {
            return
        }
        creditArray.removeAll()
        if !data.creditArray.isEmpty {
            creditArray = data.creditArray
            completion(true)
        } else {
            completion(false)
        }        
    }
}

extension MyCreditViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CreditSlipTableViewCell.identifier) as? CreditSlipTableViewCell {
            cell.item = creditArray[indexPath.row]
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return creditArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        moveControllerDelegate?.moveController(id: creditArray[indexPath.row].addressId, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.addAddress)
    }
    
}
