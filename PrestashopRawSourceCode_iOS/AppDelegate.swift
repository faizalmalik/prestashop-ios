//
//  AppDelegate.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseAnalytics
import Fabric
import Crashlytics
import Reachability
import SwiftMessages
import PermissionScope
import UserNotifications
import SVProgressHUD
import FirebaseMessaging
import Siren

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate {
    
    func application(_ application: UIApplication,
                     performActionFor shortcutItem: UIApplicationShortcutItem,
                     completionHandler: @escaping (Bool) -> Void) {
        
        print("shortcutItem", shortcutItem.localizedTitle)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "3DTouch"), object: shortcutItem.localizedTitle)
        
    }
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Siren.shared.wail()
        SVProgressHUD.setDefaultMaskType(.clear)
        switch GlobalConstants.AppCredentials.environment {
        case .marketplace:
            let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
            if let options = FirebaseOptions(contentsOfFile: filePath!) {
                FirebaseApp.configure(options: options)
            }
        case .mobikul:
            let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!
            if  let options = FirebaseOptions(contentsOfFile: filePath) {
                FirebaseApp.configure(options: options)
            }
        }
        
        UITabBar.appearance().barTintColor = UIColor.black
        UITabBar.appearance().tintColor = GlobalConstants.Colors.accentColor
        if #available(iOS 10.0, *) {
            UITabBar.appearance().unselectedItemTintColor = UIColor.white
        } else {
            // Fallback on earlier versions
        }
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]        
        UINavigationBar.appearance().applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        Fabric.with([Crashlytics.self])
        Fabric.sharedSDK().debug = true
        application.delegate = self
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        if let remoteNotif = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.application(application, didReceiveRemoteNotification: remoteNotif)
            })
        }
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(SignInDataViewController.self)
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        print(url)
        var handle: Bool = true
        return handle
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var tokenq = ""
        for i in 0..<deviceToken.count {
            tokenq += String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("token", tokenq)
        Messaging.messaging().apnsToken = deviceToken as Data
        Messaging.messaging().subscribe(toTopic: "/topics/global")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Messaging.messaging().subscribe(toTopic: "/topics/global")
        UserDefaults.standard.set(fcmToken, forKey: UserDefaults.Keys.firebaseKey)
        UserDefaults.standard.synchronize()
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Messaging.messaging().subscribe(toTopic: "/topics/global")
        UserDefaults.standard.set(fcmToken, forKey: UserDefaults.Keys.firebaseKey)
        UserDefaults.standard.synchronize()
    }
    
    let  gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pushNotification"), object: nil, userInfo: userInfo)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)
        if application.applicationState == .inactive {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pushNotification"), object: nil, userInfo: userInfo)
            completionHandler(UIBackgroundFetchResult.newData)
        }
        
        if application.applicationState != .active {
            if UserDefaults.standard.value(forKey: "badge") == nil {
                UserDefaults.standard.set(0, forKey: "badge")
                UserDefaults.standard.synchronize()
            }
            UserDefaults.standard.set(((UserDefaults.standard.value(forKey: "badge") as? Int) ?? 0) + 1, forKey: "badge")
            UserDefaults.standard.synchronize()
            UIApplication.shared.applicationIconBadgeNumber = (UserDefaults.standard.value(forKey: "badge") as? Int) ?? 0
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.startMonitoring()
    }
    
    func startMonitoring() {
        let reachability = Reachability()!
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: Notification.Name.reachabilityChanged,
                                               object: reachability)
        do {
            try reachability.startNotifier()
            print("dfdffdeffeffefef")
        } catch {
            print("Could not start reachability notifier")
        }
    }
    
    @objc func reachabilityChanged(notification: Notification) {
        if let reachability = notification.object as? Reachability {
            if reachability.connection == .none {
                ErrorChecking.NoInternetView()
            } else {
                SwiftMessages.hide()
            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Swift.Void) -> Bool {
        //        print(userActivity.webpageURL?.absoluteString)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "universalLink"), object: userActivity.webpageURL?.absoluteString)
        return true
    }
    
}

@available(iOS 10, *)
extension HomeController: UNUserNotificationCenterDelegate {
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print("userInfo", userInfo)
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print full message.
        print("tap on on forground app", userInfo)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pushNotification"), object: nil, userInfo: userInfo)
        completionHandler()
    }
}

extension HomeController: MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Messaging.messaging().subscribe(toTopic: "topics/global")
        UserDefaults.standard.set(fcmToken, forKey: UserDefaults.Keys.firebaseKey)
        UserDefaults.standard.synchronize()
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Messaging.messaging().subscribe(toTopic: "topics/global")
        UserDefaults.standard.set(fcmToken, forKey: UserDefaults.Keys.firebaseKey)
        UserDefaults.standard.synchronize()
    }
}

//extension AppDelegate {
//    public func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
//        if let url = userActivity.webpageURL {
//            var view = url.lastPathComponent
//            var parameters: [String: String] = [:]
//            URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
//                parameters[$0.name] = $0.value
//            }
//        }
//        return true
//    }
//}

extension AppDelegate {
    func application(_ application: UIApplication, willContinueUserActivityWithType userActivityType: String) -> Bool {
        print(#function)
        print(userActivityType)
        return true
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Swift.Void) -> Bool {
        logInfo(continue: userActivity)
        if userActivity.activityType == "NSUserActivityTypeBrowsingWeb" {
            print(userActivity.webpageURL as Any)
            let url = URL(string: userActivity.webpageURL?.description ?? "")
            if let myTabBar = self.window?.rootViewController as? UITabBarController {
                myTabBar.selectedIndex = 0
                guard let viewcontroller = UIApplication.topViewController() else { return false }
                if viewcontroller.isModal == true {
                    viewcontroller.navigationController?.dismiss(animated: true, completion: nil)
                } else {
                    viewcontroller.navigationController?.popToRootViewController(animated: true)
                }
                if let topViewcontroller = UIApplication.topViewController() as? HomeController, let catalogProductVC = UIStoryboard.loadProductViewController() {
                    if let url = url {
                        if url.absoluteString.contains("id_product") {
                            catalogProductVC.productId = getQueryStringParameter(url: url.absoluteString, param: "product_id")!
                            topViewcontroller.navigationController?.popToRootViewController(animated: false)
                            topViewcontroller.navigationController?.pushViewController(catalogProductVC, animated: true)
                            print(topViewcontroller)
                        }
                    }
                }
            }
        }
        return true
    }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        let newurl = url.replacingOccurrences(of: "%3F", with: "?")
        guard let url = URLComponents(string: newurl) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    func application(_ application: UIApplication, didFailToContinueUserActivityWithType userActivityType: String, error: Error) {
        print(#function)
        print(userActivityType)
    }
    
    func application(_ application: UIApplication, didUpdate userActivity: NSUserActivity) {
        print(#function)
        logInfo(continue: userActivity)
    }
    
    func logInfo(continue userActivity: NSUserActivity) {
        //print(userActivity)
        //print(userActivity.activityType)
        //print(userActivity.title as String!)
        //print(userActivity.userInfo as [AnyHashable:Any]!)
        //print(userActivity.webpageURL as URL!)
        //print(userActivity.expirationDate as Date!)
        //print(userActivity.keywords)
    }
    
}

extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension UIViewController {
    var isModal: Bool {
        return presentingViewController != nil ||
            navigationController?.presentingViewController?.presentedViewController === navigationController ||
            tabBarController?.presentingViewController is UITabBarController
    }
}
