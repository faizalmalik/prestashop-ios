//
//  MerchandiseTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class MerchandiseTableViewCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateIssueLabel: UILabel!
    @IBOutlet weak var orderIdTextLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var returnIdLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        orderIdTextLabel.text = "orderId".localized
        dateIssueLabel.text = "dateofIssue".localized
        dateIssueLabel.SetDefaultTextColor()
        orderIdTextLabel.SetDefaultTextColor()
        statusLabel.layer.masksToBounds = true
        statusLabel.layer.cornerRadius = 3
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var item: MerchandiseData! {
        didSet {
            returnIdLabel.text = String.init(format: "%@ :%@", "returnId".localized, item.id_order_return)
            orderIdLabel.text = String.init(format: "#%@", item.id_order)
            dateLabel.text = item.date_add
            returnIdLabel.halfTextColorChange(fullText: returnIdLabel.text!, changeText: "returnId".localized)
            statusLabel.text = String.init(format: "  %@  ", item.state_name)
            statusLabel.backgroundColor = UIColor().HexToColor(hexString: item.state_color ?? "ffffff")

        }
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }
}
