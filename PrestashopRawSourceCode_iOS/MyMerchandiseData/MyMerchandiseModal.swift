//
//  MyMerchandiseModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

struct MyMerchandiseModal {

    var merchandiseArray = [MerchandiseData]()

    private let MerchandisePath =  ["response", "rma_returns", "rma_return"]

    init?(data: JSON) {

        if data[MerchandisePath] != JSON.null {
            if let data  = data[MerchandisePath].array {
                self.merchandiseArray = data.map { MerchandiseData( data: $0) }
            } else {
                self.merchandiseArray.append(MerchandiseData(data: data[MerchandisePath]))
            }
        }

    }
}

struct MerchandiseData {
    var id_order_return: String!
    var id_order: String!
    var date_add: String!
    var reference: String!
    var state_name: String!
    var state_color: String!
    init(data: JSON) {
        self.id_order = data["id_order"].stringValue
        self.id_order_return = data["id_order_return"].stringValue
        self.date_add = data["date_add"].stringValue
        self.reference = data["reference"].stringValue
        self.state_name = data["state_name"].stringValue
        self.state_color = data["state_color"].stringValue
    }

}
