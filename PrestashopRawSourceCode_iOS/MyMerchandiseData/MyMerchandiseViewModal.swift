//
//  MyMerchandiseViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class MyMerchandiseViewModal: NSObject {
    
    var moveControllerDelegate: MoveController?
    
    var merchandiseArray = [MerchandiseData]()
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = MyMerchandiseModal(data: jsonData) else {
            return
        }
        merchandiseArray.removeAll()
        if !data.merchandiseArray.isEmpty {
            merchandiseArray = data.merchandiseArray
            completion(true)
        } else {
            completion(false)
        }
        
    }
}

extension MyMerchandiseViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: MerchandiseTableViewCell.identifier) as? MerchandiseTableViewCell {
            cell.item = merchandiseArray[indexPath.row]
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return merchandiseArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        moveControllerDelegate?.moveController(id: merchandiseArray[indexPath.row].id_order, name: merchandiseArray[indexPath.row].reference, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.addAddress)
    }
    
}
