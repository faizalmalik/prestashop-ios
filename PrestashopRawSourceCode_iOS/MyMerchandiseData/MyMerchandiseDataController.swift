//
//  MyMerchandiseDataController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class MyMerchandiseDataController: UIViewController {
    
    var title1: String?
    fileprivate let MyMerchandiseViewModalObject = MyMerchandiseViewModal()
    var emptyView: EmptyView!
    @IBOutlet weak var merchandiseTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = title1
        merchandiseTableView.register(MerchandiseTableViewCell.nib, forCellReuseIdentifier: MerchandiseTableViewCell.identifier)
        merchandiseTableView.dataSource = MyMerchandiseViewModalObject
        merchandiseTableView.delegate = MyMerchandiseViewModalObject
        merchandiseTableView.tableFooterView = UIView()
        MyMerchandiseViewModalObject.moveControllerDelegate = self
        // Do any additional setup after loading the view.
        self.makeRequest()
    }
    
    func makeRequest() {
        var params =  GlobalConstants.ApiNames.myMerchandise
        if let custId =  UrlParameters.fetchCustomerId() {
            params += custId
        }
        params += UrlParameters.width
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                
                self.MyMerchandiseViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    if data {
                        
                        self.merchandiseTableView.reloadData()
                    } else {
                        
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension MyMerchandiseDataController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest()
    }
}

extension MyMerchandiseDataController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        if let view = UIStoryboard.loadOrderdetailViewController() {
            view.id = id
            view.reference = name
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
}
