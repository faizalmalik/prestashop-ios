//
//  EmptyView.swift
//  Odoo application
//
//  Created by vipin sahu on 9/6/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    @IBOutlet weak var labelText: UILabel!

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var button: UIButton!
    
    var params: String!
    var verb: RequestType!
    var dict = [String: Any]()
    var controller: AllController!
    var view: UIView!
    var call: WhichApiCall!
    
//    var ve
//    (params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view )
    
   static var delegate1: EmptyDelegate?
    var delegate: EmptyDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        button.clipsToBounds = true
        button.layer.cornerRadius = 7
        button.backgroundColor = GlobalConstants.Colors.accentColor
        labelText.textColor =  GlobalConstants.Colors.defaultTextColor
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func buttonClicked(_ sender: UIButton) {
        EmptyView.delegate1?.buttonPressed()
        delegate?.buttonPressed()
    }
    
    class func setemptyData(code: Int, view: UIView ) -> EmptyView? {
        if code == 503 {
            
           return EmptyView.setView(view: view, internetError: 2)
        } else {
            switch URLError.Code(rawValue: code) {
            case .notConnectedToInternet:
                print("NotConnectedToInternet")
              return  EmptyView.setView(view: view, internetError: 1)
            default:
              return  EmptyView.setView(view: view, internetError: 0)
            }
        }
    }

    class func setView(view: UIView, internetError: Int) -> EmptyView? {
        for  subView in  view.subviews {
            subView.isHidden = true
        }
        
        var emptyView: EmptyView!
        
        if emptyView == nil {
            emptyView = Bundle.loadView(fromNib: "EmptyView", withType: EmptyView.self)
            emptyView?.frame = view.bounds
            
            view.addSubview(emptyView!)
        }
        emptyView.tag = 555
        emptyView.button.setTitle("Try Again", for: .normal)

        switch internetError {
        case 1:
            emptyView?.labelText.text = "You are not connected to Internet,Please try later"
            emptyView?.image.image = #imageLiteral(resourceName: "nointernet")
            emptyView.button.isHidden = false
        case 2:
            emptyView?.labelText.text = "We are notified and working on it,we will be back soon"
            emptyView?.image.image = #imageLiteral(resourceName: "nointernet")
            emptyView.button.isHidden = true
            
        default:
            emptyView?.labelText.text = "Connection unsuccessful,Please try later"
            emptyView?.image.image = #imageLiteral(resourceName: "nointernet")
            emptyView.button.isHidden = false
        }
        return emptyView
        
    }

}

protocol EmptyDelegate {
    func buttonPressed()
}
