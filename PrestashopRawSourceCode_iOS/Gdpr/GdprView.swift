/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class GdprView: UIView {

    @IBOutlet weak var gdprDataLbl: UILabel!
    @IBOutlet weak var gdprSwitch: UISwitch!
    var delegate: gdprSwitchProtocol!
    //I agree to the terms and conditions and the privacy policy Read the terms and conditions of use.
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if let xibView = Bundle.main.loadNibNamed("GdprView", owner: self, options: nil)?[0] as? UIView {
            xibView.frame = self.bounds
            xibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.addSubview(xibView)
        }
    }
    @IBAction func gdprSwitchAct(_ sender: Any) {
       // delegate.gdprAct()
    }
    
}
