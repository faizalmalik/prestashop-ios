/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
class MyPersonalDataViewController: UIViewController, downloadGdprFileAct {
    
    // MARK: - download the gdpr file
    func downloadfile(type: String) {
        var params = ""
        params =  "\(GlobalConstants.AppCredentials.BASEURL)mobikul/getcustomerdata"
        params += "?ws_key=" + GlobalConstants.AppCredentials.WS_KEY
        if let customerId = UserDefaults.fetch(key: UserDefaults.Keys.id_customer) {
            params += "&id_customer=" + customerId
        }
        if let psgdprToken = UserDefaults.fetch(key: UserDefaults.Keys.psgdprToken) {
            params += "&psgdpr_token=" + psgdprToken
        }
        params += "&ws_key=" + GlobalConstants.AppCredentials.WS_KEY
        //        /UserDefaults.set(value: allData[UserDefaults.Keys.psgdprToken].stringValue, key: UserDefaults.Keys.psgdprToken)
        if type == "pdf" {
            params += "&file_type=" + "pdf"
            self.pdfCheck(url: params, id: "0", typeOfFile: "pdf")
        } else {
            params += "&file_type=" + "csv"
            self.pdfCheck(url: params, id: "0", typeOfFile: "csv")
        }
    }
    
    @IBOutlet weak var personalDataTable: UITableView!
    var data: JSON = ""
    var arrayDataOfTableCell = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My personal data"
        personalDataTable.register(AccessPersonalDataTableViewCell.nib, forCellReuseIdentifier: AccessPersonalDataTableViewCell.identifier)
        arrayDataOfTableCell.append(data["access_my_data_text"].stringValue)
        arrayDataOfTableCell.append(data["erase_my_data_text"].stringValue)
        personalDataTable.delegate = self
        personalDataTable.dataSource = self
        personalDataTable.reloadData()
        
    }
    
    func getMimeType(type: String) -> String {
        switch type {
        case "txt":
            return "text/plain"
        case "htm":
            return "text/html"
        case "html":
            return "text/html"
        case "php":
            return "text/html"
        case "css":
            return "text/css"
        case "js":
            return "application/javascript"
        case "json":
            return "application/json"
        case "xml":
            return "application/xml"
        case "swf":
            return "application/x-shockwave-flash"
        case "flv":
            return "video/x-flv"
        case "png":
            return "image/png"
        case "jpe":
            return "image/jpeg"
        case "jpeg":
            return "image/jpeg"
        case "gif":
            return "image/gif"
        case "bmp":
            return "image/bmp"
        case "ico":
            return "image/vnd.microsoft.icon"
        case "tiff":
            return "image/tiff"
        case "tif":
            return "image/tiff"
        case "svg":
            return "image/svg+xml"
        case "svgz":
            return "image/svg+xml"
        case "zip":
            return "application/zip"
        case "rar":
            return "application/x-rar-compressed"
        case "exe":
            return "application/x-msdownload"
        case "msi":
            return "application/x-msdownload"
        case "mp3":
            return "audio/mpeg"
        case "qt":
            return "video/quicktime"
        case "mov":
            return "video/quicktime"
        case "pdf":
            return "application/pdf"
        case "psd":
            return "image/vnd.adobe.photoshop"
        case "ai":
            return "application/postscript"
        case "eps":
            return "application/postscript"
        case "ps":
            return "application/postscript"
        case "doc":
            return "application/msword"
        case "rtf":
            return "application/rtf"
        case "xls":
            return "application/vnd.ms-excel"
        case "ppt":
            return "application/vnd.ms-powerpoint"
        case "odt":
            return "application/vnd.oasis.opendocument.text"
        case "ods":
            return "application/vnd.oasis.opendocument.spreadsheet"
            
        default:
            return ""
        }
    }
    
    func pdfCheck(url: String, id: String, typeOfFile: String) {
        // Create destination URL
        let documentsUrl: URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        print(documentsUrl)
        var destinationFileUrl = documentsUrl.appendingPathComponent(id + "webkulfile.pdf")
        if typeOfFile == "pdf" {
            destinationFileUrl = documentsUrl.appendingPathComponent(id + "webkulfile.pdf")
        } else {
            destinationFileUrl = documentsUrl.appendingPathComponent(id + "webkulfile.csv")
        }
        print(documentsUrl)
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: destinationFileUrl.path) {
            print("FILE AVAILABLE")
            if let view = UIStoryboard.loadCmsViewController() {
                view.title = "Gdpr Downloed Data"
                //view.type = "DownloedData"
                //let nav  = UINavigationController(rootViewController: view)
                view.url = destinationFileUrl
                self.navigationController?.pushViewController(view, animated: true)
            }
        } else {
            //Create URL to the source file you want to download
            var stringUrl = url
            let fileURL = URL(string: stringUrl)
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            let request = URLRequest(url: fileURL!)
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    
                    print(tempLocalUrl)
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                        
                    } catch (let writeError) {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print("Successfully downloaded. Status code: \(statusCode)")
                        self.move(url: destinationFileUrl)
                    }
                } else {
                    print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as Any)
                }
            }
            task.resume()
        }
    }
    
    func move(url: URL) {
        if let view = UIStoryboard.loadCmsViewController() {
        view.title = "Gdpr Downloed Data"
        //        view.type = "DownloedData"
        let nav  = UINavigationController(rootViewController: view)
        view.url = url
        self.navigationController?.pushViewController(view, animated: true)
        }
    }
    
}
extension MyPersonalDataViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayDataOfTableCell.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: AccessPersonalDataTableViewCell.identifier, for: indexPath) as? AccessPersonalDataTableViewCell {
            cell.dataLabel.text = arrayDataOfTableCell[indexPath.row]
            cell.delegate = self
            if indexPath.section == 1 {
                cell.heightOfStcakBtn.constant = 0
                cell.downloadBtnStackview.isHidden = true
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Access to my data"
        } else {
            return "Rectification & Erasure requests"
        }
    }
    
}
