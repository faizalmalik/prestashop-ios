/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class AccessPersonalDataTableViewCell: UITableViewCell {

    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var heightOfStcakBtn: NSLayoutConstraint!
    @IBOutlet weak var downloadBtnStackview: UIStackView!
    var delegate: downloadGdprFileAct!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    @IBAction func getPdfDataAct(_ sender: Any) {
        delegate.downloadfile(type: "pdf")
    }
    
    @IBAction func getCsvData(_ sender: Any) {
        delegate.downloadfile(type: "csv")
    }
    
}
