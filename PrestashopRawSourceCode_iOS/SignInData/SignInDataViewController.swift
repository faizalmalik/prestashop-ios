//
//  SignInDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import Pastel
import SwiftyJSON
import FirebaseAnalytics
import LocalAuthentication
import FirebaseAuth
import Security

class SignInDataViewController: UIViewController {
    
    @IBOutlet weak var appName: UILabel!
    
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var linkedInBtn: UIButton! // or google btn
    @IBOutlet weak var pwdBtn: UIButton!
    @IBOutlet weak var cross: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var forgotButton: UIButton!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var emailfield: UITextField!
    @IBOutlet weak var passwordfield: UITextField!
    let userAccount = "AuthenticatedUser"
    let accessGroup = "SecuritySerivice"
    var isShow = false
    var isSocialLogin = false
    var isDismiss = false
    //    var controller : Controllers?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.view.backgroundColor = UIColor.clear
        passwordfield.isSecureTextEntry = true
        signUpButton.normalBorder1()
        loginButton.normalBorder1()
        if GlobalConstants.AppCredentials.environment == EnvironmentType.marketplace {
            appName.text = "appNameMp".localized
        } else {
            appName.text = "appName".localized
        }
        fbBtn.shadowBorder2()
        linkedInBtn.shadowBorder2()
        if #available(iOS 11.0, *) {
            emailfield.textContentType = .emailAddress
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 11.0, *) {
            passwordfield.textContentType = .password
        } else {
            // Fallback on earlier versions
        }
        self.stringsLocalized()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let pastelView = PastelView(frame: self.view.bounds)
        
        // Custom Direction
        
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        
        // Custom Duration
        pastelView.animationDuration = 2.0
        
        // Custom Color
        pastelView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
        
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
        
        if UserDefaults.standard.value(forKey: UserDefaults.Keys.loginDict) != nil {
            self.authentication(login: true, email: "", pwd: "")
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    
    func stringsLocalized() {
        appNameLabel.text = "appName".localized
        emailLabel.text = "emailAddress".localized
        passwordLabel.text = "password".localized
        loginButton.setTitle("login".localized, for: .normal)
        signUpButton.setTitle("wantsignUp".localized, for: .normal)
        forgotButton.setTitle("forgotPassword".localized, for: .normal)
        
    }
    
    @IBAction func CrossBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func applyGradient(colours: [UIColor]) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.view.bounds
        gradient.colors = colours.map { $0.cgColor }
        self.view.layer.insertSublayer(gradient, at: 0)
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        self.dismissKeyboard()
        if (emailfield.text?.isEmpty)! {
            emailfield.shake()
            ErrorChecking.warningView(message: "enterEmail".localized)
        } else if (passwordfield.text?.isEmpty)! {
            passwordfield.shake()
            ErrorChecking.warningView(message: "enterPwd".localized)
        } else if (!(passwordfield.text?.isEmpty)! && ErrorChecking.checkPasswordcharLimit(limit: 5, text: passwordfield.text!)) {
            ErrorChecking.warningView(message: "passwordCharLimit".localized)
            passwordfield.shake()
        } else {
            UserDefaults.standard.removeObject(forKey: "loginDict")
            UserDefaults.standard.synchronize()
            var newDict = [String: Any]()
            newDict["email"] = emailfield.text
            newDict["password"] =  passwordfield.text
            newDict["id_cart"] = UserDefaults.fetch(key: UserDefaults.Keys.id_cart)
            
            self.makeRequest(call: WhichApiCall.login, email: "", newDict: newDict )
            //                self.makeRequest(dict: dict, call: .none)
        }
    }
    
    func makeRequest( call: WhichApiCall, email: String, newDict: [String: Any]) {
        DispatchQueue.main.async {
            var params = ""
            
            var verbs: RequestType = .get
            
            var dict = [String: Any]()
            switch call {
            case .forgotPassword:
                params =  GlobalConstants.ApiNames.forgotPassword
                params += "email=" + email
            case .updateCustomerCart:
                params =  GlobalConstants.ApiNames.updateCustomerCart
                params += UrlParameters.fetchCartID()!
                params += UrlParameters.fetchGuestId()!
                params += UrlParameters.fetchCustomerId()!
            case .social:
                params =  GlobalConstants.ApiNames.socialLogin
                dict[GlobalConstants.ApiNames.socialLogin.FetchApiName()] = newDict
                verbs = .post
            case .login:
                params =  GlobalConstants.ApiNames.login
                dict[GlobalConstants.ApiNames.login.FetchApiName()] = newDict
                verbs = .post
            case .firebasetoken:
                params =  GlobalConstants.ApiNames.firebasetoken
                dict[GlobalConstants.ApiNames.firebasetoken.FetchApiName()] = newDict
                verbs = .post
            default:
                params =  GlobalConstants.ApiNames.login
                
                dict[GlobalConstants.ApiNames.login.FetchApiName()] = newDict
                verbs = .post
            }
            
            NetworkManager.fetchData(params: params, verb: verbs, dict: dict, controller: AllController.signInController, view: self.view) { (responseObject: JSON?, error: Error?) in
                
                if error != nil {
                    
                    print("loginError".localized, error!)
                    
                } else {
                    
                    switch call {
                    case .forgotPassword:
                        if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                            
                        }
                    case .firebasetoken:
                        self.fingerCall()
                    case .updateCustomerCart:
                        if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                            if UserDefaults.fetch(key: UserDefaults.Keys.firebaseKey) == nil {
                                if self.isDismiss {
                                    self.dismiss(animated: true, completion: nil)
                                } else {
                                    self.performSegue(withIdentifier: "reload", sender: self)
                                }
                            } else {
                                self.makeRequest(call: WhichApiCall.firebasetoken, email: "", newDict: self.firebaseDict())
                            }
                        }
                    case .social, .login:
                        if ErrorChecking.getData(data: responseObject) != JSON.null, let data = ErrorChecking.getData(data: responseObject) {
                            let allData = data["response"]
                            if UserDefaults.fetch(key: UserDefaults.Keys.id_cart) == nil && allData[UserDefaults.Keys.id_cart].string != nil {
                                UserDefaults.set(value: allData[UserDefaults.Keys.id_cart].stringValue, key: UserDefaults.Keys.id_cart)
                            }
                            if call == .social {
                                self.isSocialLogin = true
                            }
                            Analytics.logEvent(AnalyticsEventLogin, parameters: [AnalyticsParameterItemName: (String.init(format: "%@ %@", allData[UserDefaults.Keys.firstname].stringValue, allData[UserDefaults.Keys.lastname].stringValue)) as NSObject])
                            
                            if allData[UserDefaults.Keys.firstname].string != nil {
                                UserDefaults.set(value: allData[UserDefaults.Keys.firstname].stringValue, key: UserDefaults.Keys.firstname)
                            }
                            
                            if allData[UserDefaults.Keys.lastname].string != nil {
                                UserDefaults.set(value: allData[UserDefaults.Keys.lastname].stringValue, key: UserDefaults.Keys.lastname)
                            }
                            
                            if allData[UserDefaults.Keys.email].string != nil {
                                UserDefaults.set(value: allData[UserDefaults.Keys.email].stringValue, key: UserDefaults.Keys.email)
                            }
                            
                            if allData[UserDefaults.Keys.id_admin].string != nil &&  allData[UserDefaults.Keys.id_admin].stringValue != "0" {
                                UserDefaults.set(value: "1", key: UserDefaults.Keys.isLoggedInAdmin)
                                UserDefaults.standard.synchronize()
                                UserDefaults.set(value: allData[UserDefaults.Keys.id_admin].stringValue, key: UserDefaults.Keys.id_admin)
                            }
                            
                            if allData[UserDefaults.Keys.id_customer].string != nil   &&  allData[UserDefaults.Keys.id_customer].stringValue != "0" {
                                UserDefaults.set(value: "1", key: UserDefaults.Keys.isLoggedIn)
                                UserDefaults.standard.synchronize()
                                UserDefaults.set(value: allData[UserDefaults.Keys.id_customer].stringValue, key: UserDefaults.Keys.id_customer)
                            }
                            
                            if allData[UserDefaults.Keys.is_seller].string != nil    &&  allData[UserDefaults.Keys.is_seller].stringValue != "0" {
                                UserDefaults.set(value: allData[UserDefaults.Keys.is_seller].stringValue, key: UserDefaults.Keys.is_seller)
                            }
                            
                            if allData[UserDefaults.Keys.id_seller].string != nil    &&  allData[UserDefaults.Keys.id_seller].stringValue != "0" {
                                UserDefaults.set(value: allData[UserDefaults.Keys.id_seller].stringValue, key: UserDefaults.Keys.id_seller)
                            }
                            
                            if allData[UserDefaults.Keys.customer_profile_image].string != nil {
                                UserDefaults.set(value: allData[UserDefaults.Keys.customer_profile_image].stringValue, key: UserDefaults.Keys.customer_profile_image)
                            }
                            
                            if allData[UserDefaults.Keys.cartCount].string != nil {
                                UserDefaults.set(value: allData[UserDefaults.Keys.cartCount].stringValue, key: UserDefaults.Keys.cartCount)
                            }
                            
                            if let token = allData["psgdpr_token"].string {
                                UserDefaults.set(value: token, key: UserDefaults.Keys.psgdprToken)
                            }
                            
                            if  self.tabBarController != nil {
                                self.tabBarController?.setCartCount(value: FetchDetails.fetchCartCount())
                            }
                            
                            if UserDefaults.fetch(key: UserDefaults.Keys.firebaseKey) == nil {
                                self.fingerCall()
                            } else {
                                self.makeRequest(call: WhichApiCall.firebasetoken, email: "", newDict: self.firebaseDict())
                            }
                        }
                    default:
                        print()
                    }
                    
                }
            }
        }
    }
    
    func fingerCall() {
        let authenticationContext = LAContext()
        
        if  UserDefaults.standard.value(forKey: UserDefaults.Keys.loginDict) == nil {
            let value = authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
            if !value {
                if self.isDismiss {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.performSegue(withIdentifier: "reload", sender: self)
                }
            } else {
                if isSocialLogin {
                    if self.isDismiss {
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        self.performSegue(withIdentifier: "reload", sender: self)
                    }
                } else {
                    self.touchLinkedAlertController(email: emailfield.text!, pwd: self.passwordfield.text!)
                }
            }
        } else {
            if self.isDismiss {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.performSegue(withIdentifier: "reload", sender: self)
            }
        }
    }
    
    @IBAction func SignUpTapped(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        if let view = UIStoryboard.loadSignUpViewController() {
            view.controller = AllController.signInController
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
    
    @IBAction func ForgotPasswordClicked(_ sender: UIButton) {
        let AC = UIAlertController(title: "forgotPassword".localized, message: "forgotPassMesg".localized, preferredStyle: .alert)
        AC.addTextField { (textField) in
            textField.placeholder = "enterEmail".localized
            textField.text = self.emailfield.text
        }
        let okBtn = UIAlertAction(title: "ok", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            let textField = AC.textFields![0] // Force unwrapping because we know it exists.
            
            var isValid = 1
            var errorMessage = ""
            if(textField.text?.isEqual(""))! {
                isValid = 0
                errorMessage = "enterEmail".localized
            }
            
            if isValid == 0 {
                
                ErrorChecking.warningView(message: errorMessage)
                
            } else {
                
                self.makeRequest(call: WhichApiCall.forgotPassword, email: textField.text!, newDict: [:] )
                //                let dict : [String : Any] = ["login" : textField.text! ]
                //                self.makeRequest(dict: dict, call: .forgotPassword)
                
            }
            
        })
        let noBtn = UIAlertAction(title: "cancel".localized, style: .default, handler: {(_ action: UIAlertAction) -> Void in
        })
        AC.addAction(okBtn)
        AC.addAction(noBtn)
        present(AC, animated: true, completion: nil)
    }
    
    func firebaseDict() -> [String: Any] {
        var dict = [String: Any]()
        dict["id_registration"] = UserDefaults.fetch(key: UserDefaults.Keys.firebaseKey)
        dict["id_admin"] = UserDefaults.fetch(key: UserDefaults.Keys.id_admin)
        dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer) ?? "0"
        dict["id_android"] = UIDevice.current.identifierForVendor!.uuidString
        return dict
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func passwordShow(_ sender: UIButton) {
        if isShow == false {
            isShow = true
            passwordfield.isSecureTextEntry = false
            pwdBtn.setImage(#imageLiteral(resourceName: "show"), for: .normal)
        } else {
            isShow = false
            passwordfield.isSecureTextEntry = true
            pwdBtn.setImage(#imageLiteral(resourceName: "hide"), for: .normal)
        }
        
    }
    
    func touchLinkedAlertController(email: String, pwd: String ) {
        let alertView = UIAlertController(title: "", message: "linkAccount".localized, preferredStyle: .alert)
        let action = UIAlertAction(title: "ok".localized, style: .default, handler: { (alert) in
            self.authentication(login: false, email: email, pwd: pwd)
        })
        let cancel = UIAlertAction(title: "cancel".localized, style: .cancel, handler: { (alert) in
            if self.isDismiss {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.performSegue(withIdentifier: "reload", sender: self)
            }
        })
        alertView.addAction(action)
        alertView.addAction(cancel)
        self.present(alertView, animated: true, completion: nil)
    }
    
    func authentication(login: Bool, email: String, pwd: String ) {
        
        let authenticationContext = LAContext()
        
        authenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "deviceuser".localized, reply: { [unowned self] (success: Bool, error: Error? ) -> Void in
            if( success ) {
                if login {
                    if let dict = UserDefaults.standard.value(forKey: UserDefaults.Keys.loginDict) as? [String: String] {
                        if let pass = dict["password"], pass == "SocialLogin" {
                            var newDict: [String: Any] = ["email": dict["email"] ?? ""]
                            newDict["id_cart"] = UserDefaults.fetch(key: UserDefaults.Keys.id_cart)
                            self.makeRequest(call: WhichApiCall.social, email: "", newDict: newDict)
                        } else {
                            var newDict: [String: Any] = ["email": dict["email"] ?? "", "password": dict["password"] ?? ""]
                            newDict["id_cart"] = UserDefaults.fetch(key: UserDefaults.Keys.id_cart)
                            self.makeRequest(call: WhichApiCall.login, email: "", newDict: newDict)
                        }
                    }
                } else {
                    let dict = ["email": email, "password": pwd ]
                    UserDefaults.standard.set(dict, forKey: UserDefaults.Keys.loginDict)
                    UserDefaults.standard.synchronize()
                    if self.isDismiss {
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            self.performSegue(withIdentifier: "reload", sender: self)
                        })
                    }
                }
            } else {
                if !login {
                    // Check if there is an error
                    if let error = error {
                        let message = self.errorMessageForLAErrorCode(errorCode: (error._code))
                        self.showAlertViewAfterEvaluatingPolicyWithMessage(message: message)
                    }
                }
            }
        })
    }
    func showAlertViewAfterEvaluatingPolicyWithMessage( message: String ) {
        print(message)
        if self.isDismiss {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.performSegue(withIdentifier: "reload", sender: self)
        }
    }
    
    func errorMessageForLAErrorCode( errorCode: Int ) -> String {
        
        var message = ""
        
        switch errorCode {
            
        case LAError.appCancel.rawValue :
            message = "Authentication was cancelled by application"
            
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.touchIDLockout.rawValue:
            message = "Too many failed attempts."
            
        case LAError.touchIDNotAvailable.rawValue:
            message = "TouchID is not available on the device"
            
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
            
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = "Did not find error code on LAError object"
            
        }
        
        return message
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func googleClicked(_ sender: Any) {
    }
    
    @IBAction func fbClicked(_ sender: Any) {
        
    }
    
    @IBAction func twitterClicked(_ sender: Any) {
    }
    
}

extension SignInDataViewController {
    
    func SelectedTabrelload() {
        if self.tabBarController != nil {
            self.tabBarController?.selectedIndex = 0
        }
    }
    
}
