//
//  SignUpViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import Pastel
import FirebaseAnalytics

import ActionSheetPicker_3_0

class SignUpViewController: UIViewController, gdprSwitchProtocol {
    
    // MARK: - Gdpr Signup Act
    func gdprAct() {
        
    }
    
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var genderTxtfld: UITextField!
    @IBOutlet weak var newsletterSwitch: UISwitch!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var newsletterLabel: UILabel!
    @IBOutlet weak var specialOfferLabel: UILabel!
    @IBOutlet weak var lastNameField: UITextField!
    
    @IBOutlet weak var signuGdprHeight: NSLayoutConstraint!
    @IBOutlet weak var offerSwitch: UISwitch!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var dobField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signupGdprView: GdprView!
    
    var apiName = "AccountDetail"
    var signUpDict = [String: Any]()
    var controller: AllController!
    var gdprTrue = false
    var genderData = [GenderData]()
    var genderId = "1"
    var isDismiss = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signupGdprView.delegate = self
        
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.navigationChanges()
        self.genderTxtfld.delegate = self
        self.dobField.delegate = self
        loginButton.layer.borderColor = UIColor.white.cgColor
        loginButton.layer.borderWidth = 1.0
        self.stringsLocalized()
        if !AddonsEnabled.enable_optin {
            specialOfferLabel.isHidden = true
            offerSwitch.isHidden = true
        }
        genderLabel.isHidden = true
        genderTxtfld.isHidden = true
        dobLabel.isHidden = true
        dobField.isHidden = true
        passwordField.isSecureTextEntry = true
        makeRequest()
    }
    
    override func viewWillLayoutSubviews() {
        view.backgroundColor = UIColor.clear
        mainView.backgroundColor = UIColor.clear
        let pastelView = PastelView(frame: view.bounds)
        pastelView.startPastelPoint = .bottomLeft
        pastelView.endPastelPoint = .topRight
        // Custom Duration
        pastelView.animationDuration = 2.0
        // Custom Color
        pastelView.setColors([UIColor(red: 156/255, green: 39/255, blue: 176/255, alpha: 1.0),
                              UIColor(red: 255/255, green: 64/255, blue: 129/255, alpha: 1.0),
                              UIColor(red: 123/255, green: 31/255, blue: 162/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 76/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 32/255, green: 158/255, blue: 255/255, alpha: 1.0),
                              UIColor(red: 90/255, green: 120/255, blue: 127/255, alpha: 1.0),
                              UIColor(red: 58/255, green: 255/255, blue: 217/255, alpha: 1.0)])
        pastelView.startAnimation()
        view.insertSubview(pastelView, at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func stringsLocalized() {
        self.navigationItem.title = "signUp".localized
        genderLabel.text = "gender".localized
        firstNameLabel.text = "firstName".localized
        lastNameLabel.text = "lastName".localized
        emailLabel.text = "emailAddress".localized
        passwordLabel.text = "password".localized
        dobLabel.text = "dob".localized
        newsletterLabel.text = "recieveNewsletter".localized
        specialOfferLabel.text = "recieveOffer".localized
        loginButton.setTitle("wantLogin".localized, for: .normal)
        signUpButton.setTitle("signUp".localized, for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backPress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func applyGradient(colours: [UIColor]) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.view.bounds
        gradient.colors = colours.map { $0.cgColor }
        self.view.layer.insertSublayer(gradient, at: 0)
    }
    
    @IBAction func signUpClicked(_ sender: Any) {
        let someOptional: Bool = true
        apiName = ""
        switch someOptional {
//        case (genderData.count > 0 && ErrorChecking.isEmptyString(text: genderTxtfld.text)):
//            ErrorChecking.warningView(message: "genderValidate".localized)
//            genderTxtfld.shake()
        case ErrorChecking.isEmptyString(text: firstNameField.text):
            ErrorChecking.warningView(message: "firstNameValidate".localized)
            firstNameField.shake()
        case ErrorChecking.isEmptyString(text: lastNameField.text):
            ErrorChecking.warningView(message: "lastNameValidate".localized)
            lastNameField.shake()
        case ErrorChecking.isEmptyString(text: emailField.text):
            ErrorChecking.warningView(message: "emailEmptyValidate".localized)
            emailField.shake()
        case ErrorChecking.isValidEmail(testStr: emailField.text!):
            ErrorChecking.warningView(message: "emailValidate".localized)
            emailField.shake()
        case ErrorChecking.isEmptyString(text: passwordField.text):
            ErrorChecking.warningView(message: "passwordRequiredField".localized)
            passwordField.shake()
        default:
            signUpDict["id_gender"] = genderId
            signUpDict["active"] = "1"
            signUpDict["id_default_group"] = "3"
            signUpDict["firstname"] = firstNameField.text
            signUpDict["lastname"] = lastNameField.text
            signUpDict["email"] = emailField.text
            signUpDict["passwd"] = passwordField.text
            signUpDict["id_cart"] = UserDefaults.fetch(key: UserDefaults.Keys.id_cart)
            signUpDict["id_guest"] = UserDefaults.fetch(key: UserDefaults.Keys.id_guest)
            signUpDict["optin"] = offerSwitch.isOn ? "1" : "0"
            signUpDict["newsletter"] = newsletterSwitch.isOn ? "1" : "0"
            if self.gdprTrue {
                if self.signupGdprView.gdprSwitch.isOn {
                    self.makeRequest()
                } else {
                    ErrorChecking.warningView(message: "please check the terms and conditions".localized)
                }
            } else {
                self.makeRequest()
            }
        }
    }
    
    func isValidEmail(testStr: String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func makeRequest() {
        var params = ""
        var dict = [String: Any]()
        var verbs = RequestType.post
        if apiName == "AccountDetail" {
            params = "mobikul/getcustomerform?"
            dict = [:]
            verbs = .get
        } else {
            dict = ["savecustomer": signUpDict, "json": "1"] as [String: Any]
            params = "mobikul/savecustomer?"
            verbs = .post
        }
        NetworkManager.fetchData(params: params, verb: verbs, dict: dict, controller: AllController.home, view: self.view) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                print("loginError".localized, error!)
            } else {
                if self.apiName == "AccountDetail" {
                    if ErrorChecking.getData(data: responseObject) != JSON.null, let data = ErrorChecking.getData(data: responseObject) {
                        if responseObject!["consent_box"].stringValue != "" {
                            self.signupGdprView.gdprDataLbl.text = data["consent_box"].stringValue.htmlToString
                            self.gdprTrue = true
                            self.signuGdprHeight.constant = 70
                            self.signupGdprView.isHidden = false
                        } else {
                            self.signupGdprView.isHidden = true
                            self.gdprTrue = false
                            self.signuGdprHeight.constant = 0
                        }
                        if data["genders"]["gender"] != JSON.null {
                            self.genderLabel.isHidden = false
                            self.genderTxtfld.isHidden = false
                            if let data  = data["genders"]["gender"].array {
                                self.genderData = data.map { GenderData( data: $0) }
                            } else {
                                self.genderData.append(GenderData(data: data["genders"]["gender"]))
                            }
                        }
                        if data["display_dob_field"].stringValue == "1" {
                            self.dobLabel.isHidden = false
                            self.dobField.isHidden = false
                        }
                    }
                } else {
                    if ErrorChecking.getData(data: responseObject) != JSON.null, let data = ErrorChecking.getData(data: responseObject) {
                        let allData = data["response"]
                        UserDefaults.set(value: self.firstNameField.text!, key: UserDefaults.Keys.firstname)
                        UserDefaults.set(value: self.lastNameField.text!, key: UserDefaults.Keys.lastname)
                        UserDefaults.set(value: self.emailField.text!, key: UserDefaults.Keys.email)
                        Analytics.logEvent(AnalyticsEventSignUp, parameters: [AnalyticsParameterItemName: (String.init(format: "%@ %@", self.firstNameField.text!, self.lastNameField.text!)) as NSObject])
                        if allData[UserDefaults.Keys.id_customer].string != nil {
                            UserDefaults.set(value: allData[UserDefaults.Keys.id_customer].stringValue, key: UserDefaults.Keys.id_customer)
                        }
                        UserDefaults.set(value: "1", key: UserDefaults.Keys.isLoggedIn)
                        UserDefaults.standard.synchronize()
                        //self.performSegue(withIdentifier: "reload", sender: self)
                        if self.isDismiss {
                            self.dismiss(animated: true, completion: nil)
                        } else {
                            self.performSegue(withIdentifier: "reload", sender: self)
                        }
                    }
                }
            }
        }
    }
    
    @objc func myTargetFunction( _ textField: UITextField) {
        // user touch field
        print("hhh")
        self.dismiskeyboard()
        dobField.resignFirstResponder()
        dobField.endEditing(true)
        UIBarButtonItem.appearance().tintColor = UIColor.black
        textField.resignFirstResponder()
        
        let datePicker = ActionSheetDatePicker(title: "dateOfBirth".localized, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: { _, value, _ in
            if let value = value as? Date {
                let calendar = Calendar.autoupdatingCurrent
                let components = calendar.dateComponents([.hour, .minute, .day, .month, .year], from: value)
                let dateFormatter = DateFormatter()
                dateFormatter.timeStyle = DateFormatter.Style.none
                dateFormatter.dateStyle = DateFormatter.Style.full                
                self.dobField.text = dateFormatter.string(from: value)
                self.signUpDict["years"] = String(components.year ?? 0)
                self.signUpDict["months"] = String(components.month ?? 0 )
                self.signUpDict["days"] = String(components.day  ?? 0)
                UIBarButtonItem.appearance().tintColor = UIColor.white
            }
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: textField)
        datePicker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
        datePicker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
        datePicker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
        datePicker?.maximumDate = Date()
        datePicker?.show()
    }
    
    @objc func SelectGender( _ textField: UITextField) {
        self.dismiskeyboard()
        UIBarButtonItem.appearance().tintColor = UIColor.black
        let picker = ActionSheetStringPicker(title: "gender".localized, rows: genderData.map {$0.name!}, initialSelection: 0, doneBlock: { _, indexes, _ in
            self.genderTxtfld.text = self.genderData[indexes].name
            self.genderId = self.genderData[indexes].id
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: textField)
        picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
        picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
        picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
        picker?.show()
    }
    
    func dismiskeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func loginClicked(_ sender: Any) {
        if controller != nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            if let view = UIStoryboard.loadSignInViewController() {
                self.navigationController?.pushViewController(view, animated: true)
            }
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == genderTxtfld {
            SelectGender(textField)
        } else {
            myTargetFunction(textField)
        }
        return false
    }
}

struct GenderData {
    var name: String!
    var id: String!
    init(data: JSON) {
        name = data["name"].stringValue
        id = data["id"].stringValue
    }
}
