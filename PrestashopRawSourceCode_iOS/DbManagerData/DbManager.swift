//
//  DbManager.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import RealmSwift
import SwiftyJSON

class DBManager {
    private var database: Realm!
    static let sharedInstance = DBManager()
    private init() {
        do {
            database = try Realm()
        } catch {
            print("object not created")
        }
    }
    
    func getDataFromDB() ->   Results<Item> {
        let results: Results =   database.objects(Item.self)
        return results
    }
    func addData(object: Item) {
        do {
            try database.write {
                database.add(object, update: true)
                print("Added new object")
            }
        } catch {
            print("Not Added new object")
        }
    }
    
    func deleteAllFromDatabase() {
        do {
            try database.write {
                database.deleteAll()
            }
        } catch {
            print("Not deleted all")
        }
    }
    
    func deleteFromDb(object: Item) {
        do {
            try database.write {
                database.delete(object)
            }
        } catch {
            print("Not deleted")
        }
    }
    
    func fetchData(key: String) -> JSON? {
        do {
            let realm = try Realm()
            let results =   realm.object(ofType: Item.self, forPrimaryKey: key)
            if let stringData = results?.textString {
                let data = stringData.data(using: .utf8)
                let jsonData = try JSON.init(data: data!)
                if jsonData != JSON.null {
                    return jsonData
                } else {
                    return nil
                }
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
}

@objcMembers
class Item: Object {
    dynamic var ID = ""
    dynamic var textString = ""
    override static func primaryKey() -> String? {
        return "ID"
    }
}
