//
//  ProfileController.swift
//  Odoo application
//
//  Created by vipin sahu on 9/5/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit
//import SVProgressHUD
import SwiftyJSON
import Kingfisher
import Alamofire
import SVProgressHUD

class ProfileController: UIViewController {
    
    @IBOutlet weak var profileTableView: UITableView!
    
    var height = SCREEN_WIDTH / 2
    var headerView: ProfileHeader?
    let gradient: CAGradientLayer = CAGradientLayer()
    var profileArray = [String]()
    var profileImageArray = [UIImage]()
    var cmsImageArray = [UIImage]()
    var cmsArray = [String]()
    var cmsArrayCode = [String]()
    var sectionCount = 0
    var removeHeader = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationItem.title = "profile".localized
        self.navigationController?.navigationBar.navigationChanges()
        profileTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    func setHeader () {
        headerView = (Bundle.main.loadNibNamed("ProfileHeader", owner: self, options: nil)?[0] as? ProfileHeader)!
        headerView?.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_WIDTH/2)
        headerView?.passDataDelegate = self
        profileTableView.estimatedRowHeight = UITableView.automaticDimension
        profileTableView.tableHeaderView = headerView
        profileTableView.tableHeaderView = nil
        profileTableView.contentInset  = UIEdgeInsets(top: SCREEN_WIDTH/2, left: 0, bottom: 0, right: 0)
        profileTableView.estimatedSectionHeaderHeight = 40.0
        self.blur()
        profileTableView.addSubview(headerView!)
    }
    
    func HeaderDataSet () {
        if UserDefaults.standard.value(forKey: UserDefaults.Keys.email) != nil {
            headerView?.emailLabel.text = UserDefaults.standard.value(forKey: UserDefaults.Keys.email) as? String
        }
        if UserDefaults.standard.value(forKey: UserDefaults.Keys.firstname) != nil {
            headerView?.nameLabel.text = String.init(format: "%@ %@", UserDefaults.fetch(key: UserDefaults.Keys.firstname)!, UserDefaults.fetch(key: UserDefaults.Keys.lastname)! )
        }
        if let urls = UserDefaults.fetch(key: UserDefaults.Keys.customer_profile_image) {
            self.headerView?.profileImage.setImage(imageUrl: urls)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = "profile".localized
        if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        if  self.tabBarController != nil {
            self.tabBarController?.setCartCount(value: FetchDetails.fetchCartCount())
        }
        self.setData()
        removeHeader = false
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillLayoutSubviews() {
        if !removeHeader {
            self.tabBarController?.tabBar.isHidden = false
            removeHeader = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeHeader = true
        headerView?.removeFromSuperview()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func setData() {
        profileArray.removeAll()
        profileImageArray.removeAll()
        cmsImageArray.removeAll()
        cmsArray.removeAll()
        if FetchDetails.isLoggedIn() {
            profileTableView.contentInset  = UIEdgeInsets(top: SCREEN_WIDTH/2, left: 0, bottom: 0, right: 0)
            profileArray.append("myAccount".localized)
            profileImageArray.append(#imageLiteral(resourceName: "profile-user"))
            if let urls = UserDefaults.fetch(key: UserDefaults.Keys.customer_profile_image) {
                self.headerView?.profileImage.setImage(imageUrl: urls)
            }
            sectionCount = 3
        } else if FetchDetails.isLoggedInAdmin() {
            profileTableView.contentInset  = UIEdgeInsets(top: SCREEN_WIDTH/2, left: 0, bottom: 0, right: 0)
            profileArray.append("chatwithseller".localized)
            profileImageArray.append(#imageLiteral(resourceName: "profile-user"))
            if let urls = UserDefaults.fetch(key: UserDefaults.Keys.customer_profile_image) {
                self.headerView?.profileImage.setImage(imageUrl: urls)
            }
            sectionCount = 3
        } else {
            headerView?.removeFromSuperview()
            headerView?.isHidden = true
            profileTableView.contentInset  = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
            profileArray.append("signIn".localized)
            profileImageArray.append(#imageLiteral(resourceName: "profile-user"))
            sectionCount = 2
        }
        
        profileArray.append("currency".localized)
        profileImageArray.append(#imageLiteral(resourceName: "profile-currencty"))
        profileArray.append("language".localized)
        profileImageArray.append(#imageLiteral(resourceName: "profile-language"))
        profileArray.append("notifications".localized)
        profileImageArray.append(#imageLiteral(resourceName: "profile-notificaiton"))
        if GlobalConstants.AppCredentials.environment == EnvironmentType.marketplace {
            profileArray.append("marketPlace".localized)
            profileImageArray.append(#imageLiteral(resourceName: "seller-store"))
        }
        cmsArrayCode.removeAll()
        
        let array = GlobalConstants.StoreData.cmsArray
        
        if array.contains(CmsControllers.aboutUs.rawValue) {
            cmsArray.append("aboutUs".localized)
            cmsImageArray.append(#imageLiteral(resourceName: "profile-terms&conditon"))
            cmsArrayCode.append(CmsControllers.aboutUs.rawValue)
        }
        if array.contains(CmsControllers.termsCondition.rawValue) {
            cmsArray.append("termsCondition".localized)
            cmsImageArray.append(#imageLiteral(resourceName: "profile-terms&conditon"))
            cmsArrayCode.append(CmsControllers.termsCondition.rawValue)
        }
        if array.contains(CmsControllers.support.rawValue) {
            cmsArray.append("support".localized)
            cmsImageArray.append(#imageLiteral(resourceName: "profile-support"))
            cmsArrayCode.append(CmsControllers.support.rawValue)
        }
        if array.contains(CmsControllers.privacyPolicy.rawValue) {
            cmsArray.append("privacyPolicy".localized)
            cmsImageArray.append(#imageLiteral(resourceName: "profile-privacy-policy"))
            cmsArrayCode.append(CmsControllers.privacyPolicy.rawValue)
        }
        cmsArray.append("contactUs".localized)
        cmsImageArray.append(#imageLiteral(resourceName: "profile-contact"))
        
        profileTableView.reloadData()
        
        if FetchDetails.isLoggedIn() || FetchDetails.isLoggedInAdmin() {
            self.setHeader()
            
            self.HeaderDataSet()
            profileTableView.setContentOffset(CGPoint(x: 0, y: -SCREEN_WIDTH), animated: true)
            let  indexPath1 = IndexPath.init(row: 0, section: 0)
            profileTableView.scrollToRow(at: indexPath1, at: .top, animated: true)
            
        } else {
            headerView?.removeFromSuperview()
            let  indexPath1 = IndexPath.init(row: 0, section: 0)
            profileTableView.scrollToRow(at: indexPath1, at: .top, animated: true)
            
        }
        self.profileTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

enum Customer {
    case dashboard
    case account
    case address
    case order
    case wishlist
}

extension ProfileController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            
            return profileArray.count
        case 1:
            return cmsArray.count
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                cell.textLabel?.textAlignment = .right
            } else {
                cell.textLabel?.textAlignment = .left
            }
            cell.imageView?.image = profileImageArray[indexPath.row]
            cell.imageView?.backgroundColor = GlobalConstants.Colors.primaryColor
            
            cell.imageView?.layer.cornerRadius = 14
            cell.textLabel?.text = profileArray[indexPath.row]
            cell.accessoryType = .disclosureIndicator
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                cell.textLabel?.textAlignment = .right
            } else {
                cell.textLabel?.textAlignment = .left
            }
            return cell
        case 1:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                cell.textLabel?.textAlignment = .right
            } else {
                cell.textLabel?.textAlignment = .left
            }
            cell.imageView?.image = cmsImageArray[indexPath.row]
            cell.imageView?.backgroundColor = GlobalConstants.Colors.primaryColor
            
            cell.imageView?.layer.cornerRadius = 14
            cell.textLabel?.text = cmsArray[indexPath.row]
            cell.accessoryType = .disclosureIndicator
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                cell.textLabel?.textAlignment = .right
            } else {
                cell.textLabel?.textAlignment = .left
            }
            return cell
        case 2:
            let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                cell.textLabel?.textAlignment = .right
            } else {
                cell.textLabel?.textAlignment = .left
            }
            cell.imageView?.image = #imageLiteral(resourceName: "profile-logout")
            cell.imageView?.backgroundColor = GlobalConstants.Colors.primaryColor
            cell.imageView?.layer.cornerRadius = 14
            cell.textLabel?.text = "signOut".localized
            if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                cell.textLabel?.textAlignment = .right
            } else {
                cell.textLabel?.textAlignment = .left
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                if FetchDetails.isLoggedIn() {
                    if let view = UIStoryboard.loadMyAccountViewController() {
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                } else if FetchDetails.isLoggedInAdmin() {
                    if let view = UIStoryboard.loadChatWithSellerViewController() {
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                } else {
                    if let view = UIStoryboard.loadSignInViewController() {
                        let nav  = UINavigationController(rootViewController: view)
                        self.present(nav, animated: true, completion: nil)
                    }
                }
            case 1:
                if let view = UIStoryboard.loadCurrencyViewController() {
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case 2:
                if let view = UIStoryboard.loadLanguageViewController() {
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case 3:
                if let view = UIStoryboard.loadNotificationViewController() {
                    self.navigationController?.pushViewController(view, animated: true)
                }
            case 4:
                if let view = UIStoryboard.loadSellerListViewController() {
                    self.navigationController?.pushViewController(view, animated: true)
                }
            default:
                print()
            }
        } else if indexPath.section == 1 {
            if indexPath.row < cmsArrayCode.count {
                switch cmsArrayCode[indexPath.row] {
                case CmsControllers.aboutUs.rawValue:
                    if let view = UIStoryboard.loadCmsViewController() {
                        view.controller = CmsControllers.aboutUs
                        view.titleName = cmsArray[indexPath.row]
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                case CmsControllers.termsCondition.rawValue:
                    if let view = UIStoryboard.loadCmsViewController() {
                        view.controller = CmsControllers.termsCondition
                        view.titleName = cmsArray[indexPath.row]
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                case CmsControllers.support.rawValue:
                    if let view = UIStoryboard.loadCmsViewController() {
                        view.controller = CmsControllers.support
                        view.titleName = cmsArray[indexPath.row]
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                case CmsControllers.privacyPolicy.rawValue:
                    if let view = UIStoryboard.loadCmsViewController() {
                        view.controller = CmsControllers.privacyPolicy
                        view.titleName = cmsArray[indexPath.row]
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                default:
                    break
                }
            } else {
                if let view = UIStoryboard.loadContactUsViewController() {
                    self.navigationController?.pushViewController(view, animated: true)
                }
            }
        } else if indexPath.section == 2 {
            for key in UserDefaults.standard.dictionaryRepresentation().keys {
                if key == UserDefaults.Keys.languageCode || key == UserDefaults.Keys.currencyCode || key == UserDefaults.Keys.profile_picture_upload_url || key == AppLanguageKey || key == UserDefaults.Keys.loginDict || key == UserDefaults.Keys.firebaseKey {
                } else {
                    UserDefaults.standard.removeObject(forKey: key.description)
                }
            }
            if  self.tabBarController != nil {
                self.tabBarController?.setCartCount(value: FetchDetails.fetchCartCount())
            }
            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
            rootviewcontroller.rootViewController = UIStoryboard.loadRootViewController()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            if FetchDetails.isLoggedIn() || FetchDetails.isLoggedInAdmin() {
                return UITableView.automaticDimension
            } else {
                return 44
            }
        case 1:
            return 44
        case 2:
            return 44
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.profileTableView.reloadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if headerView != nil  && (FetchDetails.isLoggedIn() || FetchDetails.isLoggedInAdmin()) {
            let yPos: CGFloat = -scrollView.contentOffset.y
            if yPos > 0 {
                var imgRect: CGRect? = headerView?.frame
                imgRect?.origin.y = scrollView.contentOffset.y
                imgRect?.size.height = SCREEN_WIDTH/2 + yPos  - SCREEN_WIDTH/2
                headerView?.frame = imgRect!
                self.profileTableView.sectionHeaderHeight = (imgRect?.size.height)!
            }
        }
    }
}

extension ProfileController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        let alert = UIAlertController(title: "upload".localized, message: nil, preferredStyle: .actionSheet)
        let takeAction = UIAlertAction(title: "takePhoto".localized, style: .default, handler: take)
        let upload = UIAlertAction(title: "uploadFromLibrary".localized, style: .default, handler: uploadImage)
        let CancelAction = UIAlertAction(title: "cancel".localized, style: .cancel, handler: cancel)
        alert.addAction(takeAction)
        alert.addAction(upload)
        alert.addAction(CancelAction)
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        self.present(alert, animated: true, completion: nil)
    }
    
    func take(alertAction: UIAlertAction!) {
        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.allowsEditing = false
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    
    func uploadImage(alertAction: UIAlertAction!) {
        
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = false
        present(picker, animated: true, completion: nil)
        
    }
    
    func cancel(alertAction: UIAlertAction!) {
        
    }
    
    func blur() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.backgroundColor = GlobalConstants.Colors.primaryColor.withAlphaComponent(0.3)
        blurEffectView.frame = (headerView?.bannerImage.bounds)!
        blurEffectView.alpha = 0.9
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        headerView?.bannerImage!.addSubview(blurEffectView)
    }
}

extension ProfileController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            let imageData = image.jpegData(compressionQuality: 0.7) as Data?
            self.upload(data: imageData!)
        } else {
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func upload(data: Data) {
        // You can change your image name here, i use NSURL image and convert into string
        SVProgressHUD.show(withStatus: "loading".localized)
        var fileUrl = UserDefaults.fetch(key: UserDefaults.Keys.profile_picture_upload_url)
        fileUrl = fileUrl?.removingPercentEncoding
        fileUrl?.append("&outputformat=json")
        print(fileUrl ?? "")
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append((UserDefaults.fetch(key: UserDefaults.Keys.id_customer)?.data(using: String.Encoding.utf8)!)!, withName: "id_customer")
            multipartFormData.append(data, withName: "profile", fileName: "filename.png", mimeType: "image/jpeg")
        }, usingThreshold: UInt64.init(), to: fileUrl!, method: .post, headers: headers) { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    SVProgressHUD.dismiss()
                    print(String(data: response.data!, encoding: String.Encoding.utf8)!)
                    let jsonData = try? JSON(data: response.data!)
                    print(jsonData ?? "")
                    if ErrorChecking.getData(data: jsonData) != JSON.null, let data = ErrorChecking.getData(data: jsonData) {
                        self.headerView?.profileImage.setImage(imageUrl: data["response"]["file_name"].stringValue)
                        UserDefaults.set(value: data["response"]["file_name"].stringValue, key: UserDefaults.Keys.customer_profile_image)
                    } else {
                        ErrorChecking.warningView(message: "someerroroccur".localized)
                    }
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
private func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
