//
//  ProfileHeader.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class ProfileHeader: UIView {

    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var editProfileView: UIButton!
    var passDataDelegate: PassData?
//    var delegate : NewViewControllerOpen?

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
        super.awakeFromNib()
        profileImage.isUserInteractionEnabled = true
        editProfileView.layer.borderWidth = 2
        editProfileView.layer.borderColor = UIColor.white.cgColor
        profileView.backgroundColor = UIColor.clear
        editProfileView.isHidden = true

//        profileView.alpha = 0.1
//      mainView.backgroundColor = UIColor.clear

    }

    @IBAction func changeImageTapped(_ sender: UITapGestureRecognizer) {
        passDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.uploadImage)
//        passDataDelegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiUse.uploadImage)

    }

    @IBAction func EditProfileClicked(_ sender: UIButton) {
        print("ghhhh")
//        delegate?.moveToController(id: "", name: "", dict: JSON.null, Controller: Controllers.accountInfo)
    }

}
