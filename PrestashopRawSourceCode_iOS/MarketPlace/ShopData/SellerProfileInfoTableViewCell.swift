//
//  SellerProfileInfoTableViewCell.swift
//  Odoo Marketplace
//
//  Created by bhavuk.chawla on 13/02/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import UIKit

class SellerProfileInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var bannerImageHeight: NSLayoutConstraint!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var starView: UIView!
    @IBOutlet weak var reviewsLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var sellerEmail: UILabel!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var sellerImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bannerImageHeight.constant = SCREEN_WIDTH / 2
        reviewsLabel.SetDefaultTextColor()
        sellerEmail.SetDefaultTextColor()
        starView.applyGradientToZeroLayer(colours: GlobalConstants.Colors.reviewGradient)

        sellerImage.layer.masksToBounds = true
        sellerImage.layer.cornerRadius = 4
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: SellerInfo! {
        didSet {
            reviewsLabel.text = String.init(format: "%@ %@", item.total_review, "reviews".localized)
            sellerName.text = item.seller_name
            sellerEmail.text = item.business_email
            ratingLabel.text = String(item.avg_rating)
            bannerImage.setImage(imageUrl: item.shop_banner)
            sellerImage.setImage(imageUrl: item.shop_image)
//            bannerImage.setImage(imageUrl: item.shop_banner, controller: Controllers.sellerBanner)
//            sellerImage.setImage(imageUrl: item.seller_profile_image, controller: Controllers.Profile)
        }
    }

}
