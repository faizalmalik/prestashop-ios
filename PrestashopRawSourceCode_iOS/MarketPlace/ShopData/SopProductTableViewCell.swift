//
//  SopProductTableViewCell.swift
//  Prestashop_Mobikul_Marketplace
//
//  Created by bhavuk.chawla on 16/07/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import UIKit
import SwiftyJSON

class SopProductTableViewCell: UITableViewCell {
    var products = [ProductData]()
    
    @IBOutlet weak var homeProductCollection: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var button: UIButton!
    
    var delegate: PassData?
    var slider_mode: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        button.clipsToBounds = true
        //        button.applyShadow(colours: GlobalData.Credentials.setBorderColor, locations: nil)
        button.SetAccentBackColor()
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        //        button.applyShadowWithoutBorder(colours: GlobalData.Credentials.setBorderColor, locations: nil)
        button.setTitle("viewAll".localized, for: .normal)
        homeProductCollection.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        homeProductCollection.backgroundColor = UIColor.clear
        homeProductCollection.delegate = self
        homeProductCollection.dataSource = self
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    @IBAction func ViewAllPress(_ sender: UIButton) {
        delegate?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.ViewAllProductBlock)
    }
    
}

extension SopProductTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath as IndexPath) as? HomeCollectionViewCell {
            cell.setNeedsLayout()
            
            cell.categoryImage.image = nil
            self.collectionHeight.constant = self.homeProductCollection.contentSize.height
            cell.cellView.layer.cornerRadius = 7
            cell.item = products[indexPath.row]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SCREEN_WIDTH / 2.5 - 8, height: SCREEN_WIDTH / 2.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.passData(id: products[indexPath.row].id_product, name: products[indexPath.row].name, dict: [:], jsonData: JSON.null, index: 0, call: WhichApiCall.loadMoreProduct)
    }
    
    func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        homeProductCollection?.collectionViewLayout.invalidateLayout()
    }
}
