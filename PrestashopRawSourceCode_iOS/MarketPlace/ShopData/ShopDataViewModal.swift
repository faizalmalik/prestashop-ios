//
//  ShopDataViewModal.swift
//  Prestashop_Mobikul_Marketplace
//
//  Created by bhavuk.chawla on 16/07/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import Foundation
import SwiftyJSON

class ShopDataViewModal: NSObject {
    
    var passDataDelegate: PassData?
    var items = [ShopViewModalItem]()
    var sellerInfo: SellerInfo!
    var moveControllerDelegate: MoveController?
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = ShopDataModal(data: jsonData) else {
            return
        }
        items.removeAll()
        
        if data.sellerInfo != nil {
            sellerInfo = data.sellerInfo
            let array = ["sellerDesc".localized, "contactSeller".localized]
            items.append(ShopViewModalInfoItem(sellerInfo: data.sellerInfo))
            if data.products.count > 0 {
                items.append(ShopViewModalProductsItem(product: data.products, head: "recentSellerProduct".localized))
            }
            items.append(ShopViewModalOtherDetailItem(array: array, description: sellerInfo.about_shop))
            completion(true)
        } else {
            completion(false)
        }
        
    }
    
}

extension ShopDataViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        
        switch item.type {
        case .profile:
            if let item = item as? ShopViewModalInfoItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: SellerProfileInfoTableViewCell.identifier) as? SellerProfileInfoTableViewCell {
                    cell.item = item.sellerInfo
                    cell.selectionStyle = .none
                    return cell
                }
            }
        case .products:
            if let item = item as? ShopViewModalProductsItem {
                if let cell = tableView.dequeueReusableCell(withIdentifier: SopProductTableViewCell.identifier) as? SopProductTableViewCell {
                    if let layout =  cell.homeProductCollection.collectionViewLayout as? UICollectionViewFlowLayout {
                        layout.scrollDirection = .horizontal
                        cell.collectionHeight.constant = SCREEN_WIDTH/2.5 + 10
                    }
                    cell.heading.text = item.heading
                    cell.homeProductCollection.tag = indexPath.section
                    cell.products = item.product
                    cell.delegate = self
                    return cell
                }
            }
        case .other:
            if let item = item as? ShopViewModalOtherDetailItem {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
                if UserDefaults.standard.string(forKey: AppLanguageKey) == "ar" {
                    cell.textLabel?.textAlignment = .right
                } else {
                    cell.textLabel?.textAlignment = .left
                }
                cell.textLabel?.text = item.array[indexPath.row]
                cell.accessoryType = .disclosureIndicator
                return cell
            }            
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.section]
        switch item.type {
        case .other:
            //            if let item = item as? ShopViewModalOtherDetailItem {
            switch indexPath.row {
            case 0:
                moveControllerDelegate?.moveController(id: sellerInfo.about_shop, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.cmsController)
            case 1:
                moveControllerDelegate?.moveController(id: sellerInfo.about_shop, name: "", dict: [:], jsonData: JSON.null, index: 0, controller: AllController.contactSellerDataViewController)
                
            default:
                break
            }
            //            }
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        return UITableViewAutomaticDimension
        let item = items[indexPath.section]
        switch item.type {
        case .products:
            return SCREEN_WIDTH / 2.5 + 55
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let item = items[section]
        switch item.type {
        case .other:
            return "AboutSeller".localized
        default:
            return nil
        }
    }
    
}

extension ShopDataViewModal: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        passDataDelegate?.passData(id: id, name: name, dict: dict, jsonData: jsonData, index: index, call: call)
    }
}

protocol ShopViewModalItem {
    var type: ShopType { get }
    var rowCount: Int { get }
}

enum ShopType {
    case profile
    case products
    case other
}

class ShopViewModalInfoItem: ShopViewModalItem {
    var type: ShopType {
        return .profile
    }
    var rowCount: Int {
        return 1
    }
    var sellerInfo: SellerInfo!
    
    init(sellerInfo: SellerInfo) {
        self.sellerInfo = sellerInfo
    }
}

class ShopViewModalProductsItem: ShopViewModalItem {
    
    var type: ShopType {
        return .products
    }
    var rowCount: Int {
        return 1
    }
    
    var heading: String
    
    var product: [ProductData]
    
    init(product: [ProductData], head: String ) {
        self.product = product
        self.heading = head
        
    }
}

class ShopViewModalOtherDetailItem: ShopViewModalItem {
    
    var type: ShopType {
        return .other
    }
    var rowCount: Int {
        return array.count
    }
    
    var description: String
    
    var array: [String]
    
    init(array: [String], description: String) {
        self.array = array
        self.description = description
        
    }
}
