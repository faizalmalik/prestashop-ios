//
//  ShopDataModal.swift
//  Prestashop_Mobikul_Marketplace
//
//  Created by bhavuk.chawla on 16/07/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ShopDataModal {
    var sellerInfo: SellerInfo!
     let productPath = ["products", "allproducts", "product"]
    var products = [ProductData]()
    
    init?(data: JSON) {
     
        sellerInfo = SellerInfo(data: data["about_seller"])
        if data[productPath] != JSON.null {
            if let data  = data[productPath].array {
                self.products = data.map { ProductData( data: $0) }
            } else {
                self.products.append( ProductData( data: data[productPath] ))
            }
        }
        
    }
}

struct SellerInfo {
    var id_seller: String!
    var seller_customer_id: String!
    var seller_image: String!
    var shop_image: String!
    var shop_banner: String!
    var profile_banner: String!
    var about_shop: String!
    var seller_name: String!
    var business_email: String!
    var phone: String!
    var facebook_id: String!
    var fax: String!
    var address: String!
    var total_review: String!
    var avg_rating: Float!
 
    init(data: JSON) {
        id_seller = data["id_seller"].stringValue
        seller_customer_id = data["seller_customer_id"].stringValue
        seller_image = data["seller_image"].stringValue
        shop_image = data["shop_image"].stringValue
        shop_banner = data["shop_banner"].stringValue
        profile_banner = data["profile_banner"].stringValue
        avg_rating = data["avg_rating"].floatValue
        address = data["address"].stringValue
        fax = data["fax"].stringValue
        facebook_id = data["facebook_id"].stringValue
        phone = data["phone"].stringValue
        business_email = data["business_email"].stringValue
        seller_name = data["seller_name"].stringValue
        about_shop = data["about_shop"].stringValue
        total_review = data["total_review"].stringValue
        
    }
}
