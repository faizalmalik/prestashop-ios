//
//  HomeCollectionViewCell.swift
//  CS-Cart MVVM
//
//  Created by vipin sahu on 6/10/17.
//  Copyright © 2017 Webkul. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productOldPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.productOldPrice.text = ""
        self.productOldPrice.SetDefaultTextColor()
        cellView.shadowBorder()
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: ProductData? {
        didSet {

            guard let item = item else {
                return
            }

            if item.show_price {
                self.productPrice.isHidden = false
            } else {
                self.productPrice.isHidden = true
            }
            
            productName.text = item.name
            productPrice.text = item.price
            productImage.contentMode = .scaleAspectFit
            productImage.setImage(imageUrl: item.image_link!)

        }
    }

}
