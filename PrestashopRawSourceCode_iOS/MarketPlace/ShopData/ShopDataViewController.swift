//
//  ShopDataViewController.swift
//  Prestashop_Mobikul_Marketplace
//
//  Created by bhavuk.chawla on 16/07/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import UIKit
import SwiftyJSON

class ShopDataViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var custId: String!
    var name: String!
    var seller_customer_id: String!
    
    fileprivate let ShopDataViewModalObject = ShopDataViewModal()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.title = self.name
        tableView.register(SellerProfileInfoTableViewCell.nib, forCellReuseIdentifier: SellerProfileInfoTableViewCell.identifier)
        tableView.register(SopProductTableViewCell.nib, forCellReuseIdentifier: SopProductTableViewCell.identifier)
        tableView.tableFooterView = UIView()
        ShopDataViewModalObject.passDataDelegate = self
        ShopDataViewModalObject.moveControllerDelegate = self
        self.makeRequest(dict: [:], call: WhichApiCall.none)
        // Do any additional setup after loading the view.
    }
    
    func makeRequest( dict: [String: Any], call: WhichApiCall ) {
        var params = ""
        let newDict = [String: Any]()
        
        let verbs: RequestType = .get
        params =  GlobalConstants.MarketPlaceApis.getSellerCollection
        params += UrlParameters.width
        if custId != nil {
            params += "&id_seller=" + custId
        } else if let sellerId = UrlParameters.fetchSellerID() {
            params += sellerId
        }
        params += "&page=" + "1"
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.productCategory, view: self.view) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                print(error)
            } else {
                self.ShopDataViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    print(data)
                    if data {
                        self.tableView.delegate = self.ShopDataViewModalObject
                        self.tableView.dataSource = self.ShopDataViewModalObject
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    } else {
                        self.tableView.isHidden = true
                    }
                }
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ShopDataViewController: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        switch call {
        case .ViewAllProductBlock:
            if let view = UIStoryboard.loadProductCategoryViewController() {
                view.controller = AllController.sellerCollection
                view.name = ShopDataViewModalObject.sellerInfo.seller_name
                view.custId = ShopDataViewModalObject.sellerInfo.id_seller
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .loadMoreProduct:
            if let view = UIStoryboard.loadProductViewController() {
                view.productId = id
                view.productName = name
                view.productDict = dict
                view.selectedIndex = index
                self.navigationController?.pushViewController(view, animated: true)
            }
        default:
            break
        }
        
    }
}

extension ShopDataViewController: MoveController {
    
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        switch controller {
        case .cmsController:
            if let view = UIStoryboard.loadDescriptionViewController() {
                view.htmlData = id
                self.navigationController?.pushViewController(view, animated: true)
            }
        case .contactSellerDataViewController:
            if let view = UIStoryboard.loadContactSellerDataViewController() {
                if seller_customer_id != nil {
                    view.custId = seller_customer_id
                } else {
                    view.custId = self.ShopDataViewModalObject.sellerInfo.seller_customer_id
                }
                self.navigationController?.pushViewController(view, animated: true)
            }
        default:
            break
        }
    }
}
