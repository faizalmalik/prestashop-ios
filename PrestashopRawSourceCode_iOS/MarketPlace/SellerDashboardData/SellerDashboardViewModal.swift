//
//  SellerDashboardViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

class SellerDashboardViewModal: NSObject {
    
    var moveController: MoveController?
    
    var orders = [OrderHistoryData]()
    var graphArray = [GraphData]()
    var sectionCount = 0
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = SellerDashboardModal(data: jsonData) else {
            return
        }
        
        if !data.orders.isEmpty {
            orders = data.orders
            graphArray = data.graphArray
            sectionCount = 2
            completion(true)
        } else {
            completion(false)
        }
        
    }
}

extension SellerDashboardViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: SellerDashboardGraphTableViewCell.identifier) as? SellerDashboardGraphTableViewCell {
                cell.selectionStyle = .none
                cell.separatorInset = .zero
                print(graphArray)
                cell.item = graphArray
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: SellerOrderTableViewCell.identifier) as? SellerOrderTableViewCell {
                cell.item = orders[indexPath.row]
                cell.selectionStyle = .none
                cell.moveController = self
                cell.separatorInset = .zero
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return orders.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "graphData".localized
        } else {
            return "orders".localized
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 200
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            moveController?.moveController(id: orders[indexPath.row].id!, name: orders[indexPath.row].reference!, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.orderDetail)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionCount
    }
    
}

extension SellerDashboardViewModal: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        moveController?.moveController(id: id, name: name, dict: dict, jsonData: jsonData, index: index, controller: controller)
    }
}
