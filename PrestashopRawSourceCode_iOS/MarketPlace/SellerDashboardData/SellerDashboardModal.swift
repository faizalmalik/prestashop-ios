//
//  SellerDashboardModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import Foundation
import SwiftyJSON

struct SellerDashboardModal {

    var orders = [OrderHistoryData]()
       var graphArray = [GraphData]()
    private let orderPath = ["orders", "order"]
     private let graphPath = ["graph_data", "data"]

    init?(data: JSON) {
        if data[orderPath] != JSON.null {
            if let data  = data[orderPath].array {
                self.orders = data.map { OrderHistoryData( data: $0) }
            } else {
                self.orders.append(OrderHistoryData(data: data[orderPath]))
            }
        }
        if data[graphPath] != JSON.null {
            if let data  = data[graphPath].array {
                self.graphArray = data.map { GraphData( data: $0) }
            } else {
                self.graphArray.append(GraphData(data: data[graphPath]))
            }
        }
    }
}

struct GraphData {
    var date_add: String!
    var total_paid: Float!
    init(data: JSON) {
        self.date_add = data["date_add"].stringValue
        self.total_paid = data["total_paid"].floatValue
    }
}
