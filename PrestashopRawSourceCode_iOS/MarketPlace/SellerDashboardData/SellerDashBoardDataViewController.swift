//
//  SellerDashBoardDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class SellerDashBoardDataViewController: UIViewController {
    
    var title1: String!
    @IBOutlet weak var sellerDashboardTablew: UITableView!
    fileprivate let SellerDashboardViewModalObject = SellerDashboardViewModal()
    var emptyView: EmptyView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.title = title1
        // Do any additional setup after loading the view.
        
        sellerDashboardTablew.delegate = SellerDashboardViewModalObject
        sellerDashboardTablew.dataSource = SellerDashboardViewModalObject
        SellerDashboardViewModalObject.moveController = self
        self.navigationItem.title = "sellerDashboard".localized
        sellerDashboardTablew.register(SellerDashboardGraphTableViewCell.nib, forCellReuseIdentifier: SellerDashboardGraphTableViewCell.identifier)
        sellerDashboardTablew.register(SellerOrderTableViewCell.nib, forCellReuseIdentifier: SellerOrderTableViewCell.identifier)
        
        self.makeRequest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeRequest() {
        var params = ""
        params += GlobalConstants.MarketPlaceApis.sellerDashBoard
        params += UrlParameters.width
        
        if let custId = UrlParameters.fetchCustomerId() {
            params += custId
        }
        if let sellID = UrlParameters.fetchSellerID() {
            params += sellID    
        }
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.sellerDashboard, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                
                self.SellerDashboardViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    if data {
                        
                        self.sellerDashboardTablew.reloadData()
                    } else {
                        
                    }
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SellerDashBoardDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        if let view = UIStoryboard.loadOrderdetailViewController() {
            view.id = id
            view.reference = name
            
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
}

extension SellerDashBoardDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest()
    }
}
