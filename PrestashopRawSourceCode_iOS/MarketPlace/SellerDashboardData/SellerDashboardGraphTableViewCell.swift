//
//  SellerDashboardGraphTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import FSLineChart

class SellerDashboardGraphTableViewCell: UITableViewCell {

    @IBOutlet weak var swiftChart: FSLineChart!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var series = [Float]()
    var item: [GraphData]! {
        didSet {
            self.series.removeAll()
            print(item)
            for i in 0..<item.count {
                print(item[i].total_paid)
//                self.series.append(Float(Int(item[i].total_paid) ?? 0))
                 self.series.append(item[i].total_paid)

            }

              print(series)
            // Generate some dummy data

            swiftChart.clearData()
            swiftChart.verticalGridStep = 5
            swiftChart.horizontalGridStep = Int32(item.count)
            swiftChart.labelForIndex = {index in "\(index)" }
//          swiftChart.labelForIndex = { index in "\(self.series[index]) + \(index)" }
            swiftChart.labelForValue = { "\($0)" }
            swiftChart.setChartData(series)
//            swiftChart.

        }
    }

}

extension String {
    var floatValue: Float {
        return (self).floatValue
    }
}
