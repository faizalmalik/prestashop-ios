//
//  SellerOrderTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class SellerOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var placedOnLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var shipToLabel: UILabel!
   
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!

    var moveController: MoveController?

    override func awakeFromNib() {
        super.awakeFromNib()
        placedOnLabel.textColor = GlobalConstants.Colors.defaultTextColor

        shipToLabel.textColor = GlobalConstants.Colors.defaultTextColor
    
        // Initialization code
        orderView.backgroundColor = UIColor.white
        statusLabel.layer.masksToBounds = true
        statusLabel.layer.cornerRadius = 3

    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    var item: OrderHistoryData? {
        didSet {
            orderIdLabel.text = String.init(format: "%@ #%@", "order".localized, (item?.id)!)
            statusLabel.text = item?.current_state_name
            placedOnLabel.text = "placedOn".localized
            dateLabel.text = item?.date_upd
            addressLabel.text = item?.payment
            shipToLabel.text = "payment".localized
            totalLabel.text = String.init(format: "%@ - %@", "orderTotalWithoutColon".localized, (item?.total_paid)!)

            totalLabel.halfTextColorChange(fullText: totalLabel.text!, changeText: "orderTotalWithoutColon".localized)
            orderIdLabel.halfTextColorChange(fullText: orderIdLabel.text!, changeText: "order".localized)

            statusLabel.text = String.init(format: "  %@  ", (item?.current_state_name)!)
            statusLabel.backgroundColor = UIColor().HexToColor(hexString: item?.status_color ?? "ffffff")

        }
    }

}
