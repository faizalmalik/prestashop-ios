//
//  SellerOrderStatusModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

struct SellerOrderStatusModal {

    var statusArray = [StatusHistory]()
    var orderStateArray = [OrderState]()
    var order_total: String!

    let statusPath = ["status_history", "history"]
    let orderStatePath = ["order_states", "order_state"]

    init?(data: JSON) {

        AddonsEnabled.AddonsChecks(data: data)
        if data[statusPath] != JSON.null {
            if let data  = data[statusPath].array {
                self.statusArray = data.map { StatusHistory( data: $0) }
            } else {
                self.statusArray.append( StatusHistory( data: data[statusPath] ))
            }
        }

        if data[orderStatePath] != JSON.null {
            if let data  = data[orderStatePath].array {
                self.orderStateArray = data.map { OrderState( data: $0) }
            } else {
                self.orderStateArray.append(OrderState( data: data[orderStatePath] ))
            }
        }
        order_total = data["order_total"].stringValue

    }
}

struct StatusHistory {
    var ostate_name: String!
    var date_add: String!
    var color: String!
    var id_order_state: String!
    init(data: JSON) {
        ostate_name = data["ostate_name"].stringValue
        date_add = data["date_add"].stringValue
        color = data["color"].stringValue
        id_order_state = data["id_order_state"].stringValue
    }
}

struct OrderState {

    var id_order_state: String!
    var name: String!

    init(data: JSON) {
        id_order_state = data["id_order_state"].stringValue
        name = data["name"].stringValue
    }

}
