//
//  SellerOrderStatusTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class SellerOrderStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var orderTotal: UILabel!
    @IBOutlet weak var headingView: UILabel!
     var orderStateArray = [OrderState]()
    var passData: PassData?
    var statusId: String?
      var selectionIndex = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        statusView.textBorder()
        statusView.isUserInteractionEnabled = true
        statusView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(statusViewTap)))

    }

    @objc private func statusViewTap() {
        
        let picker = ActionSheetStringPicker(title: "orderStates".localized, rows: orderStateArray.map {$0.name }, initialSelection: self.selectionIndex, doneBlock: { _, indexes, _ in
            self.passData?.passData(id: "", name: "", dict: [:], jsonData: JSON.null, index: indexes, call: WhichApiCall.none)
            self.selectionIndex = indexes
            self.headingView.text = self.orderStateArray[self.selectionIndex].name
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: statusView)
        picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
        picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
        picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
        picker?.show()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

}
