//
//  SellerOrderChangeStatusViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON

class SellerOrderChangeStatusViewController: UIViewController {
    
    fileprivate let SellerOrderStatusViewModalObject = SellerOrderStatusViewModal()
    
    @IBOutlet weak var statustableView: UITableView!
    var jsonData: JSON!
    var id: String!
    var emptyView: EmptyView?
    var statusId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        SellerOrderStatusViewModalObject.statusId = statusId
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        statustableView.register(SellerOrderStatusTableViewCell.nib, forCellReuseIdentifier: SellerOrderStatusTableViewCell.identifier)
        statustableView.register(SellerOrderDetailTableViewCell.nib, forCellReuseIdentifier: SellerOrderDetailTableViewCell.identifier)
        statustableView.dataSource = SellerOrderStatusViewModalObject
        statustableView.delegate = SellerOrderStatusViewModalObject
        print(statusId as Any)
        self.SellerOrderStatusViewModalObject.getValue(jsonData: jsonData!) {
            (data: Bool) in
            if data {
                self.statustableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateClicked(_ sender: UIButton) {
        self.makeRequest()
    }
    
    func makeRequest() {
        var params = ""
        params = GlobalConstants.MarketPlaceApis.updatesellerorderstatus
        var verbs: RequestType = .post
        var dict = [String: Any]()
        let newDict = ["status_code": SellerOrderStatusViewModalObject.orderStateArray[SellerOrderStatusViewModalObject.selectionIndex].id_order_state, "id_order": id, "id_seller": UserDefaults.standard.value(forKey: UserDefaults.Keys.id_seller) ?? ""]
        dict[GlobalConstants.MarketPlaceApis.updatesellerorderstatus.FetchApiName()] = newDict
        verbs = .post
        NetworkManager.fetchData(params: params, verb: verbs, dict: dict, controller: AllController.sellerOrderStatus, view: self.view) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

extension SellerOrderChangeStatusViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
    }
    
    func buttonPressed() {
        self.makeRequest()
    }
}
