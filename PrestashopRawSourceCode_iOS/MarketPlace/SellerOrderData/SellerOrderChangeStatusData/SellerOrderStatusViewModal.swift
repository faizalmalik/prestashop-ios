//
//  SellerOrderStatusViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON
import ActionSheetPicker_3_0

class SellerOrderStatusViewModal: NSObject {
    var statusArray = [StatusHistory]()
    var orderStateArray = [OrderState]()
    var order_total: String!
    var selectionIndex = 0
    var statusId: String?
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        orderStateArray.removeAll()
        statusArray.removeAll()
        guard let data = SellerOrderStatusModal(data: jsonData) else {
            return
        }
        print(statusId)
        if !data.statusArray.isEmpty {
            self.statusArray = data.statusArray
            self.orderStateArray = data.orderStateArray
            self.order_total = data.order_total
            completion(true)
        }
        
    }
}

extension SellerOrderStatusViewModal: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return statusArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: SellerOrderStatusTableViewCell.identifier) as?  SellerOrderStatusTableViewCell {
                
                if let id = statusId {
                    if let index = orderStateArray.index(where: {$0.id_order_state == id}) {
                        cell.selectionIndex = index
                        selectionIndex = index
                    }
                }
                cell.passData = self
                cell.headingView.text = orderStateArray[selectionIndex].name
                cell.orderStateArray = orderStateArray
                
                cell.orderTotal.text = String.init(format: "orderTotal".localized  + " %@", order_total)
                cell.orderTotal.halfTextColorChange(fullText: cell.orderTotal.text!, changeText: "orderTotal".localized)
                return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: SellerOrderDetailTableViewCell.identifier) as?  SellerOrderDetailTableViewCell {
                cell.orderStatusLabel.text = String.init(format: "  %@  ", statusArray[indexPath.row].ostate_name)
                cell.orderStatusLabel.backgroundColor = UIColor().HexToColor(hexString: statusArray[indexPath.row].color ?? "#ffffff")
                cell.orderStatusLabel.setCornerRadius()
                cell.orderStatusLabel.textColor = UIColor.white
                cell.dateLabel.text = statusArray[indexPath.row].date_add
                cell.dateLabel.SetDefaultTextColor()
                return cell
            }
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "orderStatus".localized
        } else {
            return "statusHistory".localized
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        delegate?.moveController(id: searchSuggestion[indexPath.row].id!, name: searchSuggestion[indexPath.row].name!, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productController)
    }
}
extension SellerOrderStatusViewModal: PassData {
    func passData(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, call: WhichApiCall) {
        selectionIndex = index
        //        ActionSheetStringPicker.show(withTitle: "Order States", rows: orderStateArray.map {$0.name }, initialSelection: self.selectionIndex, doneBlock: {
        //            picker, indexes, values in
        //            self.selectionIndex = indexes
        ////            self.headingView.text = self.orderStateArray[self.selectionIndex].name
        //
        //            return
        //        }, cancel: { ActionStringCancelBlock in return }, origin: SellerOrderStatusTableViewCell.self)
    }
}
