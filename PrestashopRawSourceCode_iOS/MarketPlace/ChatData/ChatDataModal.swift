//
//  ChatDataModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

struct ChatDataModal {
    var customerData = [CustomerData]()
      init?(data: JSON) {
        if data["Customers"] != JSON.null {
            if let data  = data["Customers"].array {
                 self.customerData = data.map { CustomerData( data: $0) }
            }
        }
    }
}

struct CustomerData {
    var timestamp: NSNumber!
    var admin_unread: Int!
    var buyer_unread: Int!
    var email: String!
    var id: String!
    var name: String!
    var notificationKey: String!
    var getTime: String!
    init(data: JSON) {
        timestamp = data["client"]["timestamp"].numberValue
        admin_unread = data["client"]["customerData"]["admin_unread"].intValue
        buyer_unread = data["client"]["customerData"]["buyer_unread"].intValue
        email = data["client"]["customerData"]["email"].stringValue
        id = data["client"]["customerData"]["id"].stringValue
        name = data["client"]["customerData"]["name"].stringValue
        notificationKey = data["client"]["customerData"]["notificationKey"].stringValue
        getTime = self.getDateDayAndTime(timestamp: timestamp)
//        print(self.getDateDayAndTime())
    }
    
    func getDateDayAndTime(timestamp: NSNumber) -> String {
        let date  = Date(timeIntervalSince1970: Double(truncating: timestamp)/1000)
        let calendar = Calendar.current

        if calendar.isDateInToday(date) {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "hh:mm a"
            let time = dateFormatter.string(from: date)
            return time
        } else if calendar.isDateInYesterday(date) {
            return "Yesterday"
        } else if calendar.isDateInWeekend(date) {
            let component = calendar.component(Calendar.Component.weekday, from: date)
            return self.getDay(value: component)
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "dd/MM/YY"
            let time = dateFormatter.string(from: date)
            return time
        }
    }
    
    func getDay(value: Int) -> String {
        switch value {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tuesday"
        case 4:
            return "Wednesday"
        case 5:
            return "Thursday"
        case 6:
            return "Friday"
        case 7:
            return "Saturday"
        default:
            return ""
        }
    }
}
