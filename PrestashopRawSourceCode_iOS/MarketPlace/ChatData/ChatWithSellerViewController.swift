//
//  ChatWithSellerViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import Firebase
import FirebaseDatabase
import SwiftyJSON
import SVProgressHUD

class ChatWithSellerViewController: UIViewController {
    
    @IBOutlet weak var chatTableView: UITableView!
    var ref: DatabaseReference!
    var customerData = [CustomerData]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.title = "Chat with Sellers"
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        chatTableView.register(ChatListView.nib, forCellReuseIdentifier: ChatListView.identifier)
        chatTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SVProgressHUD.show(withStatus: "loading".localized)
        let url = "https://us-central1-prestashop-marketplace.cloudfunctions.net/getChatList?id_admin=1"
        NetworkManager.fetchGetData(params: url) {
            
            (responseObject: JSON?, error: Error?) in
            if let _ = error {
                SVProgressHUD.dismiss()
            } else {
                SVProgressHUD.dismiss()
                guard let jsonData = responseObject, let data =  ChatDataModal(data: jsonData) else {
                    return
                }
                if !data.customerData.isEmpty {
                    self.customerData = data.customerData
                    self.chatTableView.delegate = self
                    self.chatTableView.dataSource = self
                    self.chatTableView.reloadData()
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ChatWithSellerViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.customerData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: ChatListView.identifier) as? ChatListView {
            cell.item = customerData[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chatView = ChatDataViewController()
        chatView.senderId = "0"
        chatView.senderDisplayName = customerData[indexPath.row].name
        chatView.custId = customerData[indexPath.row].id
        chatView.name = customerData[indexPath.row].name
        chatView.email = customerData[indexPath.row].email
        chatView.admin_unread = 0
        chatView.buyer_unread = customerData[indexPath.row].buyer_unread
        let chatNavigationController = UINavigationController(rootViewController: chatView)
        present(chatNavigationController, animated: true, completion: nil)
    }
}
