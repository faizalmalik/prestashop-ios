//
//  ChatListView.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class ChatListView: UITableViewCell {

    @IBOutlet weak var unreadLabel: UILabel!
    @IBOutlet weak var timestamp: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImage.layer.borderWidth = 1
        profileImage.layer.borderColor = UIColor.lightGray.cgColor
        emailLabel.SetDefaultTextColor()
        timestamp.SetDefaultTextColor()
        unreadLabel.layer.masksToBounds = true
        unreadLabel.layer.cornerRadius = 9
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    var item: CustomerData! {
        didSet {
           emailLabel.text = item.email
            nameLabel.text = item.name
            timestamp.text = item.getTime
            if item.admin_unread > 0 {
                unreadLabel.backgroundColor = GlobalConstants.Colors.unreadColor
                timestamp.textColor = GlobalConstants.Colors.unreadColor
            } else {
                unreadLabel.backgroundColor = UIColor.white
                timestamp.SetDefaultTextColor()
            }
        }
    }
    
}
