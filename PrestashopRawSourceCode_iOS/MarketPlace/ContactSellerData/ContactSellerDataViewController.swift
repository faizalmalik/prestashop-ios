//
//  ContactSellerDataViewController.swift
//  Prestashop_Mobikul_Marketplace
//
//  Created by bhavuk.chawla on 16/07/18.
//  Copyright © 2018 bhavuk.chawla. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContactSellerDataViewController: UIViewController {
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var subjectField: UITextField!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    var custId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.title = "contactSeller".localized
        descriptionField.textBorder()
        sendBtn.SetAccentBackColor()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeRequest(dict: [String: Any], call: WhichApiCall, verb: RequestType) {
        var params = ""
        var newDict = [String: Any]()
      
        params += GlobalConstants.MarketPlaceApis.contactseller
       
        newDict[GlobalConstants.MarketPlaceApis.contactseller.FetchApiName()] = dict
        
        NetworkManager.fetchData(params: params, verb: verb, dict: newDict, controller: AllController.addAddress, view: self.view) { (responseObject: JSON?, error: Error?) in
            
            if error != nil {
                
                print("loginError".localized, error!)
                
            } else {
                    if ErrorChecking.getData(data: responseObject) != JSON.null {
//                        self.delegate?.addressupdated(addressId: data["addresses"]["address"]["id"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
            }
        }
    }
    
    @IBAction func sendBtnClicked(_ sender: UIButton) {
        let someOptional: Bool = true
        
        switch someOptional {
        case ErrorChecking.isEmptyString(text: emailField.text):
            ErrorChecking.warningView(message: "emailEmptyValidate".localized)
            emailField.shake()
        case ErrorChecking.isValidEmail(testStr: emailField.text!):
            ErrorChecking.warningView(message: "emailValidate".localized)
            emailField.shake()
        case ErrorChecking.isEmptyString(text: subjectField.text):
            ErrorChecking.warningView(message: "subjectReq".localized)
            subjectField.shake()
        case ErrorChecking.isEmptyString(text: descriptionField.text):
            ErrorChecking.warningView(message: "descReq".localized)
            descriptionField.shake()
        default:
            
            var dict = [String: Any]()
            dict["seller_customer_id"] = custId
            dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            dict["subject"] = subjectField.text
            dict["description"] = descriptionField.text
            dict["email"] = emailField.text
            self.makeRequest(dict: dict, call: WhichApiCall.none, verb: RequestType.post)
//            self.makeRequest()
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
