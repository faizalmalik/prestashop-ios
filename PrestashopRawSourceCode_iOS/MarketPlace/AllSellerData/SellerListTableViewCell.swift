//
//  SellerListTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit

class SellerListTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var shopName: UILabel!
    @IBOutlet weak var sellerImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: SellerListData! {
        didSet {
            ratingLabel.text = item.avg_rating
            sellerName.text = item.seller_name
            shopName.text = item.shop_name
            sellerImage.setImage(imageUrl: item.shop_img)
        }
    }

}
