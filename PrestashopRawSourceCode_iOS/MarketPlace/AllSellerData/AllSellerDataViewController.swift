//
//  AllSellerDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class AllSellerDataViewController: UIViewController {
    
    fileprivate let  AllSellerViewModalObject = AllSellerViewModal()
    var emptyView: EmptyView?
    @IBOutlet weak var sellerListTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        // Do any additional setup after loading the view.
        sellerListTableView.delegate = AllSellerViewModalObject
        sellerListTableView.dataSource = AllSellerViewModalObject
        AllSellerViewModalObject.moveController = self
        self.navigationItem.title = "sellerList".localized
        sellerListTableView.register(SellerListTableViewCell.nib, forCellReuseIdentifier: SellerListTableViewCell.identifier)
        
        self.makeRequest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeRequest() {
        var params = ""
        params += GlobalConstants.MarketPlaceApis.getAllSellerList
        params += UrlParameters.width
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.sellerList, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                self.AllSellerViewModalObject.getValue(jsonData: responseObject!) { (data: Bool) in
                    if data {
                        self.sellerListTableView.reloadData()
                    } else {
                        ErrorChecking.warningView(message: "noseller".localized)
                    }
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension AllSellerDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        if let view = UIStoryboard.loadSellerProfileViewController() {
            view.title1 = name
            view.custId = id
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
}

extension AllSellerDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest()
    }
}
