//
//  AllSelllerModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

struct AllSelllerModal {

    var sellerArray = [SellerListData]()

    init?(data: JSON) {

        if data["sellers"]["seller"] != JSON.null {
            if let data  = data["sellers"]["seller"].array {
                self.sellerArray = data.map { SellerListData( data: $0) }
            } else {
                self.sellerArray.append(SellerListData(data: data["sellers"]["seller"]))
            }
        }
    }
}

struct SellerListData {
    var id_customer: String!
    var id: String!
    var seller_name: String!
    var business_email: String!
    var shop_img: String!
    var shop_name: String!
    var notification_key: String!
    var avg_rating: String!
    var id_seller: String!
    init(data: JSON) {
        self.id_customer = data["id_customer"].stringValue
        self.id = data["id"].stringValue
        self.seller_name = data["seller_name"].stringValue
        self.business_email = data["business_email"].stringValue
        self.shop_img = data["shop_img"].stringValue
        self.shop_name = data["shop_name"].stringValue
        self.notification_key = data["notification_key"].stringValue
        self.avg_rating = data["avg_rating"].string ?? "0"
        id_seller = data["id_seller"].stringValue
    }
}
