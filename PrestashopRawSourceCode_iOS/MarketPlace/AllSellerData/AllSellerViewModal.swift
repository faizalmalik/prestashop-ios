//
//  AllSellerViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class AllSellerViewModal: NSObject {
    
    var moveController: MoveController?
    var sellerArray = [SellerListData]()
    
    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {
        guard let data = AllSelllerModal(data: jsonData) else {
            return
        }
        print(data.sellerArray)
        if !data.sellerArray.isEmpty {
            sellerArray = data.sellerArray
            
            completion(true)
        } else {
            completion(false)
        }
        
    }
}

extension AllSellerViewModal: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: SellerListTableViewCell.identifier) as? SellerListTableViewCell {
            cell.item = sellerArray[indexPath.row]
            cell.selectionStyle = .none
            cell.separatorInset = .zero
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sellerArray.count
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if FetchDetails.isLoggedIn() {
            moveController?.moveController(id: sellerArray[indexPath.row].id_seller, name: sellerArray[indexPath.row].shop_name, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.sellerProfile)
        } else {
            moveController?.moveController(id: sellerArray[indexPath.row].id_seller, name: sellerArray[indexPath.row].shop_name, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.sellerProfile)
        }
        
    }
    
}
