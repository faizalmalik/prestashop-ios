//
//  BecomeSellerDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class BecomeSellerDataViewController: UIViewController {
    @IBOutlet weak var languageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var countryHeight: NSLayoutConstraint!
    @IBOutlet weak var countrySelectLabel: UILabel!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var languageSelectLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var shopUniqueName: UILabel!
    @IBOutlet weak var shopNameLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var shopUniqueNameField: UITextField!
    @IBOutlet weak var shopNameField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var stateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var stateFieldView: UIView!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var zipField: UITextField!
    @IBOutlet weak var cityHeight: NSLayoutConstraint!
    
    @IBOutlet weak var phoneHeight: NSLayoutConstraint!
    @IBOutlet weak var termsView: UIView!
    
    @IBOutlet weak var zipHeight: NSLayoutConstraint!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var termsSwitch: UISwitch!
    @IBOutlet weak var termsViewHeight: NSLayoutConstraint!
    @IBOutlet weak var signup_btn: UIButton!
    var countryEnable: Bool!
    var countriesArray = [CountriesData]()
    var countrySelection = 0
     var languageSelection = 0
    var stateRow = 0
    var StateId: String?
    var id_cms: String!
    var languageArray = [Languages]()
    var termsAvailable: Bool!
    var maxphonedigit: Int!
     var display_phone_field: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        languageView.textBorder()
        countryView.textBorder()
        stateFieldView.textBorder()
        SetLocalisation()
        self.navigationItem.title = "becomeSeller".localized
        termsLabel.textColor = GlobalConstants.Colors.linkColor
        self.setColor()
        termsLabel.isUserInteractionEnabled = true
        self.makeRequest(dict: [:], call: WhichApiCall.none)
        // Do any additional setup after loading the view.
    }
    
    func SetLocalisation() {
        languageLabel.text = "language".localized
        shopUniqueName.text = "shopUniqueNameRequired".localized
        shopNameLabel.text = "shopNameRequired".localized
        firstNameLabel.text = "firstName".localized
        lastNameLabel.text = "lastName".localized
        emailLabel.text = "businessemail".localized
        phoneLabel.text = "phone".localized
        cityLabel.text = "city".localized
        zipLabel.text = "zip".localized
        countryLabel.text = "country".localized
        stateLabel.text = "state".localized
        signup_btn.setTitle("signUp".localized, for: .normal)
    }
    
    func setColor() {
        shopUniqueName.SetDefaultTextColor()
        shopNameLabel.SetDefaultTextColor()
        firstNameLabel.SetDefaultTextColor()
        lastNameLabel.SetDefaultTextColor()
        emailLabel.SetDefaultTextColor()
        phoneLabel.SetDefaultTextColor()
        cityLabel.SetDefaultTextColor()
         countryLabel.SetDefaultTextColor()
        languageLabel.SetDefaultTextColor()
        zipLabel.SetDefaultTextColor()
        phoneField.keyboardType = .numberPad

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.black
    }

    override func viewWillDisappear(_ animated: Bool) {
        UIBarButtonItem.appearance().tintColor = UIColor.white
    }

    func makeRequest(dict: [String: Any], call: WhichApiCall) {
        
        var params = ""
        var newDict = [String: Any]()
        var verbs: RequestType = RequestType.get
        
        switch call {
        case .createSeller:
            params += GlobalConstants.MarketPlaceApis.createSeller
            newDict[GlobalConstants.MarketPlaceApis.createSeller.FetchApiName()] = dict
            verbs = .post
            
        default:
            params = GlobalConstants.MarketPlaceApis.getSellerReq
            if let id_customer = UrlParameters.fetchCustomerId() {
                params += id_customer
            }
            
            params += UrlParameters.width
            verbs = .get
        }
        
        NetworkManager.fetchData(params: params, verb: verbs, dict: newDict, controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if error != nil {
                
                print("loginError".localized, error!)
                
            } else {
                
                switch call {
                case .createSeller:
                    if ErrorChecking.getData(data: responseObject) != JSON.null, ErrorChecking.getData(data: responseObject) != nil {
                        if responseObject!["response"][UserDefaults.Keys.id_seller].string != nil    &&  responseObject!["response"][UserDefaults.Keys.id_seller].stringValue != "0" {
                            UserDefaults.set(value: responseObject!["response"][UserDefaults.Keys.id_seller].stringValue, key: UserDefaults.Keys.id_seller)
                        }
                        self.navigationController?.popViewController(animated: true)
                    }
                    
                default:
                    guard let data = BecomeSellerModal(data: responseObject!) else {
                        return
                    }
                    self.maxphonedigit = data.maxphonedigit
                    if !data.languageArray.isEmpty {
                        self.languageArray = data.languageArray
                        self.languageSelectLabel.text = self.languageArray[self.languageSelection].name
                    } else {
                        self.languageView.isHidden = true
                        self.languageViewHeight.constant = 0
                        self.languageLabel.text = ""
                    }
                    self.termsAvailable = data.termsAvailable
                    if !data.termsAvailable {
                        self.termsView.isHidden = true
                        self.termsViewHeight.constant = 0
                    } else {
                        self.termsLabel.text = data.termsData
                        self.id_cms = data.id_cms
                    }
                    
                    self.firstNameField.text = responseObject!["customer_details"]["firstname"].stringValue
                    self.lastNameField.text = responseObject!["customer_details"]["lastname"].stringValue
                    self.emailField.text = responseObject!["customer_details"]["email"].stringValue
                    self.countryEnable = data.countryEnable
                    self.display_phone_field = data.display_phone_field
                    print(data.countryEnable)
                    if !data.countryEnable {
                        self.countryHeight.constant = 0
                        self.countryLabel.text = ""
                        self.cityField.isHidden = true
                        self.cityLabel.text = ""
                        
                        self.zipField.isHidden = true
                        self.zipLabel.text = ""
                        
                        self.zipHeight.constant = 0
                        self.cityHeight.constant = 0
                    }
                    
                    if !data.display_phone_field {
                        self.phoneField.isHidden = true
                        self.phoneLabel.text = ""
                        
                        self.phoneHeight.constant = 0
                    }
                    
                    if !data.countriesArray.isEmpty {
                        self.countriesArray = data.countriesArray
                        self.countrySelectLabel.text = self.countriesArray[self.countrySelection].name
                        if self.countriesArray[self.countrySelection].stateArray.count > 0 {
                            self.stateLabel.text = self.countriesArray[self.countrySelection].stateArray[self.stateRow].stateName!
                            self.StateId = self.countriesArray[self.countrySelection].stateArray[self.stateRow].stateid!
                            self.stateView.isHidden = false
                            self.stateViewHeight.constant = 72
                        } else {
                            self.StateId = nil
                            self.stateLabel.text = ""
                            self.stateView.isHidden = true
                            self.stateViewHeight.constant = 0
                        }
                        
                    } else {
                        self.StateId = nil
                        self.stateLabel.text = ""
                        self.stateView.isHidden = true
                        self.stateViewHeight.constant = 0
                        
                        self.countryHeight.constant = 0
                        self.countryLabel.text = ""
                        self.countryView.isHidden = true
                    }
                }
            }
        }
    }

    @IBAction func TermsTap(_ sender: UITapGestureRecognizer) {
        if let view = UIStoryboard.loadCmsViewController() {
            view.controller = CmsControllers.termsCondition
            self.navigationController?.pushViewController(view, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func CountriesTap(_ sender: UITapGestureRecognizer) {
         if  countriesArray.count > 0 {
            let picker = ActionSheetStringPicker(title: "countries".localized, rows: countriesArray.map {$0.name }, initialSelection: self.countrySelection, doneBlock: { _, indexes, _ in
                   self.countrySelection = indexes
                self.countrySelectLabel.text = self.countriesArray[self.countrySelection].name
                if self.countriesArray[self.countrySelection].stateArray.count > 0 {
                    self.stateLabel.text = self.countriesArray[self.countrySelection].stateArray[0].stateName!
                    self.StateId = self.countriesArray[self.countrySelection].stateArray[0].stateid!
                    self.stateView.isHidden = false
                    self.stateViewHeight.constant = 72
                } else {
                    self.StateId = nil
                    self.stateLabel.text = ""
                    self.stateView.isHidden = true
                    self.stateViewHeight.constant = 0
                }
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: countryView)
            picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
            picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
            picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
            picker?.show()
        }
    }
    @IBAction func LanguageTap(_ sender: UITapGestureRecognizer) {
        if  languageArray.count > 0 {
            let picker = ActionSheetStringPicker(title: "languages".localized, rows: languageArray.map {$0.name }, initialSelection: self.languageSelection, doneBlock: { _, indexes, _ in
                self.languageSelection = indexes
                self.languageSelectLabel.text =  self.languageArray[self.languageSelection].name
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: languageView)
            picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
            picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
            picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
            picker?.show()
        }
    }
    
    @IBAction func statesClicked(_ sender: Any) {
        if  self.countriesArray[self.countrySelection].stateArray.count > 0 {
            let arr = self.countriesArray[self.countrySelection].stateArray
            let picker = ActionSheetStringPicker(title: "states".localized, rows: arr.map {$0.stateName! }, initialSelection: self.stateRow, doneBlock: { _, indexes, _ in
                self.stateLabel.text = arr[indexes].stateName!
                self.StateId = arr[indexes].stateid!
                self.stateRow = indexes
                
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: stateView)
            picker?.setDoneButton((UIBarButtonItem.init(title: "done".localized, style: .done, target: self, action: nil)))
            picker?.setCancelButton((UIBarButtonItem.init(title: "cancel".localized, style: .done, target: self, action: nil)))
            picker?.toolbarBackgroundColor = GlobalConstants.Colors.primaryColor
            picker?.show()
        }
    }

    @IBAction func saveClicked(_ sender: UIButton) {

        let someOptional: Bool = true
        var Dict = [String: Any]()
        switch someOptional {
        case ErrorChecking.isEmptyString(text: shopUniqueNameField.text):
            ErrorChecking.warningView(message: "shopUniqueNameRequired".localized)
            shopUniqueNameField.shake()
//        case ErrorChecking.specialCharacters(text: shopUniqueNameField.text):
//            ErrorChecking.warningView(message: "uniquenameInvalid".localized)
//            shopUniqueNameField.shake()
        case ErrorChecking.isEmptyString(text: shopNameField.text!):
            ErrorChecking.warningView(message: "shopNameRequired".localized)
            shopNameField.shake()
        case ErrorChecking.isEmptyString(text: firstNameField.text):
            ErrorChecking.warningView(message: "firstNameValidate".localized)
            firstNameField.shake()
        case ErrorChecking.isEmptyString(text: lastNameField.text):
            ErrorChecking.warningView(message: "lastNameValidate".localized)
            lastNameField.shake()
        case ErrorChecking.isEmptyString(text: emailField.text):
            ErrorChecking.warningView(message: "emailEmptyValidate".localized)
            emailField.shake()
        case ErrorChecking.isValidEmail(testStr: emailField.text!):
            ErrorChecking.warningView(message: "emailValidate".localized)
            emailField.shake()
        case ErrorChecking.isEmptyString(text: phoneField.text)  && display_phone_field:
            ErrorChecking.warningView(message: "phoneFieldRequire".localized)
            phoneField.shake()
        case ErrorChecking.isStringContainLetters(text: phoneField.text)  && display_phone_field :
            ErrorChecking.warningView(message: "enterValidPhone".localized)
            phoneField.shake()
        case phoneField.text?.count ?? 0 >  self.maxphonedigit && display_phone_field:
            ErrorChecking.warningView(message: "phoneLength".localized + String(maxphonedigit))
            phoneField.shake()
        case ErrorChecking.isEmptyString(text: cityField.text) && countryEnable:
            ErrorChecking.warningView(message: "cityFieldRequire".localized)
            cityField.shake()
        case termsAvailable && !termsSwitch.isOn:
            ErrorChecking.warningView(message: "agreeTerms".localized)
        default:
            Dict["shop_name_unique"] = shopUniqueNameField.text
            Dict["shop_name"] = shopNameField.text
            Dict["seller_firstname"] = firstNameField.text
            Dict["seller_lastname"] = lastNameField.text
            Dict["id_customer"] = UserDefaults.fetch(key: UserDefaults.Keys.id_customer)
            Dict["business_email"] = emailField.text
            
            if display_phone_field {
                Dict["phone_number"] = phoneField.text
            }
            
            if countryEnable {
                Dict["id_country"] = countriesArray[countrySelection].id_country
                Dict["city"] =  cityField.text
                Dict["postcode"] = zipField.text
            }
          
            if !languageArray.isEmpty {
                Dict["lang_selected"] =  languageArray[self.languageSelection].id_lang
            }
            
            if StateId != nil {
                Dict["id_state"] = StateId
            }
            self.makeRequest(dict: Dict, call: WhichApiCall.createSeller)
        }
    }
}
