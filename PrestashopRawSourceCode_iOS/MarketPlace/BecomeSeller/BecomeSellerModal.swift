//
//  BecomeSellerModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

struct BecomeSellerModal {

    var countriesArray = [CountriesData]()
    private let countriesPath =  ["sellerrequestinfo", "countries", "country"]
    var countryEnable: Bool!
    var termsData: String!
    var id_cms: String!
    var languageArray = [Languages]()
    var termsAvailable: Bool!
    var display_phone_field: Bool!
     private let languagePath = ["sellerrequestinfo", "languages", "language"]
    var maxphonedigit: Int!
    init?(data: JSON) {
        
        if data[languagePath] != JSON.null {
            if let data  = data[languagePath].array {
                languageArray = data.map { Languages( data: $0) }
            } else {
                languageArray.append(Languages(data: data[languagePath]))
            }
        }
        
        maxphonedigit = data["sellerrequestinfo"]["maxphonedigit"].intValue
        
        if data["sellerrequestinfo"]["countryrequired"].stringValue == "1" {
            countryEnable = true
        } else {
            countryEnable = false
        }
        
        if data["sellerrequestinfo"]["display_phone_field"].stringValue == "1" {
            display_phone_field = true
        } else {
            display_phone_field = false
        }
        
         termsData =  data["sellerrequestinfo"]["term_and_conditions"]["message"].stringValue
        id_cms = data["sellerrequestinfo"]["term_and_conditions"]["id_cms"].stringValue
        if data["sellerrequestinfo"]["term_and_conditions"] != JSON.null {
            termsAvailable = true
        } else {
            termsAvailable = false
        }
        if data[countriesPath] != JSON.null {
            if let data  = data[countriesPath].array {
                self.countriesArray = data.map { CountriesData( data: $0) }
            } else {
                self.countriesArray.append(CountriesData(data: data[countriesPath]))
            }
        }
    }
}

struct CountriesData {
    var id_country: String!
    var name: String!
    private let statePath = ["states", "state"]
    var stateArray = [StateData]()
    init(data: JSON) {
        self.id_country = data["id_country"].stringValue
        self.name = data["country_name"].stringValue
        if data[statePath] != JSON.null {
            if let data  = data[statePath].array {
                self.stateArray = data.map { StateData( data: $0) }
            } else {
                self.stateArray.append(StateData(data: data[statePath]))
            }
        }
    }

}
