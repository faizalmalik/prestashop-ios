//
//  SellerProfileDataViewController.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class SellerProfileDataViewController: UIViewController {
    
    @IBOutlet weak var selllerProfileTableView: UITableView!
    fileprivate let SellerProfileViewModalObject = SellerProfileViewModal()
    var title1: String!
    var custId: String!
    var emptyView: EmptyView?
    var controller: AllController?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.applyNavigationGradient(colors: GlobalConstants.Colors.gradientArray)
        self.navigationItem.title = title1
        // Do any additional setup after loading the view.
        //
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        selllerProfileTableView.register(SellerProfileDetailTableViewCell.nib, forCellReuseIdentifier: SellerProfileDetailTableViewCell.identifier)
        selllerProfileTableView.register(SellerProfileProductTableViewCell.nib, forCellReuseIdentifier: SellerProfileProductTableViewCell.identifier)
        
        self.selllerProfileTableView.dataSource = self.SellerProfileViewModalObject
        self.selllerProfileTableView.delegate = self.SellerProfileViewModalObject
        self.selllerProfileTableView.tableFooterView = UIView()
        self.selllerProfileTableView.separatorStyle = .none
        SellerProfileViewModalObject.moveDelegate = self
        self.makeRequest()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makeRequest() {
        var params = ""
        params += GlobalConstants.MarketPlaceApis.getSellerProfile
        params += UrlParameters.width
        
        if custId != nil {
            params += "&id_seller=" + custId
        } else if let sellerId = UrlParameters.fetchSellerID() {
            params += sellerId
        }
        
        NetworkManager.fetchData(params: params, verb: RequestType.get, dict: [:], controller: AllController.home, view: self.view ) { (responseObject: JSON?, error: Error?) in
            if let error = error {
                self.setEmptyData(code: error._code)
                print("loginError".localized, error)
                
            } else {
                for  subView in  self.view.subviews {
                    subView.isHidden = false
                    if subView.tag == 555 {
                        subView.isHidden = true
                    }
                }
                self.SellerProfileViewModalObject.getValue(jsonData: responseObject!) {
                    (data: Bool) in
                    if data {
                        
                        self.selllerProfileTableView.reloadData()
                    } else {
                        
                    }
                }
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension SellerProfileDataViewController: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        self.navigationController?.navigationBar.isHidden = false
        if controller == AllController.productController {
            if let view = UIStoryboard.loadProductViewController() {
                view.productId = id
                view.productName = name
                self.navigationController?.pushViewController(view, animated: true)
            }
        } else if controller == AllController.productCategory {
            if let view = UIStoryboard.loadProductCategoryViewController() {
                view.id = id
                view.name = name
                view.custId = custId
                view.controller = AllController.sellerCollection
                self.navigationController?.pushViewController(view, animated: true)
            }
        }
    }
}

extension SellerProfileDataViewController: EmptyDelegate {
    func setEmptyData(code: Int) {
        if let view = EmptyView.setemptyData(code: code, view: self.view) {
            emptyView = view
            emptyView?.delegate = self
        }
        
    }
    
    func buttonPressed() {
        self.makeRequest()
    }
}
