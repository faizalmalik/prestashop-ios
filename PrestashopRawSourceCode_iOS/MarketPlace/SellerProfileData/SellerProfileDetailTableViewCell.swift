//
//  SellerProfileDetailTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
import UIKit

class SellerProfileDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var sellerProfileImage: UIImageView!
    @IBOutlet weak var sellerName: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
     @IBOutlet weak var fbBtn: UIButton!
     @IBOutlet weak var twitterBtn: UIButton!
     @IBOutlet weak var emailBtn: UIButton!
     @IBOutlet weak var phoneBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        fbBtn.normalBorder2()
        twitterBtn.normalBorder2()
        emailBtn.normalBorder2()
        phoneBtn.normalBorder2()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func fbClicked(_ sender: Any) {
         UIApplication.shared.canOpenURL(URL(string: item.facebook_id!)!)
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: item.facebook_id!)!, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: item.facebook_id!)!)
        }
    }
    
    @IBAction func twitterClicked(_ sender: Any) {
        if let url = URL(string: item.twitter_id!) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                          completionHandler: {
                                            (success) in

                })
            } else {
                UIApplication.shared.openURL(url)

            }
        }
    }
    
    @IBAction func emailClicked(_ sender: Any) {
        if item.business_email.count > 1 {
            if let url = URL(string: "mailto:" + "\(item.business_email!)") {

                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                                  completionHandler: {
                                                    (success) in

                        })
                    } else {
                        UIApplication.shared.openURL(url)

                    }
            } else {
                ErrorChecking.warningView(message: "doesNotContainSellerId".localized)
            }
        } else {
            ErrorChecking.warningView(message: "doesNotContainSellerId".localized)
        }
    }
    
    @IBAction func callingClicked(_ sender: Any) {
        if item.phone.count > 1 {
            if let url = URL(string: "tel:\(item.phone!)") {
                print(url)
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]),
                                              completionHandler: {
                                                (success) in

                    })
                } else {
                    UIApplication.shared.openURL(url)

                }
            } else {
                ErrorChecking.warningView(message: "doesNotContainSellerphone".localized)
            }
        } else {
            ErrorChecking.warningView(message: "doesNotContainSellerphone".localized)
        }
    }

    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    static var identifier: String {
        return String(describing: self)
    }

    var item: SellerProfileData! {
        didSet {
            sellerProfileImage.setImage(imageUrl: item.seller_image)
            sellerName.text = item.seller_name
            ratingLabel.text = item.avg_rating
            addressLabel.text = item.address
        }
    }

}

// Helper function inserted by Swift 4.2 migrator.
private func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
