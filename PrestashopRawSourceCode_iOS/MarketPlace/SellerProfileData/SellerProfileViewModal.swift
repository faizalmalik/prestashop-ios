//
//  SellerProfileViewModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

class SellerProfileViewModal: NSObject {
    var sellerData: SellerProfileData!
       var moveDelegate: MoveController?
 var productArray = [ProductData]()
    var rowCount = 0

    func getValue( jsonData: JSON, completion: ((_ data: Bool) -> Void) ) {

        guard let data = SellerProfileModal(data: jsonData) else {
            return
        }
        if !data.productArray.isEmpty {
            productArray = data.productArray
            rowCount = 2
        } else {
            rowCount = 1
        }

        sellerData = data.sellerData
        completion(true)

    }
}

extension SellerProfileViewModal: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: SellerProfileDetailTableViewCell.identifier) as?  SellerProfileDetailTableViewCell {
            cell.item = sellerData
            cell.selectionStyle = .none
            return cell
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: SellerProfileProductTableViewCell.identifier) as?  SellerProfileProductTableViewCell {
            cell.productArray = productArray
            cell.moveDelegate = self
            cell.selectionStyle = .none
            return cell
            }
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
          return UITableView.automaticDimension
        } else {
          return SCREEN_WIDTH/2 + 104
        }
    }

}

extension SellerProfileViewModal: MoveController {
    func moveController(id: String, name: String, dict: [String: Any], jsonData: JSON, index: Int, controller: AllController) {
        moveDelegate?.moveController(id: id, name: name, dict: dict, jsonData: jsonData, index: index, controller: controller)
    }
}
