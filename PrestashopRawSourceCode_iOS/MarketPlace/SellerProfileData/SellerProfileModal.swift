//
//  SellerProfileModal.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import Foundation
import SwiftyJSON

struct SellerProfileModal {

    var sellerData: SellerProfileData!
    var productPath = ["recent_products", "product"]
    var productArray = [ProductData]()

    init?(data: JSON) {

        if data["about_seller"] != JSON.null {
           sellerData = SellerProfileData(data: data["about_seller"])
        }

        if data[productPath] != JSON.null {
            if let data  = data[productPath].array {
                self.productArray = data.map { ProductData( data: $0) }
            } else {
                self.productArray.append(ProductData(data: data[productPath]))
            }
        }
    }
}

struct SellerProfileData {
    var seller_name: String!
    var business_email: String!
    var phone: String!
    var facebook_id: String!
    var twitter_id: String!
    var address: String!
    var id_seller: String!
    var seller_image: String!
    var avg_rating: String!

    init(data: JSON) {
        self.seller_name = data["seller_name"].stringValue
        self.business_email = data["business_email"].stringValue
        self.phone = data["phone"].stringValue
        if let fbID = data["facebook_id"].string {
             self.facebook_id = "https://www.fb.com/" + fbID
        } else {
             self.facebook_id = "https://www.fb.com/"
        }
        if let twitterID = data["twitter_id"].string {
            self.twitter_id = "https://www.twitter.com/" + twitterID
        } else {
            self.twitter_id = "https://www.twitter.com/"
        }
        self.address = data["address"].stringValue
        self.id_seller = data["id_seller"].stringValue
        self.seller_image = data["seller_image"].stringValue
        self.avg_rating = data["avg_rating"].string ?? "0"
    }
}
