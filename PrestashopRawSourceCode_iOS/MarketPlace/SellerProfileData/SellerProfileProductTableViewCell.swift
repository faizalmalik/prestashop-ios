//
//  SellerProfileProductTableViewCell.swift
/**
 * Webkul Software.
 *
 * @Mobikul
 * @PrestashopMobikulAndMarketplace
 * @author Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

import UIKit
import SwiftyJSON

class SellerProfileProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var viewAll_btn: UIButton!
    @IBOutlet weak var recentproduct_lbl: UILabel!
    var productArray = [ProductData]()
    var moveDelegate: MoveController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        recentproduct_lbl.text = "recentproduct".localized
        viewAll_btn.setTitle("viewAll".localized, for: .normal)
        productCollectionView.register(HomeProductCollectionViewCell.nib, forCellWithReuseIdentifier: HomeProductCollectionViewCell.identifier)
        productCollectionView.delegate = self
        productCollectionView.dataSource = self
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func viewAllClicked(_ sender: Any) {
        moveDelegate?.moveController(id: "", name: "sellercollection".localized, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productCategory)
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

extension SellerProfileProductTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return self.configureProductCell(collectionView: collectionView, indexPath: indexPath )
    }
    
    func configureProductCell (collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeProductCollectionViewCell.identifier, for: indexPath as IndexPath) as? HomeProductCollectionViewCell {
            cell.productItem = productArray[indexPath.row]
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SCREEN_WIDTH/2, height: SCREEN_WIDTH / 2 + 52)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        moveDelegate?.moveController(id: productArray[indexPath.row].id_product, name: productArray[indexPath.row].name, dict: [:], jsonData: JSON.null, index: 0, controller: AllController.productController)
    }
}
